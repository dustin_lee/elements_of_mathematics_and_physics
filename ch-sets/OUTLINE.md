# Outline of Sets

## Axioms

* Sets
* Axioms interlaced with definitions and whatnot. 


## Function 

* Definition 
* Related definitions


## Numerosity 

* Definition 
* Schröder-Bernstein 
* Cardinality Theorem (Natural Numbers)
* Cantor's Theorem 