%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Sets
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Intuitively, a set is a collection of objects. 
Formally, a \emph{set}\index{set} is a primitive object. 
And, a primitive relation is for an object to be \emph{in} a set. 
If an objected is in a set, then it is said to be an \emph{element}\index{element} of the set and the set is said to \emph{contain}\index{contain} the object.
The relation that an object $a$ is in a set $A$ is denoted by\label{not:element}
\[
    a \in A
    . 
\]
And, the relation that an object $a$ is not in a set $A$ is denoted by\label{not:not-element}
\[
    a \notin A
    .
\]
If a set is intuitively geometric, then it is also called a \emph{space}\index{space}, its elements are called \emph{points}\index{point} and its elements are sometimes said to be \emph{on} it. 


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Axioms
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Extensionality 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

The next axiom establishes equality among sets. 
It is called the \emph{Axiom of Extensionality}\index{Axiom of Extensionality}.

\begin{axiom}
Two sets are equal exactly if they have the same elements, that is, if any element of one is an element of the other. 
\end{axiom}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Empty Set
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% The next axiom is called the \emph{Empty Set Axiom}\index{Empty Set Axiom}.
The set in the next axiom is called the \emph{empty set}\index{empty set}.
It is denoted by $\emptyset$\label{not:empty-set} or $\{\}$.
By the last axiom, it is unique. 

\begin{axiom}
There exists a set that does not contain any objects. 
\end{axiom}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Pairing
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

The next axiom is the called the \emph{Pairing Axiom}\index{Pairing Axiom}.

\begin{axiom}
For any object, a sets exists that contains exactly this object.
And, for any two objects, a set exists that contains exactly these two objects.
\end{axiom}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Union 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

The set that the next axioms says exists is called the \emph{union}\index{union} of the two sets.
The union of two sets $A$ and $B$ is denoted by $A \cup B$\label{not:union}.
The next axiom if called the \emph{Union Axiom}\index{Union Axiom}.

\begin{axiom}
For any two sets, a set exists that contains exactly the elements of these two sets.
\end{axiom}

By the Pairing Axiom and the Union Axiom, for finitely many objects $a_1$, $…$,$a_{n-1}$ and $a_n$, a set exists that contains exactly these objects.
This set is denoted by\label{not:set}
\[
    \set{a_1, …, a_n}
    .
\]
For example, the set of the objects $a$, $b$ and $c$ is denoted by 
\[
    \set{a, b, c}
    . 
\]

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Subset
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

A \emph{subset}\index{subset} of a set $A$ is a set whose element are in $A$. 
And, if a set and its subsets are intuitionally geometric, then the subset is called a \emph{subspace}\index{subspace}.
The relation that a set $B$ is a subset of a set $A$ is denoted by\label{not:subset}
\[
    B \subset A
    .
\]
Thus, two sets are equal if each is a subset of the other. 
The next axiom is called the \emph{Subset Axiom}\index{Subset Axiom}.

\begin{axiom}
Let $A$ be a set, and let $P$ be a predicate on the elements of $A$. 
Then, a sets exists that contains exactly the elements of $A$ for which $P$ is true.
\end{axiom}

The set that this axiom says exists is denoted by 
\[
    \set{x \in A : P(x)}
    .
\]
For example, the set of positive real numbers is denoted by 
\[
    \set{x \in \mathbf{R} : x > 0}
    .
\]

Let $A$ and $B$ be two sets. 
The \emph{intersection}\index{intersection} of the sets $A$ and $B$ is the set
\[
    \set{x \in A : x \in B}
    .
\]
% This does not depend on the ordering of these two sets. 
The intersection of the sets $A$ and $B$ is denoted by $A \cap B$\label{not:intersection}.
The \emph{difference}\index{difference} of the sets $A$ and $B$ is the set
\[
    \set{x \in A : x \notin B}
    .
\]
The difference of the sets $A$ and $B$ is denoted by $A - B$\label{not:difference-set}.
If the set $B$ is a subset of $A$, then difference of the sets $A$ and $B$ is also called the \emph{complement}\index{complement} of $B$ in $A$.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Power Set 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


The next axiom is called the \emph{Power Set Axiom}\index{Power Set Axiom}.
% It is used in the next section for defining function. 

\begin{axiom}
Let $A$ be a set. 
Then, a set exists that contains exactly the subsets of $A$.
\end{axiom}

The set of subsets of a set is called the \emph{power set}\index{power set} of the set.

Let $A$ and $B$ be two sets. 
Intuitively, the product of these sets is the set of ordered pairs of the elements of these sets. 
Formally, the \emph{product}\index{product} of the sets $A$ and $B$ is the set of elements of the form 
\[
    \set{\set{a}, \set{a,b}}
    ,
\]
where $a$ is in $A$ and $b$ and is $B$. 
This product exists because it is a subset of the power set of the power set of the union of $A$ and $B$. 
The product of $A$ and $B$ is denoted by $A×B$\label{not:set-product}.
And, for any element $a$ in $A$ and element $b$ in $B$, the element
\[
    \set{\set{a}, \set{a,b}}
\]
in the product $A×B$ is denoted by $(a,b)$\label{not:set-product-element}.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Infinity 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

The next axiom is called the \emph{Axiom of Infinity}\index{Axiom of Infinity}. 
It says that an infinite set exists. 

\begin{axiom}
A set $A$ exists such that it contains the empty set and such that, for any element $a$ in it, the set $A$ contains the set $a \cup \set{a}$, that is, the set that contains exactly $a$ and its elements.
\end{axiom}

The Axiom of Infinity is used to define the set of natural numbers.  
Intuitively, the natural numbers are the whole positive counting numbers.
And, they are denoted $0$, $1$, $2$, $3$, $4$ and so on.
Formally, the natural numbers are the sets $∅$, $\set{0}$, $\set{0,1}$, $\set{0,1,2}$ and so on. 
That is, the relations
\begin{align}
0 &= \{\}        = ∅ , \\
1 &= \{0\}    	 = \{∅\} , \\
2 &= \{0,1\} 	 = \{∅, \{∅\}\} , \\
3 &= \{0,1,2\}   = \{∅, \{∅\}, \{∅, \{∅\}\}\} , \\
4 &= \{0,1,2,3\} = \{∅,\{∅\}, \{∅, \{∅\}\}, \{∅, \{∅\}, \{∅, \{∅\}\}\}\} , 
\end{align}
and so on hold. 
The set in the Axiom of Infinity is a superset of the natural numbers, but it may contain other objects.

Now, the set natural numbers is defined. 
Let $A$ be the set in the Axiom of Infinity. 
Let $a$ be an element in $A$. 
The \emph{successor}\index{successor} of the element $a$ is the set $a \cap \set{a}$.
For example, the natural number $4$ is the successor of the natural number $3$.
And, if the element $b$ is the successor of the element $a$, then the element $a$ is the \emph{predecessor}\index{predecessor} of the element $b$.
The \emph{set of natural numbers} is the intersection of all subsets of $A$ that contain the empty set and are such that, for any element in them, the successor of this element is also in them.
This set does not contain an element in $A$ that is not a natural number. 
A \emph{natural number}\index{natural number} is an element in this set.
And, the set of natural numbers is denoted by $\mathbf{N}$\label{not:naturals}.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Choice
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

The next axiom is called the \emph{Axiom of Choice}\index{Axiom of Choice}.
It uses the concept of a function, which is introduced in Section~\ref{sets:sec:fns}

\begin{axiom}
Let $A$ be a set such that its elements are nonempty sets. 
Then, there exists a function from $A$ to the union of its elements such that the output of the function a set $B$ is an element of the set $B$. 
\end{axiom}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Regularity 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Two sets are \emph{disjoint}\index{disjoint} if there does not exist an element that is in both of them. 
If two sets are not disjoint, then they are said to \emph{intersect}\index{intersect}.
The next axiom is called the \emph{Axiom of Regularity}\index{Axiom of Regularity}.

\begin{axiom}
Any nonempty set has an element that is disjoint from it. 
\end{axiom}


The last axiom implies that a set cannot be in itself. 
Let $A$ be a set. 
Then, an element of $\set{A}$ is disjoint from $\set{A}$
And, because $A$ is the only element of $\set{A}$, the sets $A$ and $\set{A}$ are disjoint.
% That is, the relation
% \[
%     A \cap \set{A} = ∅
% \]
% holds.
Thus, $A$ is not in itself.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Replacement
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

The next axiom is called the \emph{Axiom of Replacement}\index{Axiom of Replacement}.

\begin{axiom}
Let $A$ be a set and let $R(x, y)$ be a relation such that, for any element $a$ in $A$, there uniquely exists an object $b$ such that the relation
\[
    R(a, b)
\]
holds. 
Then, a set exists such that an object $b$ is in it exactly if there exists an element $a$ in $A$ such that the relation
\[
    R(a, b)
\]
holds. 
\end{axiom}