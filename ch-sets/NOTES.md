# Notes for Sets

## Concerns

* Is the Axiom of Regularity used to define products? (Wikipedia said that it reduces a level of brackets.) 
* I want to the Pairing Axiom to just be about pairing. 


## Notation and Vocabulary 

* The notation $f^n$ for iterations of functions? 
* I think that I will just not define the notation for piecewise functions. It is intuitive enough.
* Why use *partial order* instead of just *order*? 


## General 

* More commentary on the later axioms? 


## Material 

* Russell's Paradox? 
    - I would introduce it by talking about the Axiom of Specification. This is an intuitive axiom. But, it cannot be used. 
* Do disjoint unions matter? (Technically, it is used for the Schröder-Bernstein Theorem.)  



