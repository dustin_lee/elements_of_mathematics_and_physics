# My Biggest Conceptual Weaknesses by Priority 

**Uncertainty Quantification**: 
I need to understand the primary methods, and their strengths and weaknesses. 

**Stability for Numerical Methods for Differential Equations**: 
The biggest priority for this is for time stepping with FEM. 

**Fluid Dynamics**: 
This includes computational methods. 
It also includes the **Boltzmann Equation**.  

**Statistical Physics**

**Resonance**: 
What is resonance mathematically? 
What is nonlinear resonance? 

**Quantum Mechanics**:
I need to understand **spin** and particle **statistics**. 
My biggest conceptual gap with spin is how it is only one direction. 

**Quantum Transitions**: 
How can transition rates be computed? 
How is the half-life of an excited photon computed? 
What is the Fermi model for beta-decay? 
How much of this can be understood without field theory? 

**Quantum Electrodynamics**

**General Relativity and its Geometry**

**Distributions**

**Theory of Computation**
This is about its rigourization, such as the rigourization of the proof of the Halting Problem. 

