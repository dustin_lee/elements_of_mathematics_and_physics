BUILDDIR=build
FILES=$(wildcard \
		*.tex \
		*.png \
		*.jpg \
		../tex/*.sty \
		../tex/replace.sh)
CHAPTER=$(basename $(shell ls ch*.tex overview.tex 2> /dev/null))
CONTENTS=\\\\input{elements.sty} \\n\
		 \\\\let\\\\oldchapter\\\\chapter \\n\
		 \\\\begin{document} \\n\
		 \\\\input{$(CHAPTER)} \\n\
		 \\\\end{document}


all: copy main latex

copy: 
	@mkdir -p $(BUILDDIR)
	@cp -f $(FILES) $(BUILDDIR)

main: copy 
	@echo $(CONTENTS) > build/main.tex

latex: copy main
	@cd $(BUILDDIR) \
	&& bash replace.sh \
	&& sed -i 's/\\chapter{/\\chapter*{/g' $(CHAPTER).tex \
	&& latexmk -lualatex main.tex 
	@cp $(BUILDDIR)/main.pdf $(CHAPTER).pdf

clean: 
	@rm -rf $(BUILDDIR)/*
	@rm -f $(CHAPTER).pdf


.PHONY: all clean

