%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Theories and Their Quality 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Physical prediction above all
The quality of a physical theory is its predictive power. 
Predictive power is limited by its \emph{scope of validity}\index{scope of validity}, that is, the range of physical phenomena that it is valid for. 
Furthermore, computations for some phenomena may be impractical even if the theory is valid.  
Thus, the predictive power of a theory is primarily determined by the range of phenomena that is valid for and such that its computations are practical. 
This is called the \emph{scope of applicability}\index{scope of applicatibility}. 
% A theory that is not able to make predictions is so bad that it is not even wrong. 

% Validity and Falsification 
The scope of validity is found by intentionally trying to falsify the theory. 
A theory is only weakly verified by finding supporting evidence. 
A theory is strongly verified by trying to falsify it and failing to. 
A common process of creating a physical theory is to find a rule from data and then try to break it.
This is different from engineering where a design is verified.  
It is strong evidence for a theory if it predicts and verifies phenomena that were previously unconceived.
For example, Maxwell's Equations predicted that light could be made by oscillating a charge, which was previously unconceived.
And, this is now the basis of radio communication. 

% Computational Restrictions 
It is common that he scope of validity of a theory is wider than the scope of applicability. 
For example, quantum mechanics is thought to be valid everyday phenomena, but its computations are not practical to compute the orbit of the moon with quantum mechanics. 
But, it is practical to compute the orbit of the moon with classical mechanics. 

% Collections 
In practice, a collection of theories is used to obtain the widest scope of practical applicability.
And, to pick a theory from this collection that is right for an application, the scopes of practical applicability for each theory need to be known. 

% Correspondence Principle
An indicator that a collection of theories is good is that it has many instances of the Correspondence Principle. 
Two theories satisfy the \emph{Correspondence Principle}\index{Correspondence Principle} if a more general theory reduces to a less general theory in the domain of validity of the less general theory.
For example, wave optics reduces to geometric optics as the wavelength of light becomes small, atomic theory derives thermodynamics for many atoms, special relativity derives classical mechanics at slow speed, and quantum mechanics derives classical mechanics everyday distances.  
An instance of the Correspondence Principle indicates that there is not a gap in the scope of applicability. 

% Simplicity 
The quality of a theory is primarily determined by its scope of applicability. 
But, another factor is the simplicity. 
Simplicity is important because it reduces the time to learn and apply a theory. 

% Observation Matters
In physics, only what can be observed is relevant. 
That is, if something cannot be decided by experiment it is irrelevant. 
For example, until an experiment is conceived that can decide between the two mechanisms, it is not relevant if planets orbit the sun from gravitons or angles pushing them.  
Only the validity of the Law of Gravity and the Einstein Field Equations are important. 
Predictions can be made with only such partial understanding. 

% Measurability 
Normally, a component of a theory has observable consequences if it can be measured. 
For example, mass and time can be measured. 
But, an exception is phase in quantum mechanics, which cannot be measured. 
Phase is an internal bookkeeping device that explains interference, which is observable. 

% Operational Definitions 
Furthermore, in a theory, any deep philosophical understanding of a concept is irrelevant. 
Only what is measured is relevant. 
For example, time is defined as what a clock measures. 
This is an example of an \emph{operational definition}\index{operational definition}.

% Models 
In practice, not all physical effects are accounted for. 
For example, it is not practical to compute the trajectory of a rocket by considering its gravitational interaction with every other object in the universe.
A \emph{model}\index{model} is an implementation of physical theories that do not account for all physical effects. 
A good theory is such that it is fairly straight-forward to make models that are simple and accurate, that is, that are useful. 
In a sense, all models are wrong, but some are useful. 

% !! Levels of abstraction?
% Types of bonds? 


\scenebreak 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Theories and their Scopes
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

The physical theories in this book are roughly divided into \emph{classical physics}\index{classical physics}, \emph{relativistic physics}\index{relativistic physics} and \emph{quantum physics}\index{quantum physics}. 
Their scopes of applicability are phenomena at everyday scales, big scales and small scales, respectively. 

% Furthermore, classical physics is roughly divided into mechanics, thermodynamics and electromagnetism. 
% % \emph{Mechanics}\index{mechanics} is the study of the motion of matter. 
% But, in Chapter~\ref{ch:atm}, it is showed that everyday matter is made of atoms, and that thermal energy is the vibration of atoms. 
% Thus, on a thermodynamics emerges from mechanics. 
% Furthermore, in Chapter~\ref{ch:sr}, it is showed that mass and energy are equivalent. 
% Thus, because electromagnetic waves are energy, electromagnetism is also mechanics. 
% That is, fundamentally, the fundamental theories are mechanical. 
% % !! Define fundamental: 


\begin{table}[H]
\centering
\caption{Comparison of scales of length.}
\begin{tabular}{ll} 
    $\SI{8.8d26}{\meter}$ & diameter of the observable universe \\
    $\SI{8.7d20}{\meter}$ & diameter of the Milky Way \\
    $\SI{4.0d16}{\meter}$ & distance to Proxima Centauri \\
    $\SI{1.5d11}{\meter}$ & distance to the sun \\
    $\SI{3.8d8}{\meter}$ & distance to the moon \\
    $\SI{4.0d7}{\meter}$ & circumference of Earth \\
    $\SI{1.0d5}{\meter}$ & thickness of Earth's atmosphere \\
    $\SI{1.7d1}{\meter}$ & human height \\
    $\SI{1.0d-3}{\meter}$ & diameter of a grain of sand \\
    $\SI{2.0d-6}{\meter}$ & length of the bacteria E. coli \\
    $\SI{3.0d-8}{\meter}$ & gate length of a modern transistor \\
    $\SI{3.0d-10}{\meter}$ & diameter of an oxygen atom \\
    $\SI{1.2d-14}{\meter}$ & diameter of a uranium nucleus \\
    $\SI{1.7d-15}{\meter}$ & diameter of a proton
\end{tabular}
\end{table}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Classical Physics
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Classical physics suffices to build bridges, skyscrapers, engines, refrigerators, airplanes, rockets, telescopes, microscopes, electric motors, electric generators, telephones and radios. 
It is roughly divided into classical mechanics, thermodynamics, optics and electromagnetism.

In Chapter~\ref{ch:mom} and Chapter~\ref{ch:wrk}, classical mechanics in described by Newton's Laws of Motion. 
Furthermore, with Newton's Law of Gravity, it describes planetary motion.
Furthermore, in Chapter~\ref{ch:fld}, it is used to described fluids. 

In Chapter~\ref{ch:thermo}, thermodynamics is described by the Laws of Thermodynamics. 
A consequence of this theory is that the maximal efficiency of heat engines is limited. 

In Chapter~\ref{ch:lght}, light is describes light both as particles and as waves. 
Neither of these views are completely satisfactory. 
Eventually, in quantum physics, light is described as particles that have wave-like properties.

In Chapter~\ref{ch:em}, electricity and magnetism are described by Maxwell's Equations. 
This theory predicts that light is an electromagnetic wave, and this has been verified. 
This is the basis to radios. 
Thus, although electric field and magnetic fields were introduced as auxiliary concepts, they are real. 


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Relativistic Physics
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Special Relativity 
Maxwell's Equations are not invariant under galilean transformations. 
Specifically, Maxwell's Equations predict that the speed of light is independent of the reference frame. 
This is an inconsistency in classical physics. 
In Chapter~\ref{ch:sr}, this inconsistency is resolved by the theory Special Relativity, which replaces the galilean transformations with the Lorentz transformations.
Lorentz transformations differ significantly from galilean transformations only at speeds on the order of the speed of light, that is, Special Relativity and classical physics satisfy the Correspondence Principle. 
This theory has unintuitive consequences. 
For example, simultaneous events in one reference frame may not be simultaneous in another reference frame, the length of an object may differ between different reference frames, and the time distance between two events may differ between different reference frames. 
Furthermore, in this theory, mass and energy are equivalent, that is, they can be exchanged, and energy has inertial mass. 
That is, although energy was introduced as an auxiliary concept, it is real. 
Despite these unintuitive consequences, is the most verified theory yet known.
It is susceptible to a wide range of experiments, but it has not been falsified once.  

% General Relativity 
In Special Relativity, effects at a distance are not instantaneous. 
Thus, Newton's Law of Gravity is inconsistent with Special Relativity. 
In Chapter~\ref{ch:gr}, this inconsistency is resolved by the theory General Relativity, which replace Newton's Law of Gravity with the Einstein Field Equations.
The Einstein Field Equations only differ significantly from Newton's Law of Gravity in strong gravitational fields, that is, the Einstein Field Equations and Newton's Law of Gravity satisfy the Correspondence Principle.
The Einstein Field Equations are not described with euclidean geometry, but require a more general geometry, which is introduced in Chapter~\ref{ch:curv}. 
This theory further equates mass and energy by verifying that light as gravitational mass. 
Furthermore, in this theory, time progresses slower in strong gravitational fields. 
This theory predicted black holes, which were previously unobserved. 
This theory has not been falsified, but it is only susceptible to a few experiments so far. 


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Quantum Physics
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Atoms
In Chapter~\ref{ch:atm}, it is explained that everyday matter is made up of atoms. 
Furthermore, in Chapter~\ref{ch:satm}, it is explained that atoms are made up of electrons and nuclei.
Thermodynamics is from the vibration of atoms. 
Electricity and magnetism is from the motion of electrons in the material. 
And, chemistry is from interaction of electrons between atoms.

% Break Down of Classical Physics
Classical physics is invalid at the scale of atoms. 
For example, it predicts that electrons spiral into the nucleus of an atom, but atoms are mostly stable. 
Quantum physics describes the phenomena at the scale of atoms.
In quantum physics, the position and velocity of particles is probabilistic. 
That is, quantum physics is inherently probabilistic unlike classical physics and relativistic physics, which are deterministic. 
By the Uncertainty Principle, the velocity and position of a particle cannot be simultaneously measured to an arbitrary accuracy. 
Furthermore, particles have a wave-like nature, that is, they can interfere with themselves. 

% Quantum Mechanics 
\emph{Quantum Mechanics} is the study of phenomena at smalls scales and slow speeds. 
And, \emph{Quantum Electrodynamics} is the study of phenomena at smalls scales and fast speeds. 
They satisfy the Correspondence Principle with each other. 
Furthermore, Quantum Mechanics satisfy the Correspondence Principle with Classical Mechanics, and Quantum Electrodynamics satisfies the Correspondence Principle with Special Relativity. 
Quantum Electrodynamics is both suspectable to a wide range of experiments and has not been falsified. 
The Fine-Structure Constant in Quantum Electrodynamics has been measured with a relative standard deviation of $1.6 ⋅ 10^10$.
If the distance between New York and Los Angeles was measured with this accuracy, the error would be less than the width of a human hair.
% In a sense, Quantum Electrodynamics is the crown of physics. 
These theories describe all electromagnetic phenomena, that is, they described all phenomena except for gravity and phenomena in atomic nuclei. 
They are the foundation of chemistry and the study of light. 
They suffice to design transistors, which are the building blocks of digital computers. 
Quantum mechanics is described in Chapter~\ref{ch:qm}, Chapter~\ref{ch:spin} and Chapter~\ref{ch:mbqm}. 
And, Quantum Electrodynamics is described in Chapter~\ref{ch:qed}.

% Nuclei 
An understanding of atomic nuclei has been used to make nuclear bombs and nuclear reactors. 
But, so far, a satisfactory theory has not yet been proposed. 
Such theories are often computationally impractical. 
Every proposed theory that have some practical computations has been falsified. 