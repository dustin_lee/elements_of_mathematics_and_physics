# Notes for Overview

## Issues

* Theories of Physics: 
    - I will probably rewrite this entire subsection. 
    - I want this to be more connected. 
    - Put that everything reduces to mechanics at the end? 
    - Optics needs more of a description
* I used the symbols for there-exists symbol and the for-all symbol. 


## Need to Understand

* What is the difference between a relation and a statement? 
* What is a predicate? 


## General 

* This is the most important part of the book because this is what people will see first. It must capture them.
* A bit of fluff is not bad. 
* Mention all major connections between the material. If I was glad to have understood or found a connection, then it is worth putting in. 
* Make this insightful. This should be something that they come back to. 
* Mention most of the great inventions.


## Material

* Does a double-log plot make sense? This would be nice for exponential convergence. 
* Should I mention how *validation* is an opposite philosophy of *falsification*? What is the difference between *validation* and *verification*? 
* Should there be a discussion of dimensional analysis? 
* Mathematically similar theories: 
    - In mathematics or physics? 
    - Examples: 
        + Light waves and mechanical waves. 
* Figure of the quadrants of the physical theories? 
* Should I mention quantum computers? 
    - They allow for some different methods to be used. These are not strictly better or worse. But, they are better in some situations. They can aid digital computers.
* How much can changing unit help a computation? 