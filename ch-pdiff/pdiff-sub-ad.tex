Normally, gradients cannot be accurately computed numerically because the round-off dominates for small steps in finite-difference schemes. 
Thus, it may be better to compute the gradient symbolically, such as for the Gradient-Descent Method. 

In practice, functions are computed as a composition of elementary operations. 
These elementary operations are binary operations such as addition or unary operations such as negation or the Sine Function. 
Even if the function is a solution to a differentiation equation, its computation on a computer is a composition of elementary operations. 


\begin{figure}[H]
\centering
    \hfill
    \begin{minipage}{0.48\textwidth}
        \input{pdiff-fig-ad1}
    \end{minipage}
    \hfill
    \begin{minipage}{0.48\textwidth}
        \input{pdiff-fig-ad2}
    \end{minipage}
    \hfill
    \caption{
        The breakdown of the function $\sin(x_1 \cdot x_2 + x_3) + x_3$ into elementary operations.
        The variables $x_1$, $x_2$, and $x_3$ are the inputs.
        The variables $x_4$, $x_5$ and $x_6$ are intermediate variables.
        And, the output is $x_7$.
        The right is the same computation graph, but where the operations are denoted by $f_1$, $f_2$, $f_3$ and $f_4$. 
    }
\end{figure}\label{comp-graph:ops}


Let $f(x_1, x_2, x_3)$ be the function
\[
    \sin(x_1 \cdot x_2 + x_3) + x_3
    ,
\]
and let $(a_1, a_2, a_3)$ be an input of it. 
Let $f_1(x_1, x_2)$, $f_2(x_3, x_4)$, $f_3(x_5)$ and $f_4(x_6, x_7)$ be the functions $x_1 + x_2$, $x_3 + x+4$, $\sin(x_5)$ and $x_6 + x_7$.
And, let $a_4$, $a_5$ and $a_6$ be the outputs $f_1(a_1, a_2)$, $f_2(a_3, a_4)$ and $f_3(a_5)$, respectively. 
Then, by the Chain Rule, the relation
\[
    ∂_{x_3} f (a_3)
    = ∂_{x_3} f_4 (a_3)
      + ∂_{x_6} f_4 (a_6) ⋅ ∂_{x_5} f_3 (a_5) ⋅ ∂_{x_3} f_2 (a_3)
    % &= \frac{\partial f_4}{\partial x_3}
    %     + \frac{\partial f_4}{\partial x_6}
    %         \cdot \frac{\partial f_3}{\partial x_5}
    %         \cdot \frac{\partial x_5}{\partial x_3}
    % \\
    % &= \frac{\partial f_4}{\partial x_3}
    %     + \frac{\partial f_4}{\partial x_6}
    %         \cdot \frac{\partial f_3}{\partial x_5}
    %         \cdot \frac{\partial f_2}{\partial x_3}
    % \\
\]
hold.
That is, the derivative of $f$ is the sum of products of derivatives of its elementary operations. 
This method is called \emph{automatic differentiation}\index{automatic differentation}.

In implementation, the function is computed and the intermediate outputs $a_4$, $a_5$ and $a_6$ are saved.
Then, the composition of function is traversed again to save the derivatives of the elementary operations. 
The \emph{forward mode}\index{forward mode} is when this is done from the innermost functions to the outermost functions. 
And, the \emph{reverse mode}\index{reverse mode} is when this is done from the outermost functions to the innermost functions.
Normally, the forward mode is faster for a low-dimensional input, and the reverse mode is faster for a high-dimensional input.


% !! Save the number that will be the derivative. 
% !! Compute until it is finalized. 
% !! Recomputation are not needed when a variable is used multiple times.

% For the reverse mode, the \emph{back-propagation algorithm}\index{back-propagation algorithm} is used, which is now explained. 
% For each variable in the computational graph, track a variable that will become its derivative. 
% Initialize all these variables to $0$.
% Set the derivative of the output to $1$.
% Then, move backwards through the computational graph. 
% At each node, for each child node, add the product of the derivative at this node and the derivative of the function between these nodes to the child node. 
% When this completes, the derivative variable at each node will be the derivative. 

% For example, consider the computational graph in the figure. 
% For each index $i$, let $x_i'$ be the variable that will become the derivative of the function with respect to $x_i$, and initialize it to $0$.
% First, add 
% \[
%     \frac{\partial f_4}{\partial x_3}
% \]
% to $x_3'$, and add
% \[
%     \frac{\partial f_4}{\partial x_6}
% \]
% to $x_6'$. 
% Then, add 
% \[
%     x_6' \cdot \frac{\partial f_3}{\partial x_5}
% \]
% to $x_5'$. 
% Finally, add
% \[
%     x_5' \cdot \frac{\partial f_2}{\partial x_3}
% \]
% to $x_3'$. 
% Now, the variable $x_3'$ is the derivative of the function with respect to $x_3$.
% Computing the derivatives $x_1'$ and $x_2'$ is done similarly and uses the already computed $x_5'$. 