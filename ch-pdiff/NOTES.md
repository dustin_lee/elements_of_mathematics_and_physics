# Notes for Partial Differentiations

## Material

* Should I talk about stationary points for multidimensional functions? That is, should I give the condition to determine what type a given stationary point is? 
* Should there be a discussion on higher derivatives? For example, the Hessian. This would be part of optimization. 
* Should I have the Inverse-Function Theorem or the Implicit-Function Theorem? 
* AD: Mention that derivatives of some functions (like $\sin(t)$) can be done independently of implementation? 


## Figures

* MVT: 
    - Use the notation in the theorem. 
    - Make it a three-dimensional box. 
    - Don't have lines for labels. Just put the label right there. 
    - The labels should be of the form $p + w_i$. 


## General

* 
