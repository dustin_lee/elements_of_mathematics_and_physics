# Outline of Partial Differentiation

## Concept

* Definitions: 
    - Directional differentiation 
    - Partial differentiation.
* Gradient: 
    - Definition 
    - Is unique 
    - Linearity 


## Methods

**Gradient and Partials**: 
* Gradient to compute directional derivatives
* Gradient as a matrix of partial derivatives
* Example that existence of partial derivatives does not imply differentiability 
* Continuous partial imply the existence of the gradient 
* Example of computing the gradient and using it to compute a directional derivative

**Rules**: 
* Chain Rule
* Product Rule for normal scalar multiplication 
* Product Rule for the scalar product
* Product Rule for the vector product

**Exchange of Partials**: 
* Clairaut's theorem 
    - The condition that $f$ is twice continuously differentiable in Clairaut's theorem is needed.


## Differentiation as Slope 

* Introduction:
    - The gradient points in the direction of steepest ascent
* Gradient descent
* Newton's method 
    - Can combine with the Bisection Method