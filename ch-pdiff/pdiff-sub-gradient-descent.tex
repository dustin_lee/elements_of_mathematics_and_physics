Now, the \emph{Gradient-Descent Method}\index{Gradient Descent} is generalized to functions whose domains are multidimensional. 
This is a method to find minima of functions. 

Let $f$ be a real-valued function on a region. 
Let $p$ be a point. 
Then, this function is approximately 
\[
    f(p) + ∇f(p) ⋅ (q - p)
\]
near the point $p$. 
Thus, intuitively, for a small positive real number $ε$, the relations 
\begin{align}
    f\left( p + - ε⋅∇f(a) \right) 
    &≈ f(p) + ∇f(p)(-ε⋅∇f(p)) \\
    &≈ f(p) - ε⋅∇f(p)•∇f(p) \\
    &< f(p)
\end{align}
hold, where the last relation holds because $∇f(p)•∇f(p)$ is positive.
That is, the value of $f$ at 
\[
    p + \frac{ε}{-\dot{f}(p)}
\]
is less than the value of $f$ at $p$. 

This reasoning is continued. 
Let $ε_1$, $\ldots$, $ε_{n-1}$ and $ε_n$ be small positive real numbers. 
And, recursively, let $p_1$, $\ldots$, $p_{n-1}$ and $p_n$ be points such that the relation 
\[
    p_{i+1} = p_i - -ε_i⋅∇f(p_i)
\]
holds for each index $i$. 
Then, intuitively, the relations
\[
    f(p_1) > f(p_2) > \ldots > f(p_n)
\]
hold. 
Thus, intuitively, if the integer $n$ is sufficiently big, then the number $a_n$ is an approximation of a local minimum of the function $f$. 
That is, this is an iterative method to find a local minimum of the function $f$. 

The numbers 
\[
    - ε_1 ⋅ ∇f(p_1), \textspace
    \ldots, \textspace
    - ε_{n-1} ⋅ ∇f(p_{n-1})
    \textand
    - ε_n ⋅ ∇f(p_n)
\]
are called the \emph{step increments}\index{step increment} of the method. 
And, their sizes are called the \emph{step sizes}\index{step size}.
They are a positive proportion to the negative of gradient because the negative of the gradient points in direction of the steepest descent, that is, for any unit vector $v$, the relations
\begin{align}
    ∂_v (f(p))
    &= ∇f(p)•v \\
    &= \abs{∇f(p)}⋅\abs{v}⋅\cos(θ) \\
    &≥ -\abs{∇f(p)} \\
    &= \frac{-∇f(p)•∇f(p)}{\abs{∇f(p)}} \\
    &= ∂_{\frac{{-∇f(p)}}{\abs{∇f(p)}}} (f(p))
\end{align}
hold. 


\begin{figure}[H]
\centering
    \includegraphics[width=0.5\textwidth]{pdiff-fig-grad-descent}
    \caption{Gradient descent. The blue curves are level curves of the function. The points $x_0$, $x_2$, $x_2$, $x_3$ and $x_4$ are successive steps. The red arrows are proportional to the negative of the gradients at each step.} 
\end{figure}


Chapter~\ref{ch:opt} discusses various issues with this method.
It introduces several variations of the Gradient-Descent Method to avoid these issues.
And, the convergence, speed and robustness of these methods are discussed.