# Notes for Curvature

## Need to Understand

* By far the biggest lack in my understanding is understanding what curvature is (Riemann -> Ricci -> Scalar), and how this information is used to make computations. (Like, literally, once I know this, I will at least know the Einstein Field Equation.)
    - Ricci is suppose to be a measure of how much a metric tensor differs locally from euclidean space. 
    - The scalar curvature is suppose to be a measure of curvature. (For surfaces, it is proportional to (twice) the gaussian curvature.)
    - Gaussian curvature doesn't work for dimensions other than 2. Riemann curvature is for all dimensions. 
    - Use holonomy (parallel transport) to get the Riemann curvature. (I might be able to use geodesic deviation instead because this is what actually matters.)
       + The Riemann tensor takes three-inputs – two directions and and a vector to be transported – and it outputs the difference between the original vector and after it is transported around the parallelogram. (I guess that this turns out to be multilinear.) (The video *Relativity 107c: General Relativity Basics - Curvature, Riemann Tensor, Ricci Tensor, Ricci Scalar* is not too bad.)
       + The Ricci curvature measures how much volume changes as it moves along geodesics. I think that it essentially sees hows a density changes along a geodesic. 
* I guess that I need something like the Gradient Theorem to use the curvature to get the geometry. 


## Material

* The original motivation should be the geometry of earth's surface. Mention that it is to quite a sphere. 
* The curvature curvature in a direction at a point is $κ_1⋅\cos^2(θ)+κ_2⋅\sin^2(θ)$, where $κ_1$ and $κ_2$ are the principal curvatures and where $θ$ is the angle between the direction and the principal direction.
   - This motivates of the idea of principle curvatures. 
* Another way of say the Theorema Egregium is that the gaussian curvature is independent of the second fundamental form, but depends only on the first fundamental form (and its the first and second derivatives of its coefficients). 
* This chapter includes the idea of ambient space. 
* Don't have guassian curvature – but just have scalar curvature. Introduce the Theorema Egregium after curvature in general. 
* Theorem for differential equations. 
* I think that Ricci curvature means that the geometry does impose a force. 
* Are curves are (Ricci) flat. This is why strings can be bent in any way. 


## Notation

* Probably use something like $$\mathrm{curv}$$ for Riemann curvature. 
* Maybe use use something like $\mathrm{tr}_{1,3}(\mathrm{curv})$ for the Ricci curvature. 


## Motivation

* Navigation on the globe. 
* What about for structural analysis? 
* Obviously, I really want this chapter for GR. 



## General

* There are two main results that don't hold in hyperbolic geometry. Start with these as the motivation. 
* Perhaps, curvature should be motivated with the attempt to get something like the Gauß-Bonnet theorem. (An infinitesimal version of the Gauß-Bonnet theorem could possibly give a definition of curvature.)
* Does gaussian curvature come from the Second-Derivative Test?
* One way of viewing curvature is by how much an arrow changes direction when it travels around a closed path where its direction doesn't change along the way. (I think that this is the Gauß-Bonnet Theorem or something.)
* I think that the curvature of a smooth (embedded) simple curve $C$ at an interior point $p$ can be defined at the limit
$$
  \lim_{p_1 \rightarrow p,\ p_2 \rightarrow p,\ p_3 \rightarrow p}
    \frac{1}{\mathrm{radius}(\mathrm{circle}(p_1, p_2, p_3))}.
$$
* I think that the point of triangularization is to partition the manifold into contractible manifolds. I think that this is helpful for the Gauß-Bonnet Theorem.
* Instead of thinking as GR as a geometric theory, it can be thought of a theory of the metric field. In this formulation, the metric function propagates similarly to how the electromagnetic potential propagates in electromagnetism. (The book *Relativity, Gravitation and Cosmology* by Ta-Pei Cheng talks about this.)
