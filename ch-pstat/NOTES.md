# Notes for Statistics of Many Particles

## Overarching 

* There needs to be some sort of useful prediction. (The Exclusion Principle is already done, and laser won't be until later.) I think that the Boltzmann Equation is again the best choice. (I even think that phonons are bosons, so both types of statistics are include.) Of course, there needs to be a setting, which should probably be electrons and phonons in a solid. 
    - Because I want to use this setting, I am putting this chapter after the chapter Electronic Structure of Solids. 
        + This chapter only needs the Exclusion Principle. And, then, in this chapter, current flow can be explained. But, maybe, many energy bands and semiconductor should also just be in this chapter too. 


## Need to Understand

* General idea. I need to know the what results are used, and how they are used. 
    - Fermi-Dirac statistics is used for electronic structure. Anything else? 
    - What are Bose-Einstein statistics used for? Phonos? Photons? 
    - What is entropy used for? Free energy calculations. Boltzmann, NEGF?
* Why can only static states be considered when counting states? 
    - The standing states form a basis. So, the motional states can be described by them. Thus, I guess that they give a reasonable count of the number of particles at a certain energy. 
* Mixed states. 
	- What is a motivating example other than spin? 
	- What is the mathematical benefit? 
	- This is for when the initial state is not known – for example, several spins are considered – and for entangled states. 
	- Instead of a wave function, the state is represented by an operator, which is called the *density operator*, but I could also call it the \emph{state operator}. 
	- Does this have anything to do with understanding identical particles? 
	- Observations are then traces. 
	- There is a more general Schrödinger Equation for these that involves a commutator. 
	- Almost for sure this is needed for scattering. 
	- Are calculations using a density operator reasonable? 
    - Maybe my problem is that I am not thinking about *generalized* expectation values, such as in the form $<u_i | A | u_j>$. I think that this will motivate the density operator as not being diagonal. What are examples of these? (This might just be crap.)
* Why can states be thought of as global? 
    - Even when DFT is solving for one cell, it someone see global states. 
    - But, also I think that when looking at just one cell, one can someone distinguish the valence band from the conduction band, and the conduction band is what as global states. 
* How is the Schrödinger Equation actually written with multiple particles? Is the wave function projected or something to get interaction involving each electron? 
* How are identical particles dealt with in the path integral approach? 
* Entanglement. 
	- Supposedly, entanglement is why multiple particles are described using tensor products. 
		* Given a measurement, the state in each factor of the product collapses. The collapse in each factor are independent. Thus, the composition system should include the of all linear combinations of pairs of states. (Thus, the dimension of the product space should be $n⋅m$ instead of $n+m$.) 
		* Honestly, what even is a tensor product? Also, I don't really think of the wave function as being in a vector space. 
		* I guess that the point is to correctly assign the probability density to states or properties of another particle if something about the first particle is known. 
	- What is a good example? 


## Material

* I can start with Rayleigh's law failing. 
* Intuitively, matter is what prevents other matter from occupying the same space. This kind of motivates the Exclusion Principle. 
* Massless bosons (photons) only have two polarization states instead of the usual three. 
* Gases are well approximated by assuming distinct particles because the wave functions overlap so little. 
* Should I include the Bell Inequality and whatnot? It could go at the end of multiple particles. 


## Datta Stuff

* The $Z$ is the just normalization. 
* The Boltzmann Law is used Fermi-Dirac and Einstein-Bose statistics. The Fermi function for for the non-interacting case. 
* The Boltzmann Law is different from the Boltzmann Approximation. 
* Entropy isn't the available free energy, but it is used to get it. It is the weird factor/term? 
* The Mean-Field Approximation is where the Fermi function comes from. 
* Reservoir.  
    - For electrons, it can be a contact. 
    - For just energy, it can be a heat reservoir. 
    - But, what is a example of a chemical reservoir that allows the exchange of particles? 
* When the in the Boltzmann Law, the particles are fixed, there term $\exp(μ⋅N/k⋅T)$ factors out into $1/Z$. 
* The density matrix is from moving from the classical setting to the quantum setting. Instead of $$\frac{1}{Z}⋅\exp(-E_i)$ one has $$\frac{1}{Z}⋅\exp(-H)$, where $H$ is the density matrix I guess. 


## General 

* I think that the understanding of Entanglement will come from the path integral approach. Use the path integral approach to derive the multi-particle Schrödinger/Pauli Equation. 
* I think that photons and phonons become waves macroscopically because they are bosons, which means that they tend towards the same state. 
* The bipolar representation of spin is just more convenient. 
* When the wave functions of two electrons are orthogonal, then the electron density is equal to the sum of the densities of each electron. 