# Outline of Statistics of Many Particles

## Identical Particles

* Notes. 
	- Use nuclei and light for bosons.
	- If the wave function of two identical particles overlap at all, then they cannot be distinguished.
* Bosons and Ferminons. 
	- The outcomes cannot be distinguished (by the path-integral approach). This motivates bosons behavior.
		+ This must be at low energy so that the spins do not change. 
	- There are also fermions. 
	- There are only bosons and fermions. The phase change must be either $+1$ or $-1$ because a double exchange must return the wave function to the original state. 
	- Think of two particles moving from two locations $p_1$ and $p_2$ to two new locations $p_3$ and $p_4$.
		+ Although I do want to keep the abstraction, I might want to talk about this situation in a scattering experiment (as in the Feynman lectures).
		+ It cannot be measured which particles go to each location. Thus, there are more alternatives.
		+ For bosons, as the point $p_4$ converges to $p_3$ the equality
		$$
			\abs{\prop(p_1→p_3)⋅\prop(p_2→p_3) + \prop(p_1→p_3)⋅\prop(p_1→p_3)}
			= 4⋅\abs{\prop(p_1→p_3)⋅\prop(p_2→p_3)}
		$$
		holds. Thus, the photons are more likely to end up in the same state.
		I don't fully understand why the result of this limit should be different than what one's original intuition is. 
		+ But, for some particles – fermions – the amplitudes substract. 
	- For unpolarized particles, all combinations of spins need to be accounted for.
	- Mention the Spin-Statistics Theorem. (Whether a particle is a boson of a fermion depends on its spin.) (QED is needed to prove this.)
	* The axiom. 
		- Prove that this holds under evolution. 
		- The Exclusion Principle. 
		- Slater determinates can be used with FEM to create a basis such that this property is preserved. 
			+ Don't use the term *determinate* here. 
			+ Does including the scaling factor mean that each electron function can be normalized independently? 
			+ The number of basis functions increases rapidly, so this method is not reasonable for many electrons. Mention the Self-Consistent Field Method and that it will be further explain in Bonds and Solids. 
	* Bosons try to be in the same state, and fermions tend to push apart.
		- This is seen from how such wave functions evolve. The standard deviations tend to decrease or increase. There is not any additional effects that cause this. (Griffiths gives a demonstration/derivation of this.)
		- Mention some applications. Mention that this property will also be involved in transitions. 
			+ Lasers work because having photons of a given frequency make it more likely for atoms to emit more photons at the same frequency.
				* By having transitions before this, I could fully explain lasers.
			+ Superfluidity works from bosons wanting to be in the same state as its neighbors, which prevents viscosity.
			+ Einstein-Bose Condensation? (I guess that this gives effects that are seen macroscopically. But, other than that, I don't know what it is good for.)
* Entanglement. 
	* Particles are entangled whenever the wave function cannot be written as a product. 
	* Intuition. The idea is that once an outcome for one particle is known, the possibilities of what the other particle did is restricted. (For example, the mean of momentum should be the same after a collision.)
	* Examples. 
		- Given an example where the wave function depends on the past. 
		- The spin of an electron in an atom as an example. Once one is measured, the measurement of the other in the same state is known. (Can this experiment actually be done?)
		- Explain how particles have been drastically separated and that the correlation has been seen to be faster than light. 
		- Explain that information is not actually sent. 
	* Can I explain the benefits of quantum communication here? 


## Atoms with Multiple Electrons

*Actually, this doesn't seem like it should be its own section. If anything, most of this should be mention in the first chapter on quantum mechanics.*

* The independent-electron approximation give the rough states and can explain chemical properties of an atom. 
* The Exclusion Principle.
	- Reasoning. Electrons don't exist in the lowest energy state. 
	- Hund's rule: Electrons occupy different states before two electrons with different spins occupy the same state.
		+ Should this be introduced when talking about the magnetism of solid instead? Actually, should this just be introduced in the first chapter on quantum mechanics when talking about one electron? 
	- By seeing all the possibilities of spins of an atom, it can be seen that the spins of many electrons are indeed coupled because cancel and limit the number of spin states.
	- This explains the periodic table.
		+ The core electrons are too strongly bound to interact with other atoms. Only the valence electrons do. This explains why the position of an element in the periodic table is indicates its chemical properties.
		+ The inert gases have complete shells. Explain how complete shells are the most stable configuration: The electric attraction of an electron to a nucleus increases as the atomic number increases. But, there is a drop in ionization energy when an electron is put in a further out shell (because it is bother further and the other electrons shield the charge of the nucleus.) Mention again that this explains the periodicity of the periodic table.
		+ Give a graph of the ionization elements.
			* Just do the first twenty elements or so.
			* Mark each element.	
  

## Bosons

* Examples. 
    - Photons. 
    - Phonons. 
    - Superconductivity and superfluity. 
* Bose-Einstein condensation. 
* Mention lasers. 


## Fermions

* Examples. 
    - Electrons in a solid. 
    - Nucleons in a nucleus.
* Should energy bands just be here?  


## Black-Body Radiation 

* First do Wein and then Planck? 
* Its derivation 
    - Energy comes in clumps: 
        + This is further verification of photons
        + It is not the frequency that is continuous, but the energy (density) per frequency that is discrete. 
        + Is there really any reason to think that this can even be approached by classical statistical physics? Like, they are not particles.
    - The that photons are boson must alter this computation 


## Superconductors and Superfluids 