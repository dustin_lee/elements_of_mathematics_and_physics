# Outline of Probability

## Summary 

* Summary statistics: 
    - Measures of central tendency: Mode, mean, medium, confidence  
    - Measures of spread: Mean Absolute Deviation and Standard deviation
    - How to get these from statistics 


## Introduction
  
* Introduce the idea of probability. Define a probability space.
* Explain independence.
* Probability densities:
    - Define random variables. 
    - Define probability density functions.
    - Explain that distributions are used to unify discrete and continuous probability density functions. Define *discrete* and *continuous*. Explain that proofs in this book for probability density functions could be done separately for the discrete and continuous cases, where only the (Riemann) integral is needed for the continuous case. 
    - I should probably talk about how operations between random variables corresponds to the operators between their probability density functions. 
    - Reference a few examples: 
        + Normal distribution. 
    - Existence (by the Density Theorem).


## Summary Statistics

<!--
* Overview: 
    - 
    - How to pick: 
        + Stupid example: When one is making a bet, such as career, it is more about the median income: Moderate prosperity is much better than low prosperity, but extravagant prosperity is marginally better than moderate prosperity. Thus, they would prefer a high chance of moderate prosperity to a low change of extravagant prosperity where the alternative is low prosperity even if the mean income is much higher. 
        + How to pick: The median is not affected by outliers. 
-->
* Central Tendency: 
    - Overview: 
        + 
    - The mean.
        + Note: Do first because it is what the reader cares about the most. 
        + Defining the mean.
            * Define it to be the number such that the total error is $0$. This just gives the integral
                $$
                ∫ (x - a)⋅f(x) = 0,
                $$
            which is easily solved to give common value. Then, try to get the formula from this.
            * It minimizes $∫dist(x, a)⋅f(x)$.
            * Another way is to think of the effective value. For example, if one is paid differently several times, then the mean of the payments if the effective payment each time. (The sum of mean over the number of iterations is the total.)
    - The median: 
        + This is better than the mean for skewed probability densities – such as wealth.
    - Graph of different comparisons.  
    - The mode.
        + This is weakly a measure of central tendency.
        + This still makes sense for categorical data.
        + Talk about multiple modes.
            * This might indicate classifying the samples into multiple categories.
                - Have the example of height of humans. There are two modes, but if the genders are separated, then each has one mode.
                    + Plot these three densities on one plot.
                - Is there a situation where two modes might indicate two different particles? 
        + Figure for multiple (local) modes.  
* Spread: 
    - The interquartile range:
        + I should probably use the term *midspread* or *interquartile distance* instead.
        + It is supposedly better than the standard deviation for skewed probability densities.
    - The mean-absolute deviation.
        + Definition: $∫\abs{x-mean(f)}⋅f(x)
        + Markov Inequality.
            * Confidence: This gives upper bounds on the percentage of samples that are outside of multiples of the mean-absolute deviation from the mean.
            * An interesting example from Wikipedia: With the assumption that income is nonnegative, Markov's inequality implies that at most $1/n$ of the population can have more than n times the average income.
    - The standard deviation.
        + Definition: $∫(x-mean(f)^2)⋅f(x)
            * This might not exist. (Does mean-absolute deviation always exist?)
        + Chebyshev Inequality.
            * Confidence: This gives upper bounds on the percentage of samples that are outside of multiples of the standard deviation from the mean.
        + The Error-Propagation Formula. 
            * Should this be elsewhere? I guess that I should just be able to state it as a one theorem with only a bit of commentary, so it shouldn't take up much space. 
                - Yes, it should be with the normal distribution. 
* Correlation:
    - Note. I am not sure if this should go here. And, I am not sure how much I should talk about correlation. I will most likely talk very little.     
    - Correlation does not imply causality. 
    - (Pearson) correlation coefficient: 
        + Definition.
        + It is between $-1$ and $1$.
        + Only linear correlations
    - Should I talk about other coefficients, such as Spearman's?
        + I could then give examples that compare them. 
    - Is there any statistical inference with correlation? 


## Estimation (of Summary Stats and Distributions) 

**Sampled Summary Statistics**:
* State these as sums instead of integrals. 
* Define: *population* and *sample*
    - The mean is the same. 
    - Absolute deviation.  
    - Standard deviation. 
* Explanation and comparison to the population versions. 
    - State what the population versions are. 
    - The sample versions are better estimates. 
        + But why? 
            * Provide evidence of a numerical experiment
        + Should a similar formula be used for the mean absolute deviation? 


**Confidence (Estimation of the medium and interquartile distance)**:
* Sampling the median: 
    - The Rule-of-Five reasoning: If there are $5$ samples, then the probability that the median is within the extremes of these is $1 - 2⋅\frac{1}{2^5}$.
    - Having a confidence interval seems to be a little harder, but I think that the same reasoning applies. 
* Interquartile distance: 
    - I think that this reasoning can continue. 


**Estimation/density of the mean**:
* Convolution is the correct way to think about it:
    - First, convolution is the density for the sum of multiple samples. (Is is walk style.)
    - Then, recalling give the average: 
        + (I should understand this)
        + The density function $g(x)$ for the mean is
            $$
                \frac{1}{N} ⋅ (f * \cdots f )(N⋅x).
            $$
    - Properties of convolutions: 
        + The convolution of two square-integrable functions is square-integrable. (This is required for even different functions because multiple convolutions are made (not just $f*f$).)
        + Preserves probability because $∫(f*g) = (∫f)⋅(∫g)$$. 
        + It is associative. (I guess that this is intuitively required.)
        + It is commutative.  (This is not really required.)
        + It is distributive. (This is not really required.)
        + Relationship with differentiation: $∂(f*g) = (∂f)*g = f*(∂g)$. Is this needed in any proofs? (Like, maybe the exponential comes from considering all derivatives.)
* The mean of the density of the mean stays the mean. 
* The standard deviation of the density of the mean shrinks. 
    - The standard deviation for the sum of outcomes: Let $f$ be the original density and let $g_N$ be the density of the mean. First, the relation
        $$
            \mathrm{sd}(f*\cdots*f) = N^{\frac{1}{2}}⋅\mathrm{sd}(f)
        $$
        holds by properties of convolutions. 
        Furthermore, the relations
        $$
            \mathrm{sd}(\frac{1}{N}⋅(f*\cdots*f)(N⋅x))
            = \frac{1}{N}⋅\mathrm{sd}((f*\cdots*f)(x))
            = \frac{1}{N} ⋅ N^{\frac{1}{2}}⋅\mathrm{sd}(f)
            = \frac{1}{N^{\frac{1}{2}}} ⋅ \mathrm{sd}(f)
        $$
        hold, where I guess the first relation is from [scaling](https://en.wikipedia.org/wiki/Standard_deviation#Identities_and_mathematical_properties) (although this is not clear), and the second relation is from the standard deviation of convolutions. 
* Usage: Use the sample standard deviation and the number of sample to predict confidence of the mean. 
* Then, I think that, by a concentration of probability theorem (Chebyshev Inequality), this proves the Law of Large Numbers. 
    - This is what is *convergence in probability*.
        + State it as the integral of the mean distribution over any (bounded) neighborhood of the mean converges to $1$. 
    - This proves the weak version. Do I also need the strong version? I am not really sure what it says. (I think that the strong version is required for the Fundamental Law of Statistics, which I might include.)
    - Note: This already says soo much. But, I guess that the Central Limit Theorem will give more meaning to the standard deviation with the Empirical Rule, which will be sharper than Chebyshev's Inequality or at least easier to use. (But, isn't this more precise because it doesn't have to deal with a theoretical convergence?) I do just want to be able to introduce the normal distribution. 
* Then, it is the Central Limit Theorem. 
    - Should it be motivated empirically? Probably not. 
    - State it *for any two* probability distributions. 
        <!-- * Supposedly, [Lévy's Continuity Theorem](https://en.wikipedia.org/wiki/L%C3%A9vy%27s_continuity_theorem) is need to move from convergence in distribution to convergence in probability.  -->
    - Proof: 
        + I think that I will be able to just apply the LLN to the sum of two random variables. 
    - The normal:    
        + Easy because it is a family parameterized by its mean and standard deviation. 
        + It has the most entropy, that is, it contains the least information. 
        + I guess that its usage is easy to communicate
    - The Empirical Rule
    - The Berry-Essen Theorem
        + Statement: If the third central moment exist, then the order of its convergence is at least $\frac{1}{n^{\frac{1}{2}}}$. 
        + The constant $C$ is independent of any distribution. There are upper bounds for [it](https://en.wikipedia.org/wiki/Berry%E2%80%93Esseen_theorem#Statement_of_the_theorem). 
* Commentary: 
    - LLN: Requires the standard deviation to exist. (At least, for my proof, but supposedly it still holds.)
    - CLT: Requires the standard deviation to exist and to be nonzero. 
* Usage of the CLT: 
    - Sampling: Use the sample standard deviation of the sample (rather than the standard deviation of the sample means). This means that one sample can still give information.
    - The normal distribution: The Empirical (68-95-99.7) Rule. (Better than the Chebyshev Inequality)
    - Berry-Essen Theorem: Bounds on the convergence. 
* Graphics: 
    - How increasing the number of samples makes the normal distribution sharper: Superimpose them. 
    - An empirical verification through samples
    - How successive convolutions become normal

**Other**:
* What about estimating other statistics? 
    - Does the CLT also do the standard deviation? 
    - What about the absolute deviation? 
* The Fundamental Law of Statistics (the Glivenko–Cantelli theorem): 
    - This says that the sample distribution converges (uniformly) to the true distribution (except on a null set). 
    - The proof that I saw on Wikipedia uses the cumulative distribution, and I think that the general idea of how this was used is how one goes from discrete to continuous distributions. 


## Significance

*But, does this mean anything for physics?*

* Unsorted: 
    - Supposedly, bootstrapping removes the usefulness of Student's t-distribution. 
* How confidently are sampled summary statistics close to the population summary statistics?
    - The main example is the difference of mean of two populations. 
* Permutation tests. 
    - Motivation.   
        + First, what would be the chances for such an effect to show up if there is not a difference? 
        + The current data needs to be used. So look at all the reshuffling of the data into the two groups to see how often the effect is seen. 
        + There are 126 410 606 437 752 ways to split 50 samples into two sets of 25 samples. Thus, this is impractical. Instead, just take a large number of random permutations. 
    - Best practices. 
        + The p-value can vary from application. Often, 5% is used. 
        + The number of resamplings should be somewhat big. But, again, there is no definite rule.  
* State misconceptions. 
    - The p-value is not a probability of the hypothesis being correct. 
    - You cannot even use this to deduce the probability of the experimental hypothesis being true. 
    - It is not reliable. 
    - Effectively, it means almost nothing, but it is still useful (for making decisions). 
* Do it for correlation: 
    - Have a scatter plot. 


<!-- ## Bayesian Statistics

* Unsorted notes: 
    - Actually, does this have any actual use in the physical sciences? Well, I would like to have a way of updating a probability distribution based on evidence rather than just getting information about it. But, I am not sure how Bayes's Theorem helps with this. Yeah, I'm convinced that Bayes's theorem doesn't help with understanding probability distributions. So, how do I fit it in? 
        + I guess that this could still be used for parameters (such as physical constants) of distributions. 
    - Bayes's theorem is about updating the likelihood of something. 
    - That ship-problem is really just an application of bayesian analysis, where there is an initial probability. Bayesian analysis really makes sense when there is a guess of the original probability. 
    - For example, consider the probability that a randomly selected person is male given their height. In this case, Bayes's theorem is 
        $$
            \mathrm{prob}_{height}(male)
            = \mathrm{prob}(male) ⋅ \frac{\mathrm{prob}_{male}(height)}{\mathrm{prob}(height)}
            . 
        $$
        The factor $\mathrm{prob}(male)$ is called the *prior* probability, and the term $\mathrm{prob}_{height}(male)$ is called the *posterior* probability. The ratio can be called the *evidence*. The denominator of the ratio can be expanded to 
        $$
            \mathrm{prob}(male) ⋅ \mathrm{prob}_{male}(height)
            * \mathrm{prob}(female) ⋅\mathrm{prob}_{female}(height),
        $$
        but I don't understand why this form is more intuitive to other people. (What's a better example that applies more to physics?)
    - How do I apply this to distributions? Let $f$ be a prior probability, let $a$ be an observation, and let $f'$ be the posterior probability. 
        $$ 
            f'(x) = f_y(x) = f(x)⋅\frac{f_x(y)}{f(y)}
        $$
        $$ 
            f'(x) = f_y(x) = f(x)⋅f(a)
        $$ -->


<!-- ## Experimentation 

* Data collection and the importance of doing it with quality. 
    - I could mention how measurements for experiments are randomly shifted during collection. 
        + This was done in the muon gyromagnetic ratio experiment.
        + Maybe just mention this in Laboratory Exercises. 
* Measurement. 
    - When taking measurements, the mean is the "true value"?
    - The notation $±$. 
* Calibration.
    - Calibration of instruments.
        + Pearson's (χ) test. -->

