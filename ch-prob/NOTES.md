# Notes for Probability

## Central Limit Theorem 

* I think that I can do a novel proof
    + Having referees for this would actually be nice
* Idea:     
    + Don't think of every distribution become the normal distribution. But, think of any two distributions becoming the same. And, the normal distribution is the least informative. 
    + Use Markov's Inequality twice
* What about Hoeffding's Inequality
    - This is proved in Appendix 4.4 of Wasserman using Taylor's Theorem
    - It is sharper than Markov's Inequality 
    - It already has an exponential in it
    - Well, actually, it is for bounded density functions, so it probably does not apply
    - What about Mill's Inequality? Well, this is just about bounding the tails of a normal distribution. But, this can be useful in general. 
* Convergence has to be some sort of integration:
    - Why can it not just be under the integration norm? 
        + I think that the error is normally measured by the maximum norm between the CFDs. But, how is this better? 
* Is convergence also evidence of the standard deviation? 


## Need to Understand

* Entropy: 
    - I guess start with information
    - The logarithm is so that it is additive, that is, there is a concept of *twice as surprised*. But, I think that I only need the concept of ordering so that I can say that the normal density is the least informative. 
---
* Bessel's Correction: This is the $n-1$ in a sample standard deviation. Check [this](https://en.wikipedia.org/wiki/Bessel%27s_correction#Proof) out. 
---
* Stochastic Processes: 
    - Are these really just compositions of distributions like in the CLT? Possibly even a continuous composition. 
    - Why can the path integral approach to QM not be formalized this way? 
---
* Bayes: What really is the point of this? I guess that it is used when the current distribution is wanted during the process. For example, trying to locate an item. But, what are other examples? 
    - Supposedly, it is useful for assimilating more data by reducing the computational expensive. I think that this is similar to the mean-updating formula. 
---
* Convergence from discrete to continuous probability densities: I think that this is is what *convergence in probability* is about. 
* Does the Fundamental Law of Statistics have any practical value? 
---
* Calibration.
    - I don't think that there are definite tests. It might be like metrics in machine learning.
    - If one knows that actual data compared to the predicted/measured data, then one sees if the error (computed by some metric) is statistically significant or not. How is this done? Does this make sense for measured data?
    - If the actual data is not known, then I think that the machine reading is compared to the probability that the data was outside of a confidence interval.
* Should I talk about maximum likelihood estimation? This is predicting parameters of probability densities, using linear regression and what not.


## General 

* Actually, I do need multidimensional probability densities for QM. 


## Figures

* Should I have a figure that displays how the median is taken to be the middle point? It would have probability density $0$ for an interval. 
* Should I have a figure that displays the comparison with and without Bessel's Correction? 
* CLT: 
    - Compare two different distributions. 
    - Have the theoretical comparison, not some sampling. 
    - Include 1 sample to see the distributions themselves. 


## Language and Notation

* Define summary statistics for sets. Use notation like $\lim_{i \to \infty} \mathrm{mean}(S_i)$ for the Law of Large Numbers and the Central Limit Theorem, where $S_1$, $S_2$ and so on is an increasing sequence of sets.  
* Probably use *significance* instead of p-value. Also, use the word *cutoff* for it. 
* Use $f^{(*n)}$ for multiple convolutions (unless I find something better). 


## Material

* Summary: Statistics is the inverse of probability theory. 
* A density may not have a finite mean. 
* The CLT has the convergence rate! This means that empirical convergence can be checked. 
    + Should different orderings of the samples be checked? 
* Discrete densities: Say that they are intuitive to think of them separately, but that the theory of distributions unified these two views. 
* Example of finding a parameter of a probability density: Radioactive decay. 
* Bayes: Use estimating physical constants as the motivating example? 
* It is harder to deal with probabilities that are close to $0$ or $1$.
    - I forgot the context of this. I think that it is for permutation testing. 
* Where should Stirling's formula be? 
    - Its proof is [here](https://en.wikipedia.org/w/index.php?title=Laplace%27s_method&editingStatsId=3lplr594v9q63e9ctdg9ubas823uecaq&editingStatsOversample=1&gesuggestededit=1#Example:_Stirling's_approximation). Put Laplace's method as a lemma. Although I would not define it, it is interesting that this uses the gamma function. 
* I need examples of applying statistical methods. 
    - What about determining if something is radioactive from the number of clicks of a Geiger counter? 
    - Determining if a coin is fair. 
* Should I talk about confidence? Where does it go? 
* Care about the implementation of generating random samples from probability densities? 
    - This is what rejection sampling is for. 
* Experiment Design is the selection of samples: 
    - Stratified sampling is a numerical sampling method to force the spread of the distribution with fewer samples
    - What about importance sampling? 
* Fitting a distribution: 
    - Bayes: Maximum likelihood
    - Method of moments: Set of a system of equations that equations the theoretical moments with the empirical ones. There should be as many moments as parameters.
* Confidence: 
    - This is robust because it is not significantly influenced by extreme values (unlike the mean and measures of spread). 
    - And, it is easy to quantify. 
    - "Normally, an interval of confidence is around the medium."
* The standard deviation is like an error from the mean. What about the standard deviation of the standard deviation? Is this what higher moments are about? 
    - Well, I think that convergence of the CLT indicates convergence of the standard deviation. 
* I think that the idea of the harmonic mean is the weigh the lowest/worst values. (Think about averaging the quality of moves in chess as a bad example.)
* I guess that the geometric mean makes sense when talking about speed-up factors, while the arithmetic mean applies to the times for completion. But, I don't really understand.
* Why not things like mode or median for measures of spread around the average? Why not measures of spread around the other measures of central tendency? 
* The mean and median are within one standard deviation from each other ([see](https://en.wikipedia.org/wiki/Median#Inequality_relating_means_and_medians)). 

