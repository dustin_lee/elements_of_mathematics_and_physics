# Notes for Notation 

* Should I drop Scaled Units? 
    - If I keep it, then I should add Centigrade and Fahrenheit, so I need to rename it. 
* I want the periodic table as a fold out item at the end. 
* Possibly rearrange the material for the printing to take up less space. 
* Units: Maybe I should use $\mathrm{dps}$ instead of $\mathrm{Bq}$ for decays per second. I could also do something similar for Hertz. Well, $\mathrm{cps}$ could be counts or cycles per second. 