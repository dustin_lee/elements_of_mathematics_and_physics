[![Static Badge](https://img.shields.io/badge/LaTeX-3776AB?logo=latex&logoColor=fff)](https://www.latex-project.org/)

# The Elements of Mathematics and Physics

## Making

This is a book on physics and mathematics. 
Its purpose it explained in its [manifesto](MANIFESTO.md). 
As is explained in its [license](LICENSE.md), it is illegal to redistribute this work.


## Running

The project can be made by running the program `make`. 
This creates the PDF file `elements.pdf`. 
It stores files in the directory `build`, and, if there is trouble with compiling after previously compiling, try removing the contents of this directory with `make clean`. 

When viewed on GitLab, its PDF is available as an artifact through the CI pipeline.


## File Structure

The file `elements.tex` is the main file. 
The chapters are directories that start with `ch-`. 
The contents of the chapters together with `overview`, `preface` and `notation` are the contents of the work. 
In the chapters, overview and notation, the purpose of the file is denoted by the following prefixes. 

| Prefix | Meaning |
|----------|----------|
| `aux` | auxiliary | 
| `ch`  | chapter |
| `ex`  | example |
| `sec`  | section |
| `sub`  | subsection  |
| `thm`  | theorem |
| `fig`  | figure |


