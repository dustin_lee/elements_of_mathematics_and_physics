## Notes for Length, Area and Volume

* Define *partition*:
    - `A \emph{partition}\index{partition} of a set $A$ is a set of disjoint nonempty subsets of it whose union is $A$.`
    - If I don't, then define it in Integration. 


## Issues

* 


## Need to Understand

* Does a content space need to be defined so when a measurable set is the countable union of disjoint measurable sets, then an equality holds between the contents? Can this be proved? (Should I call this the Method of Exhaustion?)


## General 

* 

## Figures

* Inner and Outer Approximations:
    - Have them as separate side-by-side figures. 
    - For the outer approximations, denote intersection by darker shades. 
* Archimedes's Theorem: 
    - Some things from the Joy of X that I like for the visual. 
        + Instead of just one figure, I may do two to demonstrate the limit aspect. 
        + Also, instead of just two colors, I may do many to have a direct correspondence between the slices. 
    - Put the rectangular organization next to the circle (instead of below). This takes up less space, and it is slightly more intuitive (especially if I do two such divisions). 


## Vocabulary and Notation

* Should I use the word *measurable*, or should I just say that the content exists? 


## Material

* A proof of the Lantern Paradox is in the paper [Surface Area and the Cylinder Area Paradox](https://www.jstor.org/stable/3026930) by Frieda Zames. 
* Surface area requires orientation
    - Talk about the Möbius Strip
* For the Latern Paradox, look at these [references](https://en.wikipedia.org/wiki/Schwarz_lantern#References).
* Archimedes's proof is explained in this [video](https://yewtu.be/watch?v=5M2RWtD4EzI&t=0).
    - The idea is to cut the disc into pizza slices and connect them together where their orientation flip each time instead of staying the same. 
    - By, cutting off the curvy bits, this gives a lower bound. 
    - By, filling in these curvy bit, this gives an upper bound. 
    - Thus, the area is bounded between the areas of two parallelograms whose areas can be made arbitrarily close. 
    - Have pictures of like what is in the video. 
* Archimedes's constant is half of the product of the circumference and the radius. What do I make of this?  


