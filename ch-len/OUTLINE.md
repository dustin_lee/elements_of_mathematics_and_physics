# Outline of Length, Area and Volume

## Concept

* Definition. 
    - Just use boxes. 
        + Mention that these are intervals in the one-dimensional case, and they are called rectangles in the two-dimensional case.
        + Explain this choice of approximating sets. Say that known sets can be used. It would actually be more formal to only use cubes that are divided up evenly (and then use translation invariance). But, all matters is that it ends up being consistent. 
    - Inner and outer approximations by boxes. 
        + Make sure the inner approximation consists of disjoint boxes. 
        + Have a picture. 
        + Should the boxes be allowed to be rotated? How would I define these? 
    - Inner and outer content. 
    - Content. 
        + First, explain the intuition of the bounds.  
        + Define measurable.
        + Define length, area and volume. 
    - Equivalence of definitions on bounded sets. 
    - The content of boxes. 
* Basic properties. (The properties of a content space.)
    - Closed under unions. 
    - Closed under intersections. And, the equality for this. 
    - Closed under complements.  
        + Just do for complements in already measurable sets? 
            * I could even define content spaces this way so that the content is real. 
    - Equality of infinite unions if the set is measurable 
* Definition of a content space. 
* Examples: Area:
    - Triangles
    - Archimedes's number
    - Not proved: Pyramids?


## Length of Paths

* Definition. 
* Archimedes's Theorem 
<!-- * More
    - Not proved: Cylinder?
    - Not proved: Sphere? -->
<!-- * Cavalier's principle?  -->


## Area of Surfaces

* Definition. 
* The Lantern Paradox. 


## Content Spaces

* Define a content space. 
    - This is used to make the proofs in this chapter more general. 
    - As motivation, mention probability spaces. 
* Topological content spaces