%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Summary 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% This section introduces some definitions for subsets of euclidean spaces. 
% Specifically, it defines bounded subsets, open subsets, closed subsets, boundaries, interiors, connected subsets and regions.
% \scenebreak

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Boundedness
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


Let $E$ be a subset of a euclidean space. 
The \emph{diameter}\index{diameter} of $E$ is the number 
\[
    \sup_{p, q \in E} \abs{q-p}
\]
if the subset is nonempty, and the diameter is $0$ if the subset is empty. 
The subset is said to be \emph{bounded}\index{bounded} if its diameter exists, that is, it is finite. 
The diameter of the subset $E$ is denoted by $\diam (E)$\label{not:diam}.
A \emph{bound}\index{bound} of the subset $E$ is a positive real number $b$ such that the relation 
\[
    \abs{v} ≤ b
\]
holds for any vector $v$ in the subset. 
A function to a euclidean space is said to be \emph{bounded}\index{bounded} if its image is bounded. 
And, a sequence in a euclidean space is bounded if it \emph{bounded}\index{bounded} as a subset.


\scenebreak

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Open and Closed Subsets
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Let $p$ and $E$ be a point and subset of a euclidean space, respectively. 
An \emph{open ball}\index{open ball} around the point $p$ is a set of the form 
\[
    \set{ q : \abs{q-p} < r }
    ,
\]
where $r$ is a positive real number. 
In such an open ball, the real number $r$ is called its \emph{radius}\index{radius}.  
The point $p$ is called an \emph{interior point}\index{interior point} of the subset $E$ if an open ball at it exists that is contained this subset.
The point $p$ is called a \emph{boundary point}\index{boundary point} of the subset $E$ if every open ball at it intersects with both this set and its complement. 
The \emph{interior}\index{interior} of the subset $E$ is to be the set of its interior points. 
The interior of the subset $E$ is denoted by $\interior (E)$\label{not:interior}.
The \emph{boundary}\index{boundary} of the subset $E$ is defined to be the set of its boundary points.
The boundary of the subset $E$ is denoted by $\bd (E)$\label{not:bd}.
The \emph{closure}\index{closure} of the subset $E$ is the union of this subset and its boundary.
The closure of the set $E$ is denoted by $\cl (E)$\label{not:cl}.


\begin{figure}[H]
\centering
    \input{lim-fig-topology}
    \caption{An interior point $p$ and a boundary point $q$ in a subset of the euclidean plane. An open ball at $p$ exists in the subset. Any open ball at $q$ intersects this subset and its complement.}
\end{figure}


A subset of a euclidean space is said to be \emph{closed}\index{closed} if it contains its boundary.
And, a subset of a euclidean space is said to be \emph{open}\index{open} if it is equal to its interior.

The next six theorems are about closed and open subsets. 
The first three of these theorems say that the definitions are as expected. 

\input{lim-thm-balls-are-open}

\input{lim-thm-closure-is-closed}

\input{lim-thm-interior-is-open}


The next theorem says that closed subsets and open subsets are complements of each other. 

\input{lim-thm-complements}


The next theorem says that finite unions of closed subsets are closed. 
Thus, by the last theorem, finite intersections of open subsets are open. 

\input{lim-thm-closed-unions} % Closed sets


The next theorem says that infinite unions of open subsets are open.  
Thus, by the second-to-last theorem, finite intersection of open subsets are open. 

\input{lim-thm-open-unions} 


% The next theorem is about when subsets of subspaces are closed. 
% It says that a subset of subspace is closed exactly if it is the intersection of a closed subset of original space and the subspace.
% Because a set is open exactly if its compliment is open, a subset of subspace is open exactly if it is the intersection of an open subset of original space and the subspace.

% \input{lim-thm-subspaces}


\scenebreak

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Connectedness
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


A subset of a euclidean space is said to be \emph{connected}\index{connected} if it is not the union of two disjoint nonempty closed sets. 
That is, a subset of a euclidean space is connected if it is not the union of two disjoint nonempty open sets. 

The next theorem say that intervals are the only connected open sets of the euclidean line. 

\input{lim-thm-only-intervals}


% Regions
A \emph{region}\index{region} is a connected open subset of a euclidean space such that its interior is the interior of its closure. 