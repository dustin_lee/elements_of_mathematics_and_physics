# Notes for Limits and Continuity

## Questions

* Should I have the open-set definition of continuity? 
* Should I bother with the open-cover definition of compactness?
    - (Heine-Borel Theorem is what I would call the equivalence)


## Figures

* Topology: This could look better. It is a little choppy. Also, the tikz code is a bit dirty. 
* Squeeze Theorem for sequences. 
    - Use $i$ for the indices. 
    - Instead of $a$, write the limit $\lim_{i→∞} a_i$. 
* Squeeze Theorem for functions. 
    - Don't have numbers that denote scaling of the axes nor the extra divisions
    - Label the function $f$, $g$ and $h$ (or however they are labeled in the theorem). 


## Notation

* The notation $|f-g|$ is ambiguous. It can be the distance between two functions or the function that is the distances between their values. 


## Vocabulary

* Should I define empty sets to be bounded? 
* What is a *region*?
<!-- * Probably use *superior limit* and *inferior limit*. 
    - Although I make the adjectives adjectives, still use the normal notation for them. 
    - Using *top limit* and *bottom limit* would be better, but it would confuse people. I could even use the notation $\mathrm{tlim}$ and \mathrm{blim}$.  -->
* Don't define *path connected*, but just say that two points can be connected by a path. Also, prove that ``regular`` subsets of euclidean space are connected exactly if they are path connected.


## Material

* The False-Position Method? This is like the Bisection Method, but takes the intersection of the line through the endpoints. 


## General
 

* According to Wikipedia, the AM-GM Inequality can be used to prove the Cauchy-Schwarz Inequality. Maybe do it this way. Then, I can have the AM-GM Inequality in Numbers. 
* What really is the motivation of compactness? 
    - If it is just subsets of euclidean, the closed and bounded is enough. 
    - I need to point to a specific example of its use elsewhere (in functional analysis). 
* What does it mean for the space of continuous/bounded functions under the uniform norm to be complete? Does this have any implication in PDEs? Can this be used to prove regularity? 


<!-- 
AM-GM: 

Here is a proof of the Arithmetic-Mean-Geometric-Mean Inequality for two positive real numbers $a$ and $b$: It suffices to prove that $ab \leq (\frac{a + b}{2})^2 = \frac{(a + b)^2}{4} = \frac{a^2 + 2ab + b}{4}$. Thus, it suffices to prove that $4ab \leq a^2 + 2ab + b$ or that $0 \leq a^2 -2ab + b^2 = (a-b)^2$. Since $(a-b)^2$ is a square, this latter inequality always holds. 

Gelfand's \emph{Algebra} has more on the Arithmetic-Mean-Geometric-Mean Inequality. 
-->