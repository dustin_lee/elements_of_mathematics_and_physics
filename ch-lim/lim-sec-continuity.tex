%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Summary 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% First, in this section, continuity is defined. 
% Second, it is proved that continuity is preserved under arithmetic operations. 
% Third, some properties are continuous functions are proved. 
% These include the Intermediate-Value Theorem, which is used for Bisection Method. 
% Finally, uniform continuity is introduced. 
% \scenebreak

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Definitions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Let $f$ be a function from a subset of a euclidean space to a euclidean space, and let $p$ be a point in its domain. 
Then, the function $f$ is said to be \emph{continuous}\index{continuous} at the point $p$ if, for any positive real number $ε$, a positive real number $δ$ exists such that, if the distance of a point $q$ from $p$ is less than $δ$, then the distance between $f(q)$ and $f(p)$ is less than $ε$.
That is, the function $f$ is continuous at the point $p$ if the relation 
\[
    f(p) = \lim_{x→p} f(x)
\] 
holds. 
The function $f$ is said to be \emph{continuous}\index{continuous} if it is continuous at every point in its domain. 
That is, a function is continuous if a small change in its input makes a small change in its output.

In practice, the numbers are approximated. 
If a function is not continuous, then its output of an approximation is not an approximation of the desired output. 
Thus, normally, for a function to be useful, it must be continuous. 
For example, if the force from gravity did not depend continuously on mass and distance, then it would likely be unrealistic to find a formula for it. 


\begin{figure}[H]
\centering
    \includegraphics[]{lim-fig-continuous}
    \caption{The left is a continuous function. The right is a discontinuous function. Intuitively, the graph of a continuous function does not have any jumps.}
\end{figure}


\scenebreak

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Results
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


The next six theorem follow directly from the theorems on the limits of functions in Section~\ref{lim:sec:limit-methods}. 


\begin{theorem}
A constant function between subsets of euclidean spaces is continuous. 
\end{theorem}

\begin{theorem}
Let $f$ be a function between subsets of euclidean spaces that is continuous at a point $p$, and let $g$ be a function from the range of $f$ to a subset of a euclidean space that is continuous at $f(p)$. 
Then, the function
\[
    g ∘ f
\]
is continuous. 
\end{theorem}

\begin{theorem}
Let $f$ and $g$ be functions from a subset of a euclidean space to a euclidean space that are continuous at a point $p$. 
Then, the function
\[
    f + g
\]
is continuous at the point $p$.
\end{theorem}

\begin{theorem}
Let $f$ be a function from a subset of a euclidean space to a euclidean space that is continuous at a point $p$, and let $g$ be a function on the same domain to the scalars of the range of $f$ that is also continuous at $p$.  
Then, the function 
\[
    g \cdot f
\]
is continuous at the point $p$. 
\end{theorem}

\begin{theorem}
Let $f(x)$ be a function from a subset of a euclidean space to the nonzero real numbers that is continuous at a point $p$. 
Then, the function 
\[
    \frac{1}{f(x)}
\]
is continuous at the point $p$. 
\end{theorem}


The next theorem says that a function is continuous exactly if its components are continuous. This is because the function it is the sum of components, and because each projection to a component is continuous. 

\begin{theorem}
Let $f$ be a function from a subset of a euclidean space to a euclidean space. 
Then, it is continuous exactly if the function 
\[
    \comp_i (f)
\]
is continuous for each index $i$. 
\end{theorem}


The next theorem say that the size function is continuous. 

\input{lim-thm-cont-size}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Intermediate-Value Theorem
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

The next theorem is called the \emph{Intermediate-Value Theorem}\index{Intermediate-Value Theorem}. 
It formalizes the intuition that graphs of continuous functions do not have any jumps. 

\input{lim-thm-ivt}


\begin{figure}[H]
\centering
    \includegraphics[width=0.5\textwidth]{lim-fig-ivt}
    \caption{The Intermediate-Value Theorem.}
\end{figure}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Bisection Method
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

The \emph{Bisection Method}\index{Bisection Method} is now described. 
It is used to find roots of equations, and it is based on the Intermediate-Value Theorem.
Let $f$ be a real-valued function on an interval $[a, b]$ such that the relation 
\[
    f(a) < 0
    \textand 
    f(b) > 0
\]
hold. 
The Bisection Method approximated a solution to the equation
\[
    f(x) = 0
    . 
\]
By the Intermediate-Value Theorem, a real number $c$ exists between $a$ and $b$ such that the relation
\[
    f(c) = 0
\]
hold. 
Let $d$ be the center of the interval $[a, b]$, that is, the number $\frac{b-a}{2}$. 
If $f(d)$ is $0$, then the method found a solution. 
Otherwise, $f(d)$ is either positive or negative.
If it is negative, then the solution $c$ is in the interval $[a, d]$.
And, if it is positive, then the solution $c$ is in the interval $[d, b]$.
Either way, the original interval is replaced by this new smaller interval, and the process is repeated. 
The error of the Bisection Method is less than 
\[
    \frac{b-a}{2^n}
    ,
\]
where $n$ is the number of steps, that is, the exponential order of convergence of the Bisection Method is $1$.

In Chapter~\ref{ch:diff}, Newton's Method is introduced. 
This exponential order of convergence of this method is $2$. 
But, it requires an accurate initial approximation of the solution. 
Thus, these two methods are often used together where the Bisection Method is used to find an accurate initial approximation to use for Newton's Method, which then converges more rapidly. 




\begin{figure}[H]
\centering
    \includegraphics[width=0.3\textwidth]{lim-fig-bisection-method}
    \caption{The Bisection Method.}
\end{figure}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Extra Stuff on Continuity 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

The next theorem says that the inverse of a continuous function is continuous if it exists.

\input{lim-thm-cont-inverse}


The next theorem says that uniform limits preserve continuity. 
% It is for continuity at a point, but it implies that the limit of a sequence of continuous functions that converge uniformly is also continuous. 

\input{lim-thm-cont-by-uniform-lims}


\scenebreak 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Uniform Continuity 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Let $f$ be a function between subsets of euclidean spaces, and let $p$ be a point in its domain. 
Then, the function $f$ is said to be \emph{uniformly continuous}\index{uniformly continuous} if, for any positive real number $ε$, a positive real number $δ$ exists such that, the relation 
\[
    \babs{f(q) - f(p)} < ε
\]
holds for any two points $p$ and $q$ whose distance from each other is less than $δ$. 
Sums and compositions of uniformly continuous functions is uniformly continuous. 
But, products and reciprocals of continuous functions are not necessarily uniformly continuous. 