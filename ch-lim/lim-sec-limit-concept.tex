%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Summary 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% This section introduces limits.
% First, limits of sequences are defined. 
% Second, uniform convergence and pointwise convergence of sequences of functions are introduced. 
% Third, limits of functions are defined. 
% Finally, limit inferiors and limit superiors are defined. 
% \scenebreak

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Sequences
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Let $(p_i)_{i≥1}$ be a sequence of points of a euclidean space. 
Then, it is said to \emph{converge}\index{convergence} to a point $p$ if, for any positive real number $ε$, an index $n$ exists such that the relation 
\[
    \abs{p_i - p} < ε
\]
holds for any index $i$ that is more than $n$. 
If a sequence converges to a point, then this point is called its \emph{limit}\index{limit}. 
The limit of a sequence $(p_i)_{i≥1}$ is denoted by\label{not:lim:seq}
\[
    \lim_{i→∞} p_i
    .
\]
A sequence is said to \emph{diverge}\index{divergence} if it does not converge.
Intuitively, a sequence converges to a point if, for any qualification of nearness, the sequence eventually remains so near. 
It can be proved that a convergent sequence is bounded. 

The next theorem says that limits of sequences are unique. 

\input{lim-thm-unique-for-seqs}


The convergence of sequence does not depend on any finite number entries. 
And, the limit of a sequence is still defined if finitely many entries do not exist. 


\scenebreak

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Sequences of Functions 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

There are several notions of convergence of functions. 
In this section, pointwise convergence and uniform convergence are introduced.
In Chapter~\ref{ch:mt}, square-integrable convergence is introduced. 

Let $(f_i)_{i≥1}$ be a sequence of functions from a set to a euclidean space. 
Then, this sequence is said to \emph{converge pointwise}\index{pointwise convergence} if, for every point $a$, the limit
\[
    \lim_{i→∞} f_i(a)
\]
exists. 
This sequence is said to \emph{converge uniformly}\index{uniform convergence} to a function $f$ if the relation
\[
    \lim_{i→∞} \left( \sup_{x} \abs{f_i(x) - f(x)} \right) 
\]
holds. 
Uniform convergence implies pointwise convergence. 

In Section~\ref{lim:sec:metric-spaces}, metric spaces are introduced, which are a general framework for limits. 
Uniform convergence of functions is convergence in the metric space of bounded functions. 
But, pointwise convergence is not convergence is the sense of convergence in a metric spaces. 

\scenebreak

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Functions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Let $f$ be a function from subset of a euclidean space to a euclidean space, and let $p$ be a point in its domain. 
Then, the function is said \emph{converge}\index{converge} to a point $q$ at $p$ if, for any positive real number $ε$, a positive real number $δ$ exists such that, for any point $p'$ that is not $p$ and whose distance from $p$ is less than $δ$, the distance between $q$ and $f(p')$ is less than $ε$. 
If the function $f$ converges to a point $q$ at $p$, then this point $q$ is called the \emph{limit}\index{limit} of the function $f$ at $p$. 
The limit of the function $f$ at $p$ is denoted by\label{not:lim:fn}
\[
    \lim_{x→p} f(x) 
    . 
\]
The function is said to \emph{diverge}\index{diverge} at a point if it does not converge at this point. 
Intuitively, a function converges at a point $p$ if, for any qualification of nearness around $f(p)$, the function is so near at points that are sufficiently near $p$. 

The next theorem says that limits of functions are unique. 

\input{lim-thm-unique-for-fns}


\scenebreak

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Inferior and Superior Limits
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Let $(a_i)_{i≥1}$ be a sequence of real numbers. 
The \emph{limit inferior}\index{limit inferior} of this sequence is the number
\[
    \lim_{i→∞} \left( \inf_{j≥i} a_j \right)
    . 
\]
The \emph{limit superior}\index{limit superior} of the sequence is the number
\[
    \lim_{i→∞} \left( \sup_{j≥i} a_j \right)
    . 
\]
The limit inferior and limit superior of the sequence are denoted by 
\[
    \liminf_{i→∞} a_i
    \textand
    \limsup_{i→∞} a_i
    , 
\]
respectively. 
These are used for the Root Test and the Ratio Test in Chapter~\ref{ch:sums}. 

If a sequence of real numbers is bounded from below, then its limit inferior exists. 
Similarly, if a sequence of real numbers is bounded from above, then its limit superior exists. 
A sequence of real numbers converges exactly if its limit inferior and limit superior are equal. 
If the inferior limit of the sequence of real numbers exists, then there is a subsequence of this sequence that converges to it. 
Similarly, if the superior limit of the sequence of real numbers exists, then there is a subsequence of this sequence that converges to it.
 