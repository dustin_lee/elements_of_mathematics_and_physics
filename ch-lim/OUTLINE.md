<!-- LTeX: enabled=false -->

# Outline of Limits and Continuity

## Topology 

**Boundedness**:
* Diameter
* Radius? 
* Boundedness

**Open Sets**
* Interior and boundaries 
* Picture
* Open and closed sets

**Connectivity**: 
* Connectivity
* Regions 
* Connectedness
    - Theorem: Only interval are connected on the euclidean line 


## Limit Concept

**Sequences**
* Definition 
* Uniqueness

**Functions as Sequences** 
* Pointwise
* Uniform 
    - This is a metric space

**Functions**: 
* Uniqueness 

**Limit inferior and limit superior**
* 


## Limit Methods

**Sequences**
* Limit laws
* The Monotone Convergence Theorem

**Functions**
* Limit laws

**The Squeeze Theorems**
* For sequences
* For function

**Accumulative sequences**
* Completeness
    - No holes 
    - Examples: The rational numbers are incomplete, and the real numbers are complete 
* Accumulative sequences in complete spaces converge
* Common spaces are complete: 
    - Closed subspaces of complete spaces are complete 
    - Complete subspaces are closed

**Norm Equivalence**: 
* 


## Some Limits

* The sequence $(\frac{1}{i}\right)_{i≥1}$
* The sequence $(a^i)_{i≥1}$
    - By the Monotone Convergence Theorem
* The sequence $(a^\frac{1}{i})_{i≥1}$
* The sequence $(i^\frac{1}{i})_{i≥1}$
    - AM-GM as a lemma
    - (Use the proof for Example 3.1.11(d) in Bartle (the book for MA 341)?)


## Continuity

**Definitions**
* Continuity (epsilon-delta)
* Intuition that there are not any breaks in its graph. 
    - Have a picture. 
* Equivalent to the definition using limits
* Define trajectory (continuous and on an interval whose interior is not empty)

**Laws** (Actually, don't these just follow from the laws of limits?)
- Constants
- Addition, multiplication, division away from zero, 
- Compositions are are continuous

**Results** 
* Intermediate-Value Theorem: 
    - Have a figure
    - The Bisection Method
        - Have a figure
* Inverses of strictly continuous monotone function are continuous. 
    - The function $x^{1/n}$ is continuous. 
* Uniform limits preserve continuity 

**Uniform Continuity**
* Definition 
* 
* Laws: The functions $a⋅f$ and $f+g$ are uniformly continuous. 
* The product of uniformly continuous functions is not necessarily uniformly continuous. For example, the product $x⋅x$. 


## Compactness

**Definition**
* The sequential definition? 
* Bolzano-Weierstraß Theorem (compact exactly if closed and bounded)
    - Should I call this the Sequential Compactness Theorem? 

**Results**
* Continuous functions on compact spaces are bounded
* The Maximum Principle
* Continuous functions are uniformly continuous on compact sets


