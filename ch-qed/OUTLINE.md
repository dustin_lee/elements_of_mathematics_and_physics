# Outline of Discretization of Electromagnetic Interaction 

## Introduction 

* Explain the scope. 
	- This is a complete theory of everything chemical. 
	- How can nuclei be dealt with? 
* Numbers. The accuracy of the prediction of the gyromagnetic ratio of the electron. 


## The Electromagnetic Field

* Introduction. 
	- Motivate this trying to explain the wave-particle duality of photons. 
	- The electric field is something to be observed at each point. Thus, it makes sense that an observation of it is a field of operators. 
	- The new photon theory must be rectified with classical electrodynamics.
	- It does not make sense for light to be particles and for there to be the electromagnetic field as is. I guess that it makes sense for the electromagnetic field to be discretized because photon – which are just an electromagnetic field – are particles. Is there an experiment that should be referenced early on?
	- Maxwell's equations are only valid on average.
	- A theory for emission and absorption of photons is wanted. 
* Formulation of the behavior of the field using action. 
	- The field also has phase. 
	- The measurement of the field is probabilistic. 
* Photons. 
	- They are excitations of the field. 
	- Their spin.


## The Electron Field 

* Introduction. 
	- Motivate this by extending the explanation of wave-particle duality of photons to particles. 
	- Quantum mechanics is generalized to fast particles (unified with special relativity). 
* The Dirac Equation. 
	- This is essentially four coupled equations. I should write it out like this. 
* Explanation of spin. 


## Interaction of Light and Electrons

* Introduction. 
	- Because the electromagnetic field is radiated, I guess that it can make sense that photons are messengers. (An analogy is how two people in space and push each other apart by throwing a ball back and forth.)
* Action of the interaction. 


## Computation 

* Perturbation for calculation.
	- The idea is about counting the interactions and weighing them by the power of the coupling factor. So, why can this be done? 
	- The computation of the gyromagnetic ratio of the electron is an example of this method. 
* Path integrals. 
	