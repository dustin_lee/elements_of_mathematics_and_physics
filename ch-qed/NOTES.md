# Notes for Discretization of Electromagnetic Interaction 

## Need to Understand

* **What is the stepping stone into QFT?**
	- I guess to take the path integral approach to the fields and then try to use oscillators as as a computational method. 
	- How do perturbations and diagrams fit in? 
* Is all the symmetry and gauge stuff just crap? 
	- Like, does it actually help to make predictions? 
* How does measurement work with fields? 
* What is the experimental justification of the Dirac Equation? 
* Can the Dirac Equation be derived (reasonably guessed) by just trying to get a relativistic equation that reduces to the Pauli Equation? (By looking at symmetries, can it be proved that there is only one such equation?)
* How are excitations of the fields localized and travel? (They feel global to me.)
* I don't understand the Dirac Equation and the Klein-Gordon Equation. 
	- I think that solutions to the Dirac Equation also satisfy the Klein-Gordon Equation. 
	- Can these be derived from the path integral formulation? 
	- The Klein-Gordon Equation can be attained from the equation $E^2 = (p\c)^2 + m^2\c^4^2$ by replacing $E$ and $p$ with their operators. 
* Symmetry.
	- It is about commuting with the hamiltonian.
	- The conservation of momentum and whatnot are from symmetry.
	- The weak interaction is not conserved under inversion. This is why there is parity.
	- How does energy conservation work in quantum mechanics?
		* I think that there can be uncertainty in the energy because there is originally uncertainty.
		* At least the mean energy remains constant.
		* The energy in the time-energy uncertainty relation is the time period of existence of the state – not the time of measurement.
			- From Wikipedia: Energy at each fixed time can in principle be exactly measured without any trade-off in precision forced by the time-energy uncertainty relations. Thus the conservation of energy in time is a well defined concept even in quantum mechanics.
* How are quarks replaced by hadrons (as approximations?)
* Scattering: 
	- (Path integral:) I think that scattering computations must integrate for each point of the cross section. 
* Maybe it is about trying to derive empirical cross-sections?  

## Vocabulary

* 


## Graphics

* Have consistent color in Feynman diagrams, that is, each type of particle should have a specific color.


## Material

* Motivation: Describe photons, that is, electromagnetism 
	- Combine with SR
	- Need to drop the classical notion of electromagnetic potential 
	- Correspondence Principle: Must be like Maxwell Equations's at big scales
* Should there be a section on gauge symmetry? 
* Spin statistics, the PCT Theorem and so on. 
	- The PCT theorem says that all three of these symmetries need to be account for. 
* The Dirac Equation is also used for quarks. 
* The Dirac Field can be split into left- and right-handed parts. 
* Supposedly, that virtual particles can go faster than light is an argument for antimatter. 


## General 

* It would be nice to have links to (experimental) quantum optics clear. 
* Probably have a section on corrections after explain the main ideas? (Numerical examples of accuracy.)
* Don't bother with deriving the factors for the Feynman diagrams. Just list them. 
* A free electron can neither emit nor absorb a photon. (This is a theorem whose proof uses the conservation of energy and momentum.)