# Outline of Atomic Bonds

## Summary 

* Mention DFT
* Mention that, in Particle Statistics, it is explained how the Boltzmann Equation can be used to actually model current flow. 


## Introduction

* Introduction:
	- Explain how the strength of bonds is measured by the amount of energy that is required to separate the atoms. Mention the order of magnitude of the strength of the bonds. 
	- Mention the order of the spacing between atoms.
	- Mention that type of bonds (just by name).
	- Explain that bond types are not fixed, but are rough classifications. Bonds can normally be thought of as a mixture of these. (Some molecules – such as carbon monoxide – oscillate between different types of bonds.)
	- Should I explain polar molecules here?
		* A molecule is polar if the center of positive charge is different than the center of negative charge. Water is an example of this.
		* This explains a big chunk of (electrical) permittivity.
* Types of bonds: 
	- Ionic bonds.
		* This (normally) happens between an element on the left side of the periodic table and an element on the right side of the periodic table.
		* This is sort of just a covalent bond where the other atom completely takes the electron.
	- Covalent bonds.
		* Have plot of how the potential between two atoms typically depends on distance.
		* Bond angles
			- These account for the majority of bonds? Bonds between nonmetals are mostly covalent.
			- This can be experimentally measure with polarization or something.
			- Use water as an example and give examples of why this matters.
			- Ideally, actually give an example using density functional theory.
		* I think that always/normally, only at most two electrons can form a covalent bond by the Exclusion Principle.
	- Metallic bonds.
		* Many delocalized electrons is why metals normally have high electrical conducitivity.
	- Van-der-Waals bonds.
		* Should these be considered bonds? Put after this subsection?
	- Hydrogen bonds.
	- Talk about mixed bonds. For example, an electron in a covalent bonds might spend more time around one atom than the other, which makes it partly and ionic bond. 
* Crystals. 
	- Explain cohesive energy. This is the energy need to evaporate the material. 
* Notes: 
	- Some bonds need energy, that is, they occur at different temperatures
		+ Combustion is a chain reaction 
	- Must satisfy the Law of Entropy 
	- Reaction rates
	- Catalysts


## Bands

**Notes**:  
* Where do I talk about conductivity (resistivity) and holes? (A applied voltage tilts the energy bands.)
	- Intuition: Electrons are slotted into high energy states. Thus, 
* Explain the Thermoelectric Effect. 

**Energy bands**:
* When two atoms bond, they produce more states that are closer in energy. Over a solid, this becomes many states. Can this increase in spectrum be seen in a trivial way from the operator? 
* This is why a current is momentarily produced when different metals touch.
* The energy difference of band gaps can be directly measured by seeing which energies that photons need to be absorbed.
* Distribution.
	+ The density of states gives the density of states over the energies.
	+ The Fermi Energy. (The ground state.)
	+ The Fermi-Dirac Distribution describes to probability of occupancy of a state.
	+ Get the actual occupancy for energies.
* Band gaps: An electron needs to be able to move to the conduction band to be mobile.
	+ Explain insulators, conductors and semiconductors. 
	+ Heat can increase conductivity by allowing more electrons to get involved. (This can be seen with glass.) (Of course, this effect is normally more minor than the resistivity loss due vibrations.)
	+ Light can be increase conductivity if it has the correct frequency to move electrons into the conduction band.
* Numbers.
	+ Only a few percent of electrons are in the conduction band.

**Current**:
* Explain the free-electron model. 
	+ Include the concepts like mean-free path and so on. 
	+ Electrons move from higher to lower electrochemical potentials. 
	+ Numbers. 
		* The mean speed of electrons is about $10^6 \unitnf{m}{s}$.
		* The mean free path of electrons is about $1 \unit{μm}$.
	+ Explain effective mass. 
	+ Explain holes. 
* Mention that models will be further explain in Transport in Solids. 

**Optical Properties**:
* What determines reflectivity, transparency and absorption?  
	- Bands and orbits must match the photon energy for absorption (and thus reflection), but how are these distinguished? 

**Semiconductor Devices**: 
* Doping.
	- The number of charge carriers or holes can be changed. For example, adding phosphorus to silicon (which was an extra electron) produces more charge carries, and add aluminum (which is short an electron) instead increase the number of holes.
	- Changing just one in a million atoms can drastically change the conductivity.
* A diode can be made with a p-n junction.
	- Electrons (or holes) cannot diffuse and keep electrically neutral.
	- These can be used to make rectifiers.
* Transistors.
	- Definition. 
	- Use as amplification. 
	- Use in computers. 
		+ Say that they can be used to make logic gates. 
		+ Say that logic gates can be used to make adders and so on. 
		+ Give diagrams of these. 
	- Mention that there are many types of transistors. But, only the field-effect transistor is used explained here. (Should I also mention that the bipolar transistor is used for amplification?)
	- Field-effect transistor. 
		+ Explanation. 
		+ Explain roughly what photolithography is, and mention that it can be used to make integrated circuits. 
		+ Give a number of how many transistors we can put in a square centimeter.
* Photodetectors
* Solar cells. 
* LEDs. 


## Thermal Properties of Solids

<!-- I don't know if this is the right place/chapter for this. -->

* Thermal expansion.
	- The potential energy curve of a bond by length is not symmetric. The bigger the oscillations, the bigger the average distance between atoms.
* Heat capacity.
	- Internal energy can only be stored in the vibrations of the nuclei and the excited electrons.
		* Since only about 3\% of electrons are excited at room temperature, the excited electrons play a small role at normal temperatures.
		* The vibration of the nuclei can be explained with phonons.
			- Phonons exist because energies of the oscillations (roughly SHM) of the atoms take discrete levels.
			- They are also used for sound. They travel the speed of sound.
	- Classical model. 
	- Einstein model. 
	- Deybe model. 
* Thermal conductivity.
	- Phonons and their contribution.
		* I think that this becomes a field theory for a scalar field. Consider the action of the motion of the nuclei, and then take the limit as the spacing becomes zero. 
	- Electron contribution. 
* Heat radiation? 



## Electric Current (Boltzmann)

* Explanation. This chapter uses the material in this chapter together with the Boltzmann Equation to describe current in solids. 
* Use effective mass instead of mass. 
    - This can be measured by cyclotron frequency (supposedly). 
    - In metals, the effective mass is similar to the mass. But, in semiconductor, the effective mass can be different from the mass by as much as a factor of $70$. 
    - Holes are also given effective mass. 