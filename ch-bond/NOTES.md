# Notes for Atomic Bonds and Interactions

## Need to Understand 

* I don't understand why holes would tend to the same side as positive charges in the Hall Effect.
* In the basic model, are conduction electrons treated as free electrons (using the effective mass)?
* Maybe, for band gaps, the Fermi energy should be defined by what satisfies the Fermi-Dirac Distribution. 


## Vocabulary 

* Use *electrochemical potential* instead of *Fermi energy* or *Fermi level*. 
* Probably use *crystal momentum* instead of *wave vector* or *quasimomentum*. 


## Material 

* Don't bother with the Drude Model.
* Two materials of the same substance are prevented from bonding when in contact because of impurities on their surface.
* Think of bond strength like escape energy for an object in a gravitational field. 


## General

* The electrochemical potential is not just the energy of the electrons. For example, on the nanoscale, a hole can cause dissipation of heat, but this has to happen away from the hole. 
* I guess that it makes sense to think about the electrochemical potential $μ$ as being the sum of $μ-U$ and $U$, where $U$ is the electric potential. The first term a measure of the the energy up to the states filled and $U$ is the energy shift of the available states. I don't really understand this. This is from the Datta book. 
* I guess that electronic current is from entropy – or at least the idea of a distribution among states. 
* How to explain that charges don't continue to accelerate? In a normal material, they undergo collisions. But, what about in a superconductor? 
* Chargers are brought up to their terminal velocity so fast that, in practice, even with radio frequency, the transient can be ignored. 
