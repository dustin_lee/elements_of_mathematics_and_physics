# Set variables 
chapter=$1
title=$2

# Contents for main.tex
contents="\\input{elements.sty} 
\\begin{document} 
\\chapter*{${title}}
\\input{${chapter}.tex}
\\end{document}"
# Set up temporary directory 
mkdir .build 2> /dev/null
# Create the main file 
echo "${contents}" > .build/main.tex
# Copy style files
cp ../tex/*.sty .build
# Copy replacement file 
cp ../tex/replace.sh .build
# Copy files
cp *.tex .build 2> /dev/null
cp *.png .build 2> /dev/null
cp *.jpg .build 2> /dev/null
# Go to the build
cd .build
# Replace
bash replace.sh 
# Compile
latexmk -lualatex main.tex
# Return 
cp main.pdf "../${chapter}.pdf"
cd ..