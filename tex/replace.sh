for file in *.tex
do 
    # Environments
    sed -i 's/{align}/{align\*}/g' $file 
    sed -i 's/{gather}/{gather\*}/g' $file 
    sed -i 's/\\intertext/\\intertext\[-3pt\]/g' $file 
    # Multiplication Spacing 
    # sed -i 's/⋅/ ⋅ /g' $file 
    # sed -i 's/∘/ \\! ∘ \\! /g' $file 
    # sed -i 's/•/ \\! • \\! /g' $file 
    # sed -i 's/×/ \\! × \\! /g' $file 
    # Fixes
    sed -i 's/\\subset/\\subseteq/g' $file # Redefinition does not work 
    sed -i 's/\\div/\\mathrm{div}/g' $file # Redefinition does not work 
    sed -i 's/\\vec/\\mathrm{vec}/g' $file # Redefinition does not work
    # Other
    # sed -i 's/∫_/\\int\\limits_/g' $file # Change notation
done


