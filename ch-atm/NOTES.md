# Notes for Atoms and Molecules

## Need to Understand

* What are the results of particle statistics? 
    - Thermal energy is average kinetic energy. This is actually quite important. 
        + Make sure to mention that this does not mean that temperature is constant. 
* I don't understand the types of problems that molecular dynamics is trying to solve. What are some examples? 


## Overarching

* There needs to be some sort of useful prediction. (Furthermore, it should just be dynamical.) I think that the Boltzmann Equation is the only candidate. But, I still need some sort of setting for it. What about combustion? 


## Numbers

* Mention the speed of particles in the air. It is about 700 meters per second. 


## Figures

* Melting temperature versus strength: 
    - Only have about ten materials
    - Use the full name of the materials instead of their chemical abbreviation?  
* Crystal structures:


## Material

* Mention catalysts. Have an example. 
* Summary: The main point is that atoms are made of electrons and nucleons. There are also some other particles, but these are less important. 
* Does the Virial Theorem matter? 
    - This relates the average energy to the average force. I think that it can be used to derive temperature or something. It can be used to derive the Ideal-Gas Law. 
    - There is also a version in quantum mechanics. 
- Thermal energy is average kinetic energy: Because of potential energy, this means that the temperature is not constant. 
* Explains why solids tend expand when they melt. They are is some sort of crystal structure where the atoms are normally placed somewhat efficiently (as solid spheres they can occupy up to 74% of the volume), while, in a liquid, the atoms are randomly arranged. 
    

## General

* Give the optical resolutions of optical microscopes, xray microscopes and electron microscopes.
* Temperature is the mean kinetic energy. Thus, if there is potential energy, it does not quite stay constant. 
* When I explain pressure or whatever using the Atomic Hypothesis, also explain how this hypothesis explains why viscosity changes with temperature. 
* Should I talk about electronegativity? 