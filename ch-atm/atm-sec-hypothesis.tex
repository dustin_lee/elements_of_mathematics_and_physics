%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Summary 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% In this section, some justifications for and implications of the Atomic Hypothesis are discussed. 
% The Atomic Hypothesis explains states of matter, thermodynamics and chemistry.
% \scenebreak 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Hypothesis
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Everyday objects are made up of small particles, which are called \emph{atoms}\index{atom}. 
This is called the \emph{Atomic Hypothesis}\index{Atomic Hypothesis}.
And, it is very verified. 
Electron microscopes have been used to image individual atoms. 

Atoms are very small. 
The diameter of an atom is about $\SI{3.0d-10}{\meter}$.
That is, if atoms are arranged side by side, there would be over three million atoms per millimeter. 
And, in just one grain of sand, there are about $10^{20}$ atoms, that is, over a billion billion. 
That atoms are so small explains why their existence can normally be neglected at everyday scales. 

For atoms to build up everyday objects, they must be able to stick together. 
Roughly, in a solid, the force between atoms is rigid, and such a connection between atoms is called a \emph{bond}\index{bond}.
In a liquid, the force between the atoms is not rigid. 
And, in a gas, the force between the atoms is small.

Roughly, when a few atoms are bonded together separately from other atoms or such that these bonds are much stronger than their surrounding bonds, then they are called a \emph{molecule}\index{molecule}.
And, when many molecules are bonded together, it is a solid. 

In Chapter~\ref{ch:satm}, it is explained that atoms are made up of smaller particles, such as electrons. 
But, for many practical purposes, atoms are effectively indivisible. 


\scenebreak 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Friction
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

The friction from rubbing two solids together converts mechanical energy into thermal energy.
This can be explained by the mechanical energy being imparted to the atoms so that the atoms vibrate. 
That is, thermal energy is mechanical energy. 
Thus, thermodynamics is an emergent theory of the mechanics of atoms. 

It seems reasonable that sufficiently heating a solid will cause the energy of the atoms to overcome their bonds. 
This is melting. 
Similarly, heating a liquid will evaporate it.

Thus, the solid state is when the interatomic forces dominate.
The liquid state is when the interatomic forces and the thermal motion are balanced.
And, the gaseous state is when the thermal motion dominates.
That is, the Atomic Hypothesis explains states and their changes. 

Furthermore, it is more requires more energy to break stronger bonds. 
This explains why stronger material tend to have higher melting points.
For example, iron metals at a higher temperature than aluminum. 


\begin{figure}[H]
\centering
    \includegraphics[width=0.7\textwidth]{atm-fig-melt-v-strength}
    \caption{
        The melting point and strength of several materials. 
        These correspond. 
        This is evidence for the Atomic Hypothesis.
    }  
\end{figure}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Crystals
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Some solids have characteristic shapes. 
For example, the edges of salt are always at right angles.
This indicates that such solids are made up of atoms that are arranged in a repeating pattern. 
Such solids are called \emph{crystals}\index{crystal}.


\begin{figure}[H]
\centering
    \includegraphics[width=0.3\textwidth]{atm-fig-crystal-simple}
    \includegraphics[width=0.3\textwidth]{atm-fig-crystal-body-centered}
    \\
    \includegraphics[width=0.3\textwidth]{atm-fig-crystal-face-centered}
    \includegraphics[width=0.3\textwidth]{atm-fig-crystal-diamond}
    \caption{
        Crystalline arrangements.
        The upper left arrangement is called a simple cubic structure. 
        The upper right arrangement is called a boundary-centered cubic  structure.
        The lower left arrangement is called a face-centered cubics tructure.
        The lower right arrangement is called a diamond structure.
        Crystalline structures tend to be more common the more compact they are, that is, the higher density of their atoms. 
        Thus, the boundary-centered cubic and face-centered cubic structures are the most common. 
    }
\end{figure}


If the wavelength of light about the distance between atoms in a crystal, then the light will be diffracted by the crystal. 
Furthermore, because the arrangement of atoms in a crystal repeats, the diffracted light will be in a regular pattern.
This can be used to infer the arrangement and distances between atoms in a crystal. 
Because the distances between atoms is on the order of an angstrom, xrays are used for this. 
In Chapter~\ref{ch:satm}, it is explained how xrays can be produced with an xray tube. 

Locally, the atoms of most solids are in a crystalline structure. 
A local crystalline structure is called a \emph{grain}\index{grain}
And, the breaks between grains are called \emph{grain boundaries}\index{grain boundary}.
A typical diameter of a grain is about $\SI{50}{\micro\meter}$. 
Thus, only about one in thirty thousand atoms are part of a grain boundary. 
That is, over $99.99\%$ of atoms are part of the grain.


\begin{figure}[H]
\centering
    \includegraphics[width=0.4\textwidth]{atm-fig-grain-boundary}
    \caption{
        Grain boundaries. 
        The atoms are arranged in crystalline structures locally.
    }
\end{figure}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Gasses
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

At atmospheric pressure, the volume of water expands by a factor of $1600$ when it evaporates into steam.
Thus, the distance between atoms in a gas is about $40$ times the diameter of an atom.
That is, a gas is mostly empty space, and the atoms mostly pass by each other without colliding. 
This explains why diffusion is much more rapid in a gas than a liquid. 
For example, smells can travel quickly. 

In a container of gas, the atoms are constantly running into the walls. 
This constant bombardment of atoms explains the pressure. 
If the same amount of gas is in a smaller container, then the atoms run into the walls more often, which explains why the pressure is higher. 
If the gas is heated, then they move faster, which explains why the pressure is higher. 
And, if the container is compressed, its walls push on the atoms, which explains why rapid compression increases the temperature. 
Thus, the Atomic Hypothesis explains thermodynamical properties of gasses.
The mathematics of this is in Section~\ref{atm:sec:kinetic}.


\begin{figure}[H]
\centering
    \includegraphics[width=0.5\textwidth]{atm-fig-piston}
    \caption{
        Atoms of a gas in a piston. 
        The atoms constantly run into the walls, which exerts a pressure. 
        When the piston is compressed, the atoms run into the walls more often, which means that the pressure increases.
        Conversely, when the piston is expanded, the atoms run into the walls less often, which means that the pressure decreases.
        If the piston was heated, then the atoms would move faster, which would also increase the pressure. 
    }
\end{figure}


\scenebreak 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Chemistry 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Atoms are separated into species. 
And, a species of atom is called a \emph{chemical element}\index{chemical element} or \emph{element}\index{elements}.

In Chapter~\ref{ch:satm}, it is explained that some chemical elements can decay or fuse into other chemical elements. 
These reactions depend on the nuclear properties of atoms. 
But, in the practice of chemistry, chemical elements are normally considered to be permanent and immutable. 
Almost all everyday matters almost entirely consists of stable chemical elements.
Historically, most chemical elements were identified by observing what substances could not be decomposed into other substances. 

The \emph{periodic table}\index{periodic table} arranges all chemical elements by common properties.
Historically, this arrangement was empirical. 
But, it is explained by the Schrödinger Equation in Chapter~\ref{ch:qm}. 

% Number:
% 118 chemical elements have been observed. 
% But, for the practice of chemistry, only about 92 chemical elements are relevant because the other often decay rapidly. 

The \emph{Law of Definite Proportion}\index{Law of Definite Proportion} says that the proportions of the quantifies of the different chemical elements in a chemical compound are constant, where the quantity of the chemical elements can be measured by mass or number of atoms. 
Historically, this was observed empirically. 
But, assuming the Atomic Hypothesis, it is equivalent to the assumption that chemical elements are immutable. 

For example, when hydrogen is burned into water, the proportion of hydrogen to oxygen is constant. 
If there is less oxygen than this proportion, then some of the hydrogen will be left unburned. 
Similarly, if there is more oxygen than this proportion, then oxygen will remain after all the hydrogen has been burned. 

Furthermore, water can be separated by electrolysis. 
\emph{Electrolysis}\index{electrolysis} is the method of separating elements in a chemical compound by passing electricity through it. 
The proportion of hydrogen to oxygen that is produce by the electrolysis of water is the same proportion as the proportion for the burning of hydrogen. 

The Law of Definite Proportion says that chemical reactions are balanced.
For example, the burning of hydrogen is represented by the chemical reaction formula
\[
    \chem{O}_2 + 2⋅\chem{H}_2
    \yield
    2⋅\Big( \chem{H}_2\chem{O} \Big)
    .
\]
And, the electrolysis of water is represented by the chemical reaction formula
\[
    2⋅\Big( \chem{H}_2\chem{O} \Big)
    \yield 
    \chem{O}_2 + 2⋅\chem{H}_2
    .
\]
For clarity, oxygen and hydrogen exists as the molecules $\chem{O}_2$ and $\chem{H}_2$ in a gas a room temperature.


\begin{figure}[H]
\centering
    \includegraphics[width=0.4\textwidth]{atm-fig-electrolysis}
    \caption{
        Electrolysis of water. 
        An electric current is passed through the water. 
        This separates water into hydrogen and oxygen.
        Oxygen accumulates at the negative plate. 
        Hydrogen accumulates at the positive plate. 
        % !! I don't know if the order here is right.
    }
\end{figure}


Furthermore, electrolysis suggests that bonds are an electrical phenomenon.
This is clarified in Chapter~\ref{ch:bond}. 