# Outline of Atoms and Molecules


**OUTLINE (of First Half)**: 


* The hypothesis:  
* Explains heat and gas
* Explains chemistry 
	- Move in chemical combinations here? 
* Explains states
	- Crystal structure
* Evidence


**Stuff**: 
* Explain that thermal properties are so definite because there are so many atoms that it statistically varies little.
* Brownian motion: Explicitly say that the suspended particles are big enough to been 


## The Atomic Hypothesis

<!-- * Explains heat: 
	- Since rubbing creates thermal energy – in a way that appears to be inexhaustible – it seems reasonable that thermal energy is the motion of atoms. 
	- Thermodynamics is thus reduced to the study of atoms.  -->
<!-- * Explains heat and gases: 
	- Ideal-gas-like properties: 
		+ Have a figure of atoms in a box. 
		+ Explains why compressing increases pressure. 
		+ Explain why heating increases pressure. 
		+ Explains why things heat up compressed and cool when expanded: Momentum in imparted to the particles. 
	- Explains diffusion – such as smells. The different types of gas blend like different types of sand.
	- Explains states -->
<!-- * Explains chemistry.  -->
<!-- * Explains states -->
<!-- * Evidence. 
	- Spectroscopy shows a crystal structure. (The diffraction agrees with what it predicted.)
	- Brownian motion. Explicitly say that the suspended particles are big enough to been seen in a microscope but are small enough to be pushed around by moving molecules.
	- Also, maybe mention something more modern like actual isolation of an atom.  -->
<!-- * In things like metal, which don't seem to have a crystal structure, they actually do locally. 
	- Only about one in 30,000 atoms are on a grain boundary. 
	- This was in a section called Crystal Structure, but what would go in this?  -->
---
* Talk about *elementary substances* and *compounds*.
* The Law of Constant Proportions. (Should I introduce Electrolysis here?)
	- Electrolysis. (Bubble form against the wires.) The proportions of hydrogen and oxygen that are produce by electrolysis of water are always the same. Furthermore, they recombine to produce water in the same proportions. (The ions pick up to deposit a charge when they come in contact with the nodes. Then, I guess that they become gases?)
		* This suggests that bonds are electrical in nature. (Mention this latter?)
		* Faraday's law suggests that the energy of these bonds is regular?
	- Mention other examples of separating and combining substances? 
* The periodic table
	- There appears to a reduction of matter into the elements.
		* Chemical means cannot change these elements into one another. (Something is preserved.)
		* Make sure to explain that explain molecules are compound substances. Mention that the mathematical theory of this is explained later. (Mention that atoms will be seen to consist of electrons that orbit nuclei.)
	- They appear to have periodic behavior. 
	- This is all explained later: 
		- Number of protons determines the chemical elements. 
		- Electrons are involved in bonds, and the mathematics is described by QM. 
		- The periodic behavior is from jumps in the electron shells. 


## General Kinetic Theory

* Without knowing much about molecules, it can be seen that, in gases, the spacing between molecules is big because the volume change is known.
* The Ideal-Gas Law.
	-The averaged squared speed is what is needed. (Section 1.2 of Schroeder.)
		* This still gives an estimate of the speed. Particles are moving at hundreds of meters per second. (I think that the weight of the atoms is needed first.)
* Avogradro's law: Equal volumes of gas at the same pressure and temperature have the same number of molecules.
	- This is derived from the Ideal-Gas Law.
* Avogadro's number: The Law of Combing volumes and Avogadro's law can be used to determine atomic weights.
	- For practical reasons, an atomic mass unit is one-twelfth the mass of the isotope $\mathrm{C}^{12}$.
	- Now with Avogadro's number, the order of magnitude of the size of atoms can be computed: In one cubic centimeter, there is about one gram of material, which contains about Avogadro's number of atoms. Thus, the number size of atoms is about $(1/N_A^3) \mathrm{cm}$. This about one angstrom.
	- A much more precise way to compute Avogadro's number is to actually see the spacing and structure of atoms, which can be done with xray diffraction.
* Unsorted stuff.
	- Mean-free path is needed to compute the thermal conductivity in a gas.
	- The mean-free path can be computed by considering a molecule with twice the radius and seeing how long it takes to sweep out an area that would include another particle based off the density.

**Statistics**: 
* 


## Entropy 


## Transport

* Motivation. 
	- Want to explain the dynamics of gases from the particle viewpoint. 
	- This is useful for rarefied gases (such as when rockets are in the upper atmosphere), combustion (I guess) and transport (heat and current in materials, stuff in batteries, particles in nuclear reactors). 
	- It can derive the Navier-Stokes Equation. 
	- I guess that it also gives some sort of proof of the second law of thermodynamics. 
* Interatomic Potentials and Molecular Dynamics
	- ❗Does it actually make sense to include this here? 
		+ Like, atoms have to interact somehow. 
	- Explain that this is an approximation with bonds are neglected. 
	- Potentials: Lennard-Jones potential and others.
		+ Don't explain them in detail, but explain which is best in which setting. 
	- Mention some general molecular dynamics here, that is, the actual simulation of atoms moving around. 
		- The cutoff radius is used because it is faster, and it avoids self-interaction. 
		- Constant temperature. 
			* The simplest way is to just rescale velocities (which is only done every 10 steps or so).
			* Explain at least one type of heat bath. 
* Boltzmann distribution.
	- The Equipartition Theorem. (This is derived using the Boltzmann distribution.)
		* Have a picture (at least in the introduction) that displays the degrees of freedom of a rod: one for rotation around the axis and one from the normal sense of spinning.
		* Modes of vibration have two degrees of freedom. One for for kinetic energy and potential energy. But, these are normally frozen out at room temperature.
	- Mention the Rotating-Slit Experiment – either the double-slit approach or the drop approach. (This verifies this distribution.)
	- Boltzmann gives the first evidence that the the energy levels of an atom might be discrete?
* Should chemical potential be dealt with here? 


**New**: 
* Boltzmann: 
	- The variable/solution is a (probability?) density of the number of particles as a function of position and momentum/velocity.
		+ Wikipedia is ambiguous as to weather this is a probability density as each space-momentum point, or if it is just a density. 
	- First step: The time derivative of the density is the sum of the time derivatives of the density due to force, diffusion and collisions. Thus, each of these three terms need to be computed. 
	- 
	- A density is need for each chemical species. 
	- What is the point? 
		+ What does this say that the Reaction-Diffusion Equation and Transport Equation don't? 
		+ Is it a foundation for transport? 
		+ Is is a framework to add different phenomena? 
		+ How are chemical reactions added? 
		+ Can it account for various temperatures, such as reactions that only happen at some temperatures, such as in the Arrhenius Equation? 
	- Supplementary Motivation:
		+ Also works for electrons in semiconductors and ions in plasmas (and photon gas). 