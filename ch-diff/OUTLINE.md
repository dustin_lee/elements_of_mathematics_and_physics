# Outline of Differentiation 

## Concept

**Definition**:
* Definition 
* Have a picture

**Equivalent Definitions**:
* Equivalence with the normal definition
* Equivalence with the linear approximation definition 

**Concluding Remarks**: 
* Definitions about higher differentiability
* Differentiable functions are continuous


## Methods

* Basics
* Product Rule
* Chain Rule
* Inverse-Function Theorem
* Power Rule
* Derivatives of trigonometric functions
    - Lemma
    - Derivative of $\sin(t)$
    - Other derivatives


## Differentiation as Growth 

* Explanation 
    - Theorem: Increasing implies positive derivative
    - Theorem: Positive derivative implies increasing 
    - Stationary Theorem 
    - Mean-Value Theorem 
    - Bounded Derivative implies Uniform Continuity 
* Gradient descent
* Newton's method 
    - Can combine with the Bisection Method