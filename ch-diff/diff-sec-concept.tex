%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Summary 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% This section starts with the intuition of differentiation. 
% Then, the derivative of a function is defined. 
% This definition is proved to be equivalent to a simpler definition. 
% The section ends with the concept of multiple derivatives and a proof that differentiable functions are continuous. 
% \scenebreak

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Definition 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Velocity 
Let $γ$ be the trajectory of a particle, and let $a$ be a time. 
Intuitively, the velocity of the particle at the time $a$ is the vector is approximately 
\[
	\frac{γ(b_2)-γ(b_1)}{b_2 - b_1}
	,
\]
where $t_1$ and $t_2$ are different time points that are near $a$ such that the relations
\[
	b_1 ≤ a ≤ b_2
\]
hold. 
Furthermore, this approximation is better when these time points are nearer the time point $a$. 
Thus, the velocity of the particle is the limit of this ratio as these time points approach the time point $a$. 
Intuitively, this limit can be thought of as the ratio of infinitesimals because the limit of the numerator and denominator separately are both $0$.


\begin{figure}[H]
\centering
	\includegraphics[scale=.1]{diff-fig-derivative.jpg}
	\caption{Differentiation. The function $γ(t)$ is sample at successively nearer time points to $a$. And, the approximations of the derivative for these samples are denoted by the red arrows. And, these approximations of the derivative continue to nearer the derivative.}
\end{figure} 


The definition of the derivative is now defined. 
Let $f$ be a function on a nonsingular interval to a euclidean space, and let $a$ be a point in its domain.
Then, the \emph{derivative}\index{derivative} of the function $f$ at the point $a$ is the vector
\[
	\lim_{\substack{(t_1, t_2) → (a, a) \\ t_1 ≤ a ≤ t_2}}
		\frac{f(t_1)-f(a)}{t_2-t_1} 
	.
\]
The derivative of the function $f$ at the point $a$ is denoted by $∂f(a)$ or $\dot{f}(a)$\label{not:derivative}. 
If the derivative of the function $f$ at the point $a$ exists, then the function $f$ is said to be \emph{differentiable}\index{differentiability} at the point $a$.

Let $f$ be a function from a nonsingular domain to a euclidean space.
If the function $f$ is differentiable at every point in its domain, then it is said to be \emph{differentiable}\index{differentiability}.
The function $∂f(t)$ is called the \emph{derivative}\index{derivative} of the function $f$.


\scenebreak 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Equivalent Definitions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

The definition of the derivative uses a limit from both sides of the point $a$. 
The next theorem says that this is equivalent to using only a limit from one side. 
This limit is normally easier to compute. 

\input{diff-thm-equivalence}

By the last theorem, the derivative of the function $f$ at a point $a$ in the interior of its domain is also equal to the vector
\[
	\lim_{ε→0} \frac{f(a+ε) - f(a)}{ε} 
    . 
\]

The next theorem is called the \emph{Linear-Approximation Theorem}\index{Linear-Approximation Theorem}. 
It is an alternative definition of the derivative, and it is used for define derivatives of function on multidimensional domain in Chapter~\ref{ch:pdiff}.
It says that the function
\[
	f(a) + \dot{f}(a)⋅(t-a)
\]
is locally the best linear approximation of the function $f$ at the point $a$. 

\input{diff-thm-linear-approx}


\scenebreak

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Concluding Remarks
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Let $f$ be a function from a nonsingular interval to a euclidean space that is differentiable at a point $a$ in its domain. 
% Let $I$ be a nonsingular subinterval of the domain of $f$ that includes the point $a$. 
% Then, the derivative of the function $f$ at the point $a$ is the derivative of the restriction of the function $f$ to the interval $I$ at the point $a$.
% That is, the derivative of a function depends only on the local behavior of the function. 

Let $f$ be a function from a nonsingular interval to a euclidean space. 
If the function $f$ is differentiable at a point $a$, then it is continuous at this point because the relations
\begin{align}
	\left( \lim_{t→a} f(t) \right) - f(a) 
	&= \lim_{t→a} \left( f(t) - f(a) \right) \\
	&= \lim_{t→a}
			\left( \frac{f(t) - f(a)}{t-a} ⋅ (t-a) \right) \\
	&= \lim_{t→a}
			\left( \frac{f(t) - f(a)}{t-a} \right) 
		⋅ \lim_{t→a} (t-a) 
		\\
	&= ∂f(a)⋅0 \\
	&= 0
\end{align}
hold.
Thus, if the function $f$ is differentiable at every point, then it is continuous. 

Let $f$ be a function from a nonsingular interval to a euclidean space.
% Then, the function $f$ is said to be \emph{two-times differentiable} at a point $a$ if there exists an open subinterval $I$ of the domain of $f$ such the function $f|_I$ is differentiable and the derivative of the function $∂(f|_I)(t)$ is differentiable at the point $a$.
% If the function $f$ is two-times differentiable at a point $a$, then the \emph{second derivative}\index{derivative} of the function $f$ at the point $a$ is the vector
% \[
% 	∂(∂(f|_I))(a)
% 	, 
% \]
% where $I$ is an open subinterval around $a$ such that $f$ is differentiable on it. 
% The second derivative of the function $f$ is denoted by $∂^2f(a)$ or $\ddot{f}(a)$.
% The function $f$ is said to be \emph{two-times differentiable} if its second derivative exists at every point in its domain.
% For example, the acceleration is second derivative of the trajectory of a particle. 
% Higher order differentiation is defined recursively.
% The function $f$ is said to \emph{infinitely differentiable}\index{infinitely differentiable} if it is differentiable at the point $a$ if its $n\te$ derivative exists at this point for any positive integer $n$. 
% And, the function $f$ is said to be \emph{infinitely differentiable}\index{infinitely differentiable} if it is infinitely differentiable at every point in its domain.
Then, the function $f$ is said to be \emph{two-times differentiable}\index{two-times differentiable} if its derivative is differentiable. 
Similarly, higher order derivatives are defined recursively. 
Then, the function $f$ is said to be \emph{$n$-times differentiable}\index{$n$-times differentiable} if its $(n-1)\te$ derivative is differentiable, where $n$ is a positive integer. 
And, the function $f$ is said to be \emph{infinitely differentiable}\index{infinitely differentiable} if it is $n$-times differentiable for every positive integer $n$.
The function $f$ is said to be \emph{continuously differentiable} if it is differentiable and its derivative is continuous. 
And, the function $f$ is said to be \emph{$n$-times continuously differentiable}\index{$n$-times continuously differentiable} if it is $n$-time differentiable and its $n\te$ derivative is continuous. 


