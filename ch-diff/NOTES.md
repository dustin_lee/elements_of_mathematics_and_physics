# Notes for Differentiation

## Vocabulary

* Instead of using *twice differentiable*, just use the term *two-times differentiable*. It will rarely be used and doesn't require a definition. 
* Should I use the *Newton Approximation* instead of the *Linear-Approximation Theorem*?


## Figures

* Derivative:
    - The text should be vertical.
    - The superscripts should be in parentheses.
    - Important: The approximations become smaller and they angle outwards towards inwards.
    - Important: the biggest approximation does not cross the image of the trajectory.
    - Important: I like that the vectors are a different color.
* Lemma for the Derivative of the Sine Function: I would like for $E$, $E'$ and $E''$ to be denoted in the figure. But, it is hard to distinguish them because they overlap and whatnot. 
* Product Rule:
    - Color the slivers two different colors (the ones to mitigate color blindness problems), and then let the tiny square be the color of their mixture. Leave the main part white. 
    - Use `|-----|` for the distances instead of brackets. 
    - For the distances, either color them with the the corresponding color of the sliver or both the same color (red). Color the $f(a+ε)$ this color too, and leave $f(a)$ as black. 
* Second Derivative Test: 
    - I want a function, its derivative and its second derivative plotted together. 
* Failure of Newton's Method: 
    - Try to put all three on the same line. And, have all the x-axes line up. 


## Material

* Newton's Method: The Method of Successive Relaxation
    - This multiplies the step by the multiplicity of the root so that the order of convergence is $2$.
    - Normally, the multiplicity is not known beforehand. But, it can be empirically estimated after a few iterations. 
* Do I mention that the derivative is a local property? 
* Multiple derivatives: Unless a reason comes up, there probably is not a reason to define local higher order derivatives.


## General

* UQ: Multiple roots for Newton's Method: Supposedly, uncertainty is magnified for roots whose multiplicity is not $1$. 
* Inverse Function Theorem: If the inverse is differentiable, then it is this because $1 = ∂(\id)(a) = ∂(f^{-1} \circ f)(a) = ∂(f^{-1})(f(a)) ⋅ \dot{f}(a)$. I think that this is a simple way to remember the Inverse Function Theorem. But, I don't know how it fits in. 



