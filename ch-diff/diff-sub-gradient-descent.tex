% Optimization is a common problem in mathematics. 

Now, the \emph{Gradient-Descent Method}\index{Gradient Descent} is explained. 
This is a method to find minima of functions. 

Let $f$ be a real-valued function on an interval. 
Let $a$ be a number. 
By the Linear-Approximation Theorem, this function is approximately 
\[
    f(a) + f'(a)(t - a)
\]
near the point $a$. 
Thus, intuitively, for a small positive real number $ε$, the relations 
\begin{align}
    f\left( a - ε⋅\dot{f}(a) \right)
    &≈ f(a) - f'(a) ⋅ ε ⋅ \dot{f}(a) \\
    &= f(p) - ε⋅(f'(a))^2 \\
    &< f(a)
\end{align}
hold, where the last relation holds because $(f'(a))^2$ is positive.
That is, the value of $f$ at 
\[
    p - ε⋅\dot{f}(a)
\]
is less than the value of $f$ at $p$. 

This reasoning is continued. 
Let $ε_1$, $\ldots$, $ε_{n-1}$ and $ε_n$ be small positive real numbers. 
And, recursively, let $a_1$, $\ldots$, $a_{n-1}$ and $a_n$ be numbers such that the relation 
\[
    a_{i+1} = a_i - ε_i⋅\dot{f}(a_i)
\]
holds for each index $i$. 
Then, intuitively, the relations
\[
    f(a_1) > f(a_2) > \ldots > f(a_n)
\]
hold. 
Thus, intuitively, if the integer $n$ is sufficiently big, then the number $a_n$ is an approximation of a local minimum of the function $f$. 
That is, this is an iterative method to find a local minimum of the function $f$. 

The numbers 
\[
    - ε_1 ⋅ \dot{f}(a_1), \textspace
    \ldots, \textspace
    - ε_{n-1} ⋅ \dot{f}(a_{n-1})
    \textand 
    - ε_n ⋅ \dot{f}(a_n)
\]
are called the \emph{step increments}\index{step increment} of the method. 
And, their sizes are called the \emph{step sizes}\index{step size}.


\begin{figure}[H]
\centering
    \includegraphics[width=.5\textwidth]{diff-fig-gradient-descent} 
    \caption{The Gradient-Descent Method. The numbers $a_1$, $a_2$, $a_3$, $a_4$ and $a_5$ are successive approximations of a minimum of the function.}
\end{figure}


As of now, there are several concerns with the Gradient-Descent Method. 
First, it is not clear what the step sizes should be. 
Second, it is not clear if the method converges. 
Third, it is not clear when to stop. 
Fourth, it is not clear how to encourage the method to find a global minimum instead of a local minimum.
And, fifth, it is not clear how to compute the derivative at each step if the function is not known symbolically. 

Chapter~\ref{ch:opt} discusses these issues.
It introduces several variations of the Gradient-Descent Method to avoid these issues.
And, the convergence, speed and robustness of these methods are discussed.