# Outline of Fluids

**Required Material**: 



## Statics

* Pressure
* Hydraulics
* Floatation 


## Flow 

* Viscosity
* Equation: Continuity Equation -> Euler -> Navier-Stokes 
* Understanding: 
    - Streamlines
        + Have a figure
        + Can be used to study flow. 


## Emergent Phenomena of Flow 

* Boundary layer
* Bernoilli Effect
* Reynolds Number: Transition form laminar flow to turbulent flow
* Speed of sound
    - Shock