# Notes for Fluids

## General

* Often, it is really about the forces on solids (such as airplane wings). 


## Need to Understand 

* Pressure.
    - I guess that the force is the negative gradient of the pressure: It points in the direction of steepest descent and it is symmetrical around it (because along its orthogonal complement, it does not change).


## Figures

* Airflow around an airplane wing. 


## Material

* Viscosity is from momentum exchange.
    - Only in liquids, is the attraction of the molecules significant.
* Mention how (very) long the wavelength of sound can be or tends to be.
* Intuition why faster means less pressure: Just like any two objects (such as bikes) where one is in front of the other, there distance should be measured in time. Thus, when they are going faster, they should be more spread out. 
* Talk about the common emergent phenomena that are important: 
    - Turbulence
    - Boundary layers
    - Shocks
* Derive Euler's equation and the Navier-Stokes Equation.
* This chapter includes sound.
    - Talk about the Doppler Effect. This include the sound barrier and what supersonic means.
* Mention the Reynolds Number.
    - Should I talk about phenomena at the small Reynolds numbers?
        * Effectively, there is not inertia. Ay swimming motion cannot depend on the speed of the motion.
* Explain how airplane wings work. (This is a great inventions.)
* Explain hydraulics. 
* Archimedes' principle (the weight of fluid displaced is the weight of the object floating).
* Talk about drag. 
* Mention balloons. 


  
