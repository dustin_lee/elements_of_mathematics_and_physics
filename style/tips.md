# Tips

> "Slow and steady wins the race."  

> "To play a wrong note is insignificant; to play without passion is inexcusable."   
–– Ludwig van Beethoven 

> "Considering the multitude of mortals that handle the pen in these days, and can mostly spell, and write without glaring violations of grammar, the question naturally arises: How is it, then, that no work proceeds from them, bearing any stamp of authenticity and permanence; of worth for more than one day?"   
–– Thomas Carlyle

> "God is in the details."   


**Consistency**:
Above everything else, write consistently. 
This is the most important thing to do to finish. 

**Shitty Version**: 
This of the first draft as the shitty version. 
Get the bulk down. 
Then, edit towards perfection. 
It can easy for me to get caught in an imperfection. 
But, just be decisive and make a decision. 
I can change it later. 

**Quantifiable Progress**:
Focus on progress that can be quantifiable. 
Have a table of contents from the start. 
Focus on the percentage of drafted chapters. 

**Easy First**: 
Write the easy stuff first. 
This helps with gumption reserves, consistency and quantifiable progress. 
(But, in a day, do the hardest part first.)

**Remember the Goal**: 
The goal is to communicate. 
It is to take what is in your head and put it into someone else's head in the most effective and efficient way possible. 
A style rule is a rule of thumb, that is, a robust principle that guides towards this goal. 
Even in mathematics, is is better to use fallacies to communicate ideas than to use perfect logic to communicate nothing. 

**Global before Local**: 
If it is great locally, then it can still be shit. 
If it is great globally, then it will still be good regardless of the local quality. 

**Delete Later**: 
It is better to write more and delete later. 
Do not shy away from details because they are difficult to write. 
If I am not sure if I should include something, write it anyway and then decide. 

**Regularly Edit**: 
Constantly edit.
Propagate style decisions throughout.

**Unprecedented Effort**:
This book will be great because nobody will match the amount of time that I put into it. 