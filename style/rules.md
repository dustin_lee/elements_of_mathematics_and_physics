# Rules of Thumb for Writing 

> "The most essential gift for a good writer is a built-in, shockproof, shit detector. This is the writer's radar and all great writers have it."   
–– Ernest Hemingway  

> "Some people will think that the use of *iff* is acceptable."   
–– Jean-Pierre Serre


**Be Clear**: 
The goal is to communicate. 
All other rules contribute to this. 

**Be Simple**:
Use simple words. Use simple grammar. Use simple organization. Use simple arguments. 
Do not use unnecessary or unhelpful mathematical terminology. 

**Be Concise**: 
Use fewer words. 
Thus, do not have unhelpful words, that is, every word should add to the work. 
This means to not use the words *clearly* or *obviously*, which plague mathematics. 
Also, do not have unhelpful material, such as history, jokes or unhelpful opinions. 
But, do not confuse conciseness with brevity.

> **Bad**: Let $(p_i)_{i≥1}$ be a sequence of points in a euclidean space.    
> **Good**: Let $(p_i)_{i≥1}$ be a sequence in a euclidean space.   

**Be Precise**:
This is the point of mathematics. 
But, sacrifice a trivial amount of preciseness for simplicity and clarity. 

**Be Explicit**:
Part of this means avoiding short-hand notation, such as Einstein Sum Notation. 
Although such short-hand notation can be valuable for personal work, an exposition is not the place for it.
Furthermore, this means distinguishing concepts. 
For example, in quantum mechanics, measurements, operators and densities are different. 
Thus, they should be called differently, and different notation should be used for them. 

**Be Consistent**: 
Use consistent notation and vocabulary. 
Use similar phrasing for similar concepts. 
Display similar formats of information in similar ways. 

**Be Pedagogical**: 
Answer the questions *Why care?*, *Why believe this?* and *How do this fit into the bigger picture?*. 
The question *Why care?* is normally answered with real-world applications. 
The question *Why believe this?* is answered by proof in mathematics and experimental evidence in physics. 
It is often easy to fill in proof. It is much harder to fill in the big picture. 

**Be Intuitive**:
Keep things grounded. 
Choose intuitive reasoning over tricks. 
Prefer first principles.
Have concrete examples that the reader can keep in mind. 

**Have Flow**:
Have flow between successive ideas. 
Do not break the flow of reasoning long examples or several examples. 
When there is a break, it should feel natural. 
A perfect mathematical exposition would read like a novel. 
The reader should feel like they are just remember, that is, that they, or at most a slightly smarter version of themselves could have come up with the material given enough time and proper motivation. 

**Narration**: 
Have narration that explains connections.
First, this is more pedagogical and has more flows. 
Second, most readers will only remember are fraction of what they read. 
Still, they should be left with an understanding of what can be solved and where to look.

**Compartmentalize**: 
Put theorems in theorem environments. 
Put proofs in proof environments. 
Prefer only one clause or one implication in a sentences, that is, words like *which* should be used with caution
Avoid parenthetical phrases.

**Be Aesthetic**:
Aesthetics matter. 
Have aesthetic typesetting. 
Write equations aesthetically. 
Make the graphics aesthetic.
Each page should look nice and uniform, where the ideal is the uniformity of a page in a novel. 