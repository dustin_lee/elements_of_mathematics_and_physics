> "Good words are worth much, and cost little."  
–– George Herbert

**Contents**:
[Rules](#rules) |
[General Terms](#geneal-terms) |
[Mathematical Terms](#mathematical-terms)


## Rules

**Shortness**:
Prefer shorter words and phrases. By *short* by the number of letters, syllables and words. 

**Commonality**:
Prefer common words, both in mathematics and common language.
An [Ngram](https://books.google.com/ngrams) can be used to see the frequencies of words in common language.

**Intuition**:
Prefer words that intuitively carries over from everyday language to its mathematical meaning.

**Specificity**:
Use words that are more specific, that is, words that have fewer meanings in other contexts.

**Few**:
Prefer fewer total different words and fewer total [lemmas](https://en.wikipedia.org/wiki/Lemma_(morphology)). 

~~**Foreign Phrases**~~:
Do not use foreign phrases, such as *vice versa* or *ad hoc*. 

**Germanic**:
Prefer words with Germanic roots. An [eptymology dictionary](https://www.etymonline.com) can be used to see the origins of words. 

~~**Capitalized Adjectives**~~:
Do not capitalize adjectives because they are derived from names. 
This is less common, but it is more natural. 
For example, use *riemannian manifold* instead of *Riemannian manifold*.

~~**Proper Names outside of Results**~~:
Do not use names as adjectives.
Do not use names for general objects. 
For example, do not say that a sequence is *Cauchy*, but say that it is *accumulative*. 
This is often less common, but it is more natural.
It is fine to use names for results. 

**Latin Plurals**: 
Use the correct plurals for latin words. 
The plurals of *maximum* and *minimum* are *maxima* and *minima*, respectively. 
*Criterion* for the singular of *criteria*.
*Datum* is the singular of *data*, and use it instead of *sample*. 

**Compound Adjectives**: 
Use hyphens for compound adjectives, such as in *the second-to-last relation*.


## Geneal Terms

**All**: 
Do not use *all* in the phrases such as *the set of all points*.
It is just an extra word. 

**And So On**:
Use *and so on* instead of *and so forth* or *et cetera*.
It is shorter than *and so forth*, and *et cetera* is a foreign phrase. 

**Associated**:
Use *associated with* instead of *associated to*.
This is more common in everyday language. 

**Assume**:
Use *assume* instead of *suppose*.
There is not a reason to have both, and the connotation of *assume* is closer to its use in mathematics.
Furthermore, *assume* is used in the phrase "Assume that" instead of "It is assumed that". 
First, this is fewer words. 
The phrase "Assume that" does use the [imperative](https://en.wikipedia.org/wiki/Imperative_mood), which is a grammatical concept that is not used in many phrase structures in mathematics, and it is better to use less varied grammar. 
But, it is not reasonable to totally avoid the imperative in this work because there is not a reasonable alternative to it use to introduce and object with *let* because this usage is so common, concise and clear.
Thus, it is clearer and more consistent to use the imperative whenever it is useful because it is already used in some places. 

**Because**:
Use *because* instead of *since*. 
There is not a reason to have both, and *because* is more commonly used for how it is used for logical implication. 
Furthermore, prefer to use the clause introduced by *because* before its consequence because it is more intuitive to say what is known and then its consequence than the other way.

**Big**:
Use the lemmas *big* and *small*. 
This means use *bigger than* instead of *greater than* or *more than*, 
and *smaller than* instead of *less than* or *fewer than*. 
The terms *greater than* and *less than* are more common in mathematics, and the connotation of *more than* and *less than* capture comparison of negative number more. 
But, there is not a reason to have more than one pair of lemmas, and the word *big* and *small* apply short, intuitive and apply outside of comparison. 
Furthermore, for the comparison of functions, use *bigger than* and *smaller than* instead of *majorize*, *minorize* and *dominate*

**By**:
To reference a theorem in an argument, use *by* instead of *because of*. 
It is shorter and fewer words. 

**Inequalities**: 
Use *more than* and *less than* for strict inequalities. 
And, use *at least* and *at most* for lenient inequalities.
Using *more* and *less* is more consistent than *greater* and *less*, and *greater* is not common in everyday language. 
Using *more* and *less* is also consistent with *at least* and *at most*, and is is consistent with negative numbers in everyday language, which are the reason why *bigger* and *smaller* are not used. 
Furthermore, these are common in everyday language and have Germanic (even Proto-Indo-European) roots. 

> **Good**: "The number $a$ is more than $3$."   
> **Good**: "The number $a$ is less than $3$."  
> **Good**: "The number $a$ is at most $3$."   
> **Good**: "The number $a$ is at least $3$."   


**Collectively**: 
Use the adverb *collectively* to group together multiple classes of objects as one thing. 

**Different**: 
Use the word *different*. 
Do not use the word *distinct*. 

**Different from**: 
Use *different from* instead of *different than*. 
It is more common. 

**Evidence**:
To describe plausibility of a physical theory, use *evidence* instead of *indication*.
It is shorter and more intuitive.
It also means to not use the verb *indicates*. 

**Exactly If**: 
Use *exactly if* instead of *if and only if*. 
It is shorter. 

**Fewer**: 
Use *fewer* instead of *less* in reference to the sizes of subsets of a countable set when not written as mathematics. 
This is correct in English. 
> **Bad**: "There are less ducks in the lake today."  
> **Bad**: "There are fewer ducks in the lake today."  

**If**: 
Use *if* instead of *when*. 
*If* is shorter and more common. 

**Let**:
Use *let* instead of *consider*.
It is shorter, more common and more general in mathematics. 

**May**:
In the setting of inductive reasoning, use *may* instead of *might*. 
It is shorter and more commonly used. 

**Method**:
Use *method* instead of *technique*. It is shorter and simpler. I understand that *method* has the connotation of being more general, while *technique* has the connotation of being more specific. But, I do not think that this is significant enough to use two words. 

**Next**:
Use *next* instead of *following*.
There is not a reason to have both, and *next* is shorter. 

**Of The Form**:
Use *of the form* instead of *in the form*.
There is not a reason to use both. 

**Outside**:
Use *outside* instead of *outside of*.
It is fewer words.

**Proof**:
Use *proof* instead of *demonstration*. 
And, use *prove* instead of *demonstrate* or *show*. 
*Proof* is more common in mathematical writing, and its everyday connotation is closer to its mathematical use. 
Furthermore, *proved* is used as the [past participle form](https://en.wikipedia.org/wiki/Participle#Forms) of *proof* instead of *proven*, which is used only as the adjective form of *proof*.  
> **Bad**: "First, is is proven that the number $n$ is even."  
> **Good**: "First, is is proved that the number $n$ is even."  

**Say**:
In reference to speech, use *say* instead of *state*. 
It is shorter, more common in everyday language, and the word *state* already has a mathematical meaning. 

**Suffices**:
Use *it suffices* instead of *it is enough* or *it is sufficient*.
It is fewer words. 

**That and Which**: 
The words *that* and *which* are used differently. 
The word *that* specifies the object. 
The word *which* provides additional information. 
There should never be a comma before *that*. 
There should always be a comma before *which*. 
Furthermore, the word *which* can be used to add information to both nouns and clauses, while the word *that* is only used to modify nouns. 

~~**There Exists**~~:
Use phases similar to *A set exists* instead of *There exists a set*.
It is shorter and the verb is placed after the subject. 

**Thus**:
Use *thus* instead of *therefore* or *hence*. 
It is shorter. 

**Whole**: 
Use *whole* instead of *entire*. 
It is more common. 

**Whose**:
Use word *whose* rather than *of* when restricting the noun by a property rather than possession. 
For example, use the phrase *a set whose content is $0$* instead of *a set of content $0$*.
It is better English. 


## Mathematical Terms

~~**Almost Everywhere**~~:
Do not use *almost everywhere* or *almost surely*. 
It is more explicit and intuitive to say *except on a set whose content is $0$*. 

**Ambient**: 
Use the term *ambient space* to refer to the bigger space when it is not ambiguous. 
For example, use *the ambient euclidean space of the region*. 

**Antiderivative**:
Use *antiderivative* instead of *indefinite integral*. 
There is not a reason to have both. 
And, *indefinite integral* is misleading. 
Thus, do not use *definite integral* because *definite* in *definite integral* is redundant. 

**Approximate**:
Use *approximate* instead of *estimate*.
There is not a reason to have both. 
Although, the word *approximate* is slightly longer, they both come from Latin and 
the everyday connotation of *approximate* as well as its origin in Latin is nearer to the mathematical meaning.

~~**Banach space**~~: 
Do not use *Banach space*. 
Instead use *complete normed space*. 
First, this is more descriptive. 
Second, it does not use a proper name in a general term. 

**Coil**:
Use *coil* instead of *solenoid*. 
It is shorter and more common in everyday language. 

**Color**:
When it is only qualitative, use *color* instead of *frequency* or *wave length* to describe light. 
It is more intuitive and shorter. 

**Content**:
Use *content* instead of *measure* in the setting of length, area and volume. 
Although, the word *content* has become less popular over the years, the word *measure* is not used similarly to this in everyday language. 

**Delayed Time**:
Use *delayed potential* instead of *retarded potential*. 
The term *delayed* is now more common than *retarded* in everyday language.

**Different**:
Use *different* instead of *distinct*.
There is not a reason to have both, and the connotation is more general in the sense that it fully captures how these are used in mathematics.  

**Displacement Current**:
For Ampère's law, do not use the term *displacement current*. 
It is misleading and unnecessary. 

**Electric**:
As an adjective, use *electric* instead of *electrical*.
It is shorter. 

**Endpoint**: 
Use *left endpoint* and *right endpoint* to refer to the endpoints of intervals. 
This is more common than *start point* and *end point*. 

**Emission and Observation Times**:
For delayed potential, use *emission time* instead of *delayed time*. 
It is more intuitive. 
Furthermore, use  *observation time* to distinguish the actual time from the *delayed time*. 
Do not use the term *advanced time*. 

**Heterogeneous**:
To describe differential equation, use the word *heterogeneous* instead of the word *non-homogeneous*.
The word *non-homogeneous* is more common in mathematics, but it does not make sense linguistically because the word *heterogeneous* already means the opposite of *homogeneous*. 

~~**Hilbert space**~~: 
Do not use *Hilbert space*. 
Instead use *complete inner-product space*. 
First, this is more descriptive. 
Second, it does not use a proper name in a general term. 

**Image and Range**: 
Use *range* for the codomain of a function. 
Use *image* for the set $f(X)$. 
These are used inconsistently in the literature, but I find these uses the most comfortable. 

**Infinitely Many**:
Use *infinitely many* instead of *an infinite number of*.
It is fewer words.

**Initial Condition**: 
Use *initial condition* instead of *start state*. 
It is more common. 

**Input**:
Use *input* instead of *substitute* and *plug in*.
It is fewer words than *plug in*. 
It is more intuitive than *substitute*, and, although it is used less in mathematics, it is used more in computing. 

**Law of Definite Proportions**: 
Use the *Law of Definite Proportions.*
It is more common than the *Law of Constant Proportions* and *Proust's Law*. 
Furthermore, *Proust's Law* is not descriptive. 

**Magnetic Potential**:
Use *magnetic potential* instead of *vector potential*.
It is more intuitive and specific. 

**Mole**:
There is not a reason to use both *mole* and *Avogadro's number*. 
Use *mole* because it also used as the unit. 
Instead of *Avogadro's number*, use *one mole*. 

~~**Natural Number**~~:
Do not use the term *natural number*. 
This means to also not use the notation $\mathbf{N}$. 
First, this is additional additional word and notation. 
Furthermore, the term *natural number* is ambiguous because, depending on the author, it includes or excludes $0$. 
Instead, use the terms *positive integer* and *nonnegative integer*. 

**Nonsingular**: 
Use the term *nonsingular* to describe a set that has more than one element. 
This is especially useful for intervals. 

**Null**:
Use *null space* and *null group* instead of *kernel*.
It is shorter and more intuitive. Furthermore, they are both similarly common in mathematics. 

**Number of Nucleons**:
Use *number of nucleons* instead of *nucleon number* and *mass number*. 
It is clear and does not require a definition. 
The definition would be more words than how many it would save. 

**Path**: 
Use *path* for a one-dimensional manifold. 
Also use *path length* and *path integral* for consistency. 

**Position**:
Use *position* instead of *location*. 
There is not a reason to use both. 
The word *location* is more specific because *position* can also refer to an objects orientation in space. 
But, the word *position* is more common both in mathematics and in everyday language. 

**Probability Density Function**:
Use *probability density function* instead of *probability distribution*. 
Although it is less common, it is more intuitive. 
This means to also use the names such as the *Normal Density Function* and the *Binomial Density Function*.

**Quotient-Remainder Theorem**:
Use *Quotient-Remainder Theorem* instead of *Euclidean division*. 
First, it is more descriptive. 
Second, it is a proper noun, which means that it fits easier into writing. 

**Relation**: 
Use *relation* instead of *relationship*. 
There is not a reason to have both, and *relation* is shorter. 

**Rotation**:
Use *rotation* instead of *curl*.
Although is is less common in mathematics, it is more intuitive. 

**Solvability**:
Use *solvability* instead of *solubility* in reference to the existence of solutions. 
This is correct. *Solubility* is for chemistry. 

**Significance Test**:
Use *significance test* instead of *hypothesis test*.
Although it is less common, but it is more intuitive.

**Size**:
Use word *size* instead of *absolute value*, *norm* or *magnitude*.
It is less common in mathematical writing, but it is shorter, more intuitive, and more common in normal speech. 

~~**Soblev space**~~: 
Do not use *Soblev space*. 
Instead use *space of $n$-times differentiable functions* or *space of $n$-times weakly differentiable functions*.
First, this is more descriptive. 
Second, it does not use a proper name in a general term. 

**Symbolic**:
Use *symbolic* instead of *analytical* to describe *symbolic solutions* and *symbolic methods*. 
It is more intuitive and its every-day connotation is closer its mathematical usage. 
And, although *analytical* is more common, the chance of the use of *symbolic* possibly hindering communication is negligible.

**Time**: 
Use the term *time* to only refer to the concept. 
Use *time point* for a point. 
Use *time interval* for an interval of time. 
Use *time domain* when referring to the time component of the domain. 
Use *duration* for the distance between two time points. 
Use *time difference* for signed distance between two time points. 

**Time Increment**:
In time stepping for dynamic differential equation, use the term *time increment* instead of *step size*. 
With this term, it makes sense for it to be positive. 

**Unequal**:
Use *unequal* instead of *not equal*. 
It is less words. 

