# Grammar and Phrasing 

> "Easy things should be easy and hard things should be possible."  
–– Slogan of Perl, L. Wall

> "Like everything metaphysical, the harmony between thought and reality is to be found in the grammar of the language."  
–– Ludwig Wittgenstein

**Contents**:
[General Grammar](#general-grammar) | 
[General Phrasing](#general-phrasing) | 
[Specific Phrasing](#specific-phrasing) |
[Phrases of Purpose](#phrases-of-purpose) 


## General Grammar

**American English**:
Use [American English](https://en.wikipedia.org/wiki/Comparison_of_American_and_British_English) instead of Commonwealth English. 
This means always using an *s* after an apostrophe for possessives, even if the name ends with an *s*, such as using *Stokes's Theorem* instead of *Stokes' Theorem*. 
Use *formulas* instead of *formulae*. 
Use *color* instead of *colour*. 

**If-Then**: 
When the first phrase of an implication starts with *if*, then the second phrase should start with *then*. 

> **Bad**: If the number is divisible by $4$, it is divisible by $2$.     
> **Good**: If the number is divisible by $4$, then it is divisible by $2$.

**Compounds with Symbols**:
Do not use mathematical symbols for compound words. 
The only exception is using integers, such as in *$n$-times differentiable*. 

> **Bad**: The $L^2$-size of the function $f$ is $2$.   
> **Good**: The size of the function $f$ in the space of square-integrable functions is $2$. 


## General Phrasing 

**Third Person**: 
Only use the third person. 
Do not use the first person, that is, do not use the words *I* or *we*. 
Do not have reference the author except possibly in the preface. 

**One Tense**: 
Only use the simple present tense for main verbs. 
There is not a reason to have more tenses. 

> **Bad**: This will be proved in Chapter 13.  
> **Bad**: This was proved in Chapter 13.  
> **Good**: This is proved in Chapter 13.  

~~**Questions**~~:
Do not have grammatical questions, that is, do not use the question mark *?*. 

**Separate Clauses**: 
If two clauses are in a sentence, then separate them with a comma or semicolon. 

> **Bad**: The dog ran and the monkey climbed.  
> **Good**: The dog ran, and the monkey climbed.
> **Bad**: The dog ran away which means that it is not home.
> **Good**: The dog ran away, which means that it is not home.

**Comma Off Prepositional Phrases**: 
Use a commas around prepositional phrases except at the end of sentences. 

> **Bad**: In the lake a loon exists.  
> **Good**: In the lake, a loon exists.  
> **Good**: A loon exists in the lake.  

~~**Oxford Comma**~~:
Do not use [Oxford commas](https://en.wikipedia.org/wiki/Serial_comma). 
They do not contribute. 
This applies to lists of nouns or adjectives, and it does not apply to multiple clauses in a sentences. 
> **Bad**: The dog, the cat, and the horse are here.  
> **Good**: The dog, the cat and the horse are here.

~~**Commas Between Attributive Adjective**~~: 
Do not put commas between [attributive](https://en.wikipedia.org/wiki/Attributive_expression) adjectival phrases, that is, the adjectival phrases that precede a noun. 
Adjectival phrases before the noun that consist of multiple words should be hyphenated except for the adverbs. 
Commas should still be used between [predicate](https://en.wikipedia.org/wiki/Predicate_grammar) adjectival phrases, that is, those that are after the noun.
And, hyphens should not be used to combine an predicate adjectival phrase. 
The reduction of commons helps with the readability of the sentence by keeping the noun phrase tightly together. 
Furthermore, this is common in mathematical writing where many adjectives are used in front of a single noun.  

> **Bad**: The finite-dimensional, real vector space … .   
> **Good**: The finite-dimensional real vector space Thus, … .   
> **Good**: The vector space is real, finite dimensional and separable.   
> **Good**: The second-to-last relation …    
> **Good**: The infinitely countable set …  

**Comma Off Stray Adverbs and Conjunctions**: 
Use a comma with an adverb that is not next to a verb. 
And, use a comma with a conjunction that starts a phrase.   

> **Bad**: They are even and odd respectively.    
> **Good**: They are even and odd, respectively.  
> **Bad**: Intuitively it is infitesimal.     
> **Good**: Intuitively, it is infitesimal.    
> **Bad**: Thus it is even.   
> **Good**: Thus, it is even.  

~~**Contractions**~~: 
Do not have contractions in technical writing. 
For example, use *do not* instead of *don't*. 

**Semicolons**: 
Use semicolons to break phrases that have commas. 


## Specific Phrasing

*For consistency, say similar things similarly. Thus, common use rubrics for common types of structures. For example, format every proof by contradiction in the same way.*

**Summaries**:
In summaries, use the words *first*, *second* and so on. 
There is a summary as a section of every chapter. 
And, start every section with a summary as an informal subsection. 

**Successive Implementations**: 
Some people think that synonyms should used instead of repeating the same term in successive sentences, especially for implementations. 
They think that it flows better. 
They are wrong. 
Use the same words and the same phrasing for the same meaning. 
This is more clear. 
And, it even flows better. 

> **Bad**: Thus, … . Therefore, … . Hence, … .   
> **Good**: Thus, … . Thus, … . Thus, … .

**Characteristics**: 
Do not use *of* or *has* to express characteristics of an object. 

> **Bad**: The set has area $0$.   
> **Bad**: The set is of area $0$.  
> **Good**: The area of the set is $0$.  

**Conclusion**: 
For the concluding the proof of a statement, restate what was to be proved and use *indeed*. 
This sentence will normally start with *thus*. 

**Proof Steps**:
It is often fine to skip a single trivial step, but it is never okay to skip two successively.

**Proofs by Reductions**: 
Within proofs, state what is being proved and why. 
Then, state when it has been proved. 

**Proof by Contradiction**:
Start with "This is proved by contradiction. Assume that …". 
Then, end with "This is a contraction. Thus, …". 

**Proof by Cases**:
Use "This is proved by cases. The cases are where … ".
If a case is trivial, then have it in the same paragraph. 
For the first nontrivial case, use "First, assume that". 
For successive cases, use "Second/Third/…, assume that … instead". 
Each case should be one paragraph. If it is not, then there should probably be lemmas. 

**Proof by Induction**:
Start with "This is proved by induction."
Then, use "For the base case, …". 
For a simple base case, use one sentence to state what the relation for the base case is and why it hold. 
For a more complicated base case, use "it is prove that …". 
For the induction step, use "For the inductive step, assume … Now, it is proved that … ". 
<!-- Use *by the inductive hypothesis*.  -->
Conclude the proof of the induction as usual with the word *indeed*. 
Finally, in the conclusion with the word *indeed* also use *by induction*. 

**Continuation**
Use the phrase template "Continuing in this way, … is constructed/defined" to construct/define iteratively.

**Chain Relations**:
Symbolic manipulation should feel like one step. 
Use phrases such as *where the third relation holds by* after the relations for justification. 
If the chain only includes one central lemma, then it is okay to start the sentence with *By ...*. 

**Explicit Inequalities**: 
If a small positive real number $ε$ is fixed, have it be the final term in the chain of inequalities. 
This is more explicit. 

**Explicit Existence**: 
If it is being proved that two limits are equal, explicitly say the limit is question exists before staying the equality. 

**Articles**: 
Use the word *this* when the object was just introduced or the word this was used to denoted the object in every sentence since the object was introduced, otherwise, use the word *the*.

**Typing**: 
For a mathematical object, specify its type when it is first used in a sentence, unless it can be inferred directly from a direct relation with something else in the sentence. 
The phrases "the point $p$" and "the function $f$" are examples of typing an object. 
Always type it if it starts a sentence or clause. 
Often, the fundamental types are enough. 
The fundamental types are *number*, *point*, *vector*, *element*, *index*, *function*, *set*, *space*, *sequence* and *matrix*. 
But, more specific types, such as *real number* are often used to. 
The terms *limit* and *sum* are not types. 

**Enumeration**: 
Put the word *and* at the end of the second-to-last item rather than at the start of the last item. 

~~**Idiomatic**~~:
Do not use idiomatic phrasing. 
This is phrasing that has a meaning as a whole that is different then the sum of its parts. 
Examples are *break the ice* and *spill the beans*. 


## Phrases of Purpose

*Use phrases of purpose at the start of sentences to provide context. Often, these are one-word adverbial phrases.*

**Intuitively**:
Use *intuitively* to indicate that the statement is not rigorous. 

**Historically**:
Use *historically* to indicate a historical development. 
This is more precise than *originally*. 

**Normally**:
Use *normally* to indicate a rule-of-thumb assumption. 

**Often**: 
Use *often* to indicate the importance of the statement. 

**For example**: 
Use *for clarity* to provide additional reasoning of a statement immediately after the statement has been made. 

**For clarity**: 
Use *for clarity* to provide additional reasoning of a statement immediately after the statement has been made. 

**Without loss of generality**:
Use *Without loss of generality* instead of *It can be and is*. 
It is more common. 

**Formally**: 
Use *formally*, to indicate that it is not intuitive but only logical. 
Also, use *informal* to an abuse of language. 

**In implementation**: 
Use *in implementation* to indicate that it is a tip for numerical implementations. 