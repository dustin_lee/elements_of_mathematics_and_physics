# Format


**Contents**:
[Organization](#organization) |
[General Rules](#general-rules) | 
[Specifics](#specifics)

## Organization 

**Divisions**: 
The labeled divisions are chapter and section. 
In a subsection, scene breaks are used for unlabeled subsection divisions. 
Furthermore, the book is informally broken into three parts. 
The summaries at the start of each chapter are unnumbered. 

**Numbering**:
Do not use numbers for theorems, figures or tables. 
Instead, for theorems, reference their name. 
Almost all the theorems in this book already have names, so they will already be referenced by their names. 
Thus, such numbering is just extra ink. 

**Recto and Verso**: 
Start all chapters on the [recto](https://en.wikipedia.org/wiki/Recto_and_verso) page. 


## General Rules

**Aesthetic**: 
It is important to aesthetic. 

**Homogeneity**: 
The pages should look homogenous. 
This is more aesthetic.
The ideal is the homogeneity of a novel.
Flush the text. 

**Information per Page**:
Prefer more information per page. 
This helps to see the whole picture. 
It reduces flipping through the pages. 
It reduces the size of the book. 

**Less Ink**:
Extra ink distracts the reader. 


## Specifics 

**Small Font**: 
This increases the information per page. 

**Small margins**: 
This increases the information per page.

**Big pages**:
This increases the information per page.

**No Indents**: 
Do not use an indent for new paragraphs, but instead have spacing between paragraphs. 
There is not a reason to both indent paragraphs and have space between paragraphs.
Paragraph spacing is already used for theorems and proofs. 
It looks better. 
It increases the information per page. 

**Small spacing**:  
Have small spacing between paragraphs, around display mathematics, around figures and around scene breaks.
This increases the information per page.
It also improves homogeneity. 
For display mathematics, have as little spacing without it looking crowded, which is about 0.5 em on top of the normal line spacing. 
Around scene break and figures, there should be just the normal paragraph break. 

**Clump equations**:
When reasonable, put multiple equations on one line of display mathematics. 
This increases the information per page.
It also improves homogeneity. 

**Relations**: 
Always put relations in display mode. 
This would normally be done regardless because relations are normally too long for inline mode. 
Furthermore, the pseudo-verb in relations is easier to parse out by it being is display mode. 

**Flush text**:
This improves homogeneity.

**Captions**: 
The text of captions should be justified and centered. 
This is more aesthetic.

**Headers and Footers**: 
Most headers and footers are just extra ink. 
Only have the page number. 
And, place the page number in the lower outer corners. 
It is on the outer corners because this makes it easier to find a page. 
And, it is on the bottom because it is the least important information the page. 

~~**Subwords in Index**~~:
Do no have subwords in the index, that is, do not use `!` for index entries. 
This breaks homogeneity. 

**Unneeded Commas in Index**: 
In the index, do not have a comma between the word and the first page number. 
Instead, just have a space. 
This is less ink. 

**Multiplication Spacing**: 
Have only a small amount of spacing around the multiplication symbols `⋅`, `•`, `×` and `∘`. 
This is done by using `\! ⋅ \!` and so on, which I do in the make file. 