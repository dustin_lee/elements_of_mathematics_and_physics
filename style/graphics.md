# Graphics

> "A picture is worth a thousand words."  


**Use Graphics**:
Often, graphics can communicate information more easily than words. 
Graphics stain the memory. 
Graphics make the work aesthetic. 

**Do not List Graphics**: 
Do not have a list of graphics at the front of the book. 
Nobody reads this. 
It just takes up spaces. 

**Captions**: 
Have caption for every figure. 
Start with sentence fragment that describes the figure, that is, essentially with a title. 
Freely repeat information that is in the main body of test. 
Often, a reader will look at the figure caption before the main body of text. 
Furthermore, this is an effective method to drill in the information. 
Also, feel free to put in additional material that does not fit naturally into the main body of text. 

**Less Ink**:
Avoid clutter. 
All ink should contribute. 
A measure of a graphic is its information density. 

**Use Color**:
Include color in graphics. They are more aesthetic.
But, because about 8% of males have red-green color blindness, avoid using red and green together to distinguish. 

**One Library**: 
To have consistency, use one graphics library to make all graphics. 
I am using Tikz because its graphics are aesthetic and minimal for scientific graphics. 
Furthermore, for Tikz, use it directly and do internal computations when reasonable because this is more portable and editable. 

**Specify Position**: 
Manually place all figures. 
For now, this means using the option `[H]`.

**Labels**: 
Avoid one-use labels in graphics. If the labels can fit next to the object, such as the function, then just put them next to them instead of having an encoding scheme with a legend. 

**Flow Charts**: 
Use flow charts for complicated algorithms. 

<!-- **Tables**:  -->
<!-- [Use tables instead of pie charts.] -->