# Style Guide for the Elements

> "God wrote the universe in mathematics."  
–– Galileo Galilei  
<br> 
And, man wrote mathematics in English.  

> "Without craftsmanship, inspiration is a mere reed shaken in the wind."  
–– Johannes Brahms

> "Telepathy … I believe that writing offers the purest distillation … books are a uniquely portable magic."  
–– Stephen King


This is a list of rules of thumb and style choices for the Elements. 
Because of the size of the Elements, it is justified to take some liberty in deviating from tradition in favor of better choices. 

A good style guide for general writing is the *Elements of Style* of Strunk and White.
A good reference for scientific graphics is the *Quantitative Display of Information* by Tufte.


**CONTENTS**  
[Rules for General Writing](rules.md)   
[Some Implications of the Rules](grammar.md)   
[Phrasing](phrasing.md)   
[Vocabulary](vocabulary.md)   
[Names](names.md)   
[Notation](notation.md)   
[Graphics](graphics.md)    
[Format](format.md)
[Tips](tips.md)