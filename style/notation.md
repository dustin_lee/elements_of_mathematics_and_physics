# Notation 

> "No scientist thinks in formulae."  
–– Albert Einstein

> "Mathematics is, to a large extent, invention of better notations."  
–– Richard Feynman


## Rules

**Less Notation Used**:
Prefer using less mathematical notation. 
Only use notation when it is better than words. 
Less notation is more clearer and more aesthetic.

**Less Total Symbols Used**: 
Prefer fewer symbols in total. 
For letters, the Greek and Latin alphabet are sufficient. 
Furthermore, do not use calagraphic, script or fraktur fonts. 

**Dense Information**: 
Prefer notation that expresses the most in the smallest amount of space. 
Prefer short and concise notation. 
Avoid unnecessary symbols in notation. 

**Latin Letters**:
Prefer using Latin letters instead of Greek letters.
Use Latin letters for the most common objects, such as numbers, point, vector and functions. 
Do not use letters from any other alphabet. 

**Objects and Variables**: 
Use different notation for fixed objects and variables. 
For example, use $p$ for a point and use $f(x)$ for a function on the ambient space of $p$. 
This distinction is more explicit. 

**Operators**: 
Except for fundamental symbols, use operators instead of one-use symbols.
For example, use $\mathrm{int}(X)$, $\mathrm{cl}(X)$ and $\mathrm{bd}(X)$ instead of $X^{\circ}$, $X^{c}$ or $∂X$ for the interior, closure and boundary of a set $X$. 
By *fundamental symbols*, it is meant that the symbols have similar standing to symbols such as $+$ and $∫$. 

**Constants**:
Use a different front for constants. 
Use unitalicized letters for latin letters, and use up-Greek letters for Greek letters. 

**Subscripts**: 
Do not have excessive subscripts, superscripts and primes. 
For a few object, normally just use different letters. 
For example, use *the functions $f$ and $g$* instead of *the functions $f_1$ and $f_2$*. 

**Expressions are Nouns**: 
Grammatically, treat all mathematical expressions as nouns, that is, do not have grammatical verbs in notation.
This means to use a mathematical expression as another noun would be used. 
Admittedly, this is not common. 
But, it is better. 

> **Bad**: Thus, $$ 1+1=2. $$  
> **Good**: Thus, the relation $$ 1+1=2 $$ holds.   
> **Bad**: The numbers $a_{1}$, $\dots$, $a_n$" are positive.  
> **Good**: The numbers $a_1$, $a_2$, $\dots$ $a_{n-1}$ and $a_n$ are positive.  

**Correct Punctuations with Expressions**:
Use correct grammar and punctuation with mathematics expressions. 
This means sometimes ending out-of-line mathematical expressions with a comma or period. 

**Do not Start a Sentence or Clause with Notation**:
This is not aesthetic. It interrupts the flow of reading. 

**Intuition**: 
Write mathematical expressions intuitively. 
For example, for dynamic differential equations, isolate the time derivative on the left-hand side. 

**Parentheses**: 
Parentheses should be visually atheistic and it should be clear what the pairs are and what they are surrounding. 
This often requires manually setting their sizes. 


## Choices

**Ceil and Floor**:
Use $\mathrm{ceil}(a)$ and $\mathrm{floor}(a)$ for the ceiling and floor of a real number $a$ instead of $\lceil a \rceil$ and $\lfloor a \rfloor$. 
This avoid special notation. 
(I may not have a need for such notation anyway.)

~~**Covariant and Contravariant**~~:
Do not use whether an index is written as a superscript and subscript to distinguish whether an object is covariant or contravariant. 
It is additional subtle notation, and most people never really understand it. 

**Differentiation**:
Use the notation $∂$, such as in $∂f$ or $∂_x f(x, y)$, to denote derivatives instead of 
$$
    \frac{\mathrm{d}}{\mathrm{d}x}f(x), \ \ 
    \frac{\mathrm{∂}}{\mathrm{∂}x}f(x, y), \ \
    Df, \ \
    f', \ \ 
    \dot{f} \ \
    \mathrm{or} \ \
    f_x.
$$
First, there is not a reason to have multiple notations for a derivative, including different notations to distinguish ordinary and partial derivatives. 
And, the most common the notation
$$
    \frac{\mathrm{d}}{\mathrm{d}x}f(x)
$$
is unintuitive because it is not a fraction. 
The notation $∂$ is the best option because, although it is less common, it is short and general.

**Divergence and Rotation**:
Use $\mathrm{div}$ and $\mathrm{rot}$ instead of $\nabla \cdot$ and $\nabla \times$.
They are require less notation and are more intuitive

~~**Einstein Sum Notation**~~:
Do not use Einstein sum notation. 
It is not explicit. 

~~**Fonts**~~:
Do not use calligraphic, fraktur or blackboard bold fonts. 
They are just additional notation. 
Furthermore, only use bold letters for number systems.

~~**Indefinite Integrals**~~:
Do not use the integral symbol $∫$ for antiderivatives. 
It is misleading. 

**Integration**:
Use the notations 
$$
    ∫ f \ \
    \mathrm{and} \ \
    ∫_{x \in X} f(x)
$$ 
for undirected integration. 
And, use 
$$
    ∫_a^b f \ \
    \mathrm{and} \ \
    ∫_{t:a}^b f(t) \ \
$$ 
for directed integration. 
Furthermore, use the shorter version of each when still clear. 
For unoriented integrals, this is shorter and denser than using $\mathrm{d}x$.
Furthermore, the notation $\mathrm{d}x$ is misleading. 

~~**Laplacian**~~:
Do not use a new notation for the Laplacian, such as $\nabla^2$. 
Instead use $\mathrm{div}(\nabla f)$. 
It is more explicit and requires less notation. 

~~**Lorentz Factor**~~:
In special relativity, do not use the Lorenz factor $γ$ for the factor
$$
    \frac{1}{(1 - \frac{v}{\mathrm{c}})^{\frac{1}{2}}}. 
$$
It is not explicit. 

**Modulus**:
Use $\mathrm{mod}$, such as in $\mathrm{mod}_4 (7) = 3$ for the modulus instead of Gauß's notation, such as $7 \equiv 3\ (\mathrm{mod}\ 4)$. 
It is shorter and more intuitive. 
It requires only the definition of an operator instead of an entire notation. 
Furthermore, notation such as $\mathrm{mod}_4 (7)$ can be used by itself. 

**Number Systems**:
For number systems, use bold letters instead of blackboard bold letters. 
For example, use $\mathbf{R}$ instead of $\mathbb{R}$ for the real numbers. 
Blackboard bold letters are how people write bold letters on blackboards. 
It does not make sense to use them in print. 
Thus, although regular bold letters are now less common, they are more simple and make more sense. 

**Piecewise Functions**:
Do not have the end-line or mid-line punctuation for the piecewise notation. 
They are unnecessary. 

**Set Builder Notation**:
Use a `:` instead of `|` for set builder notation, that is, use 
$$
    \{ a \in \mathbf{R} : a≤1\}
$$
instead of 
$$
    \{ a \in \mathbf{R} | a≤1 \}. 
$$
It is marginally better when `|` is is used as part of the restraint. 

**Size**:
Use $|a|$ to denote the size of an object, independently of whether it is a number or vector; that is, do not distinguish vectors with the notation $||v||$, but simply use $|v|$. 
There is not a reason have such additional notation. 

