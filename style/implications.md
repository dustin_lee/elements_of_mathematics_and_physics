# Some Implications of the Rules

> "What I cannot create, I do not understand."   
–– Richard Feynman 

> "Everything should be made as simple as possible, but not simpler."  
–– Albert Einstein


~~**Abbreviations**~~:
Do not use abbreviations. 
For example, do not use *CLT* for the Central Limit Theorem.
It is better to be explicit. 

**Abstraction**: 
Do not have excessive abstraction. 
The use of abstraction is clarity. 
Normally, refrain from abstraction without practical value. 

**Commentary**:
Have simple statements that act as checks of understanding. 
These are to touch base and clarify misconceptions. 

**Convergence**:
Give explicit inequalities instead of saying *as it goes to $0$*.
This is more explicit. 

~~**Excessive Qualifiers**~~:
Do not have excessive qualifiers. 
If it is clear what set an element belongs to, then do not mention the set. 
Such qualifiers are not significant in simple situations. 
But, there are situations where they can get out of hand, and it is best to be consistent with their use. 

> **Bad**: A function is continuous if it is continuous at every point in its domain.    
> **Good**: A function is continuous if it is continuous at every point.    

**Examples**: 
Examples are useful for two purposes. 
First, they can help explain how a method is used. 
But, only have such an example if it is expected to clear ambiguity for a decent number of readers. 
Second, they can be used to demonstrate why a theorem holds or what the obstacle in a proof is. 
Such examples have the germ of generality or are counter examples. 
For example, the statement of Baire's Theorem feels odd. But, thinking of a sequence of euclidean planes each with a few points removed gives intuition to what an open dense set is. 
For another example, Newton's Method may fail to converge if too far from the solution because it can runaway to infinity. 
These examples do not need to be proved in detail.
Furthermore, they are often best expressed by visualizations. 

**SI Units Only**: 
Use only SI units. 
Using only one system is consistent, and natural units are not explicit. 
Furthermore, to agree with standard SI format, use spacing between triples of digits instead of commas. 
