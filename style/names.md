# Names

## Rules

**Descriptiveness**:
Prefer descriptive names.  

**Shortness**:
Prefer shorter names. 

**Uniqueness**:
Names should be unique. 
Unlike for general word choice, for names, there is not a preference for consistency nor having fewer distinct words and lemmas.

**Letters**:
Use Diacritics and extended latin characters, such as *á* and *ß*, when appropriate. 

**Capitalization**:
Capitalize all names. 
For example, use *Wave Equation*, *Maxwell's Equations*, *Archimedes's Theorem*, *Exclusion Principle* and *Finite-Elements Method*. 

**Eponyms**: 
A descriptive name is preferable to an [eponym](https://en.wikipedia.org/wiki/Eponym) or a combination of both. 
Furthermore, for eponyms, preference is not given towards the originator, but, instead, to the most common use. 

> **Bad**: The Pauli Exclusion Principle    
> **Good**: The Exclusion Principle

**References**: 
References to chapters, sections and subsections are considered to be proper nouns, that is, capitalize them. 

> **Bad**: This is proved in chapter 13.  
> **Good**: This is proved in Chapter 13.


## Choices

**Flux Laws**:
Use *Electric Flux Law* and *Magnetic Flux Law* instead of *Gauß's Law* and *Gauß's Law for the magnetic field*.
Although they are less commonly used than *Gauß's Laws*, they are more descriptive. 

**Fundamental Theorem of Vector Calculus**:
Use the *Fundamental Theorem of Vector Calculus* instead of the *Helmholtz Decomposition Theorem*. 
Although it is less commonly used than the *Helmholtz Decomposition Theorem*, it is more descriptive. 

**Maximum Principle**: 
Use the *Maximum Principle* instead of the *Extrema Principle*. 
Although the latter is more slightly accurate, the former is more common. 

**Row-Reduction Method**:
Use the *Row-Reduction Method* instead of *Gaussian Elimination*. 
It is less common, but it is descriptive. 