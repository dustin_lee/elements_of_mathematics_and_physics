# Notes on the Formatting of the Elements

## General

* 


## Typography 

* I want to reduce the space around $⋅$ in expressions.
    - I could use `\!`, but then I would have to make a command instead of just using `⋅`.
    - The `make` could do this. 
* Use `\ldots` instead of `\cdots`.


## Math Mode

* Equations: 
    - Use `\[ \]` for single-line equations. 
    - Use `align` for multiple aligned lines. 
        + Use `\intertext` to group expressions together with *and* or *or* to preserve alignment.  
    - Use `gather` for multiple unaligned lines 
    - USe `array` when appropriate
    - Use `multiline` for anything? 
* Put all but the smallest mathematical items out of line. 
    - One reason for this is because I prefer small line spacing and that it is consistent. 
* Multiline expressions.
    - For unaligned multiline expressions use `\texttab`.
    - For aligned multiline expressions, use `\! \begin{aligned}[t]`. 
    - Use fixed-sized parentheses for multiple lines.
    - Use the gathered environment for centered multiline expressions. 
    - Just don't use any spaces or whatever for continuation of equalities.