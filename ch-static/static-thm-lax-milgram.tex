\begin{theorem}
Let $V$ be a Hilbert space. 
Let $f$ be a continuous linear function from the vector space $V$ to its scalar field. 
And, let $φ(x, y)$ be a function from the vector space $V × V$ to its scalar field such that
\begin{enumerate}
    \item it is linear in its first coordinate, and it is anti-linear in its second coordinate; 
    \item a nonnegative real number $a$ exists such that the relation
    \[
        \abs{φ(x, y)} ≤ a ⋅ \abs{x} ⋅ \abs{y}
    \]
    holds; and 
    \item a positive real number $b$ exists such that the relation
    \[
        φ(x, x) ≥ b ⋅ \abs{x}^2
    \]
    holds. 
\end{enumerate}
Then, a unique vector $u$ exists such that the relation
\[
    φ(x, u) = f(x)
\]
holds. 
Furthermore, the relation
\[
    \abs{u} ≤ \frac{1}{b} ⋅ \abs{f}_{\mathrm{op}}
\]
holds. 
\end{theorem}

\begin{proof}
First, it is proved that a unique vector $u$ exists such that the relation
\[
    φ(x, u) = f(x)
\]
holds. 

Because the function $φ(x, y)$ is continuous and linear in its first coordinate and by the Representation Theorem, a function $g$ exists such that the relation 
\[
    φ(x, y) = \sp{x}{g(y)}  
\]
holds. 
Furthermore, by the Representation Theorem, a vector $w$ exists such that the relation
\[
    f(x) = \sp{x}{w}
\]
holds. 
Thus, a unique vector $u$ exists such that the relation 
\[
    φ(x, u) = f(x)
\]
holds exactly if a unique vector $u$ exists such that the relation 
\[
     \sp{x}{g(u)} = \sp{x}{w}
\]
holds. 
Furthermore, a unique vector $u$ exists such that the relation 
\[
     \sp{x}{g(u)} = \sp{x}{w}
\]
holds exactly if a unique vector $u$ exists such that the relation
\[
    g(u) = w
\]
holds. 
Thus, it suffices to prove that the function $g$ is bijective. 

First, it is proved that the function $g$ is injective.
It suffices to prove that it is linear and that its null set is $\set{0}$.

It is proved that the function $g$ is linear. 
For any scalar $c$, the relations
\[
\begin{split}
    \sp{x}{g(c⋅y)}
    &= φ(x, c⋅y) \\ 
    &= \cconj{c} ⋅ φ(x, y) \\
    &= \cconj{c}⋅\sp{x}{g(y)} \\ 
    &= \sp{x}{c⋅g(y)} \\
\end{split}
\]
hold. Thus, for any scalar $c$, the relation
\[
    g(c⋅y) = c⋅g(y)
\]
holds. 
Furthermore, the relations
\[
\begin{split}
    \sp{x}{g(y + z)}
    &= φ(x, y+z) \\ 
    &= φ(x, y) + φ(x, z) \\ 
    &= \sp{x}{g(y)} + \sp{x}{g(z)} \\ 
    &= \sp{x}{g(y) + g(z)} \\
\end{split}
\]
hold. Thus, the relation
\[
    g(y+z) = g(y) + g(z)
\]
holds. 
Thus, the function $g$ is indeed linear. 

Now, it is proved that the null set of the function $g$ is $\set{0}$. 
Let $v$ be a vector in the null set of the function $g$. 
Then, the relations
\[
    b⋅\abs{v}^2
    ≤ φ(v, v)
    ≤ \sp{v}{g(v)}
    = \sp{v}{0} = 0
\]
hold. 
Because the number is $b$ is positive, the size of the vector $v$ is $0$, that is, it is $0$. 
Thus, the null set of the function $g$ is indeed $\set{0}$. 

Now, it is proved that the function $g$ is surjective. 
The relations
\[
\begin{split}
    b⋅\abs{x}^2
    &≤ φ(x, x) \\
    &≤ \sp{x}{g(x)} \\
    &≤ \abs{x}⋅\abs{g(x)} \\
\end{split}
\]
hold. 
Thus, the relation
\[
    \abs{x} ≤ \frac{1}{b} ⋅ \abs{g(x)}
\]
holds. 
Thus, if a vector $v$ is in the null space of $g$, then the relations
\[
    \abs{v}
    ≤ \frac{1}{b} ⋅ \abs{g(v)} 
    = 0
\]
hold; that is, the size of the vector $v$ is $0$; that is, the vector $v$ is $0$. 

To prove that the image of $g$ is closed, it is first proved that $g$ is continuous. 
The relations
\[
\begin{split}
    \abs{g(x)}^2
    &= \sp{g(x)}{g(x)} \\
    &= φ(g(x), x) \\
    &≤ a ⋅ \abs{x} ⋅ \abs{g(x)} \\
\end{split}
\]
hold. 
Thus, the relation
\[
    \abs{g(x)} ≤ a ⋅ \abs{x} 
\]
holds, that is, the function $g$ is indeed continuous. 

Now, it is proved that the image of the function $g$ is closed.
Let $(v_i)_{i≥1}$ be a convergent sequence in the image of $g$. 
Because the relation
\[
    \abs{x} ≤ \frac{1}{b} ⋅ \abs{g(x)}    
\]
holds, the relation
\[
    \abs{g^{-1}(v_i) - g^{-1}(v_j)} ≤ \frac{1}{b} ⋅ \abs{v_i - v_j}
\]
holds for any indices $i$ and $j$. 
Thus, the sequence $(g^{-1}(v_i))_{i≥1}$ accumulates.
Because the domain of $g$ is complete, this sequence $(g^{-1}(v_i))_{i≥1}$ converges. 
Because the function $g$ is continuous, the limit of the sequence $(v_i)_{i≥1}$ is the value of $g$ at the limit of the sequence $(g^{-1}(v_i))_{i≥1}$. 
Thus, the limit of the sequence $(v_i)_{i≥1}$ is in the image of $g$, that is, the image of $g$ is indeed closed. 

Now, it is proved that the relation
\[
    \abs{u} ≤ \frac{1}{b} ⋅ \abs{f}_{\mathrm{op}}
\]
holds. 
The relations
\[
\begin{split}
    \abs{u}
    &≤ \frac{1}{b} ⋅ \abs{g(u)} \\
    &≤ \frac{1}{b} ⋅ \abs{w} \\
    &≤ \frac{1}{b} ⋅ \abs{f}_{\mathrm{op}} \\
\end{split}
\]
hold, where the last relation holds by the Representation Theorem. 
Thus, the relation 
\[
    \abs{u} ≤ \frac{1}{b} ⋅ \abs{f}_{\mathrm{op}}
\]
does indeed hold. 
\end{proof}


% Explanation of the second part of the theorem (the stability condition). 
