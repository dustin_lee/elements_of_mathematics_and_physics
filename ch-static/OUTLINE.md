# Outline of Static Differential Equations

## Introduction

**Remarks**: 
* Essentially explain *static*. Mention that dynamic equations are dealt with in another chapter. 
* Mention equations that are not dealt with in the next section. 
    - The Euler-Lagrange Equation.  
* Mention that the theory here is for linear equations, but Newton's method can be used for non-linear equations (assuming other things). 

**Examples**: 
* Shooting (finding the speed to launch an object to reach a given point).
* The Beam Equation. 
    - Explain how this is used to construct buildings. (This aspect of modern construction is essentially a great invention.)
* Poisson Equation.
    - From electricity and magnetism.
    - The steady state of the Diffusion Equation.
    - This often requires the condition that the value of the solution at ``infinity`` is $0$. In practice, a finite domain is used and the boundary is just set to $0$.


## Finite-Element Method

**Idea**:
- Explain that this method is called the Finite-Element Method. Reference the Finite-Difference and Finite-Volume Method. Explain that the Finite-Element Method works better for more complicated geometries than the Finite-Difference Method, but that it has a bigger start cost for implementation. 
- Look for best fitting solutions.  
    + Note that the basis functions needs to be sufficiently differentiable to have any hope of convergence. 
    + This is a minimization problem. 
- Using the $L^2$ norm allows trial functions to be used to create a system of linear equations. (This avoid any sort of minimization.)
    + Now, what this really is is a restriction of the operator to finite-dimensional subspaces. The function $\proj \circ f \circ \proj$ is now a finite-dimensional linear equation. 
        * How does this actually work with approximation though?             
- Can reduce the number of derivatives by looking at the weak formulation. 
    + Apply the Integration-by-Parts Formula to the variational formulation to get the weak formulation. 
    + This is because the $L^2$ norm is used. 
    + Give examples. 

**Theory**: 
* Existence.
    - Lemmas. (These should already have been proved in Generalizations.)
        + The (Riesz) Representation Theorem. 
            * Note that for Lax-Milgram, it is already written in an inner-product style. 
        + The Projection Theorem. 
    - Mention how the weak formulations satisfy the conditions for Lax-Milgram. 
    - Lax-Milgram. 
        + Don't have the definition of a sesquilinear form, but just denote it as a function. 
        + I need an explanation why its matters that the image is closed for there to exist an orthogonal complement. 
        + Explanation of the stability condition. 
        + Mention that this is this a generalization of the (Riesz) Representation Theorem? 
        + Is this intuition for the coercivity condition? Is it about injectivity? 
        + Talk about generalizations? 
* Convergence of basis approach.
    - Céa's lemma. 
        + It is not the smallest possible error. 
    - That the FEM bases do fill the space. 
    - FEM Crimes. 
* Regularity. 

**Implementation**:
* The basis functions.
    - Their derivatives have to be plentiful enough to approximate not just the function, but its derivatives too. 
    - They can approximate any function. 
* Store the basis functions by their coefficients. 
    - How to differentiate.
    - Horner's algorithm. (It doesn't need a theorem environment or anything.)
* Integration scheme. It is exact. 
    - Use Guassian integration. The higher order is wanted because products are taken. 
    - Should some nodes of the basis functions be the same as for the integration scheme? 
* Use a reference element. 
    - Store the scalar products of the basis functions. Then, use the Change-of-Variables Formula to scale them to get the actual scalar products quickly. 
* Triangularization? 
* Mention the Continuation Method for Newton's method. (Use finer and finer grids. This helps find initial guesses for Newton's method.)
