# Notes for Static Differential Equations

## New: Need to Understand

* Is the boundary done by just higher interpolation? 
* Is FEM just splines? 


## Need to Understand

* How do boundary conditions reduce to the Dirichlet condition? 
* Is the weak formulation even important for the theory? 
* When solving a static nonlinear PDE, should Newton's method be thought of as being applied to finite system of equation from the discretization or to the PDE directly? 
* I think that there can be multiple solutions to the Euler-Lagrange. How do I deal with this?
* How does the integral solution to Poisson's equation work? Like, obviously, if all the charge it known, it makes sense, but can it deal with boundary conditions? 
* Try to understand how the (Riesz) Representation Theorem is used to in the Lax-Milgram Theorem on the basis of the intuition of changing the content. 
* Nonlinear static equations often result in a system of polynomials. But, these are inherently unstable. So, how does Newton's Method help?


## Figures

* Grids: The idea is to show valid and invalid grids. Don't have labels of the elements. Maybe have something more complicated for the last one. 


## Material

* For the Lax-Milgram Theorem, the spaces can be different. 
* Should I mention the Shooting Method for static ODEs?
    - Maybe this is good for nonlinear equations. 
* Laplace's equation.
    - Get this from electrostatics. Although the distribution of charge is not known, the surface of a conductor has constant potential. The actual derivation is from getting the electric field from the potential and then applying Gauß's law to get that the divergence of this is 0. (I just copied this from my phone notes and I am quite sure what it means.)
    - Uniqueness proof: Assume there are two solutions. Subtracting them reduces the problem to proving that there is uniqueness when the boundary is $0$. Because the solutions are harmonic, the value at any point is the average over a closed sphere around it. (This is the Mean-Value Property. Probably don't make a lemma out of this, but, in the Introduction, definitely do.) Thus, any extrema are on the boundary, but the boundary is $0$. (More generally, the Maximum Principle states that a harmonic function on the boundary. Don't mention this name or anything, but I might want to look it up on Wikipedia when writing this out.)
* Matrices get big fast. For example, a 100×100×100 grid is already one million points. 
* Mention that the matrices are sparse, and that good methods for these will are explained in the third volume. 


## General

* Actually, should much of the implementation stuff be done in the Exercises? 
* Solutions to elliptic (stationary) partial differential equations are continuously differentiable. Physically, this makes sense. This is somewhat explained on \url{the Wikipedia page}{https://en.wikipedia.org/wiki/Elliptic_partial_differential_equation#Qualitative_behavior}.