# List of Great Inventions from Understanding Physics

<!--
This should be more like 50 inventions.
Do sensors/detectors/instruments count? 
Furthermore, do parts of an invention count? For example, diodes are used for many things, and relays are used for telegraphs. 
-->


**Classical Mechanics**: 
* Simple machines 
* Clocks 

**Fluids**: 
* Hydralic jack
* Airplanes 

**Thermodynamics**: 
* Thermometers 
* Internal-combustion engines
* Rockets
* Jets

**Electricity and Magnetism**: 
* Generator
* Electric motor
* Batteries
* Light bulbs
* Telegraph 
* Relay 
* Antenna

**Light**: 
* Telescopes and microscopes

**Quantum**:
* Transistors (computers) 
* Light-emitting diodes 
* Photovoltaic cells

**Nuclear**: 
* Nuclear reactors
* Nuclear bomb 