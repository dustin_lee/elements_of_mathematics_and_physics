<!-- LTeX: enabled=false -->
<!-- - cspell:disable - -->

# Elements To-Do List

> "Knowledge is a virtue." 

> There are two types of people in this world.   
Those who work to get money, and those who get money to work.   
I am the latter.   


## General 

* ❗Maybe I should just do equivalence classes in the Sets. These are needed throughout Numbers. I might as well just do them. 
    - Say that equinumerosity is an equivalence relation? 
    - Use in the Completion Theorem 
* Where do I introduce proof by induction? 
* Maybe there is something to intuitionism? The formal construction of numbers and products and whatever is quite gross. Should I talk about this in the Overview? Should I push some of this stuff back to the third part? (But, regardless, the goal now is bulk.)


## Snipes 

**Small**: 
* OV: Imaginary: Platonic ideals: A word
* SR: Double stars are evidence for the constiency of the speed of light. Have a figure? 
* OV: Did I talk about the progression of integration? Intervals -> Content spaces -> Completed -> Distributions
* OV: Spaces: 
    - Have with the stuff on central ideas. 
    - Just to simplify proofs
    - The central idea 
    - These are abstractions, not objects of interest. 
    - The spaces
        + Order
            - For numbers
            - For bases? 
        + Numbers: Groups / Rings / Fields
            - Not even used. But, why is it that this one is not used? They don't actually help with anything. 
        + Vector / Scalar-Product
            * Captures functions too
        + Metric
            * Captures functions too
        + Content
            * Difference domains / Probability space
* Sets: Sequences and indices
* ❗Integration: Notation and whatnot: Theorem on partition independence
    - Add indices to the partitions. 
    - Use `\big` for double indices
    - I even dropped the $f()$ in $f(a)$. 
* OV: Tests: Hand calculations:
    - Symbolic computing libraries are still used 
    - Is this just to verify the steps? 
* OV: Experiments cannot tell you everything! But computation can. 
    - Example: Heat on the surface of a rocket reentering the atmosphere. 
* OV: Power of symbolic solutions:
    - What is an example?
    - Graph to demonstrate easy understanding
    - Compare to numerical solution
* OV: Empirical convergence: 
    - Put this with order of convergence
    - Actually, I don't think that this says anything about what the limit is. 
    - But! It estimates the bound also, so there is strong evidence for what the error is. 
* OV: Libraries: 
    - FEM is an example
* OV: Gravity: As a falling wind. 
* OV: QM and Relativity are unintuitive. They are not what humans directly experience. 
* SR: Mention that frames are less than the speed of light
    - Estimate error bound when order of convergence is observed. 
* ODE: I need to explain what uniqueness for an ode even means. 
    - Well, actually, should this be in the overview for uniqueness of problems in general? 
    - *The solution to a differential equation and an initial condition is said to be \emph{unique}\index{unique} if*
* Regions: More on them 
    - Path connected? 
    - Open set with maybe some of its boundary
    - Define their dimension 
* Ov: Nukes: Power to blowup cities. 
* Manifesto: Extreme minimal level of comphrehension. 

**Medium**:
* Nums: How one would compute the square root of $2$. 
* Sets: Well-Ordering Principle 
    - I seem to be in a loop. 
    - It follows from Zorn's Lemma. 
    - This should be easier. 
* Theorem on open/closed subset of a subspace.
* Linear-Approximation Theorem: Justification of *best*? 
    - Should this be after the theorem? For any other linear approximation, it is at least as accurate on a small neighborhood. 
    - (This also helps to explain slope methods. See the comment in [Pdiff](Current: Partial Differentiation).) 
<!-- * Clairaut: Explicit inequalities -->
* Psums: Division: Get the formula from multiplication


**Overview**: 
* [ ] Math: Probability and Statistics
    - But, what is there to say? 


**Integration**: 
* Domain division: Piecewise integral functions are integrable
* Is linear
    - Move this into Concept outside of a theorem environment. 
    - Do I not need any of this for the FTC? 
* Don't I need time reversal before the FTC?  


**Slope**: 
* [x] Growth to increasing
* [ ] Better connection for the two theorem on growth and derivatives
    - Names? 
* [x] Second-Derivative Test
    - An example of it being inconclusive? 
* [ ] Explain how these tests are used to find extrema? 
* [ ] Multidimensional domains
    - [x] First-Derivative Test
    - [ ] Second-Derivative Test
        + Put on burner of multivariate Taylor's Theorem? Honestly, I should probably just do this already. 


**Auxiliary**: 
* Improvement: Uses graphics, unlike the Bourbaki. 
* Audit: Use *another* for *a different*
* Maximum of functions: Just say that the maximum of the function exists. 
* Switch from `\ldots` to `…`. 
* Audit: Remove *is called* and so on? Like, these are just extra words. The italicization already denotes that it is a definition. 
* It does not really make sense to put sets and pairs in Symbols. Also, I should add intervals, including with the symbol `∞`. 


**Issues**: 
* Sequences: I am not sure where to define sequences. Technically, they are need for the Schröder-Bernstein Theorem.  


**Figures**:
* ODE Example Visuals: 
    - Don't really explain these. Just have them. Don't even give a name for them.
    - I want a vector field for a one-dimensional differential equation
    - I want a vector field for a phase trajectory. I can do this for the pendulum. But, then, the reduction should be for pendulum and not the rocket. 
    - Normalize the vector fields but add color to indicate the magnitude


## Current: Sets

**Order**: 
* ~~Convert from old~~
* Add to summary 
    - Just a small generalization to reduce the number of proofs
        * Also put this in the section 
* Supremum and infimium
    - The *least upper bound* and the *greatest lower bound*. 
* This is THE order for the natural numbers. 
* Advanced Results? 
    - Zorn 


## Current: Numbers

**Main**: 
* ~~Summary~~ 
* Natural numbers: Define arithmetic (multplication)
    - Start with the Peano Axioms
    - Figure for multiplcation from addition
* ~~Advanced order laws for the real numbers~~ 
<!-- * Write/Convert: Complex Numbers / Polynomials 
    - Does division of polynomials matter now? Maybe I should wait until I do the Fundamental Theorem of Alegbra.  -->
* Write/Convert: Linear Equations / Coordinates
    - ❗I am not sure how to do this. 
    - I guess that this is really just the Elimination Method. 
    - ❗When do I introduce vector spaces? 
        + Put as Coordiantes in the first section of Geometry? 
            * Would I need to separate *Concepts* into *Distance* and *Angles*? 
            * Still put coordinates in Numbers? 
        + Can I avoid generalization until *Generalizations of Distance*? 
* Write: Exponentiation 
    - Monotone: 
        + Exponentiation
        + Logarithms
        + Be sure! 
    - Do logarithms make sense for numbers that are less than $1$? Should I just define the base to be more than $1$? 
    - The table method for computation. This applies to both logarithms and exponentation 

**Linear Equations**: 
* Go ahead and introduce matrix notation. 

**Need**: 
* Define *greatest common divisor* 

**Snipes**: 
* [ ] Decimal system 
* [ ] Quadrative Formula 
* [ ] Fundamental Theorem of Algebra
<!-- * [ ] Euclidean division for polynomials  -->


## Current: Distance and Angles

**Axioms**: 
* The SAS Axiom
* Lemma for Pons-Asinorum: Line segments can be bisected. 
* Lemma for SSS Theorem: The existence of the point $λ$. 

**Touches**:
* Lemma for the Sum-180 Theorem
    - Well, I am not really sure what the lemma and what the Parallel Postulate says. 
    - I think that I also need the Crossing-Lines Lemma. (I think that this gives the lemma.)
* Define other trigonometric functions. Which ones? Do these matter?  

**Language**: 
* How do I talk about congruence? 

**Similarity**: 
* Similarity and the Pythagorean Theorem (Work backwards)
* The axioms of euclidean geometry 
    - Should this be its own section? 


## Current: Length, Area and Volume 

**Issues**: 
* Combinding boxes: 
    - I need unions and intersections of boxes to be unions of boxes. 
    - For this, I need to have the boxes all be oriented in the same direction. 

**Summary**:
* It is a geometric quantification of the size of a set. 
* The word *content* is a collective term for *length*, *area* and *volume*. 

**Content**: 
* [x] Definition 
    - Figure for *outer content* and *inner content*. 
    - Equivalent definintions: 
        + [x] First
        + [ ] Second
* [ ] Basic properties
    - The content of the empty set?
    - [x] Disjoint unions
    - [ ] Intersections 
        + 
    - [ ] Subtractions (Complements) 
    - [ ] Combined formula for overlapping unions
    - [ ] Scaling Theorem 
    - Equality with infinite unions if the set is measurable? 
* [ ] Example of Triagles
    - Parallelograms are reduced to triangles
* [ ] End with a discussion of a more basic definition? 

**Paths**: 
* Definition 
* Archimedes's Theorem 
    - Figure 

**Surfaces**:
* Definition 
* Orientation 
* Latern Paradox

**Content Spaces**: 
* [ ] Introduction
    - Unification / reduction of proofs 
    - Used for integration 
    - Used for probability theory 
    - Used for different measures on euclidean spaces
        + Probability theory 
        + Spectral measure 
* [ ] Edit definition 
* [ ] Define topological content spaces



## Current: Content and Integration: Integration on Content Spaces

**Issues**: 
* Actually, does the sampling approach still work? 
    - Well, it would at least have to be for topological content spaces so that a diamter can be assigned to the sets. 
    - Well, do I care about any content spaces that are not topological? Yes, probability spaces. 
    - Integration schemes still become this sampling approach.  
* How do I go about saying the the results for integration still hold? 

**To-Do**: 
* [x] Look at outline 
* [x] Look at notes
* [x] Introduction
* [ ] Theorem: Darboux integration 
* [x] Define 
* [ ] 
* [ ] Formula for path length: Edit 

**Need**: 
* What about just defining *orientation* to be the unit vector field? Yes, define it for the surface or path. I could denoted it by $o$ or is this bad notation?  
    - Define *inward* and *outward*. 
* Some sort of figure of majorization and minorization. (With the intuition before the first theorem.)
* Summary: Integration on any content space, not just on compact intervals. 
* Say that the integral of a positive function is the area under the graph. 
* Explain how this means that integration can be done on open domains and unconnected domains. 
* Regions
    - Surfaces are defined and closed 
        + Closed surfaces

**Snipes**: 
* Fubini's Theorem 


## Current: Content and Integration: Flow

**Issues**: 
* I want a better name than the *Fundamental Theorem of Calculus*. It should be descriptive. 
    - What about just the *Decomposition Theorem*? 
* Can I define the advection and diffusive terms better? Does it really make senses to define diffusion as the gradient? 

**Need**: 
* Properties of divergence and rotation: Linearaity and Rroduct Rule. 
* Circulation is for advective currents.  
* Explain the the vector field view does not require time. That is, intuitively, it is entirely a snapshot of time. The density view requires time for time dervatives. 

**Figures**: 
* Flux of a vector field: Fluid going through a surface:
    - I want the to see the volume of the fluid after a period of time
    - Maybe slant have two figures, where one is slanted to show why the scalar product is used. 
* Derivation of the equation for divergence

**Snipes**: 
* Gradient Theorem
* Divergence Theorem 
* Rotation Theorem 


## Current: Probability and Statistics 

* [ ] Introduction 
    - Probability space
    - Probability density 
* [x] Summary Statistics 
* [ ] Confidence
* [ ] Estimation 
    - [ ] Explanation of what other people call identically distributed 
    - [ ] Law of Large Numbers
        + [ ] Introduction 
        + [x] Statement
        + [ ] Proof
            * Normally, this is proved with Chebychev (standard deviations), but can it be proved with Markov (mean absolute deviations)? 
    - [ ] Explanation around the Central Limit Theorem
    - [ ] Statement of the Central Limit Theorem 
    - [ ] The normal distribution
        + [ ] Definition 
        + [ ] Entropy and that the normal distribution maximizes it
    - [ ] The Berry-Essen Theorem 

**Need**: 
* Markov and Chebyshev
    - They are stated in a different way. 
* Add sampled summary statistics to notation. Can I think of better abbreivations? 
* 

**Other**: 
* Should I just drop mode? 
    - Like, from a sampling perspective, does it mean anything? 


## Current: Thermodynamics / Temperature and Thermal Energy 

**NtD**: 
* Free energy is $0$ in equilibrium. Does this make it less general than entropy? 

**Issues**: 
* Thermal conductivity: 
    - I don't undertand why the meters are not square in the unit for thermal conductivity. 
    - I don't understand why the material of the hot and cold resevoirs do not matter. Well, I should define that the surface is kept at these temperatures. 
    - I guess that it has to be in equilibrium. 
    - Others don't seem to emphasize the cross sectional area. 
* The word *current*:
    - Really, *current* should be the density of *flow*/*flux*, that is, there should only be *current* and not *current density*.
    - But, in electromagnetism, this would be confusing because people like to use *current* instead of *flow. 
    - The *current* is the *vector* such that the infitesimal flux of it as a point is the time derivative. 
    - ❗And, the *flux* is the time derivative of the mass contained by the surface.
    - Well, I could use *flow velocity*. But, how is this denoted? Actually, I think that flow velocity drops the density. And, I don't think that *velocity* makes sense for all currents. 
    - ❗Actually, people just use *flux density*. 
    - Is this from the Continuity Equation? (But, this does not explain the rate.)
    - I think that a description of *flow velocity*/*current* provides a total description. 
    - Regardless, what is the formal definition of current? 
        + The vector such that? 


**To-Do**: 
* Derivation/justificaiton of heat equation 
    - I still need Fourier's Law
* Combined-Gas Law
    - ❗How do I first state it without reference to absolute zero? 
    - Justification of absolute zero. 
        + Atoms: Not movement
        + Modern: Experiments have gotten obtained within $0.001$ degrees or whatever from absolute zero, but this cannot be passed. 
    - What about *heat capacity ratio*/*adiabatic ratio*?
        + Should this already be part of Boyle's Law? 
    - Energy change of an ideal gas
* Heat engines and refigerators
    - Examples
* Edit Carnot's Theorem and its proof: 
    - Explain reversibility 
    - Lemma: Reversible engine have maximal efficiency. 
    - Computation 
* Processes:
    - Enthalphy 
    - Latent heat 
* Summary: 
    - In this chapter, these laws are empirical, but it is proved statistically in Chapter~\ref{ch:atm}

**Small**: 
* Add an exmaple of the scaling of the equivlance of thermal energy and mechanical energy. 

**Other**: 
* Free energy: 
    - This is equivlaent to the Law of Entropy 
    - Can Carnot's Theorem be proved more directly? 

<!-- 
**Issues**: 
* Do I need absolute zero for Carnot's theorem? How does absolute zero come in? Through the Ideal-Gas Law? 
* Can I introduce the Law of Conservation of Energy without heat engines? 
    - I could just mention them.
* I want to put the Law of Entropy in the first section. What is some motivation for it other Carnot's Theorem? 

**Things**: 
* [ ] Introduction 
    - The Heat Equation: Derivation  
* [ ] Heat Engines and Free
    - Define heat engines
        + Define the thermal resevoirs 
        + Carnot: Doesn't matter what the mechanism is. The engine could gnerate electricity. 
    - Edit Carnot's Theorem and its proof
    - Define free energy
        + Restate the Law of Entropy in terms of free energy. 
. 

- What is an example of free energy restricting a reaction? 
- Have examples. 
    + When hydrogen burns, the volume decreases. Thus, the water is more excited from this work.
- Examples of endothermic reactions:
    + Melting
    + Cold packs
    + Disassociation reactions

**Other**:
* Phase transitions: 
    - Energy for passing phrases (latent heat)
        + This is why the freezing and boiling point of water is used. 
* Should I mention other ways that thermal energy can be converted into work? Specificially, by making electricity? 
    - Thermoelectric Effect (Seeback Effect)
    - Thermionic Emission (Ejecting electrons)
    - Pyroelectric Effect
* This source temperature increase seems to be inexhaustible. 
-->

## Current: Special Relativity 

**Get Going**: 
* [ ] Update notes and outline
* [ ] Summary 
* [ ] Mechanics
* [ ] Electricity and Magnetism 
* [ ] $E=mc^2$
    - Is this needed for mechanics or electromagnetism? 
    - [ ] Rigorize: 
        + I did not check the formulas. 
        + I did not check the energy of the decay of uranium (or even the reaction). 
    - [ ] Set up and end material


## Current: Atoms 

**General**: 
* [ ] Redo outline
* [ ] Edit notes
* [ ] Chapter summary 

**Introduction**: 
* [x] Basics
    - Talk about different arrangements? 
* [x] Chemistry 
* [ ] More Evidence? Brownian motion

**Small**: 
* Is brownian motion useful? 


## Current: Subatomic Particles

**Remaining**: 
* Other Particles
    - Positrons
    - Neutrinos
        + Weak interaction 
    - Other beta-decay-like phenomena 

**Need**: 
* All these particles are identical! Where does this go? 
    - All electrons have the exact same charge and mass. 
* Alpha decay is just so much more likely than other decays into a different nucleus.

**Hold**: 
* [ ] Other Subatomic Particles: The particles of the Standard Model
* [ ] Detection and Accelerations


## Style Decisions

**Add**: 
<!-- * Use: "This is proved by a diagonalization argument".  -->
* Use *where subscripts denote components*
    - Should I have a list of *hacks* in phrasing? 
* Maximum of functions: Just say that the maximum of the function exists. 
    - Still use *obtain* or *attain* in the intuitive statement. (Which?)
* Use *another* for *a different*

**Need to Decide**: 
* Go normal: 
    - Maybe I should just use *less than or equal to* annd *if and only if*. How many words does this actually save? 
    - What about even using *greater than*? 
    - Just use *curl* instead of *rotation*. 
* Use *the* two-dimensional euclidean space. 
    - What do I called the three-dimensional euclidean space? 
        + The *euclidean solid space*? 
        + *Physical space*? 
* I should either use *euclidean-valued* or not use *real-valued*. 
* Use either *whole* or *entire*. I would like another word 
* Maybe I should not capitalize the names of functions
* Well, the scalar product being denoted by `•` is not susceptible to denoting the name of the space. (The euclidean scalar product is needed for the square-integrable scalar product, so they need to be distinguished.) Maybe I should keep `•` just for the euclidean scalar product.
* Should I use *unit point-mass function* instead of *unit impulse function*? Probably not, but do use the term *point mass* to describe it. Actually, maybe this is better. I could use *ump* or *umpf* to denoted it. 
* Standard basis: 
    - Use `comp` for components. 
    - Is there better notation for the standard basis? 
    - Is this sloppy?  
* I clearly need something other than *proof* for physics. I think that I should use *show* and *indicate*, where the former is stronger. The word *demonstrate* is longer, less used and latin isntead of germanic. 
* Maybe I should use *proper interval* instead of *nonsingular interval*. 
* Should I use *by definition*? 
* I do need a word for Hilbert spaces. Besides weakly differentiable functions, there are also distributions. Maybe *complete inner-product spaces* is the best choice. The only flaw is that it doesn't emphasize the function part. But, actually, it makes the connection to euclidean spaces, which is the goal. 
* *used to* -> *used for*? Actually, *used to* is more common. Plus, it is shorter. 
* If I am using *inertia* instead of *inertial mass*, then I should have something for *gravitational mass* without the word *mass*. 
* Is *value* to *number*, like *point* to *coordinate*? Or just use *output*. 


## Tex

* Up Greek: I want the `π` vertical. 
* The tombstones seem to be a little off. Some seem to not be flushed. 
    - Should I just vary the AMS proof environment? 
* Remove the commas after the terms in the index.
* Fix spacing around multiplication 
* Make the dot product look better. I want it to be the same size as ∘. 
* It appears that sometimes the indices under the integral overlap with the absolute value
* Integration: Figure out the deliminator issue. Just make bigger symbols? I think that this is for the integral of a matrix in ODEs. 
* Have audit have a rigorous mode
* Get `\emptyset` to actually be `\varnothing`. (But, like, do I even use this symbol?)
* Reduce latex's urge to flush the bottom. Or, does this have to be dealt with manually? 
* Have more exact numbers: 
    - Spacing around figures:
        + I want one paragraph break before and after a figure. 
        + I want a fixed spacing between the caption and the figure.
        + `\belowcaptionskip`
    - `\intertext`
* I would like to mostly flush the text to the bottom, but I want to limit the paragraph breaks to at most 3 times the minimum break. 
* LTeX: 
    - Eventually, have a custom dictionary, which will replace most of audit
        + Can I save this dictionary and as a text file in the project and have LTeX use it for only this project? 
    - Hidding problems: 
        + I would like to use magic comments because this is more explicit
        + If I cannot do this, then I would like a list of hidden problems as a file in the project.  
* Can I make latex more quiet? I would like the first pass to only display errors (and the small backtrace). And, I would like the last pass to display any failed references. 
* Eventually, consider writing my own class file. 


## Other

* Use `!!` for comments that I want to come back to. 