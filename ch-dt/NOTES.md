# Notes for Time Stepping for Dynamic Differential Equations

## Outline

* Multistep Schemes: 
    - Motivation: These require less evaluations than multistage schemes, and, for some problems, the evaluation is the most expensive part. 
    - There are not necessarily zero-stable like one-step schemes. because the error from previous steps can accumulate. 
    - They require one-step methods to start off. 
* Implicit Schemes: 
    - Motivation: 
        + Explicit methods may require an unreasonably small step size
        + Normally, much slower per step, but may require many less steps
        + Stiff equations
            * Are these common in fluid dynamics? Are these common for reaction equations with multiple molecular species? 
    - Region of stability, and that these are entirely stable
        + I guess that this is defined as the step sizes that give the convergence of the order, somehow



## Material 

* Adaptive stepping?
* Chord finders?
    - How does Geant4 have a better stepper for helical trajectories?
* Sometimes, different time stepping approaches to FEM matter. 