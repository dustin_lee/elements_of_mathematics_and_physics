# Outline of Discretization of the Nuclear Interaction and the Weak Interaction

## The Nuclear Interaction

* Notes. 
	- Should the drop model be introduced here?
	- The pion and its relation to the strong nuclear force.
	- It is an exchange force. (Should this be more general?)
		* Nucleones don't get infinitely close in the nucleus. The strong force must be repulsive at very close distances. This is has been confirmed by experiment. (I guess that the solution to this is that the particles actually exchange location.)
	- The reason that the calculations are much hard is because the gluon coupling factor is much larger. Thus, more terms need to be taken.
	* Quarks go somewhere in here.
		* Reasons to suspect that protons and nuetrons are composite particles.
			- Supposedly, the their unusual g-factors suggests that it is made of smaller particles.
			- They cannot be described by a similar propogation term to what was used for electrons and photons.
			- Particles have been shot at them (similarly to the Gold-Foil Experiment), and sometimes they pass through.
		* The gluon field increases with distance. This is why quarks cannot exist in isolation.
		* I think that the pion theory for the nuclear force is not quite satisfactory, which motivates quarks.


## The Weak Interaction

* Notes. 
	- This force explain beta decay.
	-  The weak force can be motivated by the failure to observe nuclei that consists of significantly different amount of protons and neutrons? (These nuclei must be unstable.)
	- The mediating particles are W bosons and the Z bosons.
	- Violation of parity for the weak force.
		* I think that this is demonstrated by seeing that the spins of electrons in beta decay (of cobalt-60) tend towards one direction.
