# Notes for the Discretization of the Nuclear Interaction and the Weak Interaction

## Need to Understand

* Parity. 
	- The parity of an observable is even if it is the same under an inversion, and its parity is odd if it becomes its negative. 
		* Even and odd are the only two options because two flips return it. 
		* For example of an odd parity, momentum switches sign. 
		* For example of an even parity, the energy and mass do not switch signs. 
		* As a rule, the parity of vectors are odd, and the parity of scalars are even.
			- Angular momentum and the magnetic field are exceptions. 
	- What is the parity of a system? 
		* Supposedly the reason to suspect that the weak interaction violated parity was because the parity of the system changed after a decay. 
		* What is the parity of a particle? (Is it related to its representation as a field?)
		* It just might be that the "irreducible representations" are each even or odd. 
* How is the electroweak theory better? How does the Higgs boson matter? 


## Material 

* Shortened: I don't actually care about QFT, but I do care about the known phenomena: 
	- Fermi model of beta decay 
	- Spin of composite particles
	- Parity 
	- Why does energy change drastically between an even and an odd number of nucleons? 
	- What does the Higgs boson mean? 
* Nuclear bonds depend on agreement of spins. This is why composite particles can have a fixed spin. For example, the spin of a helium nucleus is $0$.
* Supposedly, there a a modern twistor technique (by Penrose) that is more effective than perturbation in the sense that it is better for the nuclear and weak interactions. 


## General

* Quarks are kind of nonsense: 
	- Like, they are particles that cannot be separated because the force grows with distance
    - Have quarks made any previously unconceived predictions? 
* Maybe don't really have QFT, but keep it a bit more classical. First, QFT is wrong. Second, it has not been used in engineering applications. 
* The strong interaction is between quarks. The weak interaction is between quarks and leptons. 
* I guess that the original motivation should be to predict the rates and energies of nuclear reactions. 
* Dirac does not predict the Lamb shift. 
* Other properties – such as quark color – can be thought of as nongeometric polarizations.
* Why are neutrons stable in a nucleus? (They aren't outside of a nucleus.)
* Parity violation for the weak interaction is from having only a left-handed neutrino field. 
* Mirror nuclei help show that the nuclear behaviors of neutrons and protons are similar. 