# Notes

## General

* Submission: Include `*.gnuplot` and `*.table` in the submissions. 


## Format

* I want to use the package `hyerref`, but it is not working with my fonts. 


## Writing

* Be explicit and mention stuff.
    - List the derivatives of trigonometric functions.
    - List power series for common functions.
* Put any header for a theorem (such as its name) in the main file, not in the file that has the theorem and proof. 


## Organization

* Currently, in the third part, I put stuff on linear algebra before stuff on differentiation equations. I feel that this makes more sense. But, the finite-difference-style approach to Laplace's equation is what motivates sparse methods. 


## Undecided and Unsorted Material

* For physical constants in Notation, should I give the best known values with the standard deviation? 
* Should I combine the chapters *Momentum and Force* and *Work and Energy* into one chapter that is called *Mechanics*? 
* Should I call the Overview the Introduction? 
* Where do I talk about rockets and jet engines? There are great inventions. Because they are not heat engines, they do not seem to fit naturally in Temperature and Thermal Energy. 
* Do inverse problems matter enough? 
    - And, are basic optimization methods about as good as it gets? 
    - Obstacles: 
        + Noise
        + Does the method converge? 
        + How many solutions are there? 
* It would be nice to have an example of unrelated phenomena that have the same mathematical theory. I guess that this is philosophically meaningful. I would briefly mention this when the second phenomenon is introduced.  
* Maybe the chapter of nuclear phenomena should just not really have anything. Maybe it shouldn't exist. Well, at least, it probably shouldn't exist in the first version. 
    - What would a theory of nuclear phenomena say? Just describe fusion and decay? Does the motion of these particles matter much? 
* Should there be a chapter on the FFT and fast convolution in the third part? 
    - Convolution: 
        + The [Convolution Theorem](https://en.wikipedia.org/wiki/Convolution_theorem) says that the Fourier transform of a convolution is the product of the Fourier transforms of the functions, that is, $F(f*g) = F(f)*F(g)$. Thus, for many convolutions, this is much faster. 
        + When do I actually want to do many convolutions? (There is the central-limit-theorem-style thinking, but its actual use is much different.)
* Naiver-Stokes. That result about a property of a scheme implying convergence (or something like this). (It is not a guarantee type of thing.)
* How and where should open quantum systems be explained? Should it be its own chapter in the third part? 
* Should there be a chapter that is called Deformation?
* Should I talk about quantum confinement? 
* Should I use Type Theory instead of Set Theory? 
* Should I include Green's function? (Also, I should probably call it something better, such as *kernel operator* or *kernel solution*.)
* Should there be a chapter on general QFT? This could explain things in a more general setting. 
    - I want some certain things: 
        + The parity of the weak interaction. 
* Include a chapter of Shock. 
    - Put this in the third part
    - Cherenkov radiation?
* I want some cool chemistry examples:
	- A battery – a lead-acid battery.
	- A thermal explosion – perhaps thermite. 
		* Mention detonation. Use nitroglycerin as a brief example. I don't need to have too many details. Mention how strong nitroglycerin is and how it is used in dynamite. -->
* CEM: BEM: Idea: Still use the high-order built-in integration scheme for weighing the integrand. The Fast multi-pole method is then not using this, but a lower-order scheme. 


## Cover Graphic

* Just have inscriptions on the cover
* Maybe have multiple things:
    - Pythagorean Theorem 
        + The similarity proof with equations next to it (which can be done with $a$, $b$ and so on instead of $|p-q|$ and whatnot). 
    - A Feynman diagram 
    - Some important PDE? 


## Advertisement Options

* Wikipedia: Make a page for it. 
* Wikipedia: Reference it extensively.
* Reference it on various StackExchange posts that ask for motivation for references.
* Mail books to professors with letters asking them to use it for their course. 