# Notes for Dynamic Partial Differential Equations

## Concerns

* Should I be deriving differential equations? 


## Need to Understand

* Restrictions on schemes
    - Stability.
        * I think that this means that the solution cannot grow. For example, in the Diffusion Equation, a hotter node should not be allowed to heat a colder node to a higher temperature than the hotter node is at.
    - Propagation.
        * Propagation needs to be allowed to happen at a fast enough rate. For example, I think that the speed must be less than $\frac{Δx}{Δt}$, which is the max speed that the scheme allows?  Causality: Information can only move as much as on spatial step per time step. (Thus, there needs to be enough spatial distances and size of the time steps so that the propagation can modeled correctly.)
        * The signal can propagate can only happen one step at a time.
        * Dispersion relations? There needs to be enough to resolve the wave.
* Wave speed.
    - Phase velocity (of a harmonic at least) is the speed of the constant phase?
    - Is the speed of the medium the group speed?
* Why the open boundary condition for the Wave Equation is $∂_x = 0$.
* Absorbing boundary conditions. 
* Adaptive schemes.
  

## Material

* Empirical verification. 
    - Showing that the solver has the correct convergence rate.
    - Show that the solver gives the exact solution for a simple situation.
        * For FDM, show that the solution to the difference equations is exact.
        * For FEM, show that the solution is exact for a lower-order polynomial.
* Mixed Equations. (Use the Advection-Diffusion Equation and the Reaction-Diffusion Equation.)


## Notation

* Use $φ(x_i, t_j)$ for the actual solution and $φ_n(x_i, t_j)$ for an approximate solution.


## General

* Normally the step size is taken to be as big as possible given the restrictions for on it for stability and convergence. One wants to get through faster.
* Intuition: What are enough restraints to specify what happens. There is the differential equation, the initial condition and the boundary conditions. 
* I think that Céa's lemma is still useful It says that the basis can sufficiently approximate the solution. 