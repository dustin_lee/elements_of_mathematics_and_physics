# Outline of Partial Differential Equations

## Introduction 

* Essentially explain *dynamic*. Mention that dynamic ODEs were dealt with already. 
* Mention equations that are not dealt with in the next section. 
    - Navier-Stokes. 
    - Schrödinger Equation. 
    <!-- - Transport equations (Boltzmann, NEGF). -->


## Some Equations

- Heat Equation.
- Wave Equation.
    * Fixed boundary points.
    * Periodic boundary conditions. 
    * Open boundary condition.
    * Reflecting boundary conditions. 
    * The uniqueness of solutions explains how waves are reflected from the boundary.Think of wave pulse moving towards the boundary, and a ghost wave pulse coming from the other side. For a fixed point, to maintain this restriction at this point, the ghost wave pulse must be the reverse of the original wave pulse. Thus, the wave switches sides with the same amplitude. For an open boundary, the ghost wave pulse must be the same as the original pulse. Thus, the wave doesn't flip when reflected and the amplitude reaches double the original.


## Concept of Time-Stepping with the Finite-Element Method

* Just step and project. 
    - At least for linear equations, because the derivatives of the basis functions just lowers their order, the projections are exact.


## Theory 

* In what space solutions are looked for. 
* Stability/convergence. 
* Regularity. 

