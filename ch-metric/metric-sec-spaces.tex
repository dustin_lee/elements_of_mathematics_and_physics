%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Definition of Metric Spaces
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

A \emph{metric space}\index{metric space} is a set together with a function $\dist(x,y)$\label{not:dist:metric} from its pairs to the nonnegative real numbers such that
the relation 
\[
    \dist(p, q) = \dist(q, p)
\]
holds for any two points $p$ and $q$, 
the relation 
\[ 
    \dist(p, q) = 0
\]
holds exactly if the points $p$ and $q$ are equal, and
the relation 
\[
    \dist(p, q) ≤ \dist(p, r) + \dist(r, q)
\]
hold for any three points $p$, $q$ and $r$. 
The number $\dist(p, q)$ is called the \emph{distance}\index{distance} between the points $p$ and $q$. 
And, the function $\dist(x, y)$ is called the \emph{metric}\index{metric}. 
The relation 
\[
    \dist(p, q) ≤ \dist(p, r) + \dist(r, q)
\]
is called the \emph{Triangle Inequality}\index{Triangle Inequality}.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Definition of Normed Spaces
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

A \emph{normed space}\index{normed space} is a linear space together with a function $\abs{x}$\label{not:size:vec} from its vectors to the nonnegative real numbers such that the relation
\[
    \abs{v} ≥ 0 
\]
for any vector, 
the relation 
\[
    \abs{v} = 0
\]
holds exactly if the vector $v$ is $0$, 
the relation 
\[
    \abs{a⋅v} = \abs{a}⋅\abs{v}
\]
holds for any vector $v$ and scalar $a$, 
and the relation 
\[
    \abs{v + w} ≤ \abs{v} + \abs{w}
\]
holds for any two vectors $v$ and $w$.
The relation 
\[
    \abs{v + w} ≤ \abs{v} + \abs{w}
\]
is called the \emph{Triangle Inequality}\index{Triangle Inequality}.
A normed space is a metric space where the distance between two vectors $v$ and $w$ is the number
\[
    \abs{w - v}.
\]

The \emph{uniform norm}\index{uniform norm} on a bounded function is the function 
\[
    \sup_{x \in \dom{u}} \abs{u}
    .
\]
And, the space of bounded functions on a set with the uniform norm is a normed space. 
In Chapter~\ref{ch:mt}, the square-integration norm, that is, the norm 
\[
    \left( ∫ \abs{u}^2 \right)^{\frac{1}{2}}
\]
is used for functions. 


\scenebreak 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Theorems 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Most of the results and their proofs from Chapter~\ref{ch:lim} still hold for general metric spaces and normed spaces. 
And, the definitions are essentially the same. 
% For example, the definitions of open sets, closed sets, limits and continuity are the same. 
If a result for euclidean spaces involved the linear properties of the space, then it generalizes to normed spaces. 
And, if a result did not involve the linear properties of the space, then it generalizes to metric spaces. 
The results for the real numbers do not generalize to these settings. 
But, the results for the real numbers that do not involve the order properties of the real numbers generalize to complex numbers.

Some results in this chapter require accumulative sequences to converge. 
This is not so in all metric spaces.
A metric space is said to be \emph{complete}\index{complete} if, every accumulative sequence converges. 
The Cauchy Convergence Theorem in Section~\ref{lim:sec:limit-methods} says that euclidean spaces are complete. 

But, the Bolzano-Weierstraß Theorem is an exception to such generalizations because it does not quite hold in complete metric spaces. 
It also requires for boundedness to be replaced with total boundedness. 
A metric space is said to be \emph{totally bounded}\index{totally bounded} if, for any positive real number $ε$, there exists a finite number of balls whose radii are each $ε$ such that their union is the metric space. 
A subset of a euclidean space is bounded exactly if it is totally bounded.

Furthermore, the Norm-Equivalence Theorem in Chapter~\ref{ch:lim} is specifically for euclidean spaces. 
A general version is in Section~\ref{metric:sec:norms}.


\scenebreak 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Metric Specific
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

A subset of a metric space is also a metric space where the metric is restricted to this subset. 

\input{metric-thm-subspace}


\begin{figure}[H]
\centering
    \includegraphics[width=0.3\textwidth]{metric-fig-subspace}
    \caption{Open subset of a subspace.}
\end{figure}