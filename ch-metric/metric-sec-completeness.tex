The completeness of euclidean spaces is used to prove various results, such as the Absolute-Convergence Test in Chapter~\ref{ch:sums} and the existence of dynamic ordinary differential equations in Chapter~\ref{ch:ode}. 
Completeness of metric spaces is an abstraction of this property. 
Furthermore, spaces are extended to have this property so that it can be used in proofs. 
For example, in Chapter~\ref{ch:mt}, function spaces are extended to so that they have this property.
This is similar to how the real numbers are defined to be the ordinal completion of the rational numbers and how the complex numbers are defined to be the algebraic completion of the real numbers. 

The next theorem is an alternative definition of completeness. 
It is called the \emph{Nesting Theorem}\index{Nesting Theorem}. 
Intuitively, it says that complete spaces do not have holes. 

\input{metric-thm-nesting}


\begin{figure}[H]
\centering
    \includegraphics[width=0.5\linewidth]{metric-fig-nesting-thm}
    \caption{
        The Nesting Theorem. The intersection of a shrinking sequence of nested closed sets is nonempty.
    }
\end{figure}


In the last theorem, the point in the intersection is unique because, if two points are in a set, then the diameter of this set is at least the distance between these two points. 

A closed set of a complete space is complete because any accumulative sequence converges in the ambient space and its limit is in this subset because it is closed. 
The next theorem says that a complete space is intrinsically closed. 
Thus, intuitively, points cannot be filled in without changing its structure.

\input{metric-thm-complete-is-closed}


In Chapter~\ref{ch:nums}, the real numbers are defined as the ordinal completion of the rational numbers, and the complex numbers were defined as the algebraic completion of the real numbers. 
In both cases, it is proved that these completions are minimal, that is, additional elements are not included. 
This means that there is not an alternative completion that may be useful is some way. 
The next theorem says that there exists a unique minimal completion of a metric space. 
This completion is called the \emph{topological completion}\index{topological completion} or \emph{completion}\index{completion} of the space.

A subset of a metric space is said to be \emph{dense}\index{dense} if its closure is the whole space. 
And, two metric spaces $X$ and $Y$ are said to be \emph{isometric}\index{isometric} if there exists a  bijective function from $X$ to $Y$ such that the relation 
\[
    \dist_X(p, q) = \dist_Y(f(p), f(q))
\]
holds for any two points $p$ and $q$. 
Such a function $f$ is called an \emph{isometry}\index{isometry}.
An isometry and its inverse are both continuous. 
Intuitively, Two metric spaces are isometric if they are the same topologically. 

\input{metric-thm-completion}


If a space is not complete, then it is not closed in its completion. 
Thus, by the last two theorems, a space is complete exactly if it is closed in any superspace. 
Also, by the last theorem, the real numbers are the topological completion of the rational numbers. 


% The next theorem says that 

\input{metric-thm-uniform-spaces-are-complete}


The next theorem is called \emph{Baire's Theorem}\index{Baire's Theorem}. 
It is used to prove the Principle of Uniform Boundedness in Chapter~\ref{ch:la}. 

\input{metric-thm-baire}


For an example, let $(A_i)_{i≥1}$ be a sequence of finite subsets of the euclidean plane. 
Then, the sequence 
\[
    (\mathbb{R}^2 - A_i)_{i≥1}
\]
is a sequence of open dense subsets of the euclidean plane. 
The intersection 
\[
    \bigcap_{i≥1} \left( \mathbb{R}^2 - A_i \right) 
\]
is also dense because it is still the euclidean plane with countably many points removed. 
Intuitively, for each index $i$, the set 
\[
    \mathbb{R}^2 - A_i
\]
is topologically almost the whole space in the sense that magnifying the difference countably many times is still dense.