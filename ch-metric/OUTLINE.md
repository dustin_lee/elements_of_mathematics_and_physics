# Outline of Generalizations of Distance






## Old

<!-- Should I just restate the theorems?  -->

**Metric spaces**
* Definition of metric spaces
* Definition of linear metric spaces
* Same definitions
* Same theorems:
    <!-- - Limits of Sequences:
        + Unique limits
        + Completeness
            * Accumulative sequences in complete spaces converge
    - Limits of Functions: 
    - Continuity: 
        + Uniform limits preserve continuity 
    - Compactness: 
        + Weierstraß: Total boundedness
        + Continuous functions are bounded 
        + Maximum principle 
        + Continuous function are uniformly continuous  -->

**Norms**: 
* Equivalence of Norms

**Completeness**: 
* Give some examples of completions:
    - Reals are the completion of the rational numbers
    - The space of integrable functions is completed in Chapter~\ref{ch:mt}
* Complete spaces are intrinsically closed
* Completeness Theorem
* Uniform spaces are complete