# Notes for Generalizations of Distance

## Need to Understand

* Norm-Equivalence Theorem: 
    - Dropping the condition: Can I? It seem like just extra. 
    - Generalization: Make it more general? I am also interested in factors of the norms on each space. Really, it should be that the restriction of the norm is equivalent on each factor. 


## Figures

* Subspace: 
    - Use shades to indicate how the open set is extended out. 
* Baire: 
    - I think that it is important to have $p$ outside the nested sets. Draw a ball around it and denote its radius by $ε$. Also, mark $q$ as the final point. 


## Vocabulary

* Should I use *Category Theorem* or *Baire's Theorem*? The first sucks because this mentions the concept of category. The second sucks because it is not descriptive. What about the *Meager-Union Theorem*? 


## Material

* ❗Should I just list all the generalized theorems? Would this be the easier way? 
* Norms of linear functions are just another function norm. This one is interesting because the functions can be infinitely far apart in the sense of the uniform norm and the square-integrable norm. (This one is not related like these other norms are to each other.)
* Euclidean spaces are the archetypical metric space. 
* Mention manifolds as another example of metric spaces. 
* Don't introduce the Heine-Borel Theorem for general metric spaces until I have motivation to do so. 
    - To include with it: 
        + Concept of totally bounded. 
        + A subset of a euclidean space is totally bounded exactly if it is bounded 


## General 

* What is distance (in physical space) other than the shortest path? I guess that the idea of a metric is actually a bit abstract. 
* Another way to say Baire's Theorem is that a union of nowhere dense subsets is nowhere dense. Is this better? 