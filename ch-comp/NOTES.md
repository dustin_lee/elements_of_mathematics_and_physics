# Notes for Computability and Provability 


**Insolvability of the Quintic Polynomial**: Abel-Ruffini Theorem: 
* Idea: A solution by radical -> Ascending sequence of radical extensions -> Sequence of normal extensions -> Just find a polynomial whose automorphism group (such as $S_n$) is unsolvable for each $n$ that is at least $5$. 
**Transcendence of Some Common Numbers**: 
* Explain how the real numbers have more "imaginary" numbers than they should have. But, there simply isn't a better definition. It is proved that algebraic numbers are not enough. 
* Transcendence of Archimedes's number
* Transcendence of Euler's number


## Need to Understand

* I don't understand what makes the proof of the Halting Problem rigorous. It seems straight-forward to me. 
    - Should I just say that details are skipped? 
* I don't know that the underlying idea is for proof verification programs. 
* Do constructible numbers matter? What does it really mean to even be constructable? 


## General

* 


## Vocabulary and Notation 

* Use *automorphism group* or something instead of just *group*. Prove everything for these. (Besides not wanting to use the word *automorphism*, this still sucks be the *automorphism group* normally means the set of all automorphisms. Actually, I think that all groups are actually automorphism groups, but this is still misleading. Maybe I should call it a *transformation group*.)


## Material

* Reference the use of Galois Theory when it makes sense. 
    - Numbers: The full real numbers are needed. So, need to prove that $π$ and $e$ are transcendental. 
    - Characteristic Equations: An algebraic method does not exist. 
* Prove that euclidean spaces are categorical. Should this be somewhere else? 
* Computer verification of proofs. 
* The Halting Problem. 
    - What is the use of it? Supposedly other problems are the Halting problem in disguise. 
        + Does the compiler find the fastest possible machine code? 
        + Can someone without authorization access secured information? Is there a good cryptography example? 
        + Does a change to parser still alow it parse all the programs that it used to? 
        + Philosophically it it interesting: For example, that there are uncomputable numbers. 
    - The language Coq is designed to always halt. (It is not quite Turing complete.)
    - This can be used to prove a weaker form of Gödel's first incompleteness theorem. (It weaker forms still seems to be sufficient in principle.)
* Gödel's theorems? 
    - Let $f(x)$ be a function over the positive integers. 
    - Let $(E_i)_{i≥1}$ be an (alphabetical) ordering of all procedures that compute a function $f_i$ over the positive integers. 
    - Consider g(x) = f_x(x) + 1. 
    - Then, there is not an $i$ such that $f_i(x)$ is $g$. 
        + Suppose that $n$ is such. Then, $f_n(n) = f_n(n)+1$. This is a contradiction. 
        + (This is essentially just a diagonalization argument.)
    - Thus, there are functions that are not calculable. 
    - Thus, there is not an algorithm that can decide if an alleged routine (over the positive integers) does indeed a define a function over the positive integers. 
    - I guess that this then transfers to proving that there is not an algorithm to decide if there is a proof to certain statements. 
        + An example of a statement would be that there exists positive integers $x$ and $y$ such that $4⋅x - 3⋅y = 30$. 
* Point of Gödel. 
    - Well, the power set of the positive integers is uncountable. So, even the functions cannot be listed. So, why would one expect for their algorithms to be listed? (Side note, does the continuum hypothesis have value in these types of problems?)


