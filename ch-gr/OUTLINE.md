# Outline for General Relativity


## Introduction

* One wants to compute aging and whatnot on accelerated trajectories.
* Newton's law of gravity cannot be easily made to fit relativity.
    - Force at a distance cannot happen instantaneously because simultaneity depends on the frame. Thus, the force must be communicated through a field. What is the behavior of this field?
* Is is not clear that there even exists an inertial frame. Is this important?


## The Equivalence Principle.

* Gravitational mass and inertial mass are proportional.
    - I think that this should already be known.
    - That inertial mass and gravitational mass are proportional suggests that both inertial mass and gravitational mass are consequences of a single underlying mechanism. (How does this become the Equivalence Principle?)
* Energy has gravitational mass.
    - If any form of energy has gravitational mass, then all forms should. Energy could be raised from Earth in one form and then dropped in another. Infinite energy could be created if it didn't take anything to lift it, but took something to drop it.
    - The wavelength of light should increase when shot down because its source is accelerating in the same direction. The wavelength of light should decrease when shot up because the source is accelerating in the opposite direction. Thus, the energy changes according to gravity.
    - When charges accelerate they emit light. If this light did not have potential energy due to gravity, then energy is not conserved? This is not a relativistic argument. (Is it worth publishing?)
    - Light can be seen to bend around the Sun.
* Clocks tick slower closer to gravity.
    - Because, light has gravitational mass, it picks up energy as it falls. Thus, its frequency increases. (I am not sure why this holds in classical mechanics.) But, the second clock receives the same number of vibrations that the first clock sends over the perceived time period of each clock. Thus, the time must be slower at the lower clock.
        * This can be verified.
        * GPS depends on this phenomena to work.
    - This means that the usual geometry space-time that is drawn doesn't make sense because different stationary observers see time differently. 
    - The shortest isn't a straight line anymore. (But, the Stationary-Action Principle still holds.)
* The Equivalence Principle. A frame that is being pulled at a constant acceleration – such as a room by a rope – does not appear any different than an inertial frame with a gravitational field. Even light will bend in the same way.
    - The time shift due to gravity is the same as what would exist in an accelerating frame.
        * If a high clock sends a beam of light and then another one second after, then the times that the lower clock receives these beams is more than one second because it accelerated away.
    - Why does this matter? That clocks tick differently does matter because this means that space-time is curved. But, how does this analogy help with this? 
* I think that the aging of an object from its acceleration can now be determined. (Can this be done without any reference to gravity, but just that argument of an accelerated frame? Does gravity and the Equivalence Principle come in later?)


## Gravity

* I think the only thing that needs to be attained to give a complete description is the Einstein relation. The Einstein relation is $R = 8⋅π⋅T$, where $R$ is the Ricci curvature tensor and $T$ is the trace-reversed energy-momentum tensor. 
   - This is often written with a term that includes the cosmological constant, but I am not sure if I should have this. 
* That a constant accelerating is indistinguishable from free fall, free fall is a geodesic.
* The geodesic of free fall is determined by how gravity warps space by the stress-energy tensor.
* The movement of Mercury gives verification of general relativity. Should it be mentioned in the part on classical mechanics that classical mechanics does not explain Mercury's orbit. (Possibly include a figure of how Mercury's ellipse rotates. I have a model figure.)


## Black Holes

* The Schwarzschild metric is the solution for a single spherically symmetric mass. In this analytical solution, it is seen that something happens with sufficient mass – black holes. I don't know if I should bother with an analytical solution or just have a plot or something from supposedly computed solutions. 


## Gravitational Waves

* They can not be created from a 2-dimensional oscillation, but require movement in 3 dimensions, such as orbiting or spinning of an irregular shape.
* If they exist, then they need to have an effect. Feynman gave an intuitive device to check that they have energy.