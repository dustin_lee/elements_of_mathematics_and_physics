## Notes for General Relativity


## Need to Understand

* Approach: 
    - The main question is what law of gravity is invariant under Lorentz transformations
        + Time must change due to gravity 
        + It cannot be instantaneous action at a distance, so maybe something like Maxwell's Equations
    - Maybe the question to ask is how angles change
        + According to special relativity, the Earth would be flat to a fast rocket passing by. Thus, for relativity to still hold, the laws of gravity must adjust for relative velocity. This may be a way to approach the subject. The question would be what law of gravity would give this shape. How did Einstein guess the law? How did Hilbert derive it? Are there other approaches? 
    - Supposedly, the possible lack of an inertial frame is motivation. 


* Why does gravity propagate at the speed of light? 


## Material

* An object follow a geodesic. But, its path depends on its velocity. Thus, for the concept of such a geodesic, time does need to be part of the geometry. 
* Maybe I should mention how there are only a handful of experiments that validate General Relativity. 
* The energy-momentum tensor is the source of gravity. 


## General

* That geodesics can come together on a curved surface is the explanation of gravity. (What?)
* The Principle of Least Action in relativity becomes the principle of least aging.
* When a charge particle accelerates in a gravitational field, it emits light. The light takes away energy and momentum. I think that this implies that light has gravitational mass.
* Newton's law of gravity is inconsistent with special relativity? (Besides the needed delay, it appears that I need to do a calculation to see if it is invariant under changes of frames.)
    - The Earth is flat when viewed by a fast observer, but gravity predicts a sphere.
* Is the speed of light also be invariant in accelerating frames?
* Mention dark matter.
    - If general relativity is correct, then is must exist to explain observations.
    - This accounts for the bulk of matter.
* In a sense, matter and energy are the same. Thus, it would aesthetically pleasing if there was a theory that treated them on the same footing, that is, matter is just dense energy. This would be a classical field theory.
    - In such a theory, the equations could not be linear so that there can be scattering.
* Einstein said that general relativity rests on two pillars. The first is the curvature tensor, which is beautiful and strong. The other is the momentum-energy tensor, which is weak and must be left to the future. 
* Most of cosmology is not falsifiable.
* Does the Gödel time-travel thing mean anything? 
* Gravity affects everything. This is unlike other forces. For example, one can distinguish an electromagnetic force from an accelerating frame. This is part of the motivation of the Principle of Equivalence. 
* I guess that the Twin Paradox shows that space doesn't loop. 