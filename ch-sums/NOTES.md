# Notes for Infinite Summation

## General

* Should there be examples of using the tests? 
* Order of Summation: I would like an easier motivating example. 
* The reason that I am using real numbers is because this is the vast majority of situations. 
* Remember that absolute convergence and the ability to rearrange are equivalent to subsequences converging. How much should I emphasize this?


## Figures

* Geometric Sum: 
    - This needs to also include $1$ so that the caption is correct. 
        + Put this one to the side to take up less space. 