# Outline of Infinite Summation

## Introduction

* Motivation
* Definition
* The Tail-Test: Suffices to only look at the tail
* The Zero Test
    - The harmonic sum diverges, that is, the converse of the Zero Test does not hold


## Techniques

* Arithmetic operations
* The Comparison Test
* The Absolute-Convergence Test
* The Geometric-Sum Test
* The Ratio Test
* The Root Test
* The Alternating-Sum Test
* The Integral Test 


## Order 

* Idea/importance
* Absolutely convergent sums can be rearranged
* Fubini's theorem 