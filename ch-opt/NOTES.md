# Notes for Optimization

## Summary 

**Descriptions of Methods**: 
* First split: How is the derivative computed? 
    - Symbolically (Newton): This is not normally feasible
    - Estimations (quasi-Newton): The secant method
    - Automatic differentiation
* Second split: Step size and direction: 
    - Along the gradient? 
    - Have momentum? 
    - Dynamic stepping? 
* And I guess: Are there specific properties of problem that make certain methods better? For example, is it convex? 
* Convexity: By the Second-Derivative Test, the function is (normally?) locally convex at extrema. Thus, it is likely that an algorithm for convex functions could be chained with a general algorithm once it gets close to the extremum.


## Need to Understand

* Can automated differentiation be used when applying Newton's method to solve a static equation? Yes. It is just solving a nonlinear system. 


## Material

* Halley's Method? 
* What are motivating examples? 
* Partial Swarm and whatnot? 
    - Is this only really for problems without differentiability? 
    - Is there normally a reason to consider many start points? 


## General

* Can these normally be done to machine precision? 