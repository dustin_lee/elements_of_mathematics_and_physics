%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Summary
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% First, in this section, the order of convergence of Euler's Method is studied.
% Then, Heun's Method and the Runge-Kutta Method are introduced. 
% The study of the order of convergence of these methods uses Taylor's Theorem, which is introduced in Chapter~\ref{ch:psums}.

% Euler's Method is the prototypical finite-difference method.  
% It is similar to the definition of integration in Chapter~\ref{ch:int}. 
% And, it is similar to Heun's Method and the Runge-Kutta Method in this section. 
% More finite-difference methods are studied in Chapter~\ref{ch:fdm} and Chapter~\ref{ch:dt}.
% \scenebreak 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Order of Convergence
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

The proof of the existence of a solution to dynamic ordinary differential equations used the convergence of Euler's Method. 
Now, the order of convergence is studied. 

Let 
\[
    \dot{u}(t) = φ(u(t))
\]
be a dynamic ordinary differential equation such that the function $φ$ is Lipschitz continuous with the Lipchitz constant $L$. 
And, let
\[
    u(a) = p
\]
be an initial condition for it.
Let $f$ be a solution to these equations.

% Let $b$ be a time. 
Let $n$ be a positive integer. 
And, let $g_n$ be the approximation from Euler's Method for these equations for $n$ uniformly spaced time samples $a_0$, $\dots$, $a_{n-1}$ and $a_n$ from the initial time point $a$ to a time point $b$.

For each index $i$, let $E_i$ be the vector
\[
    f(a_i) - f_n(a_i) 
    ,
\]
that is, the error of the approximation at the time point $a_i$. 
Furthermore, for each index $i$, let $f_i$ be the solution to 
\[
    \dot{u}(t) = φ(u(t))
\]
and the initial condition
\[
    u(a_i) = g_n(a_i).
\]
And, for each index $i$, let $e_i$ be the vector
\[
    f_i(a_i) - ε⋅φ(f_n(a_{i-1}))
    , 
\]
that is, the one-step error of the approximation.

Without loss of generality, assume that the step increment is such that $\exp(L⋅\abs{ε})$ is less than $2$. 
Then, the relations
\begin{align}
    \abs{E_{i+1}}
    &= \abs{f(a_i) - g_n(a_i)} \\
    &= \abs{\Bigg( f(a_{i-1}) + ∫_{t:a_{i-1}}^{a_i} φ(f(t)) \Bigg) 
            - \Bigg( g_n(a_{i-1}) + ε⋅φ(g_n(a_{i-1})) \Bigg)
            } \\
    &= \abs{E_i + ∫_{t:a_{i-1}}^{a_i} φ(f(t)) - ε⋅φ(g_n(a_{i-1})) } \\
    &= \abs{E_i + ∫_{t:a_{i-1}}^{a_i} φ(f(t)) 
            - \left( ∫_{t:a_{i-1}}^{a_i} φ(f_i(t)) 
                     - ∫_{t:a_{i-1}}^{a_i} φ(f_i(t)) \right)
            - ε⋅φ(g_n(a_{i-1}))
            } \\
    &= \abs{E_i 
            + ∫_{t:a_{i-1}}^{a_i} \Big( φ(f(t)) - φ(f_i(s)) \Big)
            + ∫_{t:a_{i-1}}^{a_i} φ(f_i(t)) - ε⋅φ(g_n(a_{i-1}))
            } \\
    &= \abs{E_i} 
        + \abs{∫_{t:a_{i-1}}^{a_i} \Big( φ(f(t)) - φ(f_i(t)) \Big)}
        + \abs{∫_{t:a_{i-1}}^{a_i} φ(f_i(t)) - ε⋅φ(g_n(a_{i-1}))}
        \\
    &≤ \abs{E_i} 
        + ∫_{t:\min(a_{i-1}, a_i)}^{\max(a_{i-1}, a_i)}
            \Babs{ φ(f(t)) - φ(f_i(t)) }
        + \abs{∫_{t:a_{i-1}}^{a_i} φ(f_i(t)) - ε⋅φ(g_n(a_{i-1}))}
        \\
    &≤ \abs{E_i} 
        + \left( ∫_{t:\min(a_{i-1}, a_i)}^{\max(a_{i-1}, a_i)}
            \abs{E_i}⋅\exp(L⋅\abs{t-a_i})
          \right)
        + \abs{e_i}
        \\
    &≤ \abs{E_i} 
        + \abs{ε}⋅\abs{E_i}⋅\exp(L⋅\abs{ε})
        + \abs{e_i}
        \\
    &≤ \abs{E_i} 
        + 2⋅\abs{ε}⋅\abs{E_i}
        + \abs{e_i}
        \\
    % &= (1 + \abs{ε}⋅L) ⋅ \abs{E_i}  + \abs{e_i} \\
    &= \Big( 1 + 2⋅\abs{ε} \Big) ⋅ \abs{E_i}  + \abs{e_i} 
\end{align}
hold for each index $i$. 
Thus, the relations
\begin{align}
    \abs{E_n}
    &= \Big( 1 + 2⋅\abs{ε} \Big) ⋅ \abs{E_{n-1}}  + \abs{e_{n-1}} \\
    &≤ \ldots \\
    &≤ \Big( 1 + 2⋅\abs{ε} \Big)^{i+1}⋅\abs{E_0}
        + ∑_{0≤i≤n} \Big( 1 + 2⋅\abs{ε} \Big)^i ⋅ \abs{e_{n-i}} \\
    &≤ ∑_{0≤i≤n} \Big( 1 + \abs{ε} \Big)^i ⋅ \abs{e_{n-i}} \\
    &≤ \exp\Big(2⋅\abs{b-a}\Big) ⋅ ∑_{0≤i≤n} ⋅ \abs{e_{i}} 
        , 
\end{align}
where the second-to-last relation holds because $E_0$ is $0$, and where it is proved that the last relation holds in Section~\ref{ode:sec:some-sols}. 
Thus, the study of the order of convergence is reduced to the study of the one-step error. 


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Order of Euler's Method
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Let $B$ be a bound of 
\[
    \abs{\dot{f}}
    . 
\]
Then, by Taylor's Theorem, the relation 
\[
    % \abs{f_i(t-a_i) - \Big(f_i(a_i) + \dot{f_i}(a_i)⋅(t-a_i) \Big) }
    % ≤ B⋅\abs{t-a_i}^2
    \abs{f_i(t-a_i) - f_i(a_i)}
    ≤ B⋅\abs{t-a_i}
\]
holds for each index $i$. 
Thus, the relations
\begin{align}
    \abs{e_i}
    &= \Babs{f_i(a_i) - ε⋅φ(f_n(a_{i-1}))}\\
    &= \abs{∫_{t:a_{i-1}}^{a_i} \Big( φ(f_i(t)) - φ(g_n(a_{i-1})) \Big)} \\
    &≤ ∫_{t:\min(a_{i-1}, a_i)}^{\max(a_{i-1}, a_i)}
            \Babs{ φ(f_i(t)) - φ(g_n(a_{i-1})) } \\
    &≤ ∫_{t:\min(a_{i-1}, a_i)}^{\max(a_{i-1}, a_i)}
            L⋅\Babs{f_i(t) - g_n(a_{i-1}) } \\
    &≤ ∫_{t:\min(a_{i-1}, a_i)}^{\max(a_{i-1}, a_i)}
            L⋅B⋅\abs{t-a_i} \\
    &≤ \frac{L⋅B}{2}⋅\abs{ε}^2 
\end{align}
hold for each index $i$. 
Thus, the relations 
\begin{align}
    \abs{E_n}
    &≤ \exp\Big(2⋅\abs{b-a}\Big) ⋅ ∑_{0≤i≤n} \abs{e_{i}}  \\
    &≤ \exp\Big(2⋅\abs{b-a}\Big) ⋅ ∑_{0≤i≤n} \frac{L⋅B}{2}⋅\abs{ε}^2 \\
    &≤ \exp\Big(2⋅\abs{b-a}\Big) ⋅ ∑_{0≤i≤n} \frac{L⋅B}{2}⋅\left(\frac{\abs{b-a}}{n}\right)^2 \\
    &≤ \exp\Big(2⋅\abs{b-a}\Big) ⋅ \frac{L⋅B⋅\abs{b-a}^2}{2} ⋅ ∑_{0≤i≤n} \left(\frac{1}{n}\right)^2 \\
    &≤ \exp\Big(2⋅\abs{b-a}\Big) ⋅ \frac{L⋅B⋅\abs{b-a}^2}{2} 
       ⋅ \left(\frac{1}{n}\right)
\end{align}
hold. 
That is, the order of convergence of Euler's Method is $1$. 


\scenebreak 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Huen's Method and the Runge-Kutta Method
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Now, Heun's Method and the Runge-Kutta Method are described. 
% Let 
% \[
%     \dot{u}(t) = φ(u(t))
% \]
% be a differential equation whose solution is the trajectory of a particle. 
% And, let $p$ be the position of the particle at a time $a$. 
% Let $ε$ be a positive real number. 
% And, let $a_0$, $\dots$, $a_{n-1}$ and $a_n$ be uniformly space time samples for the time increment $ε$ such that $a_0$ is $a$. 
Both of these methods could be used in the proof of existence of solutions to dynamic ordinary differential equations instead of Euler's Method.
Their orders of convergence can be computed similarly to Euler's Method by bounding the one-step error with Taylor's Theorem.

First, \emph{Heun's Method}\index{Heun's Method} is described. 
Recursively, define the points $p_0$, $\dots$, $p_{n-1}$ and $p_n$ such that $p_0$ is $p_0$ and such that the relations
\[
    p'_{i+1} = p_i + ε⋅φ(p_i)
    \textand
    p_{i+1} = p_i + \frac{ε}{2}⋅\Big( φ(p_i) + φ(p'_{i+1}) \Big)
\]
hold for each index $i$. 
Let $f$ be the linear interpolation of the values $p_0$, $\dots$, $p_{n-1}$ and $p_n$ at the points $a_0$, $\dots$, $a_{n-1}$ and $a_n$. 
This function $f$ is the approximation to the solution from Heun's Method. 
For each index $i$, the point $p'_i$ is a preliminary step, which is then corrected. 
The polynomial order of convergence of Heun's Method is $2$. 

Now, the \emph{Runge-Kutta Method}\index{Runge-Kutta Method} is described. 
Recursively, define the points $p_0$, $\dots$, $p_{n-1}$ and $p_n$ such that $p_0$ is $p_0$ and such that the relations
\begin{align}
    p'_{i+1} &= p_i + \frac{ε}{2}⋅φ(p_i)
    , \\
    p''_{i+1} &= p'_{i+1} + \frac{ε}{2}⋅φ(p'_{i+1})
    , \\
    p'''_{i+1} &= p_i + ε⋅φ(p''_i)
\intertext{and}
    p_{i+1}
    & = p_i
      + \frac{ε}{6}⋅\Big( 
            φ(p_i) + 2⋅φ(p'_{i+1}) + 2⋅φ(p''_{i+1}) + φ(p'''_{i+1})
        \Big)
\end{align}
hold for each index $i$. 
Let $f$ be the function that linearly interpolates the values $p_0$, $\dots$, $p_{n-1}$ and $p_n$ at the points $a_0$, $\dots$, $a_{n-1}$ and $a_n$. 
This function $f$ is the approximation to the solution from the Runge-Kutta Method. 
For each index $i$, the point $p'_i$ is a half step, $p''_i$ is another half step, $p'''_i$ helps correct $p''_i$, and a weighted average of these points is taken. 
The polynomial order of convergence of the Runge-Kutta Method is $4$. 


\begin{figure}[H]
\centering
    \includegraphics[width=0.48\textwidth]{ode-fig-approximations-few}
    \includegraphics[width=0.48\textwidth]{ode-fig-approximations-many}
    \caption{Comparison of schemes. The schemes are used to approximation the solution to the differential equation
    \[
        \dot{u}(t) = -u(t)
    \]
    and the initial condition
    \[
        u(0) = 1
        .
    \]
    On the left, the approximations are for 50 time samples. On the right, the approximations are for 500 time samples.}
\end{figure}


\begin{figure}[H]
\centering
    \includegraphics[width=0.5\textwidth]{ode-fig-order}
    \caption{Order of convergence of schemes.}
\end{figure}


In practice, fourth-order is about optimal. 
The Runge-Kutta Method is effective for most dynamic ordinary differential equations in practice. 
But, sometimes there are better alternatives. 

Often, if there is method that captures some physics of the problem, then it is better to use it. 
For example, in Chapter~\ref{ch:mom}, it is explained that it is better to use the Leapfrog Scheme for planetary orbit because it preserves energy.

Euler's Method, Heun's Method and the Runge-Kutta Method are one-step explicit schemes. 
In Chapter~\ref{ch:dt}, multistep schemes and implicit schemes are introduced. 
For some problems, the evaluation of the defining function may be computationally expensive. 
But, the Runge-Kutta Method requires four evaluations for each step. 
Multistep schemes are good for such a situation because they use only one function evaluation at each step, but can still have a high order of convergence.
Also, for some problems, the step size for these explicit schemes may be unreasonably small. 
Implicit schemes are slower per step, but for some problems, they may require many fewer steps than explicit schemes. 

