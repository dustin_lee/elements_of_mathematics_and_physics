# Notes for Dynamic Ordinary Differential Equations

## Need to Understand 

* Understanding the Soblev-style norms: 
    - The start state is an example of why the norm includes derivatives. A different velocity will lead to a completely different solution. 


## Figures

* Pendulum System:
    - In a way, combine the two figures that I have. I want the second, but have it smooth like the first. Have different colors for the dotted/dashed lines. 
* Pendulum Graph: 
    - Also overlap the graph of the angular velocity 
* Graph of Exp: Have the graph label as $exp(t)$
* SHM System: Have it have the force as a vector and the equation
* Order: 
    - The orders in the figure is wrong
* Maximal solution: 
    - Besides having the solution $\tan(x)$, also have a figure that demonstrates a solution leaving the domain of the defining domain. Have these two figures side-by-side. 
* ? Euler's method for the exponential function: Have a figure of successive approximations. Have them as piecewise functions. Have the true solution. Have one for both the forward and backward direction. 


## Vocabulary

* I could define both *scheme order* and *method order*. 
* Use *residual* instead of *truncation error*? 
* Use the terms *state space* and *space time*. There is not a reason to have any other such things.
    - Both *phase space* (including derivatives) and *configuration space* (including multiple bodies) can fall under *state space*. 
* Do not use the term *defining function* or anything of this sort. Just get by without such.
* Use the term *spring stiffness* instead of *spring constant*.
* Should I use the term *Rocket Equation*? 


## Fixes

* In the example of reducing an equation to an integral equation, the integral sign is too small. Figure out how to make it bigger. 


## Material

* Drop the first SHM solution? Is it important because it explains linearity? 
* By the Norm-Equivalence Theorem, a function is Lipchitz independently of the norm, but for a different constant. 
* Error: Three are three types of error
    - Error from numerical method 
    - Error from initial condition
    - Error from the equation, such as from errors in the constants (This should be studied in the third part in the second version)
    - Round-off errors
* Should I mention the trick of skipping a derivative that does not appear if the equation? ($\ddot{γ}(t) = f(γ(t))$ gives $γ(t_{i+1}) = \frac{ε^2}{2}f(γ(t_i))$.) No, but, maybe in the Intro. 
* Ascoli's theorem. The intuition is that the functions can't grow too much and that they cannot oscillate too much. 
* Maximal solutions: 
    - I don't have a theorem or anything for the maximal solution. I don't feel that anything needs to be proved. So, why does Smale have a theorem on it? I guess that his theorem just says that these are the only two ways that the interval is prevented from being extended. 
    - Do I need Zorn's Lemma to say that the union is the maximal interval? 


## General 

* Is it trivial that, if there is a unique solution, then the sequence from Euler's method converges to it?
* Mention somewhat early that there exists solutions to differential equations that cannot be written in closed form. Give an example, but don't bother giving a proof?
* Runge-Kutta is about integration. Simpson's rule is used for order. In general, I should probably be explaining finite-difference schemes as integrals.
* Is Picard Iteration ever useful? 
    - It has linear exponential convergence. 
    - It is a bigger computation for each step. 
    - It could be started with a good guess, such as from a surrogate model or a time-stepping scheme. 
    - Does it reach machine precision? 
