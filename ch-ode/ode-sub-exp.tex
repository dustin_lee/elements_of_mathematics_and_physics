% Motivation
% Radioactive decay (growth is proportional to its current size) 
% Generally used? 

The \emph{Exponential Function}\index{Exponential Function} is the solution to the one-dimensional ordinary differential equation
\[
	\dot \alpha (t) = \alpha(t)
\]
and the initial condition
\[
	\alpha (0) = 1
	.
\]
It is denoted by $\exp(t)$\label{not:exp}.
It is infinitely differentiable, increasing and positive.
Furthermore, its domain is the set of real numbers. 


\begin{figure}[H]
\centering
	\includegraphics[width=0.7\textwidth]{ode-fig-exp-plot}
	\caption{The graph of the Exponential Function.}
\end{figure}


Let $a$ be a real number.
Then, both $\exp(a + t)$ and $\exp(a) ⋅ \exp(t)$ are solutions to the differential equation
\[
	\dot \alpha (t) = \alpha(t)
\]
with the initial condition 
\[
	\alpha(0) = \exp(a)
	. 
\]
Because the solution to this differential equation and initial condition is unique, the relation
\[
	\exp(a + t) = \exp(a) ⋅ \exp(t)
\] 
holds. 
This behavior is similar to a function of the form $b^t$, where $b$ is a real number. 
The next theorem says that the exponential function is indeed a function of this form. 

The number $\exp(1)$ is called \emph{Euler's Number}\index{Euler's Number}.
It is approximately 
\[
	\num{2.718 281 828}.
\]
It is denoted by $\e$\label{not:e}. 

\input{ode-thm-e}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Approximation of Euler's Number
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Now, Euler's number is approximated. 
By Euler's Method, the relations 
\begin{align}
	\exp(1) 
	&= \lim_{i→∞} \left(  
		1 
		+ \frac{1}{i}
		+ \left( 1 + \frac{1}{i} \right) ⋅ \frac{1}{i}
		+ \ldots
		+ \left( 1 
				+ \frac{1}{i}
				+ \left( 1 + \frac{1}{i} \right) ⋅ \frac{1}{i} 
				+ \ldots
				+ \left( \right) ⋅ \frac{1}{i}
		  \right) ⋅ \frac{1}{i} 
		\right) \\
	&= \lim_{i→∞} \sum_{1≤j≤i} 
			\frac{i!}{j!⋅(i-j)!} \left(\frac{1}{i}\right)^j \\
	&= \lim_{i→∞} \left(1 + \frac{t}{i} \right)^i 
\end{align}
hold.
That is, the relation 
\[
	\e = \lim_{i→∞} \left(1 + \frac{1}{i} \right)^i
\]
holds. 
Thus, the relation 
\[
	\e = \lim_{i→∞} \left(1 + \frac{1}{2^i}\right)^{2^i}
\]
holds. 
And, the sequence 
\[
	\left( \left(1 + \frac{1}{2⋅i} \right)^{2⋅i} \right)_{i≥1}
\]
is increasing because the relations
\begin{align}
	\left(1 + \frac{1}{2^{i+1}} \right)^{2^{i+1}}
	&= 
	\left(1 + \frac{1}{2⋅2^i} \right)^{2⋅2^i} \\
	&= 
	\left(1 + \frac{1}{2^i} + \frac{1}{4⋅2^i} \right)^{2^i} \\
	&≥ 
	\left(1 + \frac{1}{2^i}\right)^{2^i}
\end{align}
hold for any index $i$. 
Thus, any term in this sequence is a lower bound for Euler's Number. 
For example, the fourth term in this sequence is more than $2.63$, that is, the Euler's Number is at least $2.63$.
Similarly, Euler's Method with a negative step increment can be used to find an upper bound of the number
\[
	\frac{1}{\e}
	.
\]
For example, the fourth term in the sequence 
\[
	\left( \left(1 - \frac{1}{2⋅i} \right)^{2⋅i} \right)_{i≥1}
\]
is more than $0.35$, and Euler's number is at most $2.81$. 
Thus, Euler's number is between $2.63$ and $2.81$.
This method is slower, and a faster method is in Chapter~\ref{ch:psums}.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Derivatives of Logarithms and Exponentiation
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Now, the derivatives of logarithms and exponentiation are computed. 
The function $\log_{\e}(t)$ is called the \emph{Natural Logarithm Function}\index{Natural Logarithm Function}.
It is denoted by $\ln(t)$.
The relations
\[
	∂\ln(t)
	= \frac{1}{\left(∂ \exp \right)(\ln (t))}
	= \frac{1}{\exp(\ln(t))}
	= \frac{1}{t}
\]
hold, where the first relation hold by the Inverse-Function Theorem. 
Furthermore, for any positive real number $b$, the relations
\[
	∂ \log_b (t)
	= ∂ \left( \frac{\ln(t)}{\ln(b)} \right)
	= \frac{1}{\ln(b)} ⋅ ∂ \ln(t) 
	= \frac{1}{\ln(b) ⋅ t}
\]
hold, where first relation holds by the Change-of-Base Formula. 
And, for any positive real number $b$, the relations 
\[
	∂(b^t)
	= ∂(\exp(\ln(b^t)))
	= ∂(\exp(t ⋅ \ln(b)))
	= ∂(\exp(t) ⋅ b)
	= b ⋅ \exp(t)
\]
hold. 