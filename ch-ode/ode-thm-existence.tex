\begin{theorem}
Let
\[
    \dot{u}(t) = φ(u(t))
\]
be a dynamic ordinary differential equation, where the function $φ$ is continuous. 
And let
\[ 
    u(a) = p
\]
be an initial condition for it. 
Then, a solution to these equations exists. 
\end{theorem}

\begin{proof}
% Restriction of domain 
Let $B$ be a compact ball in the spatial domain such that $p$ is in its interior. 
Let $I$ be a compact interval in the time domain such that $a$ is in its interior and  
\[
	p + \max_{\substack{t \in I\\ x \in B}} φ(t, x) ⋅ \len(I)
\]
is in $B$. 
These restrictions of the domain are made so that the approximations of the solutions in this proof remain in the domain of $φ$. 

% Euler's method
For each index $i$, let $f_i(t)$ be the function on $I$ from Euler's Method for the time increment $\frac{b-a}{i}$. 
By the lemma, there exists a uniformly convergent subsequence of the sequence $(f_i)_{i≥1}$. 
Let $f$ be the limit of such a subsequence. 

% Set up 
It is proved that the trajectory $f$ is a solution to the differential equation and the initial condition. 
It is continuous because it is the uniform limit of continuous functions.  
Thus, it suffices to prove that it is a solution to the equation 
\[
	u(t) = p + ∫_a^t φ∘u
	. 
\]

Let $b$ be a point in $I$.  
And, let $ε$ be a positive real number. 
It is proved that the relation 
\[
	\abs{f(b) - \left(p + ∫_a^b φ∘f \right)} < ε
\]
holds. 
The relation 
\begin{align}
	\abs{f(b) - \left( p + ∫_a^b φ∘f \right)}
	≤& \abs{f(b) - f_i(b)} \\
		&+ \abs{f_i(b) - \left( p + ∫_a^b φ∘f_i \right)} \\
		&+ \abs{\left( p + ∫_a^b φ∘f_i \right)
				- \left( p + ∫_a^b φ∘f \right)}
\end{align}	
holds for any index $i$ and point $b$. 
Now, these three terms on the right-hand side are dealt with separately. 

% First term  
First, it is proved that an index $n_1$ exists such that, for any index $i$ that is more than it, the relation 
\[
	\abs{f(b) - f_i(b)} < \frac{ε}{3}
\]
holds for any point $b$.
Such an index $n_1$ does exist because the sequence $(f_i)_{i≥1}$ converges uniformly to $f$.

% Second term 
Second, it is proved that an index $n_2$ exists such that, for any index $i$ that is more than it, the relation 
\[
	\abs{f_i(b) - \left( p + ∫_a^b φ∘f_i \right)}
	< \frac{ε}{3}
\]
holds for any point $b$. 
Because $φ$ is uniformly continuous, a positive real number $δ$ exists such that the relation 
\[
	\abs{φ(p) - φ(q)} < \frac{ε}{3⋅\len(I)}
\]
holds for any two points $p$ and $q$ whose distance from each other is less than $δ$.
Let $n_2$ be an index such that, for any index more than it, the step size of Euler's Method for the approximation $f_i$ is less than $δ$.
Let $i$ be an index that is more than $n_2$. 
And, let $a_1$, $\ldots$, $a_{m-1}$ and $a_m$ be the time samples for the approximation is $f_i$.  
Then, the relations 
\begin{align}
	&\abs{f_i(b) - \left( p + ∫_a^b φ∘f_i \right)} \\
	&= \abs{
			\left( p + ∑_{0≤j≤m-1} ∫_{a_j}^{a_{j+1}} φ∘f_i (a_j) \right)
			- \left( p + ∫_a^b φ∘f_i \right)
		   }
	\\
	&≤ ∑_{0≤j≤m-1} 
		\abs{ φ∘f_i (a_j)  - φ∘f_i } \\
	&< ∫_a^b \frac{ε}{3⋅\len(I)} \\
	&= \frac{ε}{3} 
\end{align}
where the second relation holds because every step size is less than $δ$. 
Thus, the index $n_2$ is indeed such an index.  

% Third term 
Third, it is proved that an index $n_3$ exists such that, for any index $i$ that is more than it, the relation 
\[
	\abs{
		 \left( p + ∫_a^b φ∘f_i \right)
		 - \left( p + ∫_a^b φ∘f_i \right)
		}
	< \frac{ε}{3}
\]
holds for any point $b$. 
Because $φ$ is uniformly continuous, a positive real number $δ$ exists such that the relation 
\[
	\abs{φ(p) - φ(q)} < \frac{ε}{3⋅\len(I)}
\]
holds for any two points $p$ and $q$ whose distance from each other is less than $δ$. 
Because the sequence $(f_i)_{i≥1}$ converges uniformly to $f$, an index $n_3$ exists such that, for any index $i$ that is more than it, the relation 
\[
	\abs{f_i(b) - f(b)} < δ
\]
holds for any point $b$. 
Thus, the relation 
\begin{align}
	\abs{
		 \left( p + ∫_a^b φ∘f_i \right)
		 - \left( p + ∫_a^b φ∘f \right)
		}
	&≤ \abs{∫_a^b φ∘f_i - φ∘f} \\
	&< \abs{a-b} ⋅ \frac{ε}{3⋅\len(I)} \\
	&< \frac{ε}{3}
\end{align}
holds for any point $b$ and for index $i$ that is more than $n_2$. 

% Conclusion 
Let $n$ be the maximum of $n_1$, $n_2$ and $n_3$. 
Then, the relations
\begin{gather}
	\abs{f(b) - f_i(b)}
	< \frac{ε}{3}
	, \\
	\abs{f_i(b) - \left( p + ∫_a^b φ∘f_i \right)}
	< \frac{ε}{3}
\end{gather} 
and
\[
	\abs{\left( p + ∫_a^b φ∘f_i \right)
				- \left( p + ∫_a^b φ∘f \right)}
	< \frac{ε}{3}
\]
hold for any point $b$ and any index $i$ that is more than $n$. 
Thus, the relation 
\[
	\abs{f(b) - \left(p + ∫_a^b φ∘f \right)} < ε
\]
holds does indeed hold for any point $b$. 
\end{proof}











