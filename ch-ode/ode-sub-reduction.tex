%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% First-Order Reduction 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Let $f$ be a solution to a dynamic ordinary differential equation
\[
    ∂^n u (t) = φ\Big( t, u (t), ∂u (t), \dots , ∂^{n-1} u (t) \Big)
\]
and the initial condition 
\[
    u(a) = v_0, 
    \textspace  \ldots, \textspace
    ∂^{n-2} u(a) = v_{n-2}
    \textand
    ∂^{n-1} u(a) = v_{n-1}
    .
\]
Then, the function 
\[
    \Big( f(t), ∂f(t), \ldots, ∂^{n-1} f(t) \Big)
\]
is a solution to the first-order differential equation
\[
    ∂\begin{pmatrix}
        u_1(t) \\
        u_2(t) \\
        \vdots \\
        u_n(t)
      \end{pmatrix}
    = \begin{pmatrix}
        u_2(t) \\
        u_3(t) \\
        \vdots \\
        φ\Big( t, u_1(t), u_2(t), \dots , u_{n-1}(t) \Big)
      \end{pmatrix}
\]
and the initial condition 
\[
    \begin{pmatrix}
        u_1(a) \\
        u_2(a) \\
        \vdots \\
        u_{n-1}(a)
    \end{pmatrix}
    = 
    \begin{pmatrix}
        v_0 \\
        v_1 \\ 
        \vdots \\
        v_{n-1}
    \end{pmatrix}
    .
\]
Furthermore, the function 
\[
    \Big(t, f(t), ∂f(t), \ldots, ∂^{n-1} f(t) \Big)
\]
is a solution to the first-order time-independent differential equation
\[
    ∂\begin{pmatrix}
        u_0(t) \\
        u_1(t) \\
        u_2(t) \\
        \vdots \\
        u_n(t)
      \end{pmatrix}
    = \begin{pmatrix}
        1 \\
        u_2(t) \\
        u_3(t) \\
        \vdots \\
        φ\Big( u_0(t), u_1(t), u_2(t), \dots , u_{n-1}(t) \Big)
      \end{pmatrix}
\]
and the initial condition 
\[
    \begin{pmatrix}
        u_0(a) \\
        u_1(a) \\
        u_2(a) \\
        \vdots \\
        u_{n-1}(a)
    \end{pmatrix}
    = 
    \begin{pmatrix}
        a \\
        v_0 \\
        v_1 \\ 
        \vdots \\
        v_{n-1}
    \end{pmatrix}
    .
\]
Conversely, if 
\[
    \Big( f_0(t), f_1(t), f_2(t), \ldots, f_{n-1}(t) \Big)
\]
is a solution to the first-order differential equation
\[
    ∂\begin{pmatrix}
        u_0(t) \\
        u_1(t) \\
        u_2(t) \\
        \vdots \\
        u_n(t)
      \end{pmatrix}
    = \begin{pmatrix}
        1 \\
        u_2(t) \\
        u_3(t) \\
        \vdots \\
        φ\Big( u_0(t), u_1(t), u_2(t), \dots , u_{n-1}(t) \Big)
      \end{pmatrix}
\]
and the initial condition 
\[
    \begin{pmatrix}
        u_0(a) \\
        u_1(a) \\
        u_2(a) \\
        \vdots \\
        u_{n-1}(a)
    \end{pmatrix}
    = 
    \begin{pmatrix}
        a \\
        v_0 \\
        v_1 \\ 
        \vdots \\
        v_{n-1}
    \end{pmatrix}
    ,
\]
then $f_1(t)$ is a solution to the differential equation
\[
    ∂^n u (t) = φ\Big( t, u (t), ∂u (t), \dots , ∂^{n-1} u (t) \Big) 
\]
and the initial condition 
\[
    \begin{pmatrix}
        u_0(a) \\
        u_1(a) \\
        u_2(a) \\
        \vdots \\
        u_{n-1}(a)
    \end{pmatrix}
    = 
    \begin{pmatrix}
        a \\
        v_0 \\
        v_1 \\ 
        \vdots \\
        v_{n-1}
    \end{pmatrix}
    .
\]
Thus, a dynamic ordinary differential equation can be solved by solving a first-order time-independent differential equation.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Norm Equivalence
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Some proofs in the chapter involve the convergence of a sequence of approximations to a solution. 
By the Norm-Equivalence Theorem, such convergence is equivalent for the original equation and such a first-order time-independent reduction of it. 


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Integral Equation
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Let
\[
    \dot{u}(t) = φ(u(t))
\]
be a first-order time-independent dynamic ordinary differential equation, and let
\[ 
    u(a) = v
\]
be an initial condition for it. 
By, the Fundamental Theorem of Calculus, a trajectory is a solution to this differential equation and this initial condition exactly if it is a solution to the equation
\[
    u(t) = v + ∫_a^t φ∘u
    . 
\]
Thus, a dynamic ordinary differential equation can be solved by solving an integral equation. 


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Example
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

For example, consider the equation 
\[
    \ddot{u}(t)
    = \frac{1}{M(t)}⋅\Big( g(u(t)) + e(t)+ r\big(u(t), \dot{u}(t)\big) \Big)
\]
for the rocket from Section~\ref{ode:sec:concept}.
And, let
\[
    u(0) = p
    \textand 
    \dot{u}(a) = v
\]
be initial conditions for it. 
Then, this equation and these initial conditions are equivalent to the first-order time-independent equation
\[
    \begin{pmatrix}
        t \\ 
        \dot{u_0}(t) \\
        \dot{u_1}(t) \\
    \end{pmatrix}
    = 
    \begin{pmatrix}
        1 \\ 
        u_1(t) \\
        \frac{1}{M(t)}⋅\Big( g(u_0(t)) + e(t)+ r\big(u_0(t), u_1(t)\big) \Big)
    \end{pmatrix}
\]
and the initial condition
\[
    \begin{pmatrix}
        t \\ 
        u_0(0) \\
        u_1(0) \\
    \end{pmatrix}
    = 
    \begin{pmatrix}
        0 \\ 
        p \\
        v \\
    \end{pmatrix}
    .
\]
Furthermore, this is equivalent to the equation 
\[
    \begin{pmatrix}
        t \\ 
        u_0(t) \\
        u_1(t) \\
    \end{pmatrix}
    = 
    \begin{pmatrix}
        0 \\ 
        p \\
        v \\
    \end{pmatrix}
    + 
    ∫_{s:0}^t
    \begin{pmatrix}
        s \\ 
        u_1(s) \\
        \frac{1}{M(t)}⋅\Big( g(u_0(s)) + e(t)+ r\big(u_0(s), u_1(s)\big) \Big)
    \end{pmatrix}
    .
\]