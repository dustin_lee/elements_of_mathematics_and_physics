# Outline of Dynamic Ordinary Differential Equations

## Concept

* Two Examples: 
    * The pendulum 
    * What about a rocket? 
        + A thruster (time-dependence on a prescribed schedule) (it is assumed that the rocket maintains the prescribed orientation)
        + Gravity 
        + Mass follows a prescribed schedule
        + Equation:
            $$
                \ddot{γ}(t, x) = m(t)⋅(g(x)⋅ + f(t))
            $$
        + Have a figure: Shot at an angle from the Earth's surface
* Definitions:
    - ODE
        + Time, region and tangent spaces
    - Initial conditions
    - Solution 
    - Order
    - Time-dependence


## Posedness

* Integral equation
* Reduction:
    - To a first-order equation
    - To a time-dependent equation 
* Existence:
    - Explain Euler's Method
    - Ascoli's Theorem
    - Existence Theorem 
* Uniqueness:
    - Example of an equation without a unique solution 
    - What uniqueness means
    - Uniqueness Theorem 
    - Discussion of the Lipchitz condition 
* Maximal solutions
    - Example of an exploding solution
* Continuity:


## Some Solutions

* The Exponential Function:
    - Radioactive decay as a motivating example
* Simple Harmonic Motion:


## Numerics

**Order of Convergence**

**Other Schemes**: 
* Heun's Method
* Runge-Kutta Method
* Plots


---
---
---
**Old**

### Schemes

* Central-difference scheme.
    - Actually, don't have this because it is a multistep scheme. Wait until Static Differential Equations. 
* Implicit schemes. 
    - They require future information and must be solved for.
    - Backwards Euler: $γ(t_i) = γ(t_{i+1}) - \dot{γ}(t_{i+1}) = γ(t_i) = γ(t_{i+1}) - f(γ(t_{i+1}))$.
    - Implicit schemes are often unconditionally stable.
    - When averages are taken between the explicit (forward-step) and implicit (backward-step), then I think that these are rather accurate. If they can be analytically solved beforehand, then they are fast too.
* Leapfrog schemes.
    - These are useful for coupled quantities. 
        * Mention Yee's method. Well, also, would it still make to use a leapfrog scheme when representing the electric and magnetic fields with FEM? 
    - Actually, this is a multistep scheme. 
* (Almost) Energy-Conserving schemes.  
    - I guess that, in a conservative field, there are bounds placed on the total energy when using these schemes, that is, the total energy can only fluctuate so much. 
    - Verlet. (Is this the same as Euler-Cromer?)
        * A benefit of this algorithm for molecular dynamics is that the force only needs to be computed once each time step. (The force computation is the most expensive part.) And, the actually accuracy isn't really important anyways. Thus, this is far superior to something like Runge-Kutta. 
    - PERFL.
    - Make a plot of the energy drifts. This will be like in Exercise 2 of Homework 1 from MSE 597 (Material Modeling). 
* Convergence of (one-step) schemes. 
* Mention stiff equations and reference where they are dealt with. 

