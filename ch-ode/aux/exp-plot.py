import numpy as np
import matplotlib.pyplot as plt


def main():
    domain = np.linspace(-5, 5, 1000)
    image = np.exp(domain)
    plt.plot(domain, image)
    plt.show()
    return None


if __name__ == "__main__":
    main()
