import matplotlib.pyplot as plt 
import numpy as np


def first_sol(t): 
    return 0.0


def second_sol(t, a=1): 
    if t <= a: 
        return 0.0
    else: 
        return (t-a)**2


def main(): 
    domain = np.linspace(-0.25, 1.5, 100)
    image1 = [first_sol(t) for t in domain]
    image2 = [second_sol(t) for t in domain]
    image3 = [second_sol(t, .5) for t in domain]
    image4 = [second_sol(t, .75) for t in domain]
    plt.plot(domain, image1)
    plt.plot(domain, image2)
    plt.plot(domain, image3)
    plt.plot(domain, image4)
    plt.show()
    return None

if __name__ == "__main__": 
    main()