Let a spring be attached to an object on one end and to a wall on the other, such that the object only moves in the dimension along the spring and such that the only force on the object be from the spring. 
Let $M$ be the mass of the object, and let $κ$ be the spring constant. 
Furthermore, coordinates are chosen so that the equilibrium of the object is at $0$. 
Furthermore, let $p$ and $v$ be the position and the velocity of the object at the time $0$, respectively. 
Then, the trajectory of the object is a solution to the differential equation
\[
	\ddot{u}(t) = - \frac{κ}{M} ⋅ u(t). 
\]
and the initial condition 
\[
	u(0) = p
	\textand
	\dot{u}(0) = v
	.
\]
The differential equation is from Hooke's law, which is introduced in Chapter~\ref{ch:mom}. 
It is called the \emph{Equation for Simple Harmonic Motion}\index{Equation for Simple Harmonic Motion}\index{simple harmonic motion}.
It is used to compute the heat capacity of solids in Chapter \ref{ch:bond}.

\begin{figure}[H]
\centering
	\includegraphics[width=0.5\textwidth]{ode-fig-shm-system}
	\caption{A mass attached to a spring.}
\end{figure}

The functions
\[
	\cos\left(\left(\frac{κ}{M} \right)^{\frac{1}{2}} ⋅ t\right)
	\textand
	\sin\left(\left(\frac{κ}{M} \right)^{\frac{1}{2}} ⋅ t\right) 
\]
are both solutions to the equation 
\[
	\ddot{u}(t) = - \frac{κ}{M} ⋅ u(t)
	.
\]
Furthermore, for any real numbers $a$ and $b$, the function
\[
	a ⋅ \cos\left(\left(\frac{κ}{M} \right)^{\frac{1}{2}} ⋅ t\right)
	+ 
	b ⋅ \sin\left(\left(\frac{κ}{M} \right)^{\frac{1}{2}} ⋅ t\right)  
\]
is a solution.
Furthermore, the relation
\begin{align}
	& a⋅\cos\left(
		\left(\frac{κ}{M} \right)^{\frac{1}{2}} ⋅ 0\right) 
	  + b⋅\sin\left( 
		\left(\frac{κ}{M} \right)^{\frac{1}{2}} ⋅ 0\right)  \\
	&= a 
\end{align}
holds, and the relations
\begin{align}
	& ∂\left(
		a⋅\cos\left(
			\left(\frac{κ}{M} \right)^{\frac{1}{2}} ⋅ t\right)
		+ b⋅\sin\left(
			\left(\frac{κ}{M} \right)^{\frac{1}{2}} ⋅ t\right) 
		\right)\left( 0 \right) \\
	&= \left( \frac{κ}{M} \right)^{\frac{1}{2}} ⋅ \left(
		- a⋅\sin\left(
			\left(\frac{κ}{M} \right)^{\frac{1}{2}} ⋅ 0\right)
		+ b⋅\cos\left(
			\left(\frac{κ}{M} \right)^{\frac{1}{2}} ⋅ 0\right) 
		\right) \\ 
	&= b ⋅ \left(\frac{κ}{M}\right)^{\frac{1}{2}} 
\end{align}
hold. 
Thus, the function
\[
	p 
	⋅\cos\left(\left(\frac{κ}{M} \right)^{\frac{1}{2}} ⋅ t\right)
	+ 
	v ⋅ \left(\frac{κ}{M}\right)^{\frac{-1}{2}}
	⋅\sin\left(\left(\frac{κ}{M} \right)^{\frac{1}{2}} ⋅ t\right) 
\]
is a solution to the equation and the initial condition.

Similarly, it can be proved the function
\[
	c_1 ⋅ \cos\left( \frac{c_2}{2⋅\constpi}⋅t - c_3 \right)
	,
\]
is a solution to the differential equation and initial condition, where $c_1$, $c_2$ and $c_3$ are 
\[
	\sqrt{b^2 ⋅ \frac{κ}{M} + a^2}
	, \textspace
	2 ⋅ \constpi ⋅ \left(\frac{κ}{M}\right)^{\frac{1}{2}}
	\textand
	\tan^{-1}\left(
					\frac{a}{b} ⋅ \left(\frac{M}{κ} \right)^{\frac{1}{2}} 
			 \right)
	,
\]
respectively. 
The number $c_1$ is called the \emph{amplitude}\index{amplitude}, the number $c_2$ is called \emph{frequency}\index{frequency}, and the number $c_3$ is called the \emph{initial phase}\index{initial phase}. 


\begin{figure}[H]
\centering
	\includegraphics[width=0.7\textwidth]{ode-fig-shm-sol}
	\caption{Solution to the Equation for Simple Harmonic Motion.}
\end{figure}
