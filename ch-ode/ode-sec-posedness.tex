%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Summary 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Differential equations are used for physical prediction.
This is done by finding a differential equation such that the behavior of a system is a solution to it.
Thus, for this to be useful, the differential equation must have a solution. 
Furthermore, if the solution is unique, then it is indeed the behavior of the system.
And, to account for measurement error, it must depend continuously on the initial conditions.
A dynamic ordinary differential equations is said to be \emph{well posed}\index{well posed} if it has a unique solution for the initial condition that depends continuously on the initial conditions. 
By the results in this section, in practice, most dynamic ordinary differential equations are well posed. 
This section is about the existence, uniqueness and continuity of solutions to dynamic ordinary differential equations. 

First, it is explained how a dynamic ordinary differential equation is equivalent to a first-order time-dependent ordinary differential equation. 
And, it is explained how this is equivalent to an integral equation. 
This reduction is used in the proofs in this section. 
Second, it is proved that solutions to many dynamic ordinary differential equations in practice exist.  
Third, it is proved that solutions to most dynamic ordinary differential equations in practice are unique. 
Fourth, it is proved that the time domain of the solutions can be extended maximally. 
Fifth, it is proved that solutions to most dynamic ordinary differential equations in practice depend continuously on the initial conditions. 


\scenebreak

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% First-Order Reduction and Integral Form 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\input{ode-sub-reduction}


\scenebreak

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Existence
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Euler's Method: Introduction
Now, \emph{Euler's Method}\index{Euler's Method} is described. 
It is used in the proof of the next theorem. 
It is similar to the definition of integration in Chapter~\ref{ch:int}.
And, it is the prototype of the numerical methods in Section~\ref{ode:sec:numerics} and Chapter~\ref{ch:dt}. 

% Euler's Method: Set Up
Let 
\[
    \dot{u}(t) = φ(u(t))
\]
be a differential equation whose solution is the trajectory of a particle. 
And, let $p$ be the position of the particle at a time $a$. 

% Euler's Method: One Step
Let $ε$ be a small positive real number. 
Then, intuitively, the velocity of the particle is approximately $φ(f(a))$ on the interval $[a, a+ε]$.
Thus, the position of the particle at the time point $a+ε$ is approximately
\[
    p + φ(f(p))⋅ε
    .
\]
This is similar to integration. 
But, unlike integration, because the velocity of the particle is not yet known, the sample point is the point $a$ instead of any point in the interval $[a, a+ε]$.

% Euler's Method: Many Steps
This reasoning is continued. 
Let $b$ be a time point that is later than $a$.
Let $a_0$, $\ldots$, $a_{n-1}$ and $a_n$ be points such that the relation 
\[
    a = a_0 < a_1 < \ldots < a_{n-1} < c_a = b
\]
or the relation 
\[
    a = a_0 > a_1 > \ldots > a_{n-1} > c_a = b
\]
holds and such that the distance between adjacent points is small. 
Let $p_0$ be $p$.
And, recursively, for each index $i$, let $p_{i+1}$ be 
\[
    p_{i} + ε⋅φ(p_i)
\]
Then, intuitively, for each index $i$, the position of the particle at the time point $a_i$ is $p_i$. 
Let $f$ be the linear interpolation of the values $p_0$, $\dots$, $p_{n-1}$ and $p_n$ at the points $a_0$, $\dots$, $a_{n-1}$ and $a_n$. 
This function $f$ is the approximation to the solution from Euler's Method from the initial time point $a$ to the time point $b$. 

The points $a_0$, $\dots$, $a_{n-1}$ and $a_n$ are called the \emph{time samples}\index{time samples}. 
The numbers of the form $a_{i+1} - a_i$ are called the \emph{time increments}\index{time increment}. 
The size of the time increment is called the \emph{step size}\index{step size}.
The time steps are said to be \emph{uniformly space}\index{uniformly space} if the time increments are equal. 


The next lemma is called Ascoli's Theorem.  
Intuitively, the condition in it is that the functions are uniformly uniformly continuous.
% That is, it says that the space of uniformly uniformly continuous functions is compact. 

\input{ode-thm-ascoli}


The next theorem says that there are solutions for most dynamic ordinary differential equations in practice.  

\input{ode-thm-existence}


\scenebreak

The next twos theorems use Liptchitz continuity. 
% Furthermore, this condition is used for optimization in Chapter~\ref{ch:opt}. 
A function $f$ is said to be \emph{Lipchitz continuous}\index{Lipchitz continuous} if a positive real number $c$ exists that is less than $1$ such that the relation
\[
	\abs{φ(x) - (y)} ≤ c⋅\abs{x - y}
\]
holds. 
Intuitively, this condition is that the variation of the function is bounded. 
That is, the graph of the function may have corners, but not cusps. 
If a function is Lipchitz continuous, then it is continuous. 
And, if a function is differentiable, then it is Lipchitz continuous. 

\begin{figure}[H]
\centering
    \includegraphics[width=0.5\linewidth]{ode-fig-lipchitz}
    \caption{The left is a graph Lipchitz continuous function. The right is a graph of a function that is not Lipchitz continuous.}
\end{figure}


\scenebreak

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Uniqueness
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\input{ode-ex-not-unique}


The next theorem says that solutions are unique for most dynamic ordinary differential equations in practice. 


\input{ode-thm-uniqueness}


The proof of the theorem of existence used the convergence of a subsequence from the sequences of approximations from Euler's Method. 
But, if the defining function is Lipchitz, this original sequence of approximations converges, that is, Euler's Method converges. 
If it did not converge, then there exists a subsequence that does not converge to the solution. 
But, because the sequence of approximations is bounded, this subsequence has a subsequence that converges to a different limit, which is a contradiction. 

\scenebreak

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Maximal 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

The theorem of existence only says that a solution $f$ exists for some time interval $I$. 
But, if another solution $g$ exists on an interval $J$ that contains $I$, then the restriction of $g$ to $I$ is $f$ because the solution is unique. 
That is, solutions and their time intervals are partially ordered by their time intervals such that any totally order subset of them has a maximal element. 
Thus, by Zorn's Lemma, the maximal time interval is the union of the time intervals of all solutions. 
That is, the time interval can be maximally extended.

But, this does not mean that a solution exists for all time. 
The solution may leave the domain of the defining function. 
And, even if the domain of the defining function is not restricted, then, the solution may still diverge at the endpoints of time interval. 
For example, the function $\tan(t)$ is a solution to the differential equation 
\[
    \dot{u}(t) = 1 + (u(t))^2
\]
and the initial condition 
\[
    u(0) = 0
    . 
\]
And, this solution $\tan(t)$ is can be defined on the interval
\[
    \left( -\frac{\constpi}{2}, \frac{\constpi}{2} \right)
    , 
\]
but this domain cannot be extended because the solution diverges at the endpoints. 


\begin{figure}[H]
\centering
    \includegraphics[width=0.25\textwidth]{ode-fig-tan-graph}
    \caption{The graph of the Tangent Function.}
\end{figure}


If the maximal time interval is bounded, then the solution leaves any compact subset of the domain of the defining function. 
This is proved by contradiction. 
Assume that the maximal time interval is bounded and that a compact subset of the domain of the defining function is such that the solution does not leave it. 
Let $a$ be a boundary time point of the maximal time interval. 
And, let $b$ be the limit of the solution at the time point $a$. 
Then, there exists a solution for the equation with the initial condition that the value at $a$ is $b$. 
Thus, the point $a$ is not a boundary point of the maximal time interval.
This is a contraction. 
Thus, if the maximal time interval is bounded, then the solution is unbounded or reaches the boundary of the domain of the defining domain. 


\scenebreak 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Continuity 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

The next theorem says that the solution depends continuously on the initial condition.  
The function $\exp(t)$ is defined in Section~\ref{ode:sec:some-sols}. 

\input{ode-thm-continuity}