# Outline of Resonance

## Introduction

* Notes. 
    - For each equation, explain where the characteristic equation comes from, explain what the modes mean and so on. 
* SHM.
    - Explanation. Use forced simple harmonic motion as an example. If the forcing term is too slow, then the pull of the spring cannot generate momentum. If it is too fast, then it counter acts the current motion.
    - Beats.
* Wave Equation.
    - Use musical string instruments as an example of modes. 
* Other examples. 
    - Standing Schrödinger Equation.
    - (Harmonic?) Maxwell Equations. 

  
## Discrete Spectra

* Characteristic values and spectral values.
    - They are orthogonal. 
* Properties.
    - The spectrum of a compact operator is countable and has at most one limit point.
    - Spectral radius?
* Spectral Theorem for compact operators.
    - The best way to go about proving this is probably to prove that operator can be approximated by finite-dimensional operators and that the characteristic vectors of these finite-dimensional operators converge (in some sense) to the characteristic vectors of the operator in question. Is this possible? 
    - A compact operator essentially means that it can be approximated by finite-dimensional operators. 
        * Why is it easier to prove that an operator is compact? There should be an example, say for the Schrödinger Equation for an electron state. 


## Fourier Sums

* Notes. 
    - Do with the function $exp(t)$, and say that this is used because it generalizes to complex numbers, which will be useful for the Schrödinger Equation. 
* A basis is actually formed, that is, they approximate functions. This is the Fourier Theorem and Dirichlet's theorem. (How much does the Spectral Theorem cover?)
* Explain that using these basis functions are useful for waves phenomena in general. 
    - This should be done for periodic multidimensional states. 
    - The basis functions are orthogonal, which makes FEM easier. 
* Fast Fourier Transform.
    - Check out this [video](https://yewtu.be/watch?v=nmgFG7PUHfo). It is about using the same points multiple times. 
    - Should this be in the third volume? 
* The Gibbs Phenomenon? 


## Solving Finite-Dimensional Characteristic Equations

* Notes. 
    - Mention that more methods will be explained in Volume 3. 
* By Abel's theorem, eigenvalues cannot be found algebraically, that is, in a finite number of algebraic operations. (Characteristic values are solutions to the characteristic polynomial.)
* Rayleigh quotient. 
* The Power Method. 
    - Inverse Iteration.
        * The matrices $A^{-1}$ and $A-μ⋅I$ have the same eigenvalues of $A$. Thus, the matrix $(A-μ⋅I)^{-1}$ also has the same eigenvalues of $A$. Furthermore, the biggest eigenvalue of $(A-μ⋅I)^{-1}$ is the eigenvalue of $A$ that is nearest to $μ$. Thus, Power Iteration on $(A-μ⋅I)^{-1}$ can find the nearest eigenvalue to $μ$.
        * This converges quickly if $μ$ is much nearer to the eigenvalue than to others. 
        * This is the standard method to get an eigenvector for a known eigenvalue. 
    - Quotient Iteration. 
        * Nothing can be said about which eigenvalue this is. 
        * The increase in speed is because the ratio of the two largest eigenvalues for the matrix $(A-λI)^{-1}$ gets bigger and bigger. 
* Hesenberg form. 
    - This speeds up iteration. It reduces the number of multiplications, and, because multiplication by $A$ is done over and over again, this is a significant speed up. (Does the matrix have to be sparse? Does it help with Inverse Iteration?)
    - I think that the basis has two be changed back at the end. (If eigenvectors are wanted.)
    - If the matrices is self-adjoint, then this is tridiagonal. 
* After finding the smallest spectral value, how is the next one found? 


## Approximating Infinite-Dimensional Characteristic Equations

* Notes. 
    - Use FEM. 
    - I guess that near operators have near characteristic vectors and characteristic values. 
    - Approximation of self-adjoint operators should probably be self-adjoint. How is this guaranteed? (It is guaranteed for FEM?) 


## Continuous Spectra

* Notes. 
    - What is an example of a bounded operator with a continuous spectrum? 
    - Should I bother with unbounded operators? I think that they are just used for the Born Rule in quantum mechanics. I guess say this. 


## Fourier Integrals

* Path. 
    - Intuitively, these represent all frequencies. 
    - They are still eigenvector of the operator? 
    - It just needs to be proved that they do indeed form a basis. 