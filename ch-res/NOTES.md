# Notes for Resonance

## Need to Understand

* Maybe the motivation of complex spaces is to guarantee the existence of spectral values (from the Spectral Theorem). It is just another completion. And, then regularity is proved. 
* What is the point of the Spectral Theorem if it isn't constructive? For example, I don't think that it can be used to get Fourier stuff. 
* Does the Spectral Theorem replace all the Fourier proofs? 
    - If so, then state this result a Fourier's Theorem.
    - ALso, Parseval's theorem means that the transformation in unitary. (Although, I am not quite sure what the transformation really is.)
* Adjoint and self-adjoint.
    - By the (Riesz) Representation Theorem, there is a correspondence between vectors and operators. The operator combined with the inner product with another vector forms a linear functional; thus, it corresponds to another vector. This map is called adjoint of the original operator. 
    - But, what is the original point? 
        + Does this naturally come up in the proof of the Spectral Theorem? 
    - Maybe the desired relation is just $\langle v, f(w) \rangle = \langle f(v), w \rangle$. And, then the definition of adjoint is made just to generalize this to normal operators or whatever. In other words, it is not the concept of adjoint that matter, but only the concept of self-adjoint. (And this property then implies an orthogonal basis of characteristic vectors.)
* What is frequency? 
    - Fourier series doesn't seem to quite cover it because a saw-tooth function has a definite frequency, but it would be represented as a sum of many terms of the Sine Function and the Cosine Function. 
    - Is resonance how frequencies (such as for light) are picked out experimentally? How are frequencies picked out computationally?
        * How do antennas work?
    - Is this chapter really just about frequency. 
* How are eigenvalues (and general functional analysis) involved with resonance?
    - I think that the desired modes are eigenvalues of the differential operator. Being an eigenvalue of $\mathrm{d}t$ – whatever this means – implies growth. 
    - I think that resonance is about the static part of the dynamic equation. 
    - Why are modes orthogonal? What really is a mode? Why are they periodic? (Note that all confined waves are periodic because each moving wave just bounces back and forth from the wall.)
* What is a mode? 
    - They decay the slowest. 
    - They are the easiest to amplify. 
* Why do higher modes matter less? (I get that they correspond to bigger energies.)
* Supposedly these ideas can be used to solve general (linear) PDEs. Is this ever practical?
* I don't really understand the idea of a continuous spectrum. Aren't the bases countable? 
* Are characteristic equations useful outside of resonance? 
* Does nonlinear resonance make sense? 
    - Maybe it is still a scaling, but one that depends on the vector too, that is $f(v)⋅v$ instead of $c⋅v$. 
* Do characteristic equations arise from minimization? 
* Are self-adjoint operators self-adjoint on subspaces? This would be required to apply numerical schemes. 


## Vocabulary

* I don't want to use the term *Fourier sum* because it is using someone's name outside of a theorem. But, I should have something called *Fourier's Theorem*. I could use *trigonometric expansion*. 
* Should I use *characteristic root* instead of *characteristic value*? Should I only use *spectral value*? 


## Figures

* Have a figure of successively better approximation of a function by taking more terms of the Fourier series. This is similar to the figure for Taylor's Theorem. 


## Possible Material

* Maybe instead of using a concept of spectral content, I should use the concept of *spectral density*, which is just the density function on the real numbers. 
* When I introduce the concept of self-adjoint, list properties of such operators. 
* I think that Parseval's identity gives an error to how good an approximation (by finitely many terms) is. 
* I think that Neumann stability is sort of a cheat stability for what could be called the convergence formula. (Honestly, I should probably call it the convergence formula.)
* Spectral Method: Find the characteristic vectors that correspond to the biggest characteristic values and use them as a basis for a numerical scheme. (They are orthogonal, so projection is easy.) The system can be discretized before finding these anyways. 


## Simple Harmonic Motion

* Forced. 
    * Consider a forced simple harmonic motion with the forcing term $F⋅\cos(ω*t)$. 
    One can guess a solution of the form $C⋅\cos(ω⋅t)$. 
    Putting this guess into the defining equation
        $$
          \ddot γ(t) = - \frac{κ}{M}⋅γ(t) + F⋅\cos(ω*t)
        $$
        gives 
        $$
          C⋅ \left(\frac{κ}{M} - ω^2 \right) ⋅ \cos(ω⋅t) = F⋅\cos(ω*t),
        $$
        Thus, this is a solution if $C$ is 
        $$
          \frac{F}{M⋅\left(\frac{κ}{M}\right)^2 - ω^2},
        $$
        that is, $C$ is 
        $$
          \frac{F}{M⋅(ω_0)^2 - ω^2 },
        $$
        where $ω_0$ is the angular frequency of the solution without the forcing term. 
    * This shows that the amplitude it much higher when the frequency of the forcing term is close to the natural frequency. 
    * In an actual situation, there is friction. (So the solutions cannot actually become infinite at the natural frequency.)
    * This result is a bit confusing to me. I guess that this is supposed to be the steady-state solution. But, how can one come to this conclusion? 


## General

* Explanation. Swing. Being on a swing is an example of resonance. If one pushes another on a swing on the same frequency of the motion, then they go higher. If they push on a different frequency, then they don't go as high. 
* Is it beneficial to know that characteristic vectors are orthogonal? For example, for the Static Schrödinger Equation, is it beneficial to have the idea that the next minimal-energy state is orthogonal to the previous ones?
* Finding eigenvectors is essentially the same thing as finding null vectors? For example, a solution to the static equation $0 = ∂_t(ψ) = f(ψ)$ is also a solution to $(I+f)(ψ) = ψ$. The function  $I + Δt⋅f$ is a finite-difference step, that is, an eigenvector of it doesn't change.
* Do I only need to care about self-adjoint operators? I think so. 
* The actual diagonalization of the operator is not the actual goal? Is decomposing the spacing into perpendicular characteristic vectors only important when treating more general problems?
* I think that the calculus of variations can be used to find the lowest energy state. (Thus, it applies to waves and other dynamic equations.)
    - A direct variation approach:
    - In QM, the ground energy is at most $\langle φ, H(φ) \rangle$ for any wave function $φ$. This is what should be kept in mind with the following; I am not sure how general this is.
    - What's a situation that's not QM? Should this just be put in a numeric section of QM?
    - Pick a form of a function with many parameters. Do gradient descent to minimize this quantity.
    - This gives an upper bound estimate, which is supposedly often all that one needs.
    - Although the wave function found often does not resemble the actual ground state, but the value of this quantity is still very often to the value of this quantity for the ground state.
* What is the dampening in a microwave? 
* The Fourier transform gives the spectral density. 
* The adjoint is a generalization of the complex conjugate. 


## The Exponential Solution 

Many linear equations have a "simple" exponential solution. 
This is done by taking the exponential of an operator. 
Computationally, the operator can be discretized by restricting it to a finite-dimensional space. 
❗This finite space cannot be a finite-element space because then successive applications of the operator (as is used in the series) would make any basis function $0$. (Why does this not matter for normal time stepping schemes? And, it is weird that this is different than normal time stepping.)
And, if the matrix can be diagonalized, then its powers are simple to compute. 
Thus, if the eigenvalue could be found, then I guess that this becomes a practical numerical scheme.
(Is this the spectral method?)

❗Furthermore, I guess that the eigenvectors can mean things. 
And, I guess that the most important thing that they can show is resonance. 
A positive eigenvalue means growth; negative, decay; imaginary, oscillation. 
(The Hamiltonian still has oscillation with real eigenvalues because there is already a complex factor.)