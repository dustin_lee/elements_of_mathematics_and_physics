<!-- LTeX: enabled=false -->
<!-- - cspell:disable - -->

# Outline of Subatomic Particles


## Electrons

**Discovery and Properties**:
* Recall that current carries have mass and, in metal, are seen to be negative by the Hall Effect.
* Charge and mass
	+ Cathode rays produce electrons. Deflection shows that electrons come in a constant proportion of electric charge to mass.
	+ Figure for the deflection? 
	<!-- + Should I mention that SR is used for the most accurate computations because the speed is about $0.1c$ to $0.2$c?  -->
* The Oil-Drop Experiment:
	+ ❗But, how is it know that these are electrons? 
	+ Compute the charge of charge carriers. 
	+ It is noticed that they come quanta, that is, electrons are particles.
	+ Figure? 
* They have a mass that is very small compared to atoms.
* It will be seen that they are a subatomic particle.

**Drude Model**:? 
* Don't go into details. 
* Mention that a better description will be done in Electronic Structure of Solids. 
* Probably don't even give it a name. 

**Xray Tubes**:
* Design: 
* Light is emitted because the electrons rapidly decelerate.
* A little more evidence that this is light
	+ Refraction through crystals? Well, this is not much evidence when it is explain that all particles have wavelength. 


## Photons

* The Photoelectric Effect.
	- It is noticed that strong light ionizes metals, that is, the metals become positively charged.
	- Explanation. Explain how light should transfer energy to a material by the electric field affecting the electrons and nucleus. (More energy should be imparted to the electrons since their mass is so much smaller?) Thus, enough energy should free electrons.
	- Furthermore, the electrons freed by higher-frequency light have more energy.
	- But, only light with a high-enough frequency can do this – regardless of intensity. This indicates that electrons have a specific bonding energy and that light consists of particles.
	- Introduce Planck's constant. It is the slope of the graph, which appears to be the same for any element. This gives a decent way to measure it.
	- Scattering of photons and electrons:
* Photons are Particles.
	- The experiment that shows that photons cannot be split.
	- The Two-Slit Experiment for photons.
	- They still have a wave length. This is de Broglie's relation. 
		* Scattering verifies this. 
	- Implications. 
		* Thus, the current theory of electricity and magnetism is wrong, and it indicated that the electromagnetic field comes is discrete packets. This will be fixed in the Electromagnetic Interaction.
		* There is a limit to how accurate one can make measurements with light because the wavelength and the position of a wave train cannot be well determined. This preludes the Uncertainty Principle. (Well, isn't this already known?)


## Radioactivity

* Notes.
	- Explain that being stable means that there is not a feasible decay. (Energy is the main restriction.) (Can the half-life be calculated from the energy differences to the possible decays?)
	- Include the table of the nuclei. Make the observation that there are more stable isotopes with an even number of nucleons.
	- Give the scale of energy of these decays. 
* Introduction.
	- Explain that substances like uranium can put images on a photographic plate in the dark. Many other radioactive substances have been also discovered. And, after radioactive decay, the element is not the same anymore.
	- Explain half-life.
	- Radioactivity cannot be altered by temperature, pressure, electric fields, magnetic fields or binding with other atoms.
	- There are three main ways of decay – alpha, gamma and beta. 
		+ This can be seen because they each have different penetrating capabilities. Give numbers for these. Furthermore, it can be seen there are three different particles, and that one is positively charged, one is negatively charged and one is neutral.
		+ And, the change of Elements. 
		+ Two are charged 
		+ Same independent of source
		+ Mention what these really are. 
* Alpha decay: 
	- The neutrally charged one can be seen to be a helium nucleus.
	- To see that it is a helium nucleus, a vacuum is separated from a radioactive substance so that this particle can penetrate to the vaccumm. (Then, using spectrometry, it can be seen that this particle is helium.)
	- The energy take on discrete values.
	- Because the strong nuclear force has a short range, charged particles can tunnel out and then the main force on them is the electromagnetic force.
	- Because the strong nuclear force has	a short range, it makes sense that alpha decay is only seen in larger nuclei.
	- Alpha decay is just so much more likely than other decays into a different nuclei
* Beta decay: 
	- The negatively charged one can be seen to be an electron.
		+ How?  
	- Electrons are emitted at different energies during this decay. Thus, to conserve energy, it is reasonable that another particle – which is called the neutrino – is emitted. (Give the actual name of this neutrino that is, the electron antineutrino.)
		+ Its mass is small because the maximum energy carried off by the electron is almost equal to the difference in (rest) energy between the proton and neutron. 
* Positron emission and electron capture:
	- This is needed. These can only occur if additional energy is supplied. Furthermore, with positron emission, normally only the photons are detected because the positron normally annihilates with an orbiting electron.
	- Positrons. (Is this the place to do it?)
		+ Introduce them experimentally. 
		+ A electron-positron pair can annihilate each other, and the energy of the light corresponds to what is predicted from special relativity.
		+ A photon can generate an electron-positron pair.
		+ These last two points say that elementary particles are not elementary in the classical sense.
* Gamma decay:
	- The neutral particle is seen to be a photon.
	- It does not change the element
		+ This indicates that the nucelus can exist in an excited state. 
			* Normally this happens from an excited state after an alpha decay?
	- It takes on discrete values. 


## Atomic Structure

* Radioactivity gives a way to shoot doubly positive helium atoms. (Radioactivity is in more depth later in Nuclear Interactions.) The Gold-Foil Experiments shows that the nucleus is much more massive and denser than electrons. (By looking at how many atoms are reflected, the atomic diameter can be approximated.)
	- Give the number of how much more massive helium nuclei are than electrons (about 8000 times). 
* Electron orbit. 
	- But, Earnshaw's theorem shows that electrons must be in motion, which means that the classical laws of physics do not work. (Does it really though?) (This follows from either Gauß's law or properties of harmonic functions.)
	- Thus, atoms appear to be electrons orbiting a heavy nucleus.
 		+ I need to give a reason why one would think that electrons are part of the atom rather than independent particles. (Supposedly, there is a cathode experiment that separates the electrons from the gas and leaves the particles in the gas ionized.) (Also, they are sometimes independent particles in solids.)
	- The electrons do not collapse into the nucleus. 
	- Furthermore, by the Photoelectric Effect of the next section, it appears that the energies of the electron orbits are discrete. 
	- Give numbers: 
		- Weight of the helium necleus to an electron
		- Diamater of the electron orbit to the diamater of a nucleus
		- Electrons seems to be point chargers. Give a number that they are less than. 
* Spectra.
	- Notes. 
		+ Make sure to mention that nonvisible light is included. 
	- The energy levels are discrete.
		+ Elements can indeed only absorb certain frequencies of light.
		+ In collision experiments, atoms can only be excited if they have enough energy, otherwise they just collide normally. (This is the Franck-Hertz Experiment.)
		+ Identifies substances.
		+ Mention cooling lasers.
	- The Term Principle
		+ That light is from electrons changing orbits indicates that the electron orbits are discrete.
		+ The sum of two emitted energies corresponds to an electron skipping over an intermediate state.
		+ Some transitions are seen more than others. Some are not seen at all.
		+ The ground states are by far the most common. (In general, the lowest energy is what a system looks for because it is the most stable, that is, once it happens, it cannot be as easily undone.)
		+ The (Rydberg-Ritz) Combination Principle?
	- This explains florescence.
	- Should I talk about the width of spectral lines?
		+ This is about resonance.
		+ Mention that the splitting of spectral lines will have to be dealt with later?


## Nuclei 

**Protons and Neutrons**: 
* Protons.
	- Mention Proust's hypothesis.
	- Hydrogen is never doubly ionized, helium is never triplely ionized and so on.
	- The charge of the nucleus is integer multiples of $+e$.
	- The place on the periodic table depends on the charge of nucleus.
	- Explain Rutherford's experiment that showed that protons (hydrogen nuclei) exists in the other elements.
* Neutrons.
	- The nucleus cannot just be composed of protons (charges particles) because its mass is not proportional to the charge.
	- Isotopes: The nuclei of a single element are not all the same mass.
	- Verification: 
		- ?? Heavy tracks can be observed to originate in the middle of a cloud chamber. This indicates that a neutral particle entered and was involved in a reaction in the middle of the chamber. 	
		- ?? Should I mention the original way that neutrons were observed? (There was a radioactively that did not have correct attenuation.)
		- To see as neutron, it needs to be knocked out of a nucleus. (Only charged particles can be detected directly.) The reaction
		$$
				\mathrm{Be}^{9} + \mathrm{He}^{4} → \mathrm{C}^{12} + n + γ
		$$
		produces a neutron.
			+ Is this a collision? 
			+ But, how is it then detected? 
	- Since neutrons are roughly the mass of protons, this justifies Prout's hypothesis. 
* The Nuclear Force
	* It is much stronger than the electromagnetic force.
	* Must be short range.
	* Binding energy.
		- Can be calculated with relativity.
		- Explains why the mass of atoms are not quite integer multiples.
	* This exists between both neutrons and protons.
		- I think that this can be seen by looking at mirror nuclei
	* Why is a nucleus with an even number of nucleons more stable?
	* Mention that this will be dealt with in the Discretization of the Nuclear Force. 

**Reactions**:  
* Things to include: 
	- Slower neutrons can have bigger cross sections
		+ "Intuitively, the neutrons have more time to interact"
	- Reactors are possible the delayed critical and prompt critical are different. 
	- Give a number to demonstrate how much more energy these are than chemical reactions
* Fission.
	- Nuclear Reactors and Nuclear Bombs.
		+ Hitting uranium with neutrons causes the uranium to decay. Neutrons are used because they are not repelled by the electrical force so that they can actually hit the nucleus.
		+ A chain reaction can occur by having enough uranium together (the critical mass). Mention that this needs to be enriched uranium.
		+ Nuclear bombs can be created with assembling a critical mass. I guess explain both the barrel approach and the implosion approach. I think that in the implosion approach, the material is actually compressed enough to create a critical mass.
		+ Nuclear reactors
			* Explain the importance of delayed neutrons.
			* Explain moderators. Slow neutrons have bigger cross sections. (Perhaps, roughly say that slower neutrons have bigger cross sections because they have more time to interact with the strong force, and that this will be explained later.) How does this tie in with resonance.
* Fusion.
	* Thermonuclear bombs.
	* Explain why fusion reactors are so difficult.	


## Other Subatomic Particles (And their classification under the Standard Model)

* Other stuff: 
	- Quarks. 
		+ These are shown to exist in a similar way to the Gold-Foil Experiment. 
	- Positrons? 
* Introduction. (The Zoo.)
	- Recap. So far, electrons, positrons, protons, neutrons, neutrinos, positrons and photons have been introduced. 
	- Cosmic rays. 
		+ Include muons, pions, kaons and so on. 
	- Particles from accelerators? 
	- Even more particles, such as the Higgs. 
	- Explain that there are not fundamental particles in the sense that they make up everything else. 
* (Scheme.) Leptons and quarks. 
	- Normal particles. 
	- Mediators. 
	- Their antiparticles. 
* Conservation Laws. (I want a better name.)
	- If a reaction is not observed, then there should be a reason.
	- Baryon number.
	- Stability of the main subatomic particles.
		* That the electron is the smallest charged particle explains why it is stable.
		* That the proton is the smallest baryon explain why it is stable.
	- Strangeness.


## Detection and Acceleration 

* Notes:
	- This will probably have several figures. 
* Introduction. 
	- Explicitly mention that only charged particles can be directly detected.
	- Emphasize that beams of particles are normally used.
	- Mention the form of targets. Often the target is stationary, such as a tank of hydrogen to collide with protons.
* Detectors.
	- Spots on photographic plates are caused by chemical reactions. They require the extra energy of the photo to make it happen.
	- The Geiger counter.
	- Solid-state detectors.
		* The radiation makes electron-hole pairs.
	- The cloud chamber.
		* A very pure liquid can be super heated (raised above normal boiling point). Putting an impurity into a super-heated liquid caused an explosion. This is the idea of cloud chambers.
		* Should I mention that the trails left in cloud chamber is like the trails left by airplanes at high altitudes?
		* Nuclei are rarely deflected in a cloud chamber. What do I mean by this?
	- Wire/drift chambers?
* Accelerators.
	- Linear accelerators.
	- Clycrotrons.
	- Syncrotrons.
		* Storage rings can be used to hold particles until they are needed.
