%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Protons
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

The electric charge of atoms is always observed to be integer multiples of the charge of electrons. 
This indicates that the charges of nuclei are integer multiples of the charge of electrons. 
An atom is electrically neutral when it has the exact number of electrons to balance the charge of the nucleus. 
Furthermore, hydrogen atoms are never observed to be doubly ionized, helium atoms are never observed to be triply ionized and so on. 
This indicates that nuclei contain an integer multiple of a particle whose charge is the negation of the charge of the electron. 
And, the atomic number of a chemical element is how many of these particles are in its nucleus. 
For example, in alpha decay, the chemical element moves two places to the left on the periodic table because the atomic number of a helium atom is $2$. 

Such a particle is called a \emph{proton}\index{proton}, and it is denoted by $\proton$\label{not:proton}. 
The mass of a proton is
\[
    \SI{1.672 622e-27}{\kilo\gram},
\]
that is, it is about $1800$ times more massive than an electron.
The charge of a proton is exactly the negation of the charge of an electron, 
which has been verified by experiment to within one part in $10^{20}$.
The diameter of a proton is about $10^{-15}$ meters.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Neutrons
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

The mass of nuclei of the same chemical element vary. 
And, this variation is approximately by integer multiples. 
This indicates that the nucleus is also made of another particle. 
This particle is called a \emph{neutron}\index{neutron}, and it is denoted by $\neutron$\label{not:neutron}.
It is electrically neutral. 
Its mass is 
\[
    \SI{1.674 927e-27}{\kilo\gram},
\]
which is approximately the same as the mass of a proton
The diameter of a neutron is about $10^{-15}$ meters.
Collectively, protons and neutrons are called \emph{nucleons}\index{nucleon}. 

\emph{Isotopes}\index{isotope} are nuclei of the same chemical element with different number of neutrons. For example, the isotope 
\[
    \isotope{C}{12}
\]
has $6$ protons and $6$ neutrons, and the isotope
\[
    \isotope{C}{14}
\]
has $6$ protons and $8$ neutrons.

The mass of nuclei do not vary exactly by integer multiples of the mass of the neutron and mass of the proton. 
This is because of the mass of the binding energy. 
This is called the \emph{mass defect}\index{mass defect}.
It agrees exactly with the Mass-Energy Equivalence. 


Only electrically charged particles can be detected directly, that is, a neutron must be detected indirectly. 

To detect a neutron, it is first separated from a nucleus, and then another reaction is used to detect it. 
% The second reaction is done at some distance from the first reaction. 
And, because the attenuation of neutron is different from other particles, by keeping these reactions at a distance from each other, it can be determined that the particle involved is not any previously known particle. 
For example, a neutron can be isolated by the reaction
\[
    \isotope{Be}{9} + \isotope{He}{4}
    \yield
    \isotope{C}{12} + \neutron + \photon
    .
\]
Then, at some distance, the neutron can cause the reaction
\[
    \neutron + \isotope{U}{238}
    \yield
    \isotope{U}{239} + \photon
    .
\]
% !! Not a verified common reaction. 
% !! More about the distance. Tracks start in the middle of a cloud chamber.


\begin{figure}[H]
    \centering
    \includegraphics[width=0.5\textwidth]{satm-fig-table-of-nuclei}
    \caption{Table of nuclei.}
\end{figure}


\scenebreak 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% The Nuclear Force
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Because protons are electrically charged, there must be an effect that keeps contained in the small space 
And, this effect must stronger than the electric force. 
It is called the \emph{nuclear force}\index{nuclear force}. 

Bigger nuclei require more neutrons per proton to be stable. 
For example, the most common isotope of helium has two protons and two neutrons, and the most common isotope of lead has 82 protons and 126 protons.
This indicates neutrons are also involved with the nuclear force. 
Still, nuclei are not observed to have to many neutrons, which is explained in Chapter~\ref{ch:pstats} by neutrons being fermions. 

Furthermore, the nuclear force is equally strong between two protons, between two neutrons, and between a proton and a neutron.
This is seen by looking at the binding energy of \emph{mirror nuclei}\index{mirror nuclei}, that is, nuclei with the same total number of protons and neutrons. 

The nuclear force must be short range because it does not affect the force between atoms. 
The size of the potential of the nuclear force between two nucleons is 
\[
    -g ⋅ \frac{e^{-d/a}}{d}
    ,
\]
where $d$ is the distance between the nucleons, where the constant $g$ is 
\[
    \SI{1.23}{\mega\electronvolt}
    ,
\]
and where the constant $a$ is 
\[
    \SI{0.7}{\femto\meter}
    .
\]
% !! These numbers are not verified. 
% !! How is this found, that is, how is it tested empirically?  

Alpha decay is when some protons and neutrons escape from a nucleus. 
Classically, this is not possible because the nuclear force is stronger than the electromagnetic force.
But, in Chapter~\ref{ch:qm}, it is explained that this happens because of quantum tunneling.


\begin{figure}[H]
    \centering
    \includegraphics[width=0.6\textwidth]{satm-fig-nuclear-force}
    \caption{Nuclear potential compared to the electromagnetic potential for two nucleons. The nuclear force is stronger at distances around the diameter of nuclei.}
\end{figure}


\scenebreak 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Chain Reactions, Reactors and Bombs
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\emph{Fission}\index{fission} is when a nucleus decays into other nuclei. 
And, \emph{fusion}\index{fusion} is when nuclei combine into another nucleus. 
For fusion to happen, the nuclei must overcome the nuclear force. 
Thus, for fusion to happen, at least one of the nuclei must be moving fast and must be separated from its electrons. 
Fusion is the source of energy in the sun and other stars. 


\begin{figure}[H]
    \centering
    \includegraphics[width=0.5\textwidth]{satm-fig-binding-energy}
    \caption{Binding energy per nucleon. Fusion is energetically favorable at small atomic numbers, and fission at big atomic numbers.}
\end{figure}


For two nuclei to overcome their electromagnetic repulsion to interact by nuclear force, they must be moving very fast. 
For example, in the sun, fusion only occurs at its core, whose temperature is $\SI{15e6}{\degreeCelsius}$. 
But, because the neutron is electrically neutral, it can more easily interact with nuclei by the nuclear force. 

Using neutrons is the basis of nuclear reactors and nuclear bombs. 
For example, the reaction 
\[
    \isotope{U}{235} + \neutron
    \yield
    \isotope{Ba}{141} + \isotope{Kr}{92} + 3\neutron
    + \SI{200}{\mega\electronvolt}
\]
% !! Not verified 
produces three neutrons from one neutron. 
These three neutrons can then produce three more such reactions, that is, this is a chain reaction. 
A substance is said to be \emph{critical}\index{critical} if more than one neutron is produced per neutron. 
Thus, it is reasonable to expect that a sufficient amount of uranium in a sufficiently small space will be an explosion.
This is a fission nuclear bomb.
Fission bombs that weigh $\SI{4500}{\kilo\gram}$ can release more energy than  $\SI{21e6}{\kilo\gram}$ of TNT.
% !! Not verified: Also, I want the weight of the radioactive material, not the bomb.


\begin{figure}[H]
    \centering
    \includegraphics[width=0.3\textwidth]{satm-fig-fission-bomb}
    \caption{
        Designs for fission bombs. The top design uses a barrel to shoot a piece of uranium into another piece of uranium. 
        The bottom design implodes a sphere of uranium into a plutonium core.
        The second design is more difficult to engineer, but it uses less uranium.
    }
\end{figure}


For a nuclear reactor, the chain reaction must be slowed down and controlled. 
This is done by two phenomena.

First, there is a difference between prompt critical and delayed critical. 
In \emph{prompt critical}, the neutrons are produced immediately. 
For example, the reaction
\[
    \isotope{U}{235} + \neutron
    \yield
    \isotope{Ba}{141} + \isotope{Kr}{92} + 3\neutron
    + \SI{200}{\mega\electronvolt}
\]
happens immediately. 
In \emph{delayed critical}, the neutrons are produced after a delay.
This is from the reaction producing an excited nucleus that then decays after some time. 
For example, the reaction
\[
    \isotope{U}{235} + \neutron
    \yield
    \isotope{Ba}{141} + \isotope{Kr}{92} + 3\neutron
    + \SI{200}{\mega\electronvolt}
\]  
produces an excited nucleus of krypton that then decays into barium and three neutrons.
% !!This example is not verified, and I am quite sure that is wrong. Either way, the phrasing needs to be redone.  
In a reactor, delayed critical is reached without reaching prompt critical. 
Thus, energy is produced without an explosion. 

Second, cross-section of many neutron reactions is bigger if the neutron is slowed down. 
For example, the cross-section of the reaction
\[
    \isotope{U}{235} + \neutron
    \yield
    \isotope{Ba}{141} + \isotope{Kr}{92} + 3\neutron
    + \SI{200}{\mega\electronvolt}
\]
is bigger if the neutron is slowed down. 
Neutrons slow down when they pass through a medium. 
Materials used for this are called \emph{moderators}\index{moderator}.
Common moderators are water and graphite. 
Thus, my controlling the amount of moderators in a reactor, the chain reaction can be controlled. 

Chain reactions of fusion reactions only occur at high temperatures. 
In a fission bomb, many photons are emitted immediately before the explosion. 
These photons can be used to energize a container of $\isotope{H}{3}$ and $\isotope{H}{4}$ so that the fusion reaction
\[
    \isotope{H}{3} + \isotope{H}{4}
    \yield
    \isotope{He}{4} + \neutron
    + \SI{20}{\mega\electronvolt}
\]
occurs. 
% !! Not verified
This is a fusion bomb. 
They can be over $1000$ times more powerful than a fission bomb. % !! Not verified


\begin{figure}[H]
    \centering
    \includegraphics[width=0.2\textwidth]{satm-fig-fusion-bomb}
    \caption{A design for a fusion bomb. A fission bomb sets of fusion in a tank of deuterium and tritium. The material around the tank compresses from the photons to momentary contain the fusion reactions.}
\end{figure}