%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Photoelectric Effect
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

It has been observed that light is able to discharge positively charged metals when shone on them. 
Furthermore, it has been observed that the negative charge is carried off by electrons.  
That is, the light frees electrons from the metal.
This is called the \emph{Photoelectric Effect}\index{Photoelectric Effect}.


\begin{figure}[H]
    \centering
    \includegraphics[width=0.5\textwidth]{satm-fig-photoelectric-effect}
    \caption{Photoelectric Effect. Light frees electrons from a metal.}
\end{figure}


% Expected Behavior
By classical physics, the light continuously transfers energy to the metal. 
Thus, energy of the emitted electrons should depend on the intensity of the light.
Furthermore, a low intensity light should still emit electrons because it is still transferring energy to the metal. 
But, this these properties are not observed. 

% Observed Behavior
First, there is a threshold frequency of the light to emit electrons. 
That is, if the frequency of the light is below a certain value, then electrons are not emitted.
This explains why visible light does not ionize most metals.
Second, the energy of the emitted electrons is proportional to the frequency of the light, and it is independent of the intensity of the light. 
The number of emitted electrons does depend on the brightness of the light, that is, if more light at the same frequency is shone on the metal, then more electrons are emitted. 
% Third, electron begin to be emitted as soon as the metal is exposed to light.
Specifically, the energy of an emitted electron is 
\[
	\h⋅υ - E_0
	,
\]
where $υ$ is the frequency of the light, $\h$ is a number and $E_0$ is the threshold energy to free an electron from the metal, 
The number $\h$ is independent of the metal. 
It is called \emph{Planck's Constant}\index{Planck's Constant}\label{not:planck}.
And, it is approximately $\SI{6.626d-34}{J⋅s}$.


\begin{figure}[H]
    \centering
    \includegraphics[width=0.5\textwidth]{satm-fig-photoelectric-energy}
    \caption{Energy of electrons from the Photoelectric Effect.}
\end{figure}


% Photons
It is easier to explain the Photoelectric Effect if light consists of photons, that is, light comes in discrete packets of energy.
% This indicates that the energy of light is quantized, that is, it comes in discrete packets. 
% Furthermore, this indicates that light consists of particles.
And, if light does consist of photons, then the energy of a photon is 
\[
	\h⋅υ
	,
\]
where $υ$ is the frequency of the light. 
Furthermore, by the Energy-Momentum Relation, the momentum of a photon is
\[
	\h⋅λ
	,
\]
where $λ$ is the wavelength of the light.
This is the first step in connecting a particle-view and a wave-view of light because it connects the frequency and wavelength of light to the energy and momentum of single photons. 
This connection is fully developed in Chapter~\ref{ch:qed}.

% Photon-Electron Scattering 
The Energy-Momentum Relation was for electromagnetic waves, and it may not be clear that it is applicable to photons. 
But, the momentum of photons has been verified by scattering photons off electrons. 
The change in wavelength of the scattered photon is consistent with this relation and the change in momentum of the scattered electron.


\begin{figure}[H]
\centering
    \includegraphics[width=0.4\textwidth]{satm-fig-photon-scattering}
    \caption{Scattering xrays off electrons.}
\end{figure}



\scenebreak 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Verification of Photons
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Detectors can be made to detect single photons. 
At the energy of visible-light, photographic plates can be used.
A single photon can expose a photographic plate and leave a discolored dot. 
And, in Section~\ref{satm:sec:detectors}, various particle detectors are explained. 
These can be used to detect photons as the xray range and above. 
Such detectors can count the number of photons. 

Consider a dim light source and a beam splitter, that is, a device that splits incoming light equally into two directions. 
And, place a detector for each direction. 
Then, the total number of photons detected at the detectors is equal to the number of photons that would have been detected if the beam splitter was not there.
Furthermore, a photon is not detected at both detectors. 
This shows that photons cannot be split. 


\begin{figure}[H]
\centering
    \includegraphics[width=0.5\textwidth]{satm-fig-beam-splitter}
    \caption{Beam splitter.}
\end{figure}


Now, consider the Double-Slit Experiment with a dim light source. 
Single photons are detected at the screen. 
But, their overall distribution is the same as if the light was a wave. 
Furthermore, with a sufficiently dim source, it is very unlikely that two photons are in transit at the same time.
Thus, the photons are interfering with themselves. 


\begin{figure}[H]
\centering
    \includegraphics[width=0.5\textwidth]{satm-fig-photon-distribution}
    \caption{Distribution of photons in the Double-Slit Experiment for different exposure periods. Each individual photon is random. But, their statistical behavior is the same as if light is a wave.}
\end{figure}