<!-- LTeX: enabled=false -->
<!-- - cspell:disable - -->

# Notes for Subatomic Particles

## Issues

* Oil Drop Experiment: I am not convinced. 
* I assumed that *photon* is already defined. 


## To Understand

* ❗What is the evidence for the weak force? 
* How is the nuclear force known? 
* The force/potential of the nuclear interaction is known. Can this be used to used to compute the energy or decay rate or cross sections of reactions with the Schrödinger Equation? What does QFT really add? 


## General


## Figures

* Cathode Tube:
	- Don't have the vacuum pump
	- Don't have so many labels
	- Have the cathod red to indicate that it is heated. 
* Mass Spectroscopy: 
	- Only have two beams/isotopes. 
* Photons are particles / Double-Slit Experiment: I would like something for this, but I do not know what to have. 
* Alpha particles are helium nuclei:
	- Should it be a time lapse? 
	- If it is a time lapse, I should have the decay product in the second tube too. 
* ❗Neutron cross-section: Have a figure that shows the cross section of the reaction as a function of speed of the neutron. Indicate the peak and the speed of the neutron coming out of a previous reaction. 
* Figure for moderators? Have rods going in between fuel rods. 
* Fission bomb: 
	- Have them side by side. 
* Fusion bomb: 
	- Have a more simple figure. 


## Material

* I would like more emphasis on the crazyiness of alchemy. Like, these nuclear reactions are crazy.  
* Earnshaw's Theorem?
* Photon Implication: There is a limit to how accurate one can make measurements with light because the wavelength and the position of a wave train cannot be well determined. This preludes the Uncertainty Principle. (Well, isn't this already known?)
* Emphasize that electrons and nuclei are what make up every day matter. In a sense, they are the only real particles. 
* Photoelectric Effect: Should I mention [photomultipliers](https://en.wikipedia.org/wiki/Photoelectric_effect#Photomultipliers) as an application? Such things are used for nightvision. 
* Spectral lines are from transitions with core electrons. This is why they remain the same regardless of how the valence electrons are interacting with neighboring atoms.
	- Explain *core electrons*
* Electron capture is more likely to happen for heavier nuclei because the nucleus is bigger and the (inner most) electron orbits are more compact. 
* Should I mention Auger electrons? An Auger electron is a valence electron that is emitted when a core electron transitions to a lower-energy state. This are probably from a photon emitted and then hitting the valence electron.
* Why can electrons be scraped off an object (such as between wool and rubber)?
* The resonance frequencies of atoms (the emission and absorption of photons in atoms) broaden from temperature (jiggling), collisions and neighboring atoms (band theory stuff).
* Should I explain the conventions of decay schemes? Perhaps, have (the long) decay scheme of uranium. (I could mention how uranium rock is more radioactive than pure uranium because of the decay products.) (If I am using figure descriptions, then I could explain decay schemes in the description.)
* Supposedly, quantum mechanics does not "allow" the negative solutions to the Dirac Equation to be ignored (like in classical mechanics). Thus, this may actually be some motivation of the positron this way. Is this true? What do I think of it?


## Vocabulary and Notation 

* ❗I would like better words for reactions than *starting materials* and *(end) products*.

