# Advertisement for the Elements

I want this book to sell. 
I want to make money. 
I want to get status. 

I think that getting 1M exposures is a reasonable goal. 
And, I only plan on doing free advertisement. 


## Description

I need to have a good description for where it is sold. 
Such descriptions may vary depending on the vendor. 

**Possible Material**: 
* Stuff in its manifesto
    - Talk about the audience 
    - Correct level of abstraction 
    - Connects all core subjects
* Nothing particularly novel, but many small novelties and improvement. 
* For a preliminary version, such as on `leanpub.com`, it is probably better to be more passionate and polarizing to standout. This may appeal to fewer readers, but it takes more to get any reader to buy a preliminary book. Furthermore, I would guess that the readers who this would appeal to correspond strong to the readers who would be more likely to give feedback. 
    - Use the term *massive work*
    - Use strong words from the manifesto. 


## Platforms

### YouTube

I can recommend it to youtubers in hope that they mention the book. 
I have learned of books this way. 
Many of these videos have over 100k views. 
Advertisement on just one such video would be a big win. 

**HeadFloatPhysics**: He has mentioned both the Feynman Lectures and *Relativity Visualized*. I can message him saying that the books he recommends align with the books that I like and that he should check out the *Elements*. 


### Wikipedia

I can post it as a reference on Wikipedia pages. 
A common page can get 100k views per year. 
If only 1% of viewers see my reference, then it is still about 1k per year. 

I would also make various other edits so that it does not look too much like I am directly referencing my book.


### Stack Exchange

I can recommend it on various posts on stack exchange. 
Each these posts gets about 100 to 1000 views. 


