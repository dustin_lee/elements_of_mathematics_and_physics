# Etymology

<!-- 
Queue: 
* Symbolic (and analytic) and numeric
* Natural number, integer, rational number, real number, complex number
-->

**Aleatory**: This derives from a word that means *dice player*. 

**Atom**: This derives from a word that means *uncuttable*. 

**Category Theorem**: This is also called the Baire Category Theorem. The word *category* is because the terms *first category* and *second category* were used. A subset is said to be of *the first* category if it is a countable union of nowhere dense subsets. And, a subset is said to be of the *second category* if it is not of the first category. A synonym for *of the the first category* is *meager*. 

**Integral**: The word *integral* means the *whole*. This is also why *integers* are called so. The integral is called so is because it is making the function whole from the partial information of its derivative. 

**Kinetic Energy**: 
The word *kinetic* means motional. 

**Potential Energy**: Energy is already the potential for work. Thus, the term *potential energy* is misleading. A more accurate term would be *static energy*. 

**Row-Reduction Method**: This is also called *Gaussian Elimination*.

**Superposition**: This is an odd contraction because it should be *superimposition*. 

**Topological**: The root *topo* means space. Thus, the term *topology* means that study of space. And, the term *topological* is the adjective form of this lemma. 