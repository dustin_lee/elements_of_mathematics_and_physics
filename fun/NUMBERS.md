# Conceptual Understanding of Big and Small Numbers

**Atoms**: 
If atoms are arranged side by side, there would be over three million atoms per millimeter. 
In just one grain of sand, there are about $10^20$ atoms, that is, over a billion billion. 

**Atomic Spacing is Gas**: 
The spacing between atoms in a gas is about 40 times more than in solids. 
<!-- I computed this from the volume of water to steam. -->

**Atoms in a Crystalline Arrangement**: 
A typical diameter of a grain is about 50μm. 
Thus, only about 1 in 30,000 atoms are part of a grain boundary. 
That is, over 99.99% of atoms are part of the grain.