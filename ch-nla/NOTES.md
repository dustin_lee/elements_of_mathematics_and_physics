# Notes for Methods for Linear Equations

## General 

* To motivate iterative schemes, start with the method of solving Laplace's equation. 
    - Can all (square) matrices be thought of as stiffness matrices? 
* Since eigenvalues were introduced after Linear Equations, numerical methods that involve using eigenvalues (that are not used in Resonance) must be here.
* What are motivational examples of (hermitian) positive definite operators? 


## Material

* I think that Krylov is about projecting onto a smaller space to study. 