# Outline of Methods for Linear Equations


## Introduction

* Explain when to use which method. 
    - Use Gaussian Elimination or QR factorization for dense matrices. 
    - Use Conjugate Gradient for sparse self-adjoint positive-definite sparse matrices.
    - Use GMRES, CGN or BCG for normal (non-self-adjoint) sparse matrices. 


## Gaussian Elimination

* Partial pivoting can have numerical problems. 
* This isn't actually stable, but it is in practice. 


## QR Factorization. 

*


## Sparse Matrices

* Give numbers to show how much more efficient these can be.
* Prelude these in Linear Equations or Static Differential Equations.