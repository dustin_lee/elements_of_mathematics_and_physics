# The Conception of the Elements

Since high school and especially by the end of my freshmen year of college, I had an urge to put my understanding of mathematics into a book. 
I would think about the what organization of the material would be best.
I thought about what notation is best. 

Then, during my junior year, I started acting on this urge. 
I started writing personal notes on the material for my classes. 
By then, I have already embarked fully upon my journey through mathematics.
I was introduced to proof-based mathematics my sophomore, and was taking graduate-level courses my junior year. 

During the summer after my undergraduate degree, I decided to write the [Elements](manifesto.md). 
At this point, it was mostly about algebra and all of analysis was neglected on one chapter. 
It did not include any physics. 
In total, only nine chapters were planned. 
I was not yet skill at Latex, and I mostly had handwritten notes that I kept in envelopes, where I had one envelope per chapter. 

During my first-year of graduate school, the idea fully solidified. 
I started using Latex, but my project structure was trashed. 
I started accumulating notes and outlines. 

In graduate school, I started reading physics again. 
Furthermore, I was exposed to applied mathematics. 
Over the first few years of graduate school, the contents of the Elements shifted drastically to what it is now. 

I did not actually write much during graduate school. 
But, the original purity that I approached the writing from drastically improved my writing. 
Furthermore, whenever I learned something, I thought about how it would fit into the Elements, that is, the Elements created a framework for my education. 
