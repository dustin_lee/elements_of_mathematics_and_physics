# Outline for Momentum and Force


## Distance, Time and Mass

* Distance:
    - Units. 
* Time:
    - Philosophy. 
        * Time is about looking at the state of an object in comparison to a noninteracting object that changes. 
        * That time is universal is assumed, so state it as a principle. It could be called the *Principle of Universal Time*, but I don't need to name it. Do I also need to state that length and so on do not change?
    - Units. 
* Motion (Trajectories):
    - Describe a particle as being a body that is arbitrarily small and whose geometry is irrelevant to the problem.
    - It is assumed that the position of a particle can be represent by a curve in three-dimensional euclidean space. (It is normally assumed that such such curves are twice differentiable.)
    - Define velocity and acceleration. 
* Mass:
    - Mass can be measured with a scale. 
    - Mass can also be measured by having particles collide (such as on an air-slider), which avoids the use of gravity.


## Relativity and Momentum 

* The Law of Relativity: 
    - Explain what an active transformation is for the Law of Relativity. It is not just a change of coordinates. From the perspective of an observer, who does not move, if everything is rotated, shifted or whatever, then the laws are the same. (Even more so, after the interactions a rotation or whatever back gives the same results as if the transformation never happened.)
* Momentum. 


## Force

* Newton's laws. 
    - "Collectively, the Law of Inertia, the Law of Superposition of Forces and the Law of Reciprocal Action are called *Newton's laws*."


## Gravity

* Law of Gravity.
    - An object should not fall faster than its parts. 
    - Measuring the gravitational constant. (The torson balance.)
* Orbit.
    - Explain orbit when the analogy of a stone being thrown off a mountain, and if it is thrown fast enough, then it missed the Earth. 

    
