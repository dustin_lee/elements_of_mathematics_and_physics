# Notes for Momentum and Force


## Need to Understand

*


## General Notes

*


## Word Choice

* Should I call Newton's second law the *Parallelogram Law*? I could also call it the *Force-Summation Law*.
* Use the *Law of Reciprocal Forces* instead of the *Law of Reciprocal Action*.
* Use the term *Law of Gravity*. Should I use the adjectives *classical* and *relativistic* to distinguish the two or should I use *Newton's* and *Einstein's*?
* Perhaps, use *rotational momentum* instead of *angular momentum*.


## Material

* Give the normal explanation of the Law of Inertia: As friction is removed, the object goes further. 
* Have the Cavendish Experiment in (just) a figure.
    <!-- - Maybe call it the *Weighing-the-Earth Experiment* or just *Weighing the Earth*.  -->
* Is there a point of having Kepler's laws? 
* Talk about closed/isolated systems. 
* Introduce both inertial mass and gravitational mass. Since objects of different materials still fall towards the Earth as the same speed, these two versions of masses are proportional. It has been experimentally demonstrated that they are proportional to within one part in $10^{10}$.
* Force can depend on (relative) position and (relative) velocity, but not on acceleration by The Galilean Principle of Relativity.
* Have the example finding the trajectory of a rocket, where its fuel decreases continuously. 
* On top of the Principle of Relativity, there needs to be a law that says that distance and time are preserved from different frames. 
* Why force? That is, why the second derivative? Well, the laws are written as second derivatives. 
* Law of Inertia:
    - There is an inertial frame
    - And, Galileo implies that there are many frames.  