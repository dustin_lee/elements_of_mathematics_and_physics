%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Flux 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

A common problem is the study of the flow of a conserved substance, where the substance is represented by its density $ρ$. 
Examples are the mass density in a fluid, the electric charge density in a conductor and the thermal energy density in a material. 
Furthermore, for such problems, there is a concept of \emph{flux}\index{flux}, which is the instantaneous rate of flow of the substance through a surface. 

% !! Figure: Flux through a surface. 

The \emph{current}\index{current} is the vector field $J$ is such that the flux through any surface $S$ for an orientation $n$ is 
\[
    ∫_{S} J • n
    .
\]
That is, it is the flux density because the relation
\[
    J(x) = \lim_{r→0} \frac{1}{\vol(B_r(x))} ⋅ ∫_{\bd\big(B_r(x)\big)} J • n
\]
holds, where $B_r(x)$ is the ball at $x$ whose radius is $r$. 
% !! Justification? 
For example, in fluids, the current is in the form $ρ⋅v$, where $v$ is the velocity of the fluid.
And, in material, the current of the thermal energy is often in the form $-k⋅∇p$, where $k$ is a positive real number. 
A current is \emph{advective}\index{advective} if it involves the velocity of the substance, that is, if it is about bulk flow. 
And, a current is \emph{diffusive}\index{diffusive} if it involves the gradient of the density of the substance.
A current may have both advective and diffusive parts. 

The substance is \emph{conserved}\index{conserved} if the relation
\[
    ∂_t ∫_R ρ(x, t) = ∫_{\bd(R)} J • n
\]
holds for any region $R$, that is, if change of the quantity of the substance in a region is the flux through its boundary. 
Then, the relations
\begin{align}
    ∫_R \dot{ρ}(x, t) % !! Unjustified 
    &= ∂_t ∫_R ρ(x, t) \\
    &= ∫_{R} \div(J)
\end{align}
hold, where the second relation holds by the Divergence Theorem.
The divergence and the Divergence Theorem are introduced later in this section. 
% !! *Thus* is a bad transition since it is separated by a sentence
Thus, the relation 
\[
    ∫_R \Big( \dot{ρ}(x, t) - \div(J) \Big) 
\]
holds for any region $R$. 
Thus, the relation
\[
    \dot{ρ} = \div(J)
\]
holds. 
This relation is the \emph{Continuity Equation}\index{Continuity Equation}.

% Source term 
Often, a substance is conserved in the absence certain effects, but it is not actually conserved. 
For example, the thermal energy density in a nuclear reactor changes by the nuclear reactions, which may be modeled independently.
Or, the density of a chemical species in a fluid changes by chemical reactions.
In such a situation, the addition effects are described by the \emph{source term}\index{source term}.
And, the Continuity Equation is 
\[
    \dot{p} = \div(J) + S
    ,
\]
where $S$ is the source term. 
In the nuclear reactor example, the source term is a function of only time and space. 
And, in the chemical species example, the source term is also a function of the density of the chemical species.

Furthermore, in general, the current is a function of the density and time. 
And, the Continuity Equation is 
\[
    \dot{ρ}(x, t) = \div\big(φ(ρ, t)(x)\big) + S(x, t, ρ)
    ,
\]
where $φ$ is a function. 
For clarity, the function $φ$ is a function of the density, that is, it is not a composition. 
% This is the most general form of the Continuity Equation 
The Continuity Equation is the basis to several useful differential equations.
Examples are the Heat Equation in Chapter~\ref{ch:thermo} and the Navier-Stokes Equations in Chapter~\ref{ch:fld}. 
% !! I don't like *useful*, but I also don't like *of interest*.

% !! I want this to be better. 
This section is about a time-slice of the current independently of its relation to a specific density.
More specifically, the remaining results in this section are for vector fields. 
This generalization is based on flux being defined for any vector field. 
That is, the \emph{flux}\index{flux} of a vector field $J$ through a surface $S$ for an orientation $n$ is 
\[
    ∫_{S} J • n
    .
\]


\scenebreak 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Divergence
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

The \emph{divergence}\index{divergence} of a vector field $f$ is 
\[
    \lim_{r→0} \frac{1}{\vol(B_r(x))} ⋅ ∫_{\bd\big(B_r(x)\big)} f • n 
    ,
\]
where $n$ is an outward perpendicular unit vector field on the surface $\bd(R)$
Thus, if the vector field $f$ is the current of the density $ρ$, then the divergence is 
\[
    \lim_{r→0} \frac{1}{\vol(B_r(x))} ⋅ \flux_{\bd\big(B_r(x)\big)} (ρ)
    ,
\]
that is, the divergence is the flux density. 
Intuitively, the divergence of a vector field is a measure of its spread. 
% !! Need a better word than *spread*. 
The divergence of a vector field $f$ is denoted by 
\[
    \div(f)
    .
\]


\begin{figure}[H]
\centering
    \includegraphics[width=0.5\textwidth]{cont-fig-divergence}
    \caption{Divergence. The divergence of left vector field is positive. The divergence of the center vector field is negative. The divergence of the right vector field is zero.}
\end{figure}


% !! Form 
Let $p$ be a point. 
And, for any positive number $r$, let $C_r$ be the cube that is centered at $p$ that its normals are parallel to the basis vectors. 
%
Then, relations
\begin{align}
    \div(f)(p)
    & \lim_{r→0} ∫_{\bd\big(C_r(x)\big)} f • n \\ 
    &= \lim_{r→0} ∑_{1≤i≤n} ∫_{S_r^{(i)}} f • n \\
    &= ∑_{1≤i≤n} \lim_{r→0} ∫_{S_r^{(i)}} f • n \\
    &= ∑_{1≤i≤n} \lim_{r→0} ∫{S_r^{(i)}} f • n_i \\
    &= ∑_{1≤i≤n} \lim_{r→0} \frac{1}{\area(C_r)} 
        ⋅ ∫{S_r^{(i)}} f_i(p + r⋅e_i) - f_i(p - r⋅e_i) \\
    &= ∑_{1≤i≤n} \lim_{r→0} \frac{1}{r^3} ⋅ r^2 
        ⋅ \Big( f_i(p + r⋅e_i) - f_i(p - r⋅e_i) \Big) \\
    &= ∑_{1≤i≤n} \lim_{r→0} \frac{1}{r} 
        ⋅ \Big( f_i(p + r⋅e_i) - f_i(p - r⋅e_i) \Big) \\
    &= ∑_{1≤i≤n} ∂_i(p)
\end{align}
hold. 
% !! Need for formalize this. 
That is, in standard coordinates, the relation 
\[
    \div(f) = ∂_1 f + … + ∂_n f
    ,
\]
where $n$ is the dimension of the ambient space. 
% Figure?


The next theorem is the \emph{Divergence Theorem}\index{Divergence Theorem}. 


\input{cont-thm-div}


\scenebreak 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Circulation and Rotation 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Let $P$ be a closed path. 
The \emph{clockwise} orientation of $P$ is the orientation that is obtained by walking along $P$ in the direction of the clock. % !! Obviously, this is not rigorous. 
The \emph{circulation}\index{circulation} of a vector field $f$ along a closed path $P$ is the path integral of $f$ on $P$ for the clockwise orientation. 

% \emph{Right-Hand Rule}\index{Right-Hand Rule}. 

\begin{figure}[H]
\centering
    \includegraphics[width=0.3\textwidth]{cont-fig-right-hand-rule}
    \caption{The Right-Hand Rule.}
\end{figure}

% Rotation
\emph{rotation}\index{rotation}
% Turning of an infitesimal paddle. 
% This is the part of the field that is not conserved. 

% !! Rotation Existence: Similar to the existence of the current. 


\begin{figure}[H]
\centering
    \includegraphics[width=0.3\textwidth]{cont-fig-rotation1}
    \includegraphics[width=0.3\textwidth]{cont-fig-rotation2}
    \caption{Rotation. The rotation of both vector fields is nonzero.}
\end{figure}


The next theorem is the \emph{Rotation Theorem}\index{Rotation Theorem}. 

\input{cont-thm-rot}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Conservation and Gradient Theorem 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Conservative 
A vector field $f$ is \emph{conservative}\index{conservative} if for any two paths $P$ and $P$ between two points $p$ and $q$, the relation 
\[
    ∫_{P} f • o = ∫_{P'} f • o'
\]
holds, where $o$ and $o'$ are the orientations form $p$ to $q$ of the paths $P$ and $P'$, respectively.
That is, a vector field is conservative exactly if the circulation is $0$ for any path. % !! More justification : because the circulation of any path is equal to the path integral for the reverse orientation. 
Thus, by the Rotation Theorem, the rotation of a conservative vector field is $0$. 

% !! Figure: Path independence / Path reversal 

The next theorem says that the gradient of a scalar field is a conservative vector field. 
It is the \emph{Gradient Theorem}\index{Gradient Theorem}.

\input{cont-thm-grad}


Because the gradient of a scalar field is conservative and because the curl of a conservative vector field is $0$, the curl of the gradient of a scalar field is $0$, that is, the relation
\[
    \rot(∇(f)) = 0
\]
for any scalar field $f$.
This relation can also be proved by direct computation. 
% !! Is this used to prove the Gradient Theorem? 


\scenebreak 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Decomposition Theorem 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


The next theorem is the \emph{Decomposition Theorem}\index{Decomposition Theorem}. 
It says that any vector field is the sum of a rotation-free vector field and a divergence-free vector field.

\input{cont-thm-decomposition}


In Chapter~\ref{ch:em}, Maxwell's Equations describe the divergence and rotation of the electric field and the magnetic field. 
Thus, by the Decomposition Theorem, Maxwell's Equations completely describe the electric field and the magnetic field. 


% \scenebreak 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Recap?
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% !! Figure of Connections?