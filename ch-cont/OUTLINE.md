# Outline of Content and Integration

## Integration on Content Spaces

**Definition**: 
* 
* Theorem: Equivalence of integrals 
* Define integration  
* Define integration of euclidean-valued functions? 
* Results? 
    - Division of domain
    - Summation 
    - Triangle Inequality 
    - Continuous functions are integrable? 
    - Products are integrable? 
    - Preserves inequalities 
<!-- * Define Integration on an infinite domain
    - I think that this can be done in the same way as it is done in Generalizations.  -->

**Orientation**: 
* Line Integral 
    - Equivalent definition 
    - Equality of path length with the integral of the derivative. 
    - Use work as an example. 
<!-- * Surface Integral 
    - Computation of surface area -->


## Density

* Explanation and definition
* The Density Theorem
* The Radon-Nikodym Theorem 


## Flow 

<!-- Continuity Equation  -->

**Flux**: 
* Define: 
    - 
* It is a surface integral 

**Current**:  

**Divergence**: 
* Divergence Theorem 

**Rotation**: 
* Conservative
    - A current is conservative if its circulation is zero. 
    - Gradient Theorem 
    - Mention fields that follow the Inverse Square Law are conservative. Is this easy to prove without the concept of rotation? (One can prove that a field is conservative by just showing that the rotation is $0$.)
* Rotation Theorem 


<!--
**Divergence and Flux**:
* The definition of flux
* Current density
    - This is just $ρ⋅v$. I guess that it comes from the alternative version of flux (which just integrates over this.)
    - For charge or something, because the fields are additive, it is fine to think of the current density as just $ρ⋅v$, even though it might not completely make sense because various charges can be going in various directions. (Does this really make sense?)
    - Just like divergence gives the flux by the Divergence Theorem and the rotation gives the circulation by the Rotation Theorem, current density gives the current by the Fundamental Theorem of Calculus for Line Integrals. 
* Divergence definition
* Divergence Theorem
    - This is a *form of the Fundamental Theorem of Calculus*
* Green's identity
* The Continuity Equation

**Circulation and Rotation**:
* Rotation
    - The formula for two dimensions is easy to get from the circulation of an infinitesimal square.
* The Rotation Theorem
    - This is a *form of the Fundamental Theorem of Calculus*
* Miscellaneous relations
    - Conservative fields.
        * The rotation of the gradient is $0$. This makes sense because the field is conservative.
    - The divergence of the rotation are $0$. Why?
        * If the rotation of a function is $0$, then it is the gradient of a scalar function. (This gives a test to check whether a field is conservative.) This is because of Stokes's theorem, and it requires the result that any closed simple curve is the boundary of a surface. 

**The Fundamental Theorem of Vector Calculus**:
* Motivation
    - Use for Maxwell's equations in electromagnetism. They describe the divergence and rotation of the electric and magnetic fields. This theorem says that this suffices. 
* Statement 
    * Every vector field is the sum of a rotation-free term and a divergence-free term. Thus, it can be written as $\grad(φ) + \rot(A)$.
    * Technicality: The fields must decay rapidly enough at infinity.
-->



## Methods

**Symbolic**: 
* Fubini's Theorem
* Green's Identity 
    - This is a generalization of the Integration-by-Parts Formula. 

**Numeric**: 
* Convergence of schemes. 
    - They need to deal with irregular geometries. In FEM, they are broken down onto a fixed triangle and then mapped to a desired triangle. Is this what should be done here? 
* Gaussian quadrature for multiple variables. 
    - Mention that this is used for the Finite-Elements Method. 
* Oriented Paths and Surfaces: 
    - Combine the schemes with the convergence of content. 