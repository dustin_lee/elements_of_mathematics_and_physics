# Notes for Content and Integration 

## Issues

* 


## Need to Understand

* Is there a substance for electromagnetism? Is it the potential? 


## General 

* 


## Figures

* Divergence: 
    - Have three vector fields. 
    - Don't have the labels of the divergence on them. 
    - For the third field, have vectors of different magnitude that are still going in the same direction. 
    - Use colors to denote the scaling instead of the length of the vectors. 
* Divergence Theorem: 
    - Draw the boundaries of all divisions. 
* Right-Hand Rule: 
    - Use a different color for the loop arrow. This is so that it can be more easily seen to go behind the vector. 
* Rotation: 
    - 
* Rotation Theorem: 
    - Don't have the vectors pointing out. 
    - But, have the arrows of circulation around each division. 


## Material

* The functions in the Helmholtz Decomposition Theorem are not necessarily unique.
    - Is this just adding a scalar?
    - I think that it is unique if the field vanish at infinity. 
    - Well, what about with dynamic E&M. Can it always be assumed to vanish at infinity when looking at light and whatnot? 
    - Definitely does not apply to potentials. 
* Does Fubini give convergence of integration schemes? 
* Explain Fubini's theorem as computing a multidimensional by iterated low dimensional integrals. 
* Proof of Fubini: Simply [make a grid and refine it](https://en.wikipedia.org/w/index.php?title=Fubini%27s_theorem&gesuggestededit=1#Riemann_integrals). Although the inequalities might still be subtle because they could depend related rates of convergence. 
* The proof of Raydon-Nikodym theorem should be very easy. The density of at a point $p$ is just
    $$
    \lim_{\mathrm{cont}(E) → \infty;\ p \in E} \frac{\mathrm{cont}(E)}{\mathrm{cont}(f^{-1}(E))}. 
    $$
* I can use the distribution of speed versus the distribution of energy (Boltzmann) of particles as an example of changing content (Raydon-Nikodym). 
* Explain Fubini's theorem as integrating areas where the areas are integrals, that is,
    $$
      ∫ \left( ∫ f(t, s) \right) = ∫ \mathrm{area}(E_s).
    $$
Although, probably don't use such an equality in math mode.
* Explain how integration can now be used to compute Archimedes's number much for effectively.
    - A numerical integration scheme works well for the area in a quadrant. Although the idea of an integral is not really needed for this. 
    - The arc length of the upper half of the circle is
          $$
              ∫_{-1≤x≤1} \frac{1}{\sqrt{1-x^2}},
          $$
      which is equal to $2*\mathrm{asin}(1)$. The function $\mathrm{asin}$ can be computed very fast by its power-sum approximation.
* Stokes: Think of little loops to see how the orientation of the surface a curve match.
* Explain that the Divergence Theorem and the Rotation Theorem are saying that the cumulative effect and the total effect are equal. In the case of the Divergence Theorem, if there is expansion at one point, then this influences the expansion at a near-by point. In the case of the Rotation Theorem, think of two balls that are in contact; they cannot spin independently of each other.
* Explain that when $\mathrm{div}$ and $\mathrm{rot}$ are used, they apply to the spatial variables, not the time variables. 



## Vocabulary and Notation 

* 