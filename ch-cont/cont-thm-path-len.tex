\begin{theorem}
Let $γ$ be a continuously differentiable trajectory on a compact interval $[a, b]$ such that the length of its image exists. Then, the length of its image is 
\[
    ∫_{s:a → b} \abs{\dot{γ}(s)}
    .
\]
\end{theorem}

\begin{proof}
Let $ε$ be a positive real number. 

Because the function $\dot{γ}$ is continuous and its domain is compact, it is uniformly continuous. 
Thus, a positive real number $δ$ exists such that, if the distance between two points is less than $δ$, then the distance between the values of $\dot{γ}$ at these points is less than $ε$. 
Let $b_1$ and $b_2$ be two points such that the distance between them is less than $δ$. 
Then, by the Mean-Value Theorem, there exists a point $b_3$ between them such that the relation
\[
    \dot{γ}(b_3) = \frac{γ(b_2) - γ(b_1)}{b_2-b_1}
\]
holds. 
Thus, the relations 
\[
\begin{split}
    \abs{\frac{γ(b_2) - γ(b_1)}{b_2-b_1} - \dot{γ}(b_1)}
    &= \abs{\dot{γ}(b_3) - \dot{γ}(b_1)} \\
    &< ε \\
\end{split}
\]
hold. 
By the Reverse Triangle Inequality, the relation 
\[
    \abs{\frac{\abs{γ(b_2) - γ(b_1)}}{\abs{b_2-b_1}} - \abs{\dot{γ}(b_1)}} < ε
\]
holds. 
Thus, the relation 
\[
    \big| \abs{γ(b_2) - γ(b_1)} - \abs{\dot{γ}(b_1)}⋅\abs{b_2-b_1} \big|
    < ε⋅\abs{b_2-b_1}
\]
holds. 

Let $a_1$, $\dots$, $a_{n-1}$ and $a_n$ be points such that $a_1$ is $a$, $a_n$ is $b$, and the relations
\[
    a_i < a_{i+1}
    \textand
    \abs{a_{i+1} - a_i} < δ
\]
hold for each index $i$ that is less than $n$. 
Then, by possibly replacing $δ$ by a smaller number, the relations 
\[
\begin{split}
    & \abs{\len(\img(γ)) - ∫_{s:a → b} \abs{\dot{γ}(s)}}  \\    
    &= \abs{\len(\img(γ))
        + \left( 
            ∑_{1≤i<n} \abs{γ(a_{i+1}) - γ(a_i)}
            - ∑_{1≤i<n} \abs{γ(a_{i+1}) - γ(a_i)}
        \right)
        - ∫_{s:a → b} \abs{\dot{γ}(s)}} \\
    &≤ \abs{\len(\img(γ)) - ∑_{1≤i≤n} \abs{γ(a_{i+1}) - γ(a_i)}} 
        + \abs{∫_{s:a → b} \abs{\dot{γ}(s)}
               - ∑_{1≤i<n} \abs{γ(a_{i+1}) - γ(a_i)}} \\
    &< ε + \abs{∫_{s:a → b} \abs{\dot{γ}(s)}
                - ∑_{1≤i<n} \abs{γ(a_{i+1}) - γ(a_i)}} \\
    &= ε
       + \! \begin{aligned}[t]
            \Bigg| &∫_{s:a → b} \abs{\dot{γ}(s)} \\
                 & + \left(
                        ∑_{1≤i<n} \abs{\dot{γ}(a_i)}⋅\abs{a_{i+1} - a_i}
                        - ∑_{1≤i<n} \abs{\dot{γ}(a_i)}⋅\abs{a_{i+1} - a_i}
                     \right) \\
                 &- ∑_{1≤i<n} \abs{γ(a_{i+1}) - γ(a_i)} \Bigg| \\
        \end{aligned} \\
    &≤ \! \begin{aligned}[t]
             & ε \\
             &+ \abs{∫_{s:a → b} \abs{\dot{γ}(s)} 
                    - ∑_{1≤i<n} \abs{\dot{γ}(a_i)}⋅\abs{a_{i+1} - a_i}} \\
             &+ \abs{∑_{1≤i<n} \abs{γ(a_{i+1}) - γ(a_i)}
                     - ∑_{1≤i<n} \abs{\dot{γ}(a_i)}⋅\abs{a_{i+1} - a_i}} \\
         \end{aligned} \\
    &< ε + ε + \abs{∑_{1≤i<n} \Big(
                    \abs{γ(a_{i+1}) - γ(a_i)}
                    - \abs{\dot{γ}(a_i)}⋅\abs{a_{i+1} - a_i}
                    \Big)} \\
    &< ε + ε + ∑_{1≤i<n} \Big|
                            \abs{γ(a_{i+1}) - γ(a_i)}
                            - \abs{\dot{γ}(a_i)}⋅\abs{a_{i+1} - a_i}
                         \Big| \\
    &< ε + ε + ∑_{1≤i<n} ε ⋅ \abs{a_{i+1} - a_i} \\
    &= ε + ε + ∑_{1≤i<n} ε ⋅ \abs{b-a} \\
\end{split}
\]
hold; where the second and fifth relations hold by the Triangle Inequality, where the third relation holds because the length of the image of $γ$ exists, and where the sixth relations hold because the function $\abs{\dot{γ}(t)}$ is integrable. 
Thus, the length of the image of $γ$ is indeed
\[
    ∫_{s:a → b} \abs{\dot{γ}(s)}
    .
\]
\end{proof}

