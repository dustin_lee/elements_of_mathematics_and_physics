# Outline of CEM

## Introduction

* Motivation. 
    - Used for optics for radar and diffraction. 
    - Used to determine fundamental forces in particle collisions. 
    - What about phonons? 
* Concept of incident wave and scattered wave. 
    - Have the standard figure. 
    - Scattering amplitude. 


## Yee's Method

* I guess that this is about getting closer.
* This scheme preserves Gauß's laws.
    - I think that in theory, Gauß's laws are preserved by the other two Maxwell Equations.
    - Thus, it just needs to be made sure that Gauß's laws hold at the start?
* Electric flux is continuous across interfaces, but the electric field isn't.
* I think that this is from using the integral form of Maxwell's equations for the spatial part. 


## Optics

**Material.**
    - Optical Theorem


<!-- ## Particle Collisions (Quantum Mechanics)

**Material.**
* Scattering matrix.  -->

