# Notes for CEM

## Need to Understand

* 


## General 


## Language

<!-- * Use *scattering matrix* instead of *S-matrix*. -->
* Use the word *deflect* at some point to help clarify what scattering is. 


## Material 

* Read the article *Optical Theorem and Beyond* by Newton.
* Note that the scattering angle in a classical concept. 
<!-- * The $S$-matrix determines the probability of the limiting behavior as time goes to infinity after the collision.  -->
<!-- * I think that the cross section is a function of point of impact and angle. But, averages over fairly large variations are what are actually used. -->
* How is it computed? I think that the path-integral approach deals with this in a somewhat obvious way.