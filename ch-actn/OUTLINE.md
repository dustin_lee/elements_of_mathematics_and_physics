# Outline of Action

## Introduction 

* Mention that it is used for quantum mechanics. 


## The Stationary-Action Principle

* Intuition. The Universe is Lazy. 
* The Law of Least Action for a potential-energy function.
    - Define action. 
    - Don't give it a name. 
    - Explain that this is not quite correct. 
* Law of Stationary Action. 
    - Define what it means to be stationary. 
* The Euler-Lagrange Equation. 
    - Switching the integral and the derivative is essentially the Leibniz Rule, but I am not sure if I am going to have this. 
    - How the Euler-Lagrange Equation becomes $F=m⋅a$. 
* The Law of Stationary Action. 
* Motes on the lagrangian. 
    - A constant can be added to the lagrangian. 
    - The lagrangian for electrically charged particles. 
    - Write the law for multiple particles. 


<!-- ## Hamiltonian Mechanics?

* Notes. 
    * This is useful because it introduces a newtonian-like framework for nonconservative forces. 
    * Should I bother? I don't know what I need it for. 
    * Should this be its own section?  -->


## Action of the Electromagnetic Potentials

* Notes. 
    - This describes the electromagnetic fields using the Principle of Stationary Action. 
* Euler-Lagrange Equation. 
    - I am doing somewhat of a more general approach that doesn't restrict $η$ to be $0$ on the boundary of the spacial domain, which doesn't seem correct anyway. 
    - For its lemma, I could say that its proof is the exact same as for the lower-dimensional case.
    - I don't even think that Green's identity is needed. The integral in question is still over time. 
    - Is this even a PDE? Something is wrong, there should be a spatial derivative. The lagrangian should include spatial derivative of $ψ$. Furthermore, there is probably some sort of boundary condition to prevent additional fields from interfering, such as the gradient dotted with the normal of the surface is $0$. 
    - Derive Maxwell's equations? 
* For a 3D grid of 100×100×100 points and 100 time steps, numerically solving the Euler-Lagrange Equation involves a matrix that is 100 million by 100 million. But, at least it is sparse. 
* I think that the Euler-Lagrange Equation when there is not any sources becomes the Electromagnetic Wave Equation. (So, I should probably write the Electromagnetic Wave Equation with potentials.)
* Is there a reasonable generalization for both fields and particles together? 