# Notes for Action

## Issues

* Actually, do I care about the action of the electromagnetic fields or do I just care about the action of the potentials? That is, which is needed for QED? 


## Need to Understand

* What is the use of action in classical mechanics? Can is help to solve problems?
      - Once the endpoint of a path is found, can the Principle of Least Action be used to get a better estimate of the trajectory?
      - I think that it does not have any practical use. 
* Wouldn't the actual action (not just up to a constant) matter in quantum mechanics? Wouldn't any additional term alter the phase? 
* What really is the connection with symmetries? (Supposedly, this is useful in QFT.)

## Figures

* Have a figure of a bump for the lemma of the Euler-Lagrange Equation (1d). 


## Vocabulary

* This use the term *First Derivative Test* and *Second Derivative Test*. 


## Material

* Path of light: Don't actually need the speed of light to find the path of least time? 
* If Jefimenko's equations are not already proved, then prove them here. (Although, they should be introduced in Electricity and Magnetism.)
* How do I combine mechanical and electrical actions? Are they already scaled together? 
* Should I action for continuous mechanics (such as waves) before E&M? 
* Should I include Noether's theorem? (There could be a section at the end called Conservation.)
* Should I have Gauge Theory? 


## General

* Should this go after relativity? 
      - The Einstein Equation can just be stated in General Relativity, but derived here.  
* How do I feel about the direct derivation of the Euler-Lagrange Equation? It is called the *alternate derivation of the one-dimensional Euler–Lagrange equation* on [Wikipedia](https://en.wikipedia.org/wiki/Euler–Lagrange_equation#Statement).
* Guages are imposed as an additional condition. Which guage is chosen is about the problem at hand. 