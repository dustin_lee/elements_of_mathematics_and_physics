# Outline of the Self-Consistent Field Method

## Introduction

* The Independent-Electron Approximation is poor because the force of the electrons in on the same order of the nucleus.
* Should I explain why direct estimates can become quickly to numerically expensive. Perhaps, talk about how number of electrons that are involved in molecules of big atoms.
* The ground states are the most important. The ground state is the main factor that helps to predict structural properties and reactions. 
* Approximations.
    - The Fixed-Nuclei Approximation.
    	* Each nucleon is about $1800$ times more massive than an electron. They barely move in comparison.
    	* The force can still be found on them. Repeating DFT calculations can be used for relaxation.
    - The Frozen-Core Approximation.
    - The Classical-Nuclei Approximation.
    	* The force on the nuclei can be calculated by considering them to be classical particles, that is, they are effected in the direct way from the electron cloud (Born-Oppenheimer). 
            - The Hellmann-Feynman Theorem is a formal justification, but I am not sure if I should care. 
            - This uses the electron density. 
    	* An iteration process where the electron states are calculated at each step can be used to find the equilibrium state.  
            - Examples. 
                * This can find bond length or lattice parameters.
                * Elasticity.
                * Vibration. 
    	* This works well for all but the lightest elements, mainly hydrogen.


## The Self-Consistent Field Approximation (DFT)

* The idea of the mean-field approximation. 
* Kohn-Sham Equation. 
* How this is solved.
    - This is done by iteration.
    	* Explain this using an enumeration.
    - The initial state is usually the states for isolated atoms.
    - Plane waves are used for crystals.
* Notes. 
    - Its general accuracy. The error cannot be determined (from the theory). I think that it is within $3\%$ normally.
    - It doesn't do well with van-der-Waals forces.
* Pseudopotentials. 
* Projector augmented waves. 
* Examples. 
    - Excited states. Energy bands.
        * Cannot estimate band gaps well?
    - Magnetism?
* Talk about various functionals.
    - Should this be in its own section at the end? 
    - Functionals are found according to physics, and are intuitive approximations. Just because the process converges does not mean that it converges to the physical solution because the process is not actually solving the many-body Schrödinger Equation.
    - Mention some functions and say which is best for which situation, but don't go into details. 


## Solids

* Notes. 
    - While the last chapter is more about bonds, this one will be about applying DFT to solids. 
* Bloch's theorem. 
    - Statement. This gives a form for a the solution of single electron. These then form a basis. 
    - Restriction to the Brillouin zone is for unique k-points. This is what the reciprocol lattice is from because of the term $\exp(\i⋅\sp{k, x})⋅u(x)$. The selection of k-points is then for the best integration of this or something. 
