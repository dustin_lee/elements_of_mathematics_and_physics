# Notes for the Self-Consistent Field Method


## Need to Understand

* Wait, is DFT really about solving a bunch of different equations in parallel or is it about taking many eigenvalues of one equation? 
* If DFT is about solving many equations, could it be done without synchronizing in the same way that Gauss-Seidel works? I mean that the electron density is updated when a new equation is solved. (What is the difference of convergence?)


## Vocabulary

* Use the term *effective potential*.
* Use the term *exchange-and-correlation energy* or *exchange-correlation* energy. (Make it clear that this is one thing – not two.)


## General

* Actually, the Kohn-Sham Theorem is actually kind of reasonable motivation. It is saying that we want to use a functional that is somehow close to this unknown functional.
* Actually, the single electron density approach to the self-consistent computations isn't any faster. (Just get their value and then remove of density of the electron in question.)