# Notes for Light

## Need to Understand

* Blackbody spectrum versus line spectrum: 
    - Is Wein's Law only weakly accurate? 
    - The line spectrum becomes the primary effect in gasses.
    - Do I introduce the line spectrum in this chapter? 
* When a beam of light is split and then merged so that it cancels out, what happens with the energy? Will it still heat objects?
* Can the Principle of Least Time be stated without the speed of light, that is, by using the index of refractions? 
    - It is needed for computations involving continuously changing index of refraction. 


## Material

* Summary: "In some situations, the particle-view makes sense. And, in others, the wave-view make sense."
* Brightness: 
    - Also, unlike in Chapter~\ref{ch:lght}, if light consists of photons, then it makes sense to define brightness for different frequencies. 
    - The \emph{brightness}\index{brightness} is the number of photons per unit area per unit time.
And, the intensity of the light depends on the frequency and the brightness. 
* Go ahead and list the entire light spectrum, including xrays and gamma rays. 
* Light as a wave: Explain how this sort of explains why light refracts when changing the medium. Use `lght-fig-ladder1` and `lght-fig-ladder2` (side-by-side). 
* High-frequency waves barely refract around an object. For example, wave waves and a ship. Thus, frequency of visible light must be high. Give numbers to demonstrate this. 
* Color corresponds to frequency. 
* Waves: But what is vibrating? 
* Explain the different models of light in the summary. 
    - Geometric optics is when the wave length is insignificantly small. Explain this in the summary.
    - Also mention photons. 
    - Seeing wave phenomena is easier for radio waves than visible light. 
* Talk briefly about aberrations (chromatic and spherical). 
* Mention that light does not collide with other light? It is linear. 
* Derive the optical resolution of light of visible light? Explain that higher frequencies can be and are used. Is there a limit to the resolution as the frequency goes to infinity and is it positive?
* Mention that higher frequencies correspond to higher energies. Is there a formula or something in classical physics or does the intensity get in the way of isolating these concepts?
* Is a limitation of lenses that different wavelength focus differently?
* Don't bother including the Wien Approximation or the Rayleigh-Jean law. 


## General

* Is the Three-Lens Effect explainable with classical physics? (What is this?)

