# Outline of Light

## Introduction 

* A pinhole can be used to see that light is something that comes to the eye. 
* The existence of shadows implies that light travels in straight lines.


## Reflection and Refraction

* Notes. 
    - Have a picture for reflection, refraction, lenses and a prism. 
* Reflection. 
    - Notes. 
        * There isn't really an explanation for partial reflection. But, this will be explain in Electricity and Magnetism.
* Refraction. 

    - Lenses. 

    - Prism: White light consists of several colors. Besides splitting, it can also be recombined. Have a figure of white light being split by a prism and being merged back into white light by another prism.


## The Speed of Light 

* Measurement. 
    - Digital experiment. 
        * It has even been reflected from a mirror on the moon.
        * Should I describe the Tooth-Wheel Experiment?Definitely, explain the Tooth-Wheel Experiment in the Introduction. (Also, wherever I mention the Tooth-Wheel Experiment mention how far the mirror needs to be. Originally, it was over $8 \unit{km}$ away.)
        * Should I mention Jupiter's moons. 
* Compare the speed of light to sound. Mention fireworks.
* The speed of light is constant.
    - Mention that this already violates relativity. 
* The speed of light can be measured to be different in different media. This explain refraction.
    - The Principle of Stationary Time


## Energy of Light

* Notes. 
    - Mention hot objects first glow red and then white. Explain that this is evidence that red light lower energy than other visible light.
* Light carries energy because it can heat things up. 
* There exists nonvisible light. 
    - Evidence. 
        * Eye dilate when looking at the sky on (a cloudy day) even though it does not seem more bright. 
        * When splitting light through a prism and measuring the intensity of the different colors, it can be seen that there are nonvisible colors. 
    - The light spectrum. 
        * Talk about this a little.
        * Have a picture. 
* Hot objects emit light. 
    - Evidence. 
        * This can be seen by holding one's hand over a stove top (which is not heat transferred from conduction/diffusion because this heat is felt even in a vacuum or with a fan blowing the air away). (Also, that the heat can be felt through glass before the glass is heated.)
        * Furthermore, this radiation can be seen to be light because, when splitting visible light through a prism, there exists radiation (which is sensed by heating) in spots where the visible light is not shown, and that radiation can be manipulated similarly to how light can be manipulated (such as being reflected, refracted and interference).  
    - Blackbody spectrum: Wein's Law (Just skip Stefan-Boltzman Law)
        * Should I somehow try to derive it? Should I derive it later? 
        * Notes. 
            - For now, this is taken as empirical. But, it can be derived from Planck's law. 
        * The energy in a cavity is evenly distributed and depends only on temperature because, if it wasn't, connecting two different cavities at the same temperature by a small hole, would lead to a violation of the Second Law of Thermodynamics.
        * Intuitively, some sort of relation between of the emission and absorption (such as their ratio) should only depend on the temperature because the temperature is literally defined to be the inclination to give heat. If a reasonable relationship did not exist, then a cold wall could heat a warm wall.
    - Line spectrum


## The Wave Approach

* Notes. 
    - I obviously need a better title. 
* The Double-Slit Experiment
    - This suggests that light is wave-like (with wavelength) and interferes with itself. In the next chapter, a wave equation is given.
    - This wave equation is linear, so light does interfere.
* Emphasize that the wave nature of light is needed on the smaller scale. Also, mention that the equation that governs the wave nature of light is introduced in the next chapter.
* Explain how the wavelength of light can be measures with the Michelson Interferometer.
    - Different colors have different wavelengths.
* The phase changes by $π$ when it is reflected from an interface where it is running into a material whose index of refraction is bigger than its current medium – or metal. (Do metals have an index of refraction?)
    - The same thing happens when a mechanical wave hits an interface, where it is running into a material whose wave speed is smaller than its current medium. (The wave flips upside down.)
* Iridescence: Explain how light refracts differently depending on the thickness of the medium and its wavelength. (This is why white light reflects off thin soap bubble in a spectrum of colors – because the thickness is not constant.)
  \end{enumerate}


## Polarization 