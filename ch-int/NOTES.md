# Notes for Integration

## Need to Understand

* What do integration schemes look like for recursive integrals? 
* What is the point of Chebyshev polynomials?
    - I think that, like Gaussian quadrature are used for polynomials in FEM, these are used for trigonometric functions. Actually, I think that these are just meant to minimize the error of the approximation by a polynomial. (I should move this note elsewhere.)
    - They reduce the problem of Runge's phenomenon. 


## Figures

* Integration Methods: 
    - Have all the schemes 
        + Endpoint Methods
    - Have them all the same size
    - Don't have scaling of the axes nor subdivisions
    - Drop a method if I do not talk about


## Material

* Gaussian Quadrature: Should I mention something about finding the roots? 
    - The polynomial alternates
    - The roots are samplings of a uniform partition 


## General

* Fineness is not measure theoretic, but is topological. It is about the nearness of the sample set to the sample point. Fineness should be defined as the max diameter, not the max content.
* Maybe drop that the product of integrable functions is integrable. 
* I am using $f$ and $γ$ because $γ$ is for trajectories, but the integral is more general. 