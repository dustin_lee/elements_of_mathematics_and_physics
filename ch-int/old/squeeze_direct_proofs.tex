\input{"/home/lee/tex/preamble_english.tex"}
\begin{document}
\mytitle{Direct Proofs for the Squeeze Approach}


\theorem Let $f$ be a continuous real-valued function on an interval $[c, d]$. Then, the derivative of the function 
\[
  ∫_{s:c→t} f(s)
\]
is $f(t)$. 

\begin{proof}
Let $a$ be a point in the interval $a$. 
Then, the relations
\[
\begin{split}
  ∂\left(∫_{s:c→t} f(s) \right)(a)
  &= \lim_{ε→0,\ ε≠0} \frac{∫_{s:c→(a+ε)} f(s) - ∫_{s:c→a} f(s)}
                           {ε} \\
  &= \lim_{ε→0,\ ε≠0} \frac{∫_{s:a→(a+ε)} f(s)}
                           {ε} \\
\end{split}
\]
hold. 
Thus, it suffices to prove that the number 
\[
  \lim_{ε→0,\ ε≠0} \frac{∫_{s:a→(a+ε)} f(s)}
                        {ε} \\
\]
is $f(a)$.

For any real number $ε$, the relations
\[
\begin{split}
  ε ⋅ \inf_{\min(a,a+ε)≤t≤\max(a,a+ε)} (f(t))
  &≤  \int_{t:a→(a+ε)} \inf_{\min(a,a+ε)≤t≤\max(a,a+ε)} (f(t)) \\
  &≤  \int_{t:a→(a+ε)} f(t) \\
  &≤  \int_{t:a→(a+ε)} \sup_{\min(a,a+ε)≤t≤\max(a,a+ε)} (f(t)) \\
  &≤ ε ⋅ \sup_{\min(a,a+ε)≤t≤\max(a,a+ε)} (f(t)) \\
\end{split}
\]
hold. 
Thus, for any real number $ε$, the relations 
\[
  \inf_{\min(a,a+ε)≤t≤\max(a,a+ε)} (f(t))
  ≤  \frac{\int_{t:a→(a+ε)} f(t)}
           {ε}
  ≤ \sup_{\min(a,a+ε)≤t≤\max(a,a+ε)} (f(t))
\]
hold. 
Thus, by the Squeeze Theorem, the number 
\[
  \lim_{ε→0,\ ε≠0} \frac{∫_{s:a→(a+ε)} f(s)}
                        {ε} \\
\]
is indeed $f(a)$.
\end{proof}


\theorem Let $f$ be a continuous real-valued function on an interval $[c, d]$. Then, it is integrable. 

\begin{proof}
Let $ε$ be a positive real number. 
Because the function is continuous and its domain in compact, it is uniformly continuous. 
Let $δ$ be a positive real number such that, if the distance between two points $a$ and $b$ in the interval $[c, d]$ is smaller than $δ$, then the distances between $f(a)$ and $f(b)$ is smaller than $δ$. 
Let $P$ be a partition of the interval $[c, d]$ whose fineness is smaller than $δ$. 
And let $φ$ and $ψ$ be the step functions such that, for each interval in the partition $P$, their values are the infimum and the supremum of $f$ on this interval, respectively. 
Then, $φ$ is at most $f$, and $f$ is at most $ψ$. 
Furthermore, the relation
\[
  ∫_{t:c→d} ψ(t) - ∫_{t:c→d} φ(t) < ε⋅\dist(c, d)
\]
holds. 
Thus, the function $f$ is indeed integrable. 
\end{proof}


\theorem Let $f$ be a monotone real-valued function on an interval $[c, d]$. Then, it is integrable. 

\begin{proof}
Let $ε$ be a positive real number,
let $P$ be a finite partition of the interval $[c, d]$ into intervals whose fineness is smaller than $ε$,
and let $φ$ and $ψ$ be step functions such that, for each interval in the partition $P$, their values on this interval are the values of $f$ at the start and end of this interval, respectively. Then, the function $f$ is between these functions, and the relations 
\[
\begin{split}
  \abs{∫_{t:c→d} ψ(t) - ∫_{t:c→d} φ(t)}
  &= \abs{∑_{I \in P} \left( ψ(t) - φ(t) \right) ⋅ \len(I) }\\
  &< ε⋅∑_{I \in P} \abs{ψ(t) - φ(t)} \\
  &= ε⋅∑_{I \in P} \abs{f(\ept(I)) - f(\start(I))} \\
  &= ε⋅\abs{f(d) - f(c)} \\
 \end{split}
\]
hold. 
Thus, the function $f$ is indeed integrable. 
\end{proof}


\theorem Let $(f_i)_{i≥1}$ be a uniformly convergent sequence of integrable real-valued function on an interval $[c, d]$. 
Then, the relation
\[
  \lim_{i→\infinity} ∫_{t:c→d} f_i(t)
  = ∫_{t:c→d} \left(\lim_{i→\infinity} f_i\right)(t)
\]
holds. 

\begin{proof}
Let $f$ be the function $\lim_{i\toi} f_i$. 
First, it is proved that the function $f$ is integrable. 
Let $ε$ be a positive real number. 
Let $n$ be an index such that the distance between the functions $f$ and $f_n$ is smaller than $ε$.
Let $φ$ and $ψ$ be step functions such that $φ$ is smaller than $f_n$, $f_n$ is smaller than $ψ$, and the distance between them is less than $ε$. 
Then, the function $φ-ε$ is smaller than $f$, $f$ is smaller than $ψ+ε$, and the distance between the numbers
\[
  \abs{∫_{t:c→d} (ψ+ε)(t)
  \textand
  ∫_{t:c→d} (φ-ε)(t)}
\]
is smaller than $3⋅ε⋅\abs{d-c}$.
Thus, the function $f$ is indeed integrable. 

Now, it is proved that the relation 
\[
  \lim_{i\toi} ∫_{t:c→d} f_i(t)
  = ∫_{t:c→d} f(t)
\]
holds. 
Let $ε$ be a positive real number; and let $n$ be an index such that, for any index $i$ that is bigger than it, the distance between $f_i$ and $f$ is smaller than $ε$. 
Then, the relations
\[
\begin{split}
  \abs{∫_{t:c→d} f_i(t) - ∫_{t:c→d} f(t)}
  &≤ ∫_{t:c→d} \abs{f_i(t) - f(t)}  \\
  &≤ ε⋅\abs{d-c}
\end{split}
\]
hold for any index $i$ that is bigger than $n$. 
Thus, the relation 
Die Beziehung
\[
\lim_{i→\infinity} ∫_{t:c→d} f_i(t)
= ∫_{t:c→d} f(t)
\]
does indeed hold. 
\end{proof}


\end{document}
