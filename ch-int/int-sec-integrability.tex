%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Summary 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% First, it is proved that integrable functions are bounded and that integration preserves inequalities.
% It is proved that continuous functions and monotone functions are both integrable.
% And, it is proved that addition, multiplication and the size function preserve integrability.
% The section ends with an alternative viewpoint of integration that is based on uniform convergence. 
% \scenebreak

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Sums and Components
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Sums
The next theorem says that sum of two integrable functions is integrable. 
Its proof follows directly from the definition of the integral.

\begin{theorem}
Let $f$ and $g$ be integrable function on a compact interval $[a, b]$ to a euclidean space. 
Then, the relation 
\[
    ∫_a^b \left( f + g \right) = ∫_a^b f + ∫_a^b g
\]
holds. 
\end{theorem}

% Components
The next theorem says that the integral of a function is the integrals of its components. 
Thus, an integral can be computed by computing integrals of real-valued functions. 
Its proof follow directly from the last theorem. 

\begin{theorem}
Let $f$ be a function from a compact interval $[a, b]$ to a euclidean space. 
Then, the relation
\[
    ∫_a^b f
    = \left(
        ∫_a^b \comp_1 (f),
        \ldots,
        ∫_a^b \comp_n (f)
      \right)
\]
holds, where $n$ is the dimension of the range of $f$. 
\end{theorem}


\scenebreak 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Continuity and Monotone
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Continuous functions
The next theorem says that continuous functions are integrable. 

\input{int-thm-continuous}


% Monotone 
The next theorem says that monotone functions are integrable. 

\input{int-thm-monotone}


\scenebreak 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Products, Max, Min, Triangle Inequality
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Products 
The next theorem says that the product of two integrable functions is integrable. 

\input{int-thm-product}


The next theorem says that integration preserves inequalities.
It follows directly from the definition of the integral. 

\begin{theorem}
Let $f$ and $g$ be integrable real-valued function on a compact interval $[a,b]$ such that $f$ is at most $g$. 
Then, the relation
    \[
      ∫_a^b f(t) ≤ ∫_a^b g(t)
    \]
holds. 
\end{theorem}


% Max and Min
The next theorem says that the maximum and minimum preserve integrability. 

\input{int-thm-max}


% Triangle Inequality
The next theorem says that the size function preserves integrability.
It is called the \emph{Triangle Inequality}\index{Triangle Inequality}. 

\input{int-thm-triangle-inequality}


\scenebreak

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Uniform Convergence
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

The next theorem is called the \emph{Uniform Integration Theorem}\index{Uniform Integration Theorem}.
It says that integrals and uniform limits can be swapped. 

\input{int-thm-uniform}

The last theorem gives an alternative construction of integrals. 
Let $v(t)$ and $v'(t)$ be the velocities as a function of time of two particles whose positions are both $p$ at the time point $a$.
Then, intuitively, the distance between these particles at a later time point $b$ is less than 
\[
    \sup_{a≤t≤b}\big( \abs{v(t) - v'(t)} \big)⋅\abs{b - a}
    . 
\]
Furthermore, if the integral of the velocity $v'$ of the second particle is known, then this gives an approximation of the position of the first particle. 
Because the integrals of piecewise-constant functions are known, if a function can be uniformly approximated by them, then this constructs its integral. 

The last theorem says that these two constructions of the integral are equivalent if the function can be uniformly approximated by piecewise-constant functions. But, integrable functions exist that are not the uniform limit of a sequence of piecewise-constant functions.
For example, let $f(t)$ be the function on the interval $[0, 1]$ whose value is $1$ when $t$ is of the form $\frac{1}{n}$, where $n$ is an integer, and whose value is $0$ otherwise. Then, the integral of $f$ is $0$, but there does not exist a sequence of piecewise-constant functions that converge uniformly to $f$.
Intuitively, such functions are not physical.