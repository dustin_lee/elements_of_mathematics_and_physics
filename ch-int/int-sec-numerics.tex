%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Summary 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% First, in this section, some integration schemes and their order of convergence are explained. 
% Then, integration schemes that are exact on polynomials are introduced 
% These latter schemes are useful for the Finite-Elements Method, which is introduced in Chapter~\ref{ch:static}.
% \scenebreak

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Schemes
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Let $f$ be an integrable real-valued function on a compact interval $[a, b]$.
Let $P$ be a partition of the interval $[a, b]$, and let $\set{c_I}_{I \in P}$ be a sampling of this partition.
Then, the integral of $f$ is approximately 
\[
    ∑_{I \in P} f(c_I)⋅\len(I)
\]
if $P$ is sufficiently fine. 
Thus, this is a method to approximate the integral of $f$. 
The choice of the partition $P$ and its sampling $\set{c_I}_{I \in P}$ is an integration scheme.
An \emph{integration scheme}\index{integration scheme} is defined to be an approximation of the integral of $f$ by a sum of the form 
\[
    ∑_{1≤i≤n} f(c_i)⋅w_i
    ,
\]
where $c_i$ is a point in the domain of $f$ and $w_i$ is a positive number for each index $i$. 

A \emph{partition up to singletons} of a set $X$ is a set of subsets of $X$ such that their union is $X$ and the intersection between pairs of them has at most one element. 
The definition of the integral can use partitions up to singletons instead of just partitions because the lengths of these intervals are equal.  
And, a \emph{uniform partition}\index{unfiorm partition} of an interval $I$ is a partition of the interval $I$ into interval of equal length.

The integration schemes now studied are the Endpoint Rules\index{Endpoint Rules}, the Midpoint Rule, the Trapezoidal Rule and Simpson's Rule.
Let 
\[
    [a_1, b_1], \textspace
    \ldots, \textspace
    [a_{n-1}, b_{n-1}]
    \textand 
    [a_n, b_n]
\]
be a uniform partition up to singletons of the interval $I$. 
And, for each index $i$, let $c_i$ be the center of the interval $[a_i, b_i]$, that is, the number $\frac{c_i+b_i}{2}$. 
The \emph{Left-Endpoint Rule}\index{Left-Endpoint Rule} is the approximation of the integral of $f$ by 
\[
    \sum_{0≤i≤(n-1)} f(a_i) ⋅ \frac{\len(I)}{n}
    .
\]
The \emph{Right-Endpoint Rule}\index{Right-Endpoint Rule} is the approximation of the integral of $f$ by 
\[
    \sum_{0≤i≤n} f(b_i) ⋅ \frac{\len(I)}{n}
    .
\]
The \emph{Midpoint Rule}\index{Midpoint Rule} is the approximation of the integral of $f$ by 
\[
    \sum_{0≤i≤(n-1)}
    f(c_i) ⋅ \frac{\len(I)}{n}
    .
\]
The \emph{Trapezoidal Rule}\index{Trapezoidal Rule} is the approximation of the integral of $f$ by
\[
    \sum_{0≤i≤n}
    \frac{f(a_i) + f(b_i)}{2}⋅ \frac{\len(I)}{n}
    .
\]
The Trapezoidal Rule is the mean of the Left-Endpoint Rule and the Right-Endpoint Rule.
And, \emph{Simpson's Rule}\index{Simpson's Rule} is the approximation of the integral of $f$ by
\[
    \sum_{0≤i≤n}
    \frac{f(a_i) + 4⋅f(c_i) + f(b_i)}{6}⋅ \frac{\len(I)}{n}
    .
\]
Simpson's Rule is the mean of the Left-Endpoint Rule, the Right-End Point Rule and four terms of the Midpoint Rule. 

Each of these schemes converge to the integral as $n$ goes to infinity. 
The Trapezoidal Rule and Simpson's Rule both converge because they are the mean of schemes that converge. 
But, the previous figure and the next figure indicate the convergence of the Endpoint Rules are slower than the Midpoint Rule and Trapezoidal Rule, which are slower than Simpson's Rule. 


\begin{figure}[H]
\centering
    \includegraphics[width=0.5\textwidth]{int-fig-midpoint}
    \includegraphics[width=0.5\textwidth]{int-fig-trapezoid}
    \includegraphics[width=0.5\textwidth]{int-fig-simpson}
    \caption{Integration methods. The top is the Midpoint Method. The middle is the Trapezoidal Method. And, the bottom is Simpson's Method.}
\end{figure}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Order of convergence
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

The order of convergence of these integration schemes can be computed with Taylor's Theorem, which is introduced in Chapter~\ref{ch:psums}.
For example, the order of convergence of the Midpoint Rule is computed. 
Assume that the function $f$ is two-times continuous differentiable. 
Then, by Taylor's Theorem, a positive real number $B$ exists and a function $φ_i(t)$ for each index such that the relations 
\[
    f(t) = f(c_i) + f'(c_i)⋅(t-c_i) + φ_i(t)
\]
and 
\[
    \abs{φ_i(t)} ≤ B⋅\abs{t-c_i}^2
\]
hold fo reach index. 
Furthermore, without loss of generality, assume that this number $B$ is independent of any partition because the second derivative of $f$ is continuous and is on a compact domain. 
Thus, the relations 
\begin{align}
    & \abs{
        f\left(c_i\right) ⋅ \frac{\len(I)}{n}
        - ∫_{t:a_i}^{b_i} f(t)s} 
    \\
    &= 
    \abs{
        f(c_i) ⋅ \frac{\len(I)}{n}
        - ∫_{t:a_i}^{b_i} \Big( 
            f(c_i) 
            + f'(c_i)⋅(t-c_i)
            + φ_i(t)
        \Big)
        } 
    \\
    &= 
    \abs{
        f(c_i) ⋅ \frac{\len(I)}{n}
        - ∫_{t:a_i}^{b_i} f(c) 
        - ∫_{t:a_i}^{b_i} f'(c_i)⋅(t-c_i)
        - ∫_{t:a_i}^{b_i} φ_i(t)
        } 
    \\
    &= \begin{aligned}[t] 
        \Bigg|
             & f(c_i) ⋅ \frac{\len(I)}{n} 
             - f(c_i) ⋅ \frac{\len(I)}{n}
        \\
            &- \Big( f'(c_i)⋅(b_i-c_i)^2 - f'(c_i)⋅(c_i-c_i)^2 \Big)
            - ∫_{t:c_i}^{b_i} φ_i(t)
        \Bigg|
    \end{aligned}
    \\
    &= \abs{∫_{t:a_i}^{b_i} φ_i(t)} \\
    &≤ ∫_{t:a_i}^{b_i} \abs{φ_i(t)} \\
    &≤ \frac{\len(I)}{n} ⋅ B ⋅ \left(\frac{\len(I)}{2⋅n}\right)^2 \\
    &= \frac{B⋅\left(\len(I)\right)^3}{4}  ⋅ \frac{1}{n^3} 
\end{align}
hold for any index $i$. 
Thus, the relations
\begin{align}
    &\abs{
        \sum_{0≤i≤(n-1)}
            f\left(
                c_i
            \right) ⋅ \frac{\len(I)}{n}
        - ∫_a^b f
    }
    \\
    &≤ \sum_{0≤i≤(n-1)} 
        \abs{
        f\left(
                c_i
            \right) ⋅ \frac{\len(I)}{n}
        - 
        ∫_{a_i}^{b_i} f
        }
    \\
    &≤ \sum_{0≤i≤(n-1)} \frac{B⋅\left(\len(I)\right)^3}{4}  ⋅ \frac{1}{n^3} 
    \\
    &= n ⋅ \frac{B⋅\left(\len(I)\right)^3}{4}  ⋅ \frac{1}{n^3} 
    \\
    &= \frac{B⋅\left(\len(I)\right)^3}{4}  ⋅ \frac{1}{n^2} 
\end{align}
hold. 
Thus, the polynomial order of convergence of the Midpoint Rule is $2$. 

The orders of convergence can be computer similarly for the other schemes. 
The polynomial order of the Right-Endpoint Rule and the Left-Endpoint Rule are both $1$.
The polynomial order of the Trapezoidal Rule is $2$.
And, the polynomial order of Simpson's Rule is $4$.
In practice, fourth-order is about optimal, and Simpson's Rule is effective for most problems. 


\begin{figure}[H]
\centering
    \includegraphics[width=0.5\textwidth]{int-fig-convergence-order}
    \caption{
        Convergence of integration schemes.
        % Each scheme is used to approximate the integral of the function $\sin(x) + x^9$ on the interval $[0, 1]$ for various partitions of the domain.
        The scales are logarithmic.}
\end{figure}


\scenebreak

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Polynomials
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Now, more integration schemes are introduced. 
These schemes are exact on polynomials. 
And, they are useful for the Finite-Elements Method, which is introduced in Chapter~\ref{ch:static}.

Let $[a, b]$ be a compact interval. 
Let $c_0$, $\ldots$, $c_{n-1}$ and $c_n$ be different points in this interval. 
And, let $f$ be an $n$-degree polynomial. 
By the Lagrange Interpolation Formula, the relation 
\[
    f(t) = ∑_{0≤i≤n}
            f(c_i) ⋅ \left(
            \prod_{\substack{0≤j≤n \\ j≠i}} \frac{t - c_j}{c_i - c_j}
            \right)
\]
holds. 
Thus, the relation
\[
    ∫_{t:a}^b f(t)
    = ∑_{0≤i≤n} f(c_i) ⋅ \left(
        ∫_{t:a}^b
        \prod_{\substack{0≤j≤n \\ j≠i}} \frac{t - c_j}{c_i - c_j}
        \right)
\]
holds.
And, for each index $i$, the number
\[
    ∫_{t:a}^b \prod_{\substack{0≤j≤n \\ j≠i}} \frac{t - c_j}{c_i - c_j}
\]
does not depend on $f$. 
Thus, this is an integration scheme that is exact for any polynomial whose degree is at most $n$. 


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Guassian-Quadrature Rule
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 

The points $c_0$, $\ldots$, $c_{n-1}$ and $c_n$ are arbitrary. 
The next theorem is an optimization of the selection of these points. 
% It is called the \emph{Guassian-Quadrature Rule}\index{Gaussian-Quadrature Rule}.
The next lemma uses the inner-product properties of the space of square-integrable functions, which are explained in Chapter~\ref{ch:la}. 

\input{int-thm-quadrature}

The proof of the lemma is constructive. 
Thus, this method is explicit. 

This method requires the construction of the $g$, the computation of its roots and the computation of the number
\[
    ∫_{t:a}^b
        \prod_{\substack{0≤j≤n \\ j≠i}} \frac{t - c_j}{c_i - c_j}
\]
for each index $i$. 
This is computationally expensive. 
But, these computations are only done once to explicitly describe the integration scheme. 
And, then, the integration scheme itself is fast. 
Thus, this method is only practical if the integrals of many polynomials are computed.
This is common for the Finite-Element Method. 


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Change of Interval 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Now, it is explained how the explicit description of this scheme for one interval can be used for other interval. 
This means that the such a scheme does not need to be computed form scratch for each interval. 
Let $[a', b']$ be another compact interval. 
Let $h$ be the function 
\[
    \frac{b'-a'}{b-a}⋅(t-a) + a'
    . 
\]
Then, the relations
\[
    h(a)
    = \frac{b'-a'}{b-a}⋅(a-a) + a'
    = a'
\]
and
\[
    h(b)
    = \frac{b'-a'}{b-a}⋅(b-a) + a'
    = b' - a' + a'
    = b'
\]
hold. 
Thus, by the Integration-by-Substitution Formula, the relations
\begin{align}
    &∫_{t:a'}^{b'} f(t) \\
    &= ∫_{t:h(a)}^{h(b)} f(t) \\
    &= ∫_{t:a}^b f(h(t))⋅\dot{h}(t) \\
    &= \frac{b'-a'}{b-a} ⋅ ∫_{t:a}^b f(h(t)) \\
    &= ∑_{0≤i≤n} f(g(c_i)) ⋅ \left(
        ∫_{t:a}^b
        \prod_{\substack{0≤j≤n \\ j≠i}} \frac{t - c_j}{c_i - c_j}
        \right) \\
    &= ∑_{0≤i≤n}
        f\left(\frac{b'-a'}{b-a}⋅(t-a) + a'\right) 
        ⋅ \left(
            ∫_{t:a}^b
            \prod_{\substack{0≤j≤n \\ j≠i}} \frac{t - c_j}{c_i - c_j}
        \right)
\end{align}
hold, where the fourth relation holds because the degree of $f(h(t))$ is the degree of $f(t)$ because the degree of $h(t)$ is $1$. 
This is an integration scheme for the interval $[a', b']$. 

