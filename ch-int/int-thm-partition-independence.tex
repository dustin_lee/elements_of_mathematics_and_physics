\begin{theorem}
Let $f$ be a function from an interval $[a, b]$ to a euclidean space. 
And, let $(P_i)_{i≥1}$ be a sequence of partitions of $[a, b]$ into intervals such that the sequence of their fineness converges to $0$. 
Then, the function $f$ is integrable exactly if, for any two sequences 
\[
    \Big( \set{c_I}_{I \in P_i} \Big)_{i≥1}
    \textand
    \Big( \set{c'_I}_{I \in P_i} \Big)_{i≥1}
\]
of samplings of these partitions, the relation
\[
    \lim_{i→∞} ∑_{I \in P_i} c_I ⋅ \len{I}
    = \lim_{i→∞} ∑_{I \in P_i} c'_I ⋅ \len{I}
\]
holds. 
\end{theorem}

\begin{proof}
First, assume that the function $f$ is integrable. 
Then, by definition, for any two sequences 
\[
    \Big( \set{c_I}_{I \in P_i} \Big)_{i≥1}
    \textand
    \Big( \set{c'_I}_{I \in P_i} \Big)_{i≥1}
\]
of samplings of the partitions $(P_i)_{i≥1}$, the relation
\[
    \lim_{i→∞} ∑_{I \in P_i} c_I ⋅ \abs{I}
    = \lim_{i→∞} ∑_{I \in P_i} c'_I ⋅ \abs{I}
\]
holds.

Now, instead assume that, for any two sequences 
\[
    \Big( \set{c_I}_{I \in P_i} \Big)_{i≥1}
    \textand
    \Big( \set{c'_I}_{I \in P_i} \Big)_{i≥1}
\]
of samplings of the partitions $(P_i)_{i≥1}$, the relation
\[
    \lim_{i→∞} ∑_{I \in P_i} c_I ⋅ \abs{I}
    = \lim_{i→∞} ∑_{I \in P_i} c'_I ⋅ \abs{I}
\]
holds. 
Let $L$ be the common limit.
Let $(Q_i)_{i≥1}$ be a sequence of partitions whose sequence of finenesses converge to $0$, and let $(d_i)_{i≥1}$ be a sequence of samplings of them. 
It suffices to proved that the relation 
\[
    \lim_{i→∞} ∑_{I \in P'_i} d_I ⋅ \len{I} = L
\]
holds. 
Let $ε$ be a positive number.

For each index $i$, let $\set{c_I}_{I \in P_i}$ be a sampling of the partition $P_i$ such that, for any other sampling $\set{c'_I}_{I \in P_i}$ of the partition $P_i$, the relation
\[
    \abs{∑_{I \in P_i} c_I ⋅ \len{I} - L} 
    ≥ \abs{∑_{I \in P_i} c'_I ⋅ \len{I} - L} 
\]
holds, that is, the error from sampling $\set{c_I}_{I \in P_i}$ is maximized. 
Let $n$ be an index such that the relation
\[
    \abs{∑_{I \in P_i} c_I ⋅ \len{I} - L} < \frac{ε}{6}
\]
holds. 

For each index $i$, let $R$ be the common refinement of $P_n$ and $Q_i$.
And, for each index $i$, let $\set{e_I}_{I \in R_I}$ be a sampling of $R_i$ such that it contains the sampling $\set{d_I}_{I \in Q_i}$.
Let $m$ be an index such that, for any index $i$ that is more than $m$, the fineness of $Q_i$ is less than 
\[
    \frac{ε}{6⋅B⋅\card(P_n)}
\]
Furthermore, by possibly replacing $m$ and without loss of generality, assume that, for any index $i$ that is more than $m$, such that the relation 
\[
    \max_{I \in Q_i} \len(I) ≤ \frac{1}{2} ⋅ \min_{I \in P_n} \len(I)
\]
holds, that is, such that the fineness of $Q_i$ is less than the length of the smallest interval in $P_n$. 
Then, if an index $i$ is more than $m$, at most 
\[
    2⋅\card(P_n)
\]
intervals in $Q_i$ are not in $R$.
Thus, the relations
\begin{align}
    &\abs{∑_{I \in Q_i} \Big( f(d_I)-f(d_I) \Big) ⋅ \len(I) - L} \\
    &≤ \begin{aligned}[t] 
        & \abs{∑_{I \in Q_i} \Big( f(d_I)-f(d_I) \Big) ⋅ \len(I)
            - ∑_{I \in R_i} \Big( f(e_I)-f(e_I) \Big) ⋅ \len(I)
            }
        \\
        &+ \abs{∑_{I \in R_i} \Big( f(e_I)-f(e_I) \Big) ⋅ \len(I)
               - ∑_{I \in P_n} \Big( f(c_I)-f(c_I) \Big) ⋅ \len(I)
               } 
        \\
        &+ \abs{∑_{I \in P_n} \Big( f(c_I)-f(c_I) \Big) ⋅ \len(I)
              - L 
               } 
    \end{aligned}
    \\
    &< 2⋅B⋅\card(P_n)⋅\frac{ε}{6⋅B⋅\card(P_n)}
       + 2⋅\frac{ε}{6}
       + \frac{ε}{6} 
    \\
    &< ε
\end{align}
hold. 

% For after:
% The relations
% \begin{align}
%     & \abs{∑_{I \in Q_i} \Big( f(d_I)-f(d_I) \Big) ⋅ \len(I)
%           - ∑_{I \in R_i} \Big( f(e_I)-f(e_I) \Big) ⋅ \len(I)
%           }
%     \\
%     &= \abs{∑_{\substack{I \in Q_i\\I \notin R_i}} 
%             \Big( f(d_I)-f(d_I) \Big) ⋅ \len(I)
%           - ∑_{\substack{I \in R_i\\I \notin Q_i}} 
%             \Big( f(e_I)-f(e_I) \Big) ⋅ \len(I)
%           }
%     \\
%     &≤ \card(P_n) ⋅ \max_{I \in Q_i} \Big( \len(I) \Big) ⋅ 2⋅B  
%     \\
%     &≤ \frac{ε}{3}
% \end{align}
% hold. 

% Connecting the refinement to the maximum sampling: 
% Still, how do I get it close to a refinement? 
% Intuitively, this should be easy. 
% Well, for two different partitions, the bound is double error of the maximum sampling. 
% The problem is that the refinement gives both more and less freedom. 

% 
\end{proof}