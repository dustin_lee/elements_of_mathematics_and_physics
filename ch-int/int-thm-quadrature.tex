%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Lemma
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{lemma}
Let $[a, b]$ be a compact nonempty interval, and let $n$ be a positive integer. 
Then, a polynomial $g$ exists such that, for any polynomial $f$ whose degree is at most $n-1$, the relation
\[
    ∫_a^b f⋅g = 0
\]
holds. 
Furthermore, the roots of this polynomial are in the interval $[a, b]$ and the multiplicity of each is $0$.
\end{lemma}

\begin{proof}
First, it is proved that a polynomial $g$ exists such that, for any polynomial $f$ whose degree is at most $n-1$, the relation
\[
    ∫_a^b f⋅g = 0
\]
holds.
This is done by applying the Gram-Schmidt Process to the polynomials $1$, $t$, $\ldots$, $t^{n-1}$ and $t^n$ in the space of square-integrable functions. 
Let $g_0(t)$ be the polynomial $1$. 
And, recursively, let $g_i(t)$ be the polynomial 
\[
    t^i
    - ∑_{1≤j≤(i-1)} \frac{∫_{t:a}^b t^i ⋅ g_{j}(t)}
                         {∫_{t:a}^b g_{j}(t) ⋅ g_{j}(t)}
                    ⋅ g_{j}(t) 
\]
for any index $i$ between $1$ and $n$. 
Then, for any two different indices $i$ and $j$, the relation
\[
    ∫_{t:a}^b g_i(t)⋅g_j(t) = 0
\]
holds. 
Let $f(t)$ be a polynomial whose degree is at most $n-1$. 
Then, there exists real numbers $d_1$, $\ldots$, $d_{n-1}$ and $d_n$ such that the relation
\[
    f(t) = ∑_{1≤i≤(n-1)} d_i⋅g_i(t)
\]
holds. 
Thus, the relations 
\[
    ∫_a^b f⋅g_ns
    = ∫_a^b \left(∑_{1≤i≤(n-1)} d_i⋅g_i⋅g_n \right)
    = 0
\]
hold. 
Thus, the polynomial $g_n$ is such a desired polynomial. 

Now, it is proved that the roots of this polynomial $g$ are in the interval $[a, b]$ and the multiplicity of each is $1$.
If a real root $c$ of $g$ is not in the interval $[a, b]$, then the factor $t-c$ of $g$ is either nonnegative or nonpositive on the interval $[a, b]$.
If a root $c$ of $g$ is complex, then the factor $(t-c)(t-c)$ of $g$ is either nonnegative or nonpositive. 
If the multiplicity of a root $c$ of $g$ is even, the factor $(t-c)^{\mul(c)}$ of $g$ is either nonnegative or nonpositive.
Let $c_1$, $\dots$, $c_{m-1}$ and $c_m$ be the real roots of the polynomial $g$ in the interval $[a, b]$ whose multiplicities are odd. 
Let $h(t)$ be the polynomial
\[
    g(t) ⋅ ∏_{1≤i≤m} (t - c_i)
    . 
\]
Then, the polynomial $h(t)$ is either nonnegative or nonpositive on the interval $[a, b]$.
Thus, the relations
\[
    ∫_{t:a}^b h(t)  = ∫_{t:a}^b g(t) ⋅ ∏_{1≤i≤m} (t - c_i) ≠ 0
\]
holds. 
Thus, the degree of the polynomial
\[
    ∏_{1≤i≤m} (t - c_i)
\]
is at least $n$. 
Thus, the polynomial $g$ has $n$ roots in the interval $[a, b]$ whose multiplicity is odd. 
And, because the degree of $g$ is $n$, it indeed has $n$ single roots in the interval $[a, b]$.
\end{proof}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Theorem
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{theorem}
Let $[a, b]$ be a compact nonempty interval, and let $n$ be a positive integer. 
Then, there exist different points $c_1$, $\ldots$, $c_{n-1}$ and $c_n$ in this interval such that, for any polynomial $f$ whose degree is at most $2⋅n-1$, the relation
\[
    ∫_a^b f
    = ∑_{1≤i≤n} f(c_i) ⋅ \left(
        ∫_{t:a}^b
        \prod_{\substack{1≤j≤n \\ j≠i}} \frac{t - c_j}{c_i - c_j}
        \right)
\]
holds. 
\end{theorem}

\begin{proof}
By the lemma, an $n$-degree polynomial $g$ exists such that, for any polynomial $f$ whose degree is at most $n-1$, the relation
\[
    ∫_a^b f⋅g = 0
\]
holds, and such that the roots of $g$ are different and in the interval $[a, b]$.
Let $c_1$, $\ldots$, $c_{n-1}$ and $c_n$ be the roots of $g$.
Let $f$ be a polynomial whose degree is at most $2⋅n-1$. 
By the Quotient-Remainder Theorem, polynomials $h$ and $r$ exist such that the relation 
\[
    f = h⋅g + r
\]
holds and such that the degree of $r$ is at most $n-1$.
Furthermore, the degree of $h$ is at most $n-1$. 
Thus, the relations
\begin{align}
    ∫_a^b f(t)
    &= ∫_a^b \Big( h⋅g + r \Big) \\
    &= ∫_a^b h⋅g + ∫_a^b r \\
    &= 0 + ∫_a^b r \\
    &= ∑_{1≤i≤n} r(c_i) ⋅ \left(
        ∫_{t:a}^b \prod_{\substack{1≤j≤n \\ j≠i}} \frac{t - c_j}{c_i - c_j}
        \right) \\
    &= ∑_{1≤i≤n} f(c_i) ⋅ \left(
        ∫_{t:a}^b \prod_{\substack{1≤j≤n \\ j≠i}} \frac{t - c_j}{c_i - c_j}
        \right) 
\end{align}
hold, where the third relation holds because the degree of $h$ is at most $n-1$, where the second-to-last relation holds because the degree of $r$ is at most $n-1$, and where the last relation holds because the relations 
\begin{align}
    f(c_i)
    &= h(c_i)⋅g(c_i) + r(c_i) \\
    &= h(c_i)⋅0 + r(c_i) = r(c_i) 
\end{align}
hold for each index $i$. 
\end{proof}