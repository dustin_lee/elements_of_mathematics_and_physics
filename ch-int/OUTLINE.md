# Outline of Integration

## Introduction

* Intuition and Definition
    - Intuition
    - Definition 
* Making Rigorous
    - Explanation of how this is made rigorous
    - Law of domain division (for FTC) 
    - The Fundamental Theorem of Calculus
    - Constant-Difference Theorem 
* Additional remarks
    - Laws with constants (for iterated integrals)
    - Iterated integrals
    - Time Reversal 
        + Is this even used anywhere? 


## Integrability 

* Common functions are integrable
    - Continuous functions
    - Monotone functions
* Operations preserve integrability 
    - Sums 
    - Components 
    - Products
    - Triangle Inequality 
* Uniform limits and integration


## Symbolic Methods

* The Newton-Leibniz Formula
* The Integration-by-Substitution Formula
* The Integration-by-Parts Formula


## Numerical Methods

* Basic Schemes. 
    - The Fundamental Theorem doesn't just provide existence, but by how integrals are defined, it gives a numerical method.
    - The Midpoint Rule.
    - The Trapezoidal Rule.
        + This is used for the Runge-Kutta Method. 
    - Simpson's Rule. 
    - Error and Order:
        + Have the convergence plots. (It is good to repeat such figures.)
        + Mention that fourth or fifth order is normally as good as is needed in practice. 
    - Talk testing:
        + Checking the rate of convergence (as above) is a test. 
        + Compare to symbolic solutions. 
* Integration of polynomials. 
    - Basics. 
        + The scheme. 
    - Guassian quadrature. 
        + Should I mention that all the roots of $f$ are simple, real and in the interval?
        + Should I talk about the error?
    - Change of intervals. 


