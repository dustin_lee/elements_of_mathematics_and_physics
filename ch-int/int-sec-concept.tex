%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%Summary 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% This section starts with the intuition of integration. 
% Then, the integral is defined. 
% The intuition is made rigorous by the Fundamental Theorem of Calculus and the Constant-Difference Theorem.
% The section ends with an explanation of integration for higher order derivatives and time reversal. 
% \scenebreak

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%Intuition and Definition
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Let $f$ be the velocity of a particle as a function of time, and let $p$ be the position of this particle at a time $a$.. 
Now, a method is explained to approximate the trajectory of the particle its velocity and its position at the time point $a$.

% Let $γ$ be the trajectory of a particle as a function of time on the compact interval $[a, b]$, that is, let it be a function from the interval $[a, b]$ to physical space.
% Assume that this function is not yet known, but that its velocity $\dot{γ}$ and position $γ(a)$ of the particle at the start time point $a$ is known. 
% A method is now given to approximate the position $γ(b)$ of the particle at the end time point $b$. 

Let $ε$ be a small positive real number, and let $c$ be a point between $a$ and $a+ε$. 
Then, intuitively, the velocity of the particle is approximately on the interval $[a, a+ε]$.
Thus, the position of the particle at the time point $a+ε$ is approximately
\[
	p + f(c)⋅ε
	. 
\]
Let $ε'$ be another small positive real number, and let $c'$ be a point between $a+ε$ and $a+ε+ε'$.
Then, intuitively, the position of the particle at the time point $a+ε+ε'$ is approximately
\[
	p + f(c)⋅ε + f(c')⋅ε'
	.
\]

This reasoning is continued. 
Let $b$ be a time point that is later than $a$. 
Now, let $ε_1$, $\ldots$, $ε_{n-1}$ and $ε_n$ be small positive real numbers such the sum 
\[
	a + ε_1 + \ldots + ε_n
\]
is $b$.
And, let $c_1$, $\ldots$, $c_{n-1}$ and $c_n$ be points such that, for each index $i$, the point $c_i$ is between 
\[
	a + ε_1 + \ldots + ε_{i-1}
	\textand 
	a + ε_1 + \ldots + ε_{i}
	. 
\]
Then, intuitively, the position of the particle at the time point $b$ is approximately
\[
	p + ∑_{1≤i≤n} f(c_i)⋅ε_i
	.
\]
Furthermore, intuitively, this approximation is closer if $n$ is bigger and the numbers $ε_1$, $\ldots$, $ε_{n-1}$ and $ε_n$ are smaller.
That is, this approximation should converge to $γ(b)$ as $n$ converges to infinity and the these numbers converge to $0$. 

Integration\index{integration} is now defined. 
Let $f$ be a function from a compact interval $[a, b]$ to a euclidean space.
Then, the function $f$ is said to be \emph{integrable}\index{integrability} if, for two sequences $(P_i)_{i≥1}$ and $(Q_i)_{i≥1}$ of partitions of $[a, b]$ interval such that their sequence of the finesses converges to $0$, and for any two sequences
\[
	\left( \{ c_I^{(i)} \}_{I \in P_i} \right)_{i≥1}
	\textand
	\left( \{ d_I^{(i)} \}_{I \in Q_i} \right)_{i≥1}
\]
of samplings of these partitions, the vectors 
\[
	\lim_{i →∞} \left( ∑_{I \in P_i} f(c_I^{(i)})⋅\len(I) \right)
	\textand
	\lim_{i →∞} \left( ∑_{I \in Q_i} f(d_I^{(i)})⋅\len(I) \right)
\]
exist and are equal. 
If the function $f$ is integrable, this common limit is called the \emph{integral}\index{integral} of $f$. 
The integral of $f$ is denoted by\label{not:integral}
\[
	∫_{t:a}^b f(t)
	\textor 
	∫_{a}^b f
	.
\]

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Validity of Definition (FTC and Constant-Difference Theorem)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

The next two theorems prove that this definition is valid, that is, they prove that the relation
\[
	γ(t) = γ(a) + ∫_a^t \dot{γ}
\]
does indeed hold. 
First, by the Fundamental Theorem of Calculus, the derivative of the function 
\[
	γ(a) + ∫_a^t f
\]
is indeed $γ(t)$. 
Then, by the Constant-Difference Theorem and because the vector
\[
	∫_a^a f
\]
is $0$, the function 
\[
	γ(a) + ∫_a^t f
\]
is the only function whose derivative is $γ(t)$ and whose value at the time point $a$ is $γ(a)$.

Let $f$ be a function from a compact interval $[a, b]$ to a euclidean space, and let $c$ be a number between $a$ and $b$. 
Then, the relation 
\[
	∫_a^b f = ∫_a^c f + ∫_c^b f
\]
holds.
This is used in the next proof as the relation
\[
	∫_a^b f - ∫_a^c f =  ∫_c^b f
	. 
\]

The next theorem is called the \emph{Fundamental Theorem of Calculus}\index{Fundamental Theorem of Calculus}. 
It is for functions that are both continuous and integrable. 
But, in Section~\ref{int:sec:integrability}, it is proved that all continuous functions are integrable, that is, the assumption of continuity is unnecessary.

\input{int-thm-ftc}


The next theorem is called the \emph{Constant-Difference Theorem}\index{Constant-Difference Theorem}.

\input{int-thm-const-diff}


\scenebreak


The next theorem says that integrable functions are bounded. 

\input{int-thm-bounded}


The next theorem is an equivalent definition of integration. 
It says that integrability is determined by any single sequence of partitions. 
It is easier to use for proofs. 

\input{int-thm-partition-independence}


\scenebreak

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Iterated Integrals and Time Reversal 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Let $[a, b]$ be a compact interval, and let $c$ be a real number. 
Then, the relation 
\[
	∫_{a}^b c = c⋅(b-a)
\]
holds.
Let $f$ be a function from a compact interval $[a, b]$ to a euclidean space. 
Then, the relation
\[
	∫_a^b c⋅f = c⋅∫_{a}^b f
\]
holds. 
Their proofs follow directly from the definition of the integral.

Let $f$ be a function from a compact interval $[a, b]$ to a euclidean space.
Then, the vector 
\[
	∫_b^a f 
\]
is defined to be 
\[
	-∫_a^b f
	. 
\]
Intuitively, this is thought of as time reversal. 

% Multiple integrals
Let $γ$ be the trajectory of a particle as a function of time on a compact interval $[a, b]$. 
Now, instead of assuming that its velocity is known, assume that its acceleration $\ddot{γ}$ is known. 
Then, by the Fundamental Theorem of Calculus, the relations
\begin{align}
    γ(t) &= γ(a) + ∫_{s:a}^t \dot{γ}(s) \\
    &= γ(a) + ∫_{s:a}^t \left( \dot{γ}(a) + ∫_{r:a}^s \ddot{γ}(r) \right) \\
    &= γ(a) + \dot{γ}(a)⋅(t-a) + ∫_{s:a}^t ∫_{r:a}^s \ddot{γ}(r) 
	. 
\end{align}
Thus, integrating the acceleration twice gives the position of the particle if the position and velocity of the particle at the initial time point $c$ are both known.
Together the position and velocity at the start time point are called the \emph{initial conditions}\index{initial condition}.
This reasoning can be extended for higher derivatives. 

For example, consider a particle whose acceleration $a$ is constant. 
Let $f(t)$ be the trajectory of this particle. 
And, let $p$ and $v$ be the position and velocity of the particle at the time point $0$. 
Then, the relations
\[
	f(t)
	= p + ∫_{s:0}^t \left( v + ∫_{r:0}^s a \right)
	= p + v⋅t + a⋅t^2
\]
hold. 
% !! Say something else? 

