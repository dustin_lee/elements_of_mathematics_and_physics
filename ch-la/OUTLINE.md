# Outline of Linear Equation

**Note.** For each result, mention when it is used. 


## Introduction

* Explain how these are used for differential equations. 
* Mention again how Newton's method can reduce nonlinear equations to linear equations. 


## Finite-Dimensional Linear Functions and Linear Equations

* Motivation? 
    - What is really the point of these things? 
* Rank-Nullity Theorem. 
* What other results are there? 


## Infinite-Dimensional Linear Functions and Equations

* Continuity is equivalent to being bounded. 
* Definition of size (norm). 
* The space of continuous functions is complete? 
* Principle of Uniform Boundedness?
    - This is used for the Stability Theorem.
    - Proof.
        + This is normally proved with the Baire Category Theorem, but this is not the best proof.
        + I think that I need to prove it for sequences.
        + A simple proof is supposedly in [*A really simple elementary proof of the uniform boundedness theorem* by Sokal](\url{https://arxiv.org/abs/1005.1585}). This seems to target the sequence case.
* Stability Theorem?
    - This says that under stability, the limit of solutions to a sequence of approximate equations is a solution.
    - This is used for stability proofs.
* Fredholm Alternative? 


## Scalar-Product Spaces

* Motivation. 
    <!-- - Definition of $\mathrm{H}_n$ and its inner product.  -->
    - Define the size (norm) of a vector. Explain that it will be proved that this forms a metric. 
    - This is essentially a generalization of euclidean space. 
* Basics of Geometry. 
    - Pythagorean Theorem. 
    - Cauchy-Schwarz Inequality. 
    - Triangle Inequality. 
        + This proves that the space is a metric space. 
        + Also, explain the properties of the norm in paragraph form. 
    - Parallelogram Law.
        - Used: Projection Theorem. 
    - These spaces are complete. 
    - Projection Theorem. 
        + Used: The Lax-Milgram Theorem. 
* The (Riesz) Representation Theorem. 
    - Where it is used. 
        + The Density Theorem
        + The Max-Milgram Theorem


**Possible results.**
* The Extension Theorem? (The Hahn-Banach Theorem.)
    - I guess that this is used so that differential operators make sense on the completions ($L$ and $L^2$) of the integration spaces. This relates to how not-quite solutions (discontinuities) can be still be considered to be weak solutions. So should this be in Generalizations? 
    - Supposedly it helps with error estimates by getting an estimate on test functions and then extending this to all functions. 
* Open-Mapping Theorem.
    - I guess that this is how the residual is related to the error.
    - This is essentially the Bounded-Inverse Theorem.
* Closed-graph theorem. 
    - This can be stated to have nothing to do with the graph: If a linear operator $f$ is closed (that is, if a sequence $(v_i)$ converges to v and if the sequence $(f(v_i))$ converges to u, then the relation $f(v) = u$ holds), then it is continuous/bounded. 