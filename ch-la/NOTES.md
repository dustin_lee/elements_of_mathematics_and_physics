# Notes for Linear Equations

## Concerns

* 


## Need to Understand

* In iterative refinement, doesn't another system just need to be solved?
* Adjoint
    - What is an adjoint? 
    - How do adjoints normally appear? 
    - Why are modes naturally orthogonal? 
    - What is the meaning of normal and self-adjoint matrices? 
        + What are full-rank normal operators of use? 
        + Does normal really just mean that is preserves orthogonality? 
        + A matrix is normal exactly if, for every vector $v$, $|Tv|$ is $|T^*v|$: 
            $$
                T^*T - TT^* = 0
                ⇔ \left<(T^*T - TT^*)v, v \right> = 0
                ⇔ 0 = \left<T^*Tv,  v\right> - \left< TT^*v, v\right> = |Tv| = |T^*v|
            $$
        + If all eigenvalues of a normal linear function are real, then it is self-adjoint. 
* Frequency/momentum representations (in QM) are still just writing it in a different basis. 
    - But, it needs to be integrated across this basis. What really does such a basis mean. (Maybe Moe has a point.)


## Language

* Use *null set* instead of *kernel*. Use $\mathrm{null}$. (Do this for any algebraic structure.)
* Use *complete number field*. 
* Should I use *unitary* instead of *orthogonal*?
* Just use *row-reduced* instead of *echelon*? 
* Perhaps, don't use *square integral* but just say that its *square is integrable*.
* How should I refer to the different norms? Well, first I should use the term *metric* or *distance*. (But, I guess that I should refer to the spaces instead if I can.)
    - *euclidean distance*. 
    - *uniform distance*. 
    - *square-integration distance*. 
    - *integration distance*. 
    - *Hilbert distance*. 
* When dealing with different metrics, say *with respect to the [] distance* instead of saying what the space is. 


## Notation

* Use $M$ for general matrices.
* Use $I$ for the identity matrices. 
* Use $Q$ for orthogonal matrices. 
* Use $U$ for upper-triangular matrices. 
* Use $L$ for lower-triangular matrices. 
* Use $H$ for Hessenberg matrices. 
* Use $D$ for diagonal matrices. 
* Use $B$ for banded matrices. 
* Use $P$ for projections. 
* Should I use $T$ for tridiagonal matrices? 
* Should I use $N$ for normal matrices? 


## Material

* Explain why unitary matrices are useful. (Their condition number is $1$.) Do this after the first proof that has them. (This might be Householder triangularization.) (Then, maybe mention it every addition time that they are used.)


## General 

* Bring attention to the specific results that require completeness. 
* I guess that being closed compensates for not being continuous.
    - Weak formulations are good because they are bounded? 
* Solving linear systems is used for implicit schemes in dynamic differential equations. Should I explicitly mention this here? 
* If I include SVD, then do the maximum-scaling approach instead of the hyperellipse approach. 
* A matrix is positive if it can be written as $MM*$. 
* Instead of proving that all norms on a vector space are equivalent, maybe just prove that the regular norm on spaces like $\mathbb{R}^n \times \mathbb{R}^n$ is equivalent to the norm $\vert a_1 \vert + \vert a_2 \vert$. Then, also just something that relates the $L^2$ norm to the uniform norm. 
    - Use the term *composite term*. 
    - The relation between the $L^2$ norm and the uniform norm will probably involve the derivative. Thus, extra smoothness is needed on solutions to relate these. 
    - How do I put a norm on derivatives? (This must include mixed derivatives.)
* Mixed spectra: When writing the Spectral Theorem, separate the discrete and continuous parts, that is, write something like
$$
    ∑ λ⋅v_λ + ∫ λ*v_λ. 
$$