# Reasons that the Elements is an Improvement of the Current Literature

> "If you're entering anything where there's an existing marketplace, against large, entrenched competitors, then your product or service needs to be much better. It can't be a little bit better."   
–– Elon Musk

> "Easy reading is damn hard writing."   
–– Nathaniel Hawthorne


## General Improvements

This book covers a very wide range of material. 
This unification allows for various improvements. 
<!-- For example, in such a large work, there can be some leeway on the vocabulary and notation used.  -->


**Pedagogical**: 
This book is pedagogically complete. 
Everything provides foundational understanding or has an end in engineering. 
This is done by being intuitive and making connections between the material throughout the book. 
It uses the correct level of abstraction, that is, it does not have the excessive unhelpful abstraction that has become so common in the literature. 
In this book, abstraction is functional. 
Unhelpful ideas are omitted. 
<!-- Pure mathematics lures the mind. Applied mathematics keeps it. This book is for those who care about why, not those interested in mental masturbation with abstract nonsense.  -->

**Emphasizes Computation**: 
First, it is able to connect and motivate material that is current taught in different courses. 
For example, it is able to emphasize computation and explain what computational methods help with a theory. 
Currently, in the literature, theory and computation are not able to fit together because they cannot both be covered in a single course. 

**Not Crap**: 
Most of the literature is very poorly written, that is, it is mostly unconnected fluff. 
I have pursued the study of mathematical exposition as an end in itself.
This book presents the material clearly, concisely and pedagogically. 

**Use of Figures**: 
Figures are used extensively throughout the work. 
This is not as common as it should be in proof-based books. 
(Perhaps, this tradition is still carried over by the Bourbaki.)


## Specific Improvements 

This book is not great from some single huge improvement, but from huge number of minor improvements. 

**Complex Numbers**: 
The complex numbers are downplayed much more than in other books. 
Complex numbers do not have any real physical meaning, but only serve as placeholders. 

**~~Compton Scattering~~**: 
This book does not discuss Compton Scattering. 
Historically, this was the first way that photons were scattered of electrons. 
But, the only important point is the scattering. 
It is not justified to go into Compton Scattering specifically. 

**Definite Integration**: 
In this book, integration is definite integration.
And, instead of the term *indefinite integral*, the term *antiderivative* is used. 
The use of *definite integration* and *indefinite integration* is misleading because the latter is not integration. 

**~~Differentials~~**: 
The concept of a *differential* is excluded, both explicitly or implicitly. 
This is a misleading concept. 
In normal calculus, it is normally not rigorous, and differentiation is better.
In thermodynamics, the talk about exact differentials is distracting and does not contribute to understanding.
In differential geometry, the idea is misleading about what integration is, and the measure-theoretic definition of integration is sufficient. 
This outdated concept is also excluded from notations. 
That is, derivatives are not written as a fraction of differentials, such as $\frac{\mathrm{d}x}{\mathrm{d}y}$. 
And, integrals are not denoted by them either. 

**~~Ether~~**: 
There is not a discussion of ether. 
Students today do not have any attachment to this invalid theory, and they do not need any convincing against it. 

**Eigenvalue Problems**: 
Most books on eigenvalue problems do not have any motivation for them. 
This book does motivate them. 
Furthermore, it explains the theory of approximation an infinite-dimensional eigenvalue equation with a finite-dimensional eigenvalue equation, which is rare in expositions of differential equations. 

**Entropy**: 
In classical thermodynamics, free energy is used instead of entropy. 
Entropy is only considered from a microscopic viewpoint. 
Defining entropy without reference to atoms is unintuitive and not general. 

**Euclidean Geometry and Trigonometry**: 
This books discusses euclidean geometry and trigonometry. 
This includes proofs such as for the Angle-Addition Formulas. 
Despite these being core topics, this very uncommon for proof-based mathematics books. 

**Functional Analysis**:
Normally, the study of functional analysis and differential equations are separated.
This leaves a lack of motivation of functional analysis and poor understanding of its use in differential equations. 
In this book, the studies of these go hand in hand. 

**Galois Theory**: 
A minimalistic approach is taken to Galois Theory and the Abel-Ruffini Theorem. 

**~~Groups, Rings and Fields~~**: 
Groups, rings and fields are not defined. 
Such definitions do not actually help reduce any proofs. 
Furthermore, there is not a discussion of representation theory because it does not have any practical applications. 

**~~Hamiltonian Mechanics~~**: 
Many books on classical mechanics ramble on about hamiltonian mechanics. 
But, it is not of any real importance. 

**Integral as a Solution to a Differential Equation**: 
In this book, the integral is first introduced as the solution to some differential equations.

**Integration by Sampling**: 
The integral is first define using the sampling approach (Riemann), instead of the squeeze approach (Darboux). 
This is less common in advanced courses because the upfront cost for proofs is more. 
But, it is more intuitive because it immediately applies to functions whose ranges are multidimensional.  
Furthermore, it is a step towards Euler's Method and other finite-difference schemes. 
The squeeze approach is still defined later. 

**Integration and Content**: 
Because the integral is introduced to solve differentiation equations, it is not originally related to content. 
That the area under a positive real function is the integral is more subtle than people are lead to believe. 
Furthermore, it does not have as much use as people often think. 
This connection is not presented until a later chapter than the first chapter on integration. 

**Jordan Measure**: 
In this book, Jordan measure is used instead of Lebesgue measure. 
This is very uncommon because there is a common misconception that much theory depends on this generalization. 
Jordan measure is much more intuitive because Lebesgue measure does not always physically make sense.
Lebesgue theory is motivated in its proper setting, and the reader is left with an understanding of its actual use. 

**~~$L^p$ Spaces and Other Useless Spaces~~**: 
This books does not get sucked into the useless aspects of functional analysis. 
All functional analysis in this book is motivated by wanting to solve differential equations.
This means that general $L^p$ spaces not introduced or studied. 
These spaces do not have any significant applicability. 
Still, many book on measure theory and functional analysis give the impression that these are central to mathematics. 
Their inclusion is just extra details and indices. 

**Numerical Methods**: 
In this book, numerical methods are emphasizes over symbolic methods. 
With modern computing resources, numerical methods are much more practical overall. 
The emphasis of symbolic methods in current education is outdated. 

**Probability Interpretations**: 
Most books on probability have extensive rants on the interpretations of probability, that is, the frequentist view and the degree-of-belief view.  
This does not help the reader understand. 
Thus, it is not included in this book. 

**Positrons are Empirical**:
The only evidence for positrons is empirical. 
Many books motivate positrons by the Dirac Equation, but this is a big jump and very weak evidence. 

**Power Series for Approximation**: 
Despite it being the main motivation of power series, most books do not motivate them by their ability to compute common functions, such as trigonometric functions. 

<!-- **~~Reciprocal Lattice~~**: 
The concept of reciprocal lattices are not introduced. 
These are not essential. 
They confuse students.  -->

**Spacetime**: 
This book does not centralize spacetime in relativistic physics. 
Most books spend more time on explaining spacetime than the time it saves explaining the theory. 
This philosophical obstacle is not needed. 
Both Special Relativity and General Relativity were originally done without it. 
This also means that semiriemannian geometry is not needed.
Originally, Einstein did both Special Relativity and General Relativity without this concept. 
It is more intuitive. 

**~~Splines~~**:
This book does not present splines. 
These have a very small applicability. 
But, many numerical analysis books present a significant study of them and give the impression that they are as important as other interpolation methods. 

**SVD**: 
The singular-value decomposition is not discussed. 
This is normally just a distraction in numerical linear algebra. 

**Taylor's Theorem**:
This book presents a straight-forward approach to Taylor's Theorem by the Fundamental Theorem of Calculus. 
This is uncommon. 
Furthermore, this approach gives a form of the remainder is not common in the literature, but that is more intuitive. 

**~~Topological Spaces~~**
This books does not define topological spaces. 
In practice, these are very rare because almost every practical topological space is a metric space. 

**Unbounded Integration**: 
This uses a novel definition of integration of unbounded functions or on unbounded sets. 