%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Summary 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% First, the trigonometric functions are defined. 
% The sine and cosine of an angle are quantification of the results of Section~\ref{geom:sec:congruence} and Section~\ref{geom:sec:similarity} for right triangles. 

% Second, the Law of Sines and the Law of Cosines are proved. 
% These extend this process of quantification of the results of Section~\ref{geom:sec:congruence} and Section~\ref{geom:sec:similarity} to general triangles. 

% Third, the sine and cosine of some angle widths are computed. 
% These are used as a starting point to compute the sine and cosine of arbitrary angle widths using the Addition Formulas and Half-Angle Formulas.

% Fourth, the Addition Formulas and Half-Angle Formulas are proved.
% And, it is explained how these results can be used to compute the sine and cosine of arbitrary angle widths.
% A faster method is in Chapter~\ref{ch:psums}. 
% \scenebreak 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Definitions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Let $θ$ be a positive real number that is less than $\frac{\constpi}{2}$. 
Let $p$, $q$ and $r$ be three noncollinear points such that the relations
\[
    \width(r, p, q) = θ
    \textand 
    \width(p, q, r)  = \frac{\constpi}{2}
\]
hold. 
By the Angle-Angle-Angle Theorem, any ratios between 
\[
    \abs{r-q},
    \textspace
    \abs{r-p}
    \textand
    \abs{q-p}
\]
do not depend on the points $p$, $q$ and $r$ chosen. 
Then, the \emph{sine} of the number $θ$ is
\[
    \frac{\abs{r-q}}{\abs{r-p}}
    ,
\]
and the \emph{cosine} of the number $θ$ is
\[
    \frac{\abs{q-p}}{\abs{r-p}}
    .
\]
The sine and cosine of $θ$ is denoted by $\sin(θ)$\label{not:sin} and $\cos(θ)$\label{not:cos}, respectively. 


\begin{figure}[H]
\centering
    \input{geom-fig-sin-def}
    \caption{The definition of the sine and cosine of an angle width. The width is $θ$. The triangle is normalized in the sense that the distance between $p$ and $r$ is $1$. The number $\sin(θ)$ is the distance between $q$ and $r$. And, the number $\cos(θ)$ is the distance between $p$ and $q$.}
\end{figure}


By the Triangle Sum Theorem, the relations
\[
    \cos(θ) = \sin\left(\frac{\constpi}{2} - θ\right)
    \textand
    \sin(θ) = \cos\left(\frac{\constpi}{2} - θ\right)
\]
holds. 
And, by the Pythagorean Theorem, the relation
\[
    (\cos(θ))^2 + (\sin(θ))^2 = 1
\]
holds. 


The definitions of sine and cosine are now extended. 
The sine and cosine of $0$ are defined to be $0$ and $1$, respectively. 
The sine and cosine of $\frac{\constpi}{2}$ are defined to be $1$ and $0$, respectively. 
Let $θ$ be a number between $\frac{\constpi}{2}$ and $\constpi$.
Then, the sine of $θ$ is defined to be 
\[
    \sin\left( \constpi - θ \right)
    ,
\]
and the cosine of $θ$ is defined to be 
\[
        - \cos\left( \constpi - θ \right)
        .
\]
The sine and cosine is defined this way so that the Law of Sines and the Law of Cosines hold.


\begin{figure}[H]
\centering
    \includegraphics[scale=.7]{geom-fig-sin-graph.jpg}
    \caption{Graphs of the functions $\sin(t)$ and $\cos(t)$.}
\end{figure}


Let $θ$ be a number between $0$ and $\constpi$.
Then, the relation
\[
    (\cos(θ))^2 + (\sin(θ))^2 = 1
\]
still holds. 
This is also called the \emph{Pythagorean Theorem}\index{Pythagorean Theorem}.


\scenebreak 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Laws of Sines and Cosines
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

The next theorem is called the \emph{Law of Sines}\index{Law of Sines}.
It quantifies the idea that the longest side of a triangle is the side opposite to the widest angle. 

\input{geom-thm-sin-law}


The next theorem is called the \emph{Law of Cosines}\index{Law of Cosines}. 
It is a generalization of the Pythagorean Theorem.

\input{geom-thm-cos-law}


\scenebreak 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Computations
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Now, the sin of various angle widths are computed. 
In order, the angles widths are $\frac{\constpi}{6}$, $\frac{\constpi}{4}$, and $\frac{\constpi}{3}$.
Because the relation 
\[
    \cos(θ) 
    = \sin\left( \frac{\constpi}{2} - θ \right) 
\]
holds, these also compute the cosine of these three angles. 


\input{geom-ex-sin45}

\input{geom-ex-sin30}

\input{geom-ex-sin60}


\scenebreak 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Formulas
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

The next lemma says that opposite angles of crossing lines are equal. 

\input{geom-thm-crossing-lines}


The relations in the next theorem are called the \emph{Angle-Addition Formulas}\index{Angle-Addition Formulas}. 

\input{geom-thm-add-formulas}


The relations in the next theorem are called the \emph{Half-Angle Formulas}\index{Half-Angle Formulas}.

\input{geom-thm-half-angle}


The Addition Formulas and Half-Angles Formulas can be used to compute the sine and cosine of arbitrary angle widths.
For example, the relation
\[
    \sin\left(\frac{\constpi}{6}\right) = \frac{1}{2}
\]
holds. 
Thus, by the Half-Angle Formulas, the number 
\[
    \sin\left(\frac{\constpi}{12}\right)
\]
is approximately
\[
    \num{0.258 819}. 
\]
And, again by the Half-Angle Formulas, the number
\[
    \sin\left(\frac{\constpi}{24}\right)
\]
is approximately
\[
    \num{0.130 526}. 
\]
Then, by the Addition Formulas, the number 
\[
    \sin\left(\frac{3⋅\constpi}{24}\right)
\]
is approximately
\[
    \num{0.382 683}.
\]
This method can be continued to create a table of values. 


\scenebreak 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Dot Product
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



