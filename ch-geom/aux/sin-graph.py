import matplotlib.pyplot as plt
import numpy as np


def main():
    domain = np.linspace(0.0, np.pi, 1000)
    sin_img = np.sin(domain)
    cos_img = np.cos(domain)
    plt.plot(domain, sin_img, label="sin")
    plt.plot(domain, cos_img, label="cos")
    plt.legend()
    plt.savefig("singraph.jpg")
    return None


if __name__ == "__main__":
    main()