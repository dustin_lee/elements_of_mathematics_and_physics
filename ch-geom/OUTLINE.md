# Outline of Distance and Angles

## Concepts 

**Distance**: 
* Intuition: A quantification of nearness. 
* Give the formula for distance: 
    - This chapter shows that this is the correct formula, that is, the Pythagorean Theorem is proved

**Angles**:
* Definition of an angle and its width. 
    - Do I need to define rays? 
    - The width of angle. 
        + Length is not needed for angles to reach the Pythagorean Theorem because only the concepts of smaller, bigger and equal widths of angles are required. 
            * Explain this in the summary/introduction? 
* Explanation how triangles define geometry. 
    - In the next section, congruence is dealt with. In the section after, measurements are dealt with.  
<!-- * Axioms of geometry. 
    - Explain that euclidean space is categorical. Thus, it is just about the consistency.  -->


**Axioms**: 
* Maybe needed: Rotation: Generated continuously from distance preserving functions (that is, it can be generated without jumps).
* Properties: 
    - Pons Asinorum: 
        + An angle can be bisected. Furthermore, the ray at this angle interests any line segment between points on the other rays. 
    - Sum 180:
        + A segment line can be extended
        + Parallel postulate
            - State is as adding vectors


* The distance between two different points is not $0$. 
    - Thus, if it is proved that the distance between two points is less than any positive real number, then they are the same point. This is a common method. (Give examples.)
* Triangle Inequality 
    - This is extensively used
        + Give examples? 
    - It is the basis of metric spaces in Chapter~\ref{lim}. 
* Reverse Triangle Inequality 
    - Give examples


## Congruence

* Congruence definition
* Theorem. Side-Angle-Side Theorem
* Theorem. Side-Side-Side Theorem
    - Lemma. Pons-Asinorum Theorem. (Is this the correct place for this lemma?)
        + Corollary. The widths of the angles in an equilateral triangle are equal. 
    - The proof of this is weak at best. Supposedly, it is wrong. Probably replace it with a better one. (Supposedly, Hadamard's works. What did Hilbert use?)
* Angle-Side-Angle Theorem



## Similarity (the Pythagorean Theorem)

* Similarity definition
* Sum-180 Theorem 
* The Angle-Angle-Angle Theorem
* The Pythagorean Theorem
    - Explain its proof. Then, explain how the rest of the section is dedicated to filling this one part. 


## Trigonometry 

**Definitions**:
* The sine and cosine of an acute angle
    - Relations, such as the Pythagorean Theorem. 
* The sine and cosine of an obtuse angle
    - The relations (such as the Pythagorean Theorem) still hold. 
    - Is the motivation for the Law of Sines and the Law of Cosine to work? 
    - But, mention that differential equations and analytical continuation reinforce this extension. 
    - Should I have the unit circle? Or at least, half of it? 
* Graphs 

**Laws of Sine and Cosine**:
* Law of Sines
    - It quantifies the idea the longest side is opposite the widest angle. 
    - It also shows that the Sine Function in monotone on the interval between $0$ and $90\degs$? 
* Law of Cosines
    - When the angle in the Law of Cosines is right, then it is the Pythagorean Theorem. 

**Example Computations**:
* The value of the Sine Function
    - At 45 degrees 
    - At 30 degrees 
    - At 60 degrees 

**Formulas**:
* Introduction of calculating the Sine Function and Cosine Function
    - Relation between the Sine and Cosine Function. Only need to compute one. 
    - Explain how a table in constructed in this section using known values and the Addition Formulas and Half-Angle Formulas.
    - Mention that power sums will be used later. 
* The Additions Formulas
    - Lemma 1. Equal angles for crossing lines. 
    - Lemma 2. The length of opposite sides of a rectangle are equal. 
* The Half-Angle Formulas

**Dot Product**:
* Way to calculate angles? 