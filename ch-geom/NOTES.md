# Notes for Distance and Angles

## Language Choices

* Just say *noncolinear points*. ❗But, how do I talk about congruence? And, how do I talk about them as one object? Triplet? *Let $(p, q, r)$ and $(p', q', r')$ be triplets of noncolinear points that are congruent. 
* Use `dist`
* Use `width(p, q, r)` (and refer to them as numbers) 
* Use `\vec(p, q)`


## Graphics

* Have colors for new lines in triangles (for the basic geometric proofs). 


## Material

* An angle is a relation between three points that disregards the proportion and overall position. 
* Should the Law of Sines and the Law of Cosines (the uses of sine and cosine) be done before the Angle-Addition Formulas and the Half-Angle Formulas (the computation of sine and cosine)? 
* Drop isometries.
* Introduce scalar products.
* Should I talk about chords and why they are not used? 
* Should I have the Sum-to-Product Formulas? (They are useful for integrating things like $\cos^2(t)$.)
* Should I introduce other trigonometric functions, such as the Tangent Function? 
    - What is the motivation? 
* The Pythagorean Theorem holds for obtuse angles too. 
* Analytic continuation is further justification of the definitions of the sine and cosine of obtuse angles. 


## Language and Notation

* Use the term *angle width*. 


## Figures

* Reverse Triangle Inequality: 
    - Have the `|---|` to denote length. And, make the middle length as `< a - b`, where `a` is the longest length and `b is the shortest length
* For the graph of sine and cosine, use $π$ as the scale. Mark $π/4$, $π/2$, $3π/4$ and $π$. 
* Should the figure for ASA be split into two figures so that the spacing between them is automatically formatted? 

## General

* Have some emphasis on the Parallel Postulate throughout. 
* Vector products depend on the *signed* angle between the vectors.
* If I use the Crossbar Theorem, then call it so. 
