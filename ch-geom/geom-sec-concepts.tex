%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Summary 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% This section introduces distance and angle widths. 
% Then, it proved that euclidean space satisfies the basic properties that are expected. 
% Intuitively, these basic properties are axioms. 
% But, the approach here is in the framework of the axioms from Chapter~\ref{ch:sets}. 
% \scenebreak 


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Distance
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Intuitively, the distance between two points is a measure of how far apart they are. 
Formally, it is a function from the set of pairs of points to the nonnegative real numbers.
Furthermore, it is expected to satisfy the properties that are introduced in this section. 
An example of such a function is 
\[
    \left(
        \Big( \comp_1(y - x) \Big)^2
        + \ldots 
        + \Big( \comp_n(y - x) \Big)^2
    \right)^{\frac{1}{2}}
    ,
\]
that is, the square root of the sums of the squares of the differences of the components of the points.
The Pythagorean Theorem says that this is the only such function, that is, this defines \emph{distance}\index{distance} in euclidean spaces. 
The Pythagorean Theorem is proved in Section~\ref{geom:sec:similarity}.
The distance between two points $p$ and $q$ in a euclidean space is denoted by\label{not:size:euclid}\label{not:dist:euclid} 
\[
    \abs{q - p}
    \textor 
    \dist(p, q) 
    .
\]
The \emph{size}\index{size} of a vector in a euclidean space is its distance from the vector $0$. 
The size of a vector $v$ in a euclidean space is denoted by $\abs{v}$.


% Triangle Inequality
\input{geom-thm-triangle-inequality}


\begin{figure}[H]
\centering
    \input{geom-fig-triangle-inequality}
    \caption{The Triangle Inequality. The numbers $a$ and $b$ are the distances between two pairs of three points. The distance between the third pair of these points is at most sum $a+b$.}
\end{figure}


% Reverse Triangle Inequality
The next theorem is called the \emph{Reverse Triangle Inequality}\index{Reverse Triangle Inequality}.
While the Triangle Inequality says that the length of a side of a triangle is less than the sum of the lengths of the other two sides, the Reverse Triangle Inequality says that the length of a side of a triangle is more than the difference of the lengths of the other two sides.
The Triangle Inequality is used as an upper bound, and the Reverse Triangle Inequality is used as a lower bound.

\input{geom-thm-reverse}


\begin{figure}[H]
\centering
    \input{geom-fig-reverse-triangle}
    \caption{The Reverse Triangle Inequality. The numbers $a$ and $b$ are the distances between two pairs of noncollinear points. The distance between the third pair of points is at least $b-a$.}
\end{figure}


\scenebreak 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Angles
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Let $p$ and $q$ be different points in a euclidean space. 
The \emph{line}\index{line} through $p$ and $q$ is the image of the function
\[
    p + t⋅(q - p)
\]
on the real numbers. 
The \emph{ray}\index{ray} from $p$ through $q$ is the image of the function
\[
    p + t⋅(q - p)
\]
on the nonnegative real numbers.
The \emph{line segment}\index{line segment} between $p$ and $q$ is the image of the function
\[
    p + t⋅(q - p)
\]
on the real numbers between $0$ and $1$.
Intuitively, a line segment is breathless length. 
Points in a euclidean space are said to be \emph{collinear}\index{collinear} if there is a line that contains all three points.
And, they are said to be \emph{noncollinear}\index{noncollinear} if they are not collinear.
A point $r$ is said to be \emph{between}\index{between} $p$ and $q$ if it is on the line segment between $p$ and $q$, and it is said to be \emph{strictly between}\index{strictly between} if it is between $p$ and $q$, and it is not either of them. 


\begin{figure}[H]
\centering
    \input{geom-fig-between}
    \caption{The point $r$ is between $p$ and $q$.}
\end{figure}


Let $p$, $q$ and $r$ be three noncollinear points in a euclidean space. 
The \emph{angle}\index{angle} at $p$ between $q$ and $r$ is the union of the rays from $p$ through $q$ and from $p$ through $r$.
The \emph{width}\index{width}\index{angle width} of the angle at $p$ between $q$ and $r$ is the length of the shorter path between the intersection of this angle and the circle that is centered at $p$ and intersects with both these rays and whose radius is $1$. 
It is denoted by $\width(q, p, r)$\label{not:width}.
Intuitively, the width of an angle is a measure of the divergence of its rays. 


\begin{figure}[H]
\centering
    \input{geom-fig-width}
    \caption{An angle whose width is $\frac{\constpi}{6}$.}
\end{figure}


An angle is said to be \emph{acute}\index{acute} if its width is less than $\frac{\constpi}{2}$.
An angle is said to be \emph{right}\index{right} if its width is equal to $\frac{\constpi}{2}$.
And, an angle is said to be \emph{obtuse}\index{obtuse} if its width is more than $\frac{\constpi}{2}$.


\begin{figure}[H]
\centering
    \input{geom-fig-acute}
    \hspace{1cm}
    \input{geom-fig-right}
    \hspace{1cm}
    \input{geom-fig-obtuse}
\caption{The angle on the left is acute. The angle in the middle is right. And, the angle on the right is obtuse.}
\end{figure}


The concept of length is introduced in Chapter~\ref{ch:len}. 
Furthermore, the length of the circle whose radius is $1$ is $2⋅\constpi$.
The number $\constpi$ is also introduced in Chapter~\ref{ch:len}, and it is approximately $3.141\ 593$.
Instead of measuring angles by their width, they can be measured by their chord, which does not use the concept of length. 
In this section, Section~\ref{geom:sec:congruence} and Section~\ref{geom:sec:similarity}, all results hold if the concept of the width an angle is replaced with the chord.

Let $p$, $q$ and $r$ be three noncollinear points in a euclidean space.
The \emph{chord}\index{angle chord}\index{chord} of an angle at $p$ between $q$ and $r$ is the
distance between the intersection of this angle and the circle that is centered at $p$ and intersects with both these rays and whose radius is $1$. 


\begin{figure}[H]
\centering
    \input{geom-fig-chord}
    \caption{The chord of an angle.}
\end{figure}


Let $p$, $q$ and $r$ be noncollinear points. 
And, let $λ$ be a point that is between $p$ and $r$. 
Then, the relation
\[
    \width(q, p, r) = \width(q, p, λ) + \width(λ, p, r)
\]
holds. 
A similar property does not hold for the chord of an angle. 
This is why the width of an angle is used instead of the chord of an angle.


\begin{figure}[H]
\centering
    \input{geom-fig-add-width.tex}
    \caption{The additivity of angel widths.}
\end{figure}


The width of an angle could be measured by any scaling of it. 
When the angle width define here is used, angles are said to be measured in \emph{radians}\index{radian}.
Another common scaling is \emph{degrees}\index{degree}, where the width of a right angle is $90$ degrees.
Degrees are used because the integer $90$ is divisible by $2$, $3$, $4$, $5$ and $6$, which makes it convenient to use. 
Radians are used because the derivatives of the functions $\sin(t)$ and $\cos(t)$ are $\cos(t)$ and $-\sin(t)$, respectively. 
These functions are introduced in Section~\ref{geom:sec:trig}, and differentiation is introduced in Chapter~\ref{ch:diff}. 



\scenebreak 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Axioms
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


The next theorem is called the \emph{Side-Angle-Side Axiom}\index{Side-Angle-Side Aximo}. It says that a triangle is determined by the length of two of its sides and the angle between these two sides. 

\input{geom-thm-sas}

