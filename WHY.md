# Why I am Writing the Elements

>  "What one fool can understand, another can."  
–– Richard Feynman

## Impact

> "Sometimes, you need to be the answer to your own prayer."  
–– Proverb


I have two rules of thumbs of if a project is worth it: 
First, create what you have always wanted. 
Second, create what will be an order of magnitude better than the competition. 

In regards to the first rule of thumb, I have always felt that the scientific literature could be better written, and that better books would have accelerated my learning. 
I have heard many other voice this opinion also.

If regard to the second rule of thumb, I do believe that this book will be an order of magnitude better. 
I believe that, for many people, it will lower the bar for an education in advanced mathematics and physics. 
And, from the printing press to Wikipedia to computer programming, a lower bar for knowledge tends to have great effects.

This book is about knowledge, and knowledge is the most scalable good.
For example, the Feynman Lectures have been downloaded over a million times. 
Thus, I think that it has the ability to be broadly impactful. 
Still, I would still rather profoundly impact a few than light impact many, which is how I will write it. 

This project seems obvious to me. 
But, of course, nobody else is stepping up. 
But, this makes sense because of how ambitious it is. 
It is even more ambitious than the *Art of Computer Programming*, which Knuth has been working on for over fifty years. 


## Why Me Specifically 

> "If not you, who else?"  
Proverb 

For a long time, I have an the urge to put my understanding of mathematics into a book. 
This is explained in the [Conception of the Elements](conception.md). 

I have read many more books on mathematics and physics than anyone who I have ever known by far. 
As an undergraduate, I constantly ran into the twenty-five-book limit for interlibrary loans. 
I would regularly skip class to read books instead because books are my preferred medium for learning. 
I have gained an eye for quality scientific writing, which it the most important skill to write quality scientific writing. 

When reading science books, I never felt that they were close to how good they could be. 
Even, in the books that I consider to be the best books on the market, I see significant potential improvements. 
Others around me have voice similar feelings, but I seem to be the only one with passion about this. 
I seem to be the only one who sees the full potential of such a work. 


## Fulfillment

> "First you have to know the subject;  
then you have to know how to write.  
Both take a lifetime to learn."  
–– Ernst Hemingway

First and foremost, I am writing the *Elements* for myself, that is, even if it doesn't sell a single copy, then its completion will still be a success to me. 
Writing it is useful for my education, and learning is an end in itself. 
The material is the material that I am most interested in. 
Writing it down forces me to truly understand it.
Furthermore, I have developed a habit that, whenever I learn something new in science, I automatically think about how it will fit in the framework of the *Elements*. 
The *Elements* has become my basis of understanding. 

I want to contribute to the world, and I want to be the best at something. 
And, the trades that resonant most with me are mathematics and writing. 
The *Elements* is how I will train and materialize these skills. 
It will be my masterpiece. 

