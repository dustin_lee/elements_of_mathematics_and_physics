# Notes for the Preface

## Decisions

**Less than One Page**:
Keep the preface less than one page. 
Prefaces should be simple and to the point. 

~~**Acknowledgements**~~:
Do not have acknowledgements. 
The reader does not want to read this. 
It is not concise. 

~~**Signature**~~:
Do not sign off with the author's name, date and location. 
This information is redundant or useless. 

~~**Multiple**~~:
Do not have multiple prefaces, where there is one for each version. 
It is not concise. 


## Material

* When I publish *Advanced Calculus*, reference it instead of an arbitrary real analysis course. 
* When I publish *Scientific Computing*, also suggest that the reader has some experience with implementation through code and reference this. 