# Outline of the Preface

* Description.
* The intended audience.
    - Reference my guide? Or put `top.md` into a repository and reference it? 
* Explanation of version system? 
* Reference my others works? 
    - The Exercises. 
    - My Experiments (if I write it). 
* Error handling. 
    - Options. 
        + Directly contact me. Give me an email. 
        + Merge request with git. 