# Outline of Temperature and Thermal Energy

## Concepts / Introduction 

**Basics**:
* Temperature is a thing
* Define the centigrade scale.
* The Law of Equal Temperatures.
* Thermal energy is energy 
* Heat capacity 
* Law of Entropy 
* Conduction 
* Wave Equation

**Ideal Gases**: 
* I guess introduce this as an example? 
* The Ideal-Gas Law. 
    - ❗Call it the Combined-Gas Law? 
        + Take from Boyle's Law. 
        + Then, the Ideal-Gas Law is from adding Avogadro's Law. 
    - Derivation. Introduce the Ideal-Gas Law as a combination of Boyle's law, Charles's law, Avogadro's law and Gay-Lussac's law. 
        + Note. These names do not need to be introduced. 
        + Introduce absolute zero.
        + Introduce the specific-gas constant $R$. 
    - Statement. The number $\frac{P⋅V}{M⋅(T-\mathrm{T}_0)}$ is constant for a gas. 
    - Define the mass-specific gas constant. 
* Define an *ideal gas* to be what satisfies the Ideal-Gas Law. 
* Energy only depends on temperature. (How is the proved?)
    - I think the energy is $\frac{3}{2}⋅R⋅(T-\mathrm{T}_0)$. 
* The value $P⋅V^γ$ is constant. 

**Absolute temperature**:
* Notes. 
    - Does this actually have any value? 
    - This defines temperature independently of any substance. 
    - The key idea is that the efficiency of a reversible heat engine (with absolute zero) increases (monotonically) as the temperature difference increases. Thus, there is a bijection between the efficiency of a reversible heat engine and the temperature.
    - That the efficiency of a reversible engine increases with temperature can be easily shown by using a third engine between the two temperature. The composite of two reversible engines is also reversible. (This is only helpful if the formula for the efficiency is not already known.)
    - Most likely don't use the term *absolute temperature*.
    - ❗I guess that I am introducing in here for ideal gases. And, then, after Carnot's Theorem, I think that there is further reasoning for it. 


## Heat Engines and Free Energy

**Carnot's Theorem**:
* Notes. 
    - Should picture that shows the difference in efficiency and the inverse of the coefficient of performance of two small perturbations? This would be a PV diagram with two nearby cycles. 
    - Should I mention the following? In a reversible engine, only only configuration work in done – not dissipative work. That is, the engine is frictionless. 
* Statement. 
* Before.
    - Introduce the efficiency of heat engines and the coefficient of performance of refrigerators. (Should this already be done? Or should I call this section Efficiency of Heat Engines? If I call it Carnot's Theorem, then I can put absolute temperature into this section.)
* Proof. 
    - Explain that the efficiency of any heat engine is less than the inverse of the coefficient of performance of any refrigerator.
    - If a heat engine is reversible, then it has the maximum efficiency.
    - The Carnot cycle.
    - Calculation of the Carnot cycle with ideal gases. 

**Free Energy**:
* Notes. 
    <!-- - The entropy $\frac{Q}{T}$ might just naturally fit in the proof the Carnot's theorem. -->
    - The Second Law can be restated as saying entropy increases.
    - Carnot's theorem can be proved more easily.
<!-- * This can be intuitively thought of as there being statistically invertible processes. For example, it is much more likely for an excited atom to emit a photon than absorb one because once the photon is emitted, there is a lot of space that it can exist.  -->




## Thermodynamic Processes

* Enthalpy. 
    - Explanation. The work done to the environment needs to be accounted for in phase transitions and reactions.