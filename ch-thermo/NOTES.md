# Notes for Temperature and Thermal Energy
 
## Issues

* 


## Need to Understand

**Entropy / Free Energy**:
* The existence of endothermic reactions and the that some exothermic reactions do not happen motivate entropy and free energy: The question is what does dictate if these happen. 
* What's the point? Like, how does this help with predictions or whatever? 
    - It can be used to predicted if a chemical reaction will occur spontaneous. The free-energy must decrease. (How can the change of free energy be calculated for a chemical reaction?)
        + Make a grid about sign of the changes of entropy and enthalpy. Some interactions are spontaneous, some are only at low temperatures, some are only at high temperatures, some aren't. 
* Idea. 
    - Can entropy be seen as a property of (thermal) energy? 
    - The change in entropy is a quantification of the irreversibility of a thermal process. 
    - Entropy is how spread out the energy is? 
    - It is a measure of the freedom motion. (Even more energy means that there are more possibilities for the motion.)
* Why not just use free energy? 
    - The statement can be that the free energy always decreases. 
    - I also think that the free energy can be seen as a sum of its part with the concept of absolute zero. 
    - The free energy $G$ is $H − (T-T_0)⋅S$, where $H$ is the enthalpy. Thus, the entropy is
    $(H-G)/(T-T_0)$. The enthalpy is the internal energy, plus its spatial existence energy. Thus, the entropy is essentially proportional to the complement of the "total existence energy". 
    - Is entropy just used for computing free-energy? Is it easier? 
    - Would I call the Second law of Thermodynamics the Law of Free Energy? 
    - If the system is closed, the enthalpy doesn't change; thus, an increase in entropy is equivalent to an increase in free energy. 
* Atomic.
    - How are continuous states counted?
    - Why the logarithm and Boltzmann Constant?
        + Some constant is sort of needed for units, I think.
        + The logarithm makes it so that the entropy of a system is equal to the entropy of its parts.
        + I think that the constant is to more directly relate the slope of entropy-energy function to temperature.
    - Splitting molecules increases entropy.
    - Should I talk about inaccessible states?
* I guess that the derivative of the entropy-energy function somehow relates to temperature.
    - This can introduce negative temperatures that are actually bigger than positive temperatures.
* Definition of free energy:
    - It is the maximum amount of energy that can be converted to work. 
    - But, some outside interference is allowed. But, what kind? 
    - I think that such outside interference is what is called "reversible" effects. 
    - (The internal processes, such as the thermal evolutions, are not reversible.)


**Third Law of Thermodynamics**: 
* Does this matter? What are its applications? 
* Until I see its importance, just ignore it. 



## General

* 


## Figures

* General: Color hot reservoirs red and cold reservoirs blue. Don't do the whole box, but only the lines/boarders. (Might as well use less ink.)

* Carnot's Theorem:
    - The heat engine should be on the left. 
    - Because the reservoirs are colored, maybe the heat engine and refrigerator should be colored to draw attention to them. They are the main features. How would they be colored? 
    - Should some symbols – such as $W+Q_2$ – be dropped? Are they just ink? 
* Carnot Cycle:
    - The loop structures is important because it really demonstrates that it is a cycle. 
    - Instead of just having Step 1, Step 2 and so on, also accompany these with short descriptions. Put Step 1 and so on in small caps. 
* Carnot PV:
    - Draw the curves exactly. 
    - Similarly to the carnot cycle, put in the same short descriptions of each step. 
    - Denote the temperature at each corner.  
    - How can it be made more clear the useful work is equal to the *area*? Should this be in a figure description? 


## Material

* Do explain the internal combustion engine. It is a great invention. 
    - Draw the PV diagram. The lines are vertical on the sides. 
* Have examples of predicting if reactions are spontaneous by using free energy. 
    - Have an example of a reaction that cannot happen even though the energy would decrease. 
* The relation $W = ∫P$ only holds when the process is reversible. 


## Vocabulary and Notation

* Don't use the words *adiabatical*, *diathermal* and so on. If I need such words, then, instead, use the phrases *thermally connected*, *thermally isolated* and *thermally separated*.
* Use $E$ instead of $Q$ for energy. (The letter $Q$ is used for observables.)
* Use $T - \mathrm{T}_0$ in expression that require absolute temperature.
* Don't use the Gas Constant $\mathrm{R}$, but only specific gas constants. Use Boltzmann's constant when there is a reference to atoms.


## Old Course Notes

* Have an exercise-free section on attempts to violate the Second Law of Thermodynamics.
    - Mention that the Second Law of Thermodynamics has not been proved.
    - Maxwell's demon.
        + Read *Maxwell's Demon: Why Warmth Disperses and Time Passes* by von Baeyer and *Maxwell's Demon: Entropy, Information, Computing* by Harvey and Rex (editors).
        + ❗Maybe this should be briefly mentioned in Atoms
    - The Furnace Paradox   
        + Wait, I forgot what the Furnace Paradox is. Is this just having a reflecting object in the middle, and trying to get it to lose energy to light?
        + Should I mention that one-way mirrors aren't really one way?
    - Should I mention trying to use a ratchet and pawl, as in the chapter Ratchet and Pawl in the first volume of the Feynman Lectures?
    - Repetitively combining an n-type and a p-type semiconductor?
        + These would have to be such that the electrons and holes annihilate into light. This light carries away energy. Then, they are separated until they return to thermal equilibrium with the cold source.