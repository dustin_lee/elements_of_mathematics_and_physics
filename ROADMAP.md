<!-- LTeX: enabled=false -->
<!-- - cspell:disable - -->

# Roadmap 🗺️

> "The road to hell is paved with works-in-progress."   
–– Philip Roth


## Summary 

The main goal is version 1. 
I consider version 1 to be the minimal to have its spirit. 
After version 1, I will work on version 2, which will include a few more chapters. 

<!-- I will publish other books along the way. 
These will help my writing, both the additional practice, editing from converting formats and getting feedback. 
They will produce money. 
They will familiarize me with publishing. 
And, they will be advertisement. (The best advertisement for a book is other books.) -->


## Subversions and Queue

*Bulk! As fast as possible, get to **600 pages**, **100k words** and **Version 0.1**.*

**Bulk: Start**: 
* [x] Overview 
* [ ] Distance and Angles
* [x] Limits and Continuity 
* [x] Differentiation 
* [x] Integration
* [x] Dynamic Ordinary Differential Equations
* [x] Infinite Sums
* [x] Power Sums
* [x] Partial Differentiation 
* [x] Generalizations of Distance
* [x] Special Relativity: Frames
* [x] Atoms: Atomic Hypothesis

**Bulk: Continuation**: 
* [x] Sets 
* [ ] ❗Numbers
* [ ] Length, Area and Volume (❗Start with Content of Regions.)
* [ ] ❗Content and Integration 
* [ ] ❗Probability and Statistics (Evening)
* [ ] Momentum and Force 
* [ ] Work and Energy 
* [ ] ❗Temperature and Thermal Energy 
* [ ] Light 
* [ ] Electricity and Magnetism 
* [ ] ❗Action: 1d (Evening)
* [ ] Special Relativity 
* [ ] ❗GR: Intro (Evening)
* [ ] Atoms: Basic Kinetic Theory 
* [x] ❗Subatomic Particles 
* [ ] ❗QM
* [ ] Computability: Galois Theory 

**Partial Differential Equations**: 
* [ ] ❗Static Differential Equations
* [ ] Dynamic Partial Differential Equations
* [ ] ❗Generalizations of Integration and Differentiation: Lebesgue Theory 
* [ ] ❗Linear Equations 

**Subversion 0.1**: (This is three fifths.)
* [ ] Flush the to-do list 
* [ ] Hard Edit

**Medium Stuff**: 
* [ ] Fluids 
* [ ] Atoms: Statistics, Entropy and Transport
* [ ] Generalizations of Integration and Differentiation: Distributions 
<!-- * [ ] Action: Nd -->

**Numerics**: 
* [ ] Uncertainty Quantification
* [ ] Optimization 
* [ ] Methods for Linear Equations
* [ ] Methods for Characteristic Equations
* [ ] Time Stepping for Differential Equations
* [ ] Finite-Difference Method for PDEs

**Advanced QM**:
* [ ] Spin 
* [ ] Particle Statistics
* [ ] Atomic Bonds

**Subversion 0.2**: (This is about four fifths.)
* [ ] Flush the to-do list
* [ ] Hard Edit

**Advanced Numerics and Computation**: 
* [ ] Resonance
* [ ] The Self-Consistent Field Method
* [ ] CFD
* [ ] Scattering
* [ ] Computation 

**QED**: 
* [ ] Action: E&M  
* [ ] Transitions 
* [ ] Discretization of Electromagnetic Interactions 

**General Relativity**: 
* [ ] Curvature 
* [ ] General Relativity 

**Version 0.3**: 
* [ ] Flush the to-do list
* [ ] Hard Edit

**Version 1.0**
* [ ] Flush all notes
* [ ] Flush the to-do list
* [ ] Finish all figures
* [ ] Hard Edit


**Hard Edits**: At the end of each subversion, do a hard edit: 
* [ ] LTeX and audit 
---
* [ ] On tablet
* [ ] On Paper
* [ ] Out-loud
---
* [ ] Aesthetics (such as equations) 
* [ ] For conciseness (by sentence)
* [ ] Factual correctness 
* [ ] For consistency 
* [ ] Flow and pedagogy 
---
* [ ] Notes and outlines
    - Notes: Purge the notes, that is, remove just about everything. It should only be issues and general comments, that is, it should only be Issues, Need to Understand, General,  unimplemented details in Figures and small undecided items in Material. 
        + Vocabulary and Notation: Remove or move to Style. 
    - Outlines: Make clean and concise. 
        + Go through it simulataneous while reading through the chapter. 
* [ ] Read through and edit the style guide 
* [ ] Raw text
    - Just look through? I could do this when I go through LTeX (after an audit). 
---
* Copy some parts by hand? 