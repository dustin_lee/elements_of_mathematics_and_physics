# Notes for Work and Energy


## Material

* The Story of Energy: 
    - Many forms: Something like Feynman's blocks. 
    - The Conservation of Energy always holds. 
        + It holds in all physical theories. 
        + It is used to predict many phenomena, such as the existence of neutrinos.
    - Use for action. It is better to approach quantum mechanics with energy than force. Force is not very meaningful quantum mechanics. 
    - It is real!
* Should I mention Stevin's [argument](https://en.wikipedia.org/wiki/Inclined_plane#History) for the inclined plane? At least do so in the Introduction. 


## General

* Take a look at the classical mechanics part of Quantum Mechanics for Mathematicians by Hall.
* When objects don't have internal parts (such as electrons), then collision between them must be elastic. 
* Is the energy in a spring considered to be configurational or internal energy? 