# Outline of Work and Energy

## Summary 


## Machines and Work

* Machines
    - Mention the level, pulleys, and gears. 
    - Explain the incline plane using forces. 
    - Then, the incline plane using Stevin's argument. 
* Work
    - Cannot every raise weight higher. 
    - The Law of the Lever is a general statement: Work in is work out. 
        + It explains all these (simple) machines without having to analyze the forces
            * For example, the forces through the rod of a lever are difficult to calculate and the calculation would be different each time. 
            * I could use the jack as a simple application. It is both an incline plane and a lever/gear. 


## Torque and Angular Momentum 


## Energy 

* Energy is potential work. 
* Potential energy. 
    - This is about the configuration of system. 
* Kinetic Energy. 
    - This is the energy from motion. 
* The Conservation of Energy. 
     - Other forms of energy – thermal, electromagnetic and so on – will be introduced later.
    - Define *mechanical energy*. 
* Conservative fields.
    - Use gravity as the example.
    - Positional energy only depends on the position, that is, it does not depend on the path taken.
    - Force is the gradient of the potential energy.
    - Explain how energy is conserved in a many-body system.

    
    