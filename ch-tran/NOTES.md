# Notes for Transitions


## Material

* One particle can always be assumed to be stationary. 
* The Golden Rule. 
    - It is only about scattering at infinitely. Thus, only the direction of the momentum matters. 
    - This makes it important to write state in the basis of momentum characteristic vectors. 
* Conservation laws. 
* The Energy-Time Uncertainty Relation. 
    - Write it as $τ ≥ \frac{\mathrm{h}}{4⋅π⋅\mathrm{sd}(E)}$. 
    - Note that this is not an uncertainty relation like the other. Time is not an observable. 
    - Intuitively, it is a lower bound the minimum time $τ$ required for an appreciable change to happen in an observable. It is not that precise, but there is something there intuitively and it is functional experimentally. 
    - The idea is that this hold for any observable, but the RHS involves the energy observable. 
    - The minimum time $τ$ is defined as $\frac{\mathrm{sd}(Q)}{\abs{∂⋅\mathrm{mean}(Q)}}$.
    - Supposed application. 
        + It explains the width of spectral lines. (There must be uncertainty in the energy because a transition is possible.)
        + Lifetime of particles? 
* Mention the indivisibility of quantum transitions. 
* Some transitions cannot happen due to symmetry. An electron with a spherically symmetric state cannot drop to a spherically symmetric state.
* Mention that the type of force decides probability/speed? More precisely, I think that the energy difference is related to the halflife.
* Supposedly, classical QM can explain stimulated emission, but not spontaneous emission.
* Are orthonormal bases need for the transition matrix? 
* Examples: Fluorescence, light absorption, radioactivity, measurement, fission/fusion and spin/quantum gates.
* Finite-bases situations, such as spin gates, might be the easiest way to do this.