# Outline of Mechanics of Particles

## Introduction

<!-- * Basic introduction. 
	- Explain that a nonclassical theory is about to be explained. 
	- This chapter deals with the motion of one particle in an electric field. 
	- This is extended to magnetic fields in the next chapter Spin. 
	- This is extended to multiple particles in the chapter after this. 
	- Nuclear phenomena and a nonrelativistic theory is then done after those.  -->
* Should I also explain the mirror experiment from Feynman's *QED* where scratches are made that make the light reflect from a different part of the mirror (rather than not reflect).
* Briefly reiterate the problems at hand. Emphasize that classical physics fails.
	- Classical physics doesn't predict that atoms are stable, that is, electrons are in motion but do not emit light and do not fall into the nucleus.
	- The energies of orbits of electrons in atoms are discrete.
	- Not just photons, but also electrons and other particles have wave-like behavior.
	- Light consists of particles. Thus, Maxwell's equations are not valid on the small scale.
* Explain that the next two chapters will be needed for a complete description of this theory. 


## State Function

* Notes. 
	- Explicitly say that complex numbers are just used for convenience.
	- Remark on how quantum behavior is only observable when the action is comparable to Planck's constant. This is because, when Planck's constant is big, the term $\exp(\frac{2⋅\uppi⋅\mathrm{i} ⋅S}{\mathrm{h}}$ is oscillates to much except on the classical trajectory.
	- Just do this for multiple particles. 

**Particle Have Wave Behavior**: 
* Have the de-Broglie Relation for any particle. 
	- Mention how this is the basis of electron microscopes. 
	<!-- - This relation with the Planck's Constant is need to start off the path integral approach.  -->
* The Double-Slit Experiment for photons and other particles.
	- This works for even composite systems, such as nuclei and atoms.
	- Mention that this experiment is difficult for particles other than photons. Give the widths of the slits. Mention that, as an alternative, diffraction through a crystal can also be done.
	- De Broglie's relation is for all particles. 
		* Give the wave length of a grain of sand for intuition on how small things need to be to see quantum effects.
		* Emphasize that the energy (that determines the wave length) includes the rest energy. 
	- Detecting whether a particle goes through a slit stops the interference. (An electron can be detected by having a thin photographic film.)

**Path Integral Approach**: 
* Introduction.
	- The Alternatives Law.
		* I think that the square comes from the classical formula for the wave interference.
		* Only definite interactions interfere with it. (We are only measuring between definite iterations.) Explain what a definite interaction is – anything that leaves a trace.
	- The slits are close enough that the particle has approximately the same phase at them.
	- Mention that the phase cannot be measured.
* Formula for the propagation factor
	- Denote it by
	\[
		\mathrm{prop}(\phi_1(x) \to \phi_2(x)).
	\]
	- The Composition Law.
		* This allows for the propagation factor to be written as a product of propagation factors over shorter periods of time. Furthermore, over short periods of time it is approximately
		\[
			\exp \left( \frac{2 \pi \pi}{\h} ⋅ S \right),
		\]
		where $S$ is the action of the classical trajectory between the two points, but with a weighting factor. Together, these allow for it to be computed.
		* Furthermore, when many factors are taken, the form can be approximated by straight trajectories. 
		* Mention that a violation of the Alternatives Law and the Composition Law has never been found.
	- It preserves total probability through time.
	- The wave function gives enough information to decide the future.

**The Schrödinger Equation**: 
* The Schrödinger Equation.
	- Notes. 
		* Mention that this is easier to solve or something. It uses local information instead of global information. 
	- Derivation. 
		* I guess that the Hamiltonian is essentially the chance of a particle to (infinitesimally) jump to a nearby state per unit time. (I think that this is more general than just the Schrödinger Equation can apply to other systems – such as discrete ones.)
		* The time step operator should look like $I - A⋅t$. (Then, figure out A.). (This is in the Feynman Lectures.)

**Probability Current**: 
* Notes: 
	- I definitely want to have this because it does help with intuition, and it connects to the Continuity Equation. 
	- Likely this will not be its own section, but it is fine if it is. 
	- As a theorem, prove that probability is conserved, that is, it satisfies the Continuity Equation. 
	- Formally, define the *(probability) current*. 


## Measurement

* Introduction. 
	- Reemphasize that observables are probabilistic, and that their distribution is what matters.
	- Explain that measurements are interpreted classically. Another way of saying this is that they are classically describable. 
* The Momentum Operator. 
	- The mean of momentum in terms of an operator. 
* Observables as Operators. 
	- Rule. All observables have a corresponding operator. 
		+ Give the notation for the operator of an observable. 
		+ Give examples. 
			* Energy. 
			* Angular momentum. 
				+ Coordinates. 
				+ Size. 
		+ They are self-adjoint. 
		+ For measurements that can be expressed classically using location and momentum, replacing the position and momentum by $\hat{x}$ and $\hat{p} = -\i⋅\h⋅\grad$ gives the operator. 
		+ There are measurements – such as parity – that cannot be written in terms of location and momentum. 
	- Rule. (Born's rule) The probability density is the square of the spectral density. 
		+ Give the notation for the density of an observable. 
		+ The formula for the mean using operators. 
		+ The formula for the standard deviation using operators. 	
* The Collapse Postulate. 
	- Before. 
		+ Two of the same measurement immediately after each other gives the same reading. 
		+ Thus, the measurement puts the wave function into a space that is invariant under the operator such that every vector in the space gives the same measurements. 	
		+ One can think that an observation disturbs the system. (Explain using light to measure things, that is, although it can measure the speed of a baseball, on the atomic level, it creates a large disturbance.) But, measurements don't need to interact with the particle. When measuring position, it reduces (but not necessarily to $0$) the amplitude at places where the particle is searched for. Should I talk about this? 
	- Statement. (This is about the probability of being an eigenvector.)
* The Uncertainty Principle.
	- Before. 
		+ Two different types of measurements (location and momentum) might not be compatible because the first changes the state.
			* Suppose that a property of a system is measured. Then, an incompatible property is immediately measured. Then, the original property is immediately measured. Thus, first and last measurement might not agree.
			* If two operators commute, then they have the same eigenvectors. But, don't think too deeply because actually eigenvectors are not realized. 
	- Intuition. 
		+ Explanation of the theorem. Intuitively, the second factor on the right-hand side of the relation in the theorem is the mean of how much the operators of the measurements commute. 
		+ It is how far away the closest eigenvectors of the operators are from each other? No, this doesn't actually make sense because actual eigenvector cannot be attained because they are not physical. Furthermore, this distance is normally infinite. 
	- General statements. 
	- After.
		+ Give the version for location and momentum. 
			* Explain how a more localized particle has a bigger variation in momentum because the gradient is bigger. 
			* Should this be related to the classical uncertainty principle? (Explain how momentum is about wavelength.)
		+ Give the versions for angular momentum. 
* Evolution of the Means of Measurements. 
	- Heisenberg Equation. 
		+ What's the point? 
			* I don't understand the intuition. 
			* I guess that it helps with conservation laws. 
			* It is sort of an ODE. If the operator is time-independent (in the Schrödinger picture (normal intuition)), then the equation is $∂_t(A) = \frac{1}{ih} [A, H]$, and the mean is attained by applying it to the initial wave function. I guess that if the operator is restricted to a finite-dimensional space, then this relatively easy to compute. 
			* Supposedly it is better in the relativistic setting. 
			* What even is an observable that changes in time (in the Schrödinger picture (normal intuition))? I guess that this is more so for the hamiltonian (the external field changes). 
	- Ehrenfest's theorem.
		+ I guess that this only has philosophical importance. 	
			* Well, does this roughly mean that the Correspondence Principle holds? 
		+ I should also include that the average change in location	is equal to the average momentum over the mass. 
	- Conserved measurements. 
		+ The mean is conserved if it commuted with the hamiltonian. 
		+ Give examples. 
			- Energy is conserved.


## Standing States

* The standing Schrödinger Equation. 
	- This is attained by separation of variables. The system of equations is not static because the phase still changes (but the probability density doesn't). The static equation in this system is called the static Schrödinger Equation.
	- The intuition of static state is that it has the maximum spread.
	- The variance of the energy is $0$.
* Electrons in Atoms.
	- Assume the location of the nucleus is fixed.
		+ Give an analogy to describe how much more massive the electron is to the proton – which is the smallest nucleus. What is a good example of something with a factor of 1800? 
	- This is just an eigenvalue problem. (Like, what is introduced in Resonance.)
	- There is a countably many bounded states. They have definite energies and angular momenta. (I think that the operator/Hamiltonian in the static-state Schrödinger Equation is compact. Thus, the energies are discrete.)
	- That unbound states are not discrete intuitively indicates that classical physics is attained on a bigger scale.
	- Plot some probability densities of the electron of a hydrogen atom.
	- Solutions.
		+ This can be solved exactly, although this is not done here. Give the exact solution. (These can be found in *Intermediate Quantum Mechanics* by Bethe.)
		+ Explain that these are quite accurate because the equation is not general enough, but the general shell structure remains, that is, the location of the energy drops. 
		+ Classification of solutions by the quantum numbers. 
		+ Give a graph of where the energy levels of different subshells. (What do I mean by this?)
			* This is 1D, so just have a 1D plot, that is, a line/arrow. Mark the shells, 1s, 2s, 2p and so on.
			* Mark where the inert gases structures are filled.
			* Mark how many states each subshell has.
			* Put the energy on a logarithm scale.
	- Briefly mention the Exclusion Principle and that it explains the Periodic Table. 
	- Mention that these states are roughly the same with multiple electrons and more accurate equations, that is, these number don't qualitatively change, and the jumps are in the same location.
		+ The don't normally need relativistic corrections. For example, the speed of an the electron in hydrogen is about one hundredth the speed of light. 


## Dynamic States

* Talk about wave packets. 
	- Guassian packets are the only states such that the Uncertainty Relation becomes an equality, that is, it minimizes $\mathrm{sd}(p)\mathrm{sd}(x)$. 

**Mixed States (Density operator)**: 
* Notes. 
	- What's really the point of this? It feels like more than it is. (It is really just about considering all possible initial states weighted by probability.)
	- Should I motivate this by the states not being exactly known? Supposedly, it can also be motivated by entangled states. 
	- This really is weird/complex because the is variation of an entire function. But, like, does this really mean anything? Is it not just UQ? 
* The Schrödinger Equation. 
* Measurements. 



## Multiple Particles

* The Multi-particle Schrödinger Equation. 