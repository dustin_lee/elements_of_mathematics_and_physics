# Notes for Mechanics of Particles

## Issues

* This there a good argument for the de-Broglie Relation? Is de Broglie's any good? Is there some sort of connection between light and matter similar to the connection between energy and matter in special relativity? 


## Need to Understand

* I should at least understand the idea of the Feynman-Kac Formula. 
* For path integral, for each position, can there be considered a momentum distribution? Or is momentum from the entire wave function? 
	- It seems that if one found a particle at a certain place, then it would have required a momentum to get there. But, the particle's position can be determined without direct interaction, so the momentum couldn't get a broader variation this way. 
	- It just seems so weird that momentum would be global. 
* Measurements as operators. 
	- I don't understand how to go from the first idea of measurements to operators. 
	- How can the probability density be recovered from its operator?
		* Integration with respect to the spectral measure on the eigenvectors?
		* Is the Collapse Postulate needed? 
	- How is the standard deviation obtained from the operator? 
* How does momentum space contain information about position? 
* How are dynamic states done? 
	- How could one know the actual state of a dynamic wave function?      
	- Even for a stationary particle, how is this done? 
* Feynman claims that the Uncertainty Principle says one cannot determine which of two alternatives was taken without destroying the interference. I don't really understand the connection. This is Section 1-8 of volume 3 of the Feynman Lectures. 
* Perturbation theory. 
	- Supposedly, the order of the term is the same order of the corresponding Feynman diagram. 
	- Supposedly, this makes some computations more manageable. (But, it is probably mostly useless.)
	- When finding the state of an atom in an applied field, the basis of the finite-vector space approximation used to find eigenvalue approximations can include the solutions without the applied field.
* What is a good scheme for solving the Schrödinger Equation? I would want one that preserves probability – at least when solving the dynamic Schrödinger Equation. 
* How well are energy and momentum conserved in quantum mechanics? 
* When computing the electron state in a helium atom, the nucleus is localized to a point, which gives an infinity for the potential. How is this dealt with? 


## Summary 

* Explain that *force* is not as informative as in classical mechanics. 


## Figures

* Ionization energy: 
	- Have the it as a function of atomic number
	- Overlay the one-electron approximation of it to see that the Schrödinger Equation makes sense?


## Vocabulary

* Probably use *(quantum) state function* instead of *wave function*. (Or just *quantum state*.)
* Use *Standing Schrödinger Equation* for the actual version of the Schrödinger Equation. Use *Static Schrödinger Equation* for the reduction. The connection is that multiplying the solution to the Static Schrödinger Equation by the phase factor gives the solution to the Standing Schrödinger Equation. 


## Notation

* Use $\mathrm{prop}(s_1 → s_2)$. 
* It is probably fine to use some short-hand notation such as $\mathrm{tran}(e^{+}e^{-} → γ)$. 
* Should fundamental particle be upright or italicized? 
* How should I denote operators and densities? 
	- For now, just use $p_{\mathrm{op}}$. What about $p_{\mathrm{fn}}$. 
	- I could also use $\mathrm{fn}_{p}$, $\mathrm{op}_{p}$, $\mathrm{F}_{p}$ and $μ_{p}$. 
* Is it okay that $L^2$ denotes the square of the size of the angular momentum? (It only differs from $\mathrm{L}^2$ by the font.)


## Material

* Introduction: de Broglie: 
	- Double-Slit: *This experiment is difficult. And, historically, the De-Broglie Relation was not verified this way. But, now this experiment has been done with electrons and even helium nuclei."
* Tunneling. 
	- What is a good example that isn't alpha decay. 
	- This probably shouldn't be thought of as a transition. It should be seen as accumulation of the wave function on the other side of the potential hill (even though it stays negligible on the hill). 
	- This explains alpha decay.
	- Is there a more direct situation (such as for light) than alpha decay?
* The Feynman-Kac Formula is not done for a particle with spin. So, why bother at all? Probably just don't have it. 
* Numbers. Explain how small Planck's constant is by writing the action of an everyday object in terms of it. 


## General

* After finding an eigenvector for the static Schrödinger Equation, it needs to be normalized. 
* I don't think that the hamiltonian is still the energy operator when there is a magnetic field.
* The de-Broglie Relation was experimentally confirmed within an error of $1\%$.
* In the equations, the momentum needs to be used as a separate quantity $p$ instead of as $\frac{1}{2}⋅m⋅\abs{v}^2$ because of light and to unify classical and relativistic momentum.
* The Schrödinger Equation can be seen to not be relativistically invariant because a delay in the electric field will cause it to not satisfy the Continuity Equation. Supposedly, another way to see this is that the orders of the time derivative and the space derivative are different. 
* Explain the effect of detecting a particles position. 
	- Suppose that it is determined that it is in the region $E$. 
	- (Then, it is an eigenvector of the unit-impulse function on $E$.)
	- The state function is then scaled on $E$, and reduced to $0$ outside $E$. 
	- Because the state function is made bigger on $E$, the derivatives are bigger. Thus, the variation in momentum is probably increased. 

