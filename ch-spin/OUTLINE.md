## Outline of Spin 

## Introduction

* Mention the (Pauli) Exclusion Principle. 
* The Zeeman Effect. 
    - These are when the spectrum changes when placed in an magnetic field. 
    - Don't bother naming them. 
    - Spin explains this. Although, further improvements from multiple particle and relativistic effects are still required. 
* Give a rough description of spin and the particles.  
    - It is like a magnetic moment, but is not classical. The electron would have to be spinning faster than light. 
    - Even neutral particles, such as neutrons, can have it. 
    - Discrete values. 
* Mention that spin is a cause of some magnetism (ferromagnetism), and that this will be explained later. 


## (Static)

* The Stern-Gerlach Experiment. 
    - Do this for a spin-half particle (electron). 
    - Spin in one direction means nothing for the spin in a perpendicular direction. 
    - 
    - Removing filters can allow for more particles to go through. 
* Explain the operators and eigenvalues. 
    - Pick one direction, but spin still makes sense when measured in other directions. 
    - Explain how to compute the spin operator in any given direction. 
* Generalize to arbitrary spin. 


## (Dynamic) The Pauli Equation

* Motivation. 
    - The magnetic dipole of the particle affects how it behaves in a magnetic field. Include this in the action. 
    - Different spin states need to be accounted for. 
    - The magnetic field can affect the spin. 
* The Pauli Equation. 
    - Don't be too rigorous with deriving this. 
    - Static version, but don't derive it. 
* The Aharonov-Bohm Effect. 
    - Only quickly mention it. 
    - Have a figure. 
    - Effect. When a solenoid is placed between a doubles-slit experiment with electrons, the interference pattern changes even though there is no electric field in the path of the electron. This shows that the potential is indeed real to some extent. (By Stokes's theorem, the integral of the magnetic potential on a loop around the solenoid is not zero. Thus, the paths through the different slits get different phases because of the different magnetic potentials.)
    - What do I make of this? 


## Magnetism

* Notes. 
	- Effects stay (ferromagnetism) because the energy state is lower. 
* Introduction.
	- Talk about permanent magnets, paramagnetism and diamagnetism. (Maybe don't if these are already talked about in classical physics.)
	- The Einstein-de-Haas Effect. Iron is shown to rotate when subject to a magnetic field. This should that the magnetism is from the angular momentum of the electrons. Should I bother mentioning this? 
* Paramagnetism.
	- The magnetic permeability is smaller than $μ_0$.
	- Curie paramagnetism.
		* Should this be done when talking about the atom?
	- Pauli paramagnetism.
* Diamagnetism.
	- This is pushed away from magnetic fields. The magnetic permeability is smaller than $μ_0$.
	- Rule of thumb: If all electrons in the particle are paired, then the substance made of this particle is diamagnetic; If it has unpaired electrons, then the substance is paramagnetic.
* Permanent magnets.
	- Ferromagnetism.
		* Obviously, use iron as the main example.
		* Does it make sense to say that it magnetizes itself, and this is why an external field is not needed?
		* Antiferromagnetism.
			- Should diamagnetism just go here?
		* Ferrimagnetism.
		* Curie temperature.
	- Are there other types of permanent magnets, such as from the actual orbits of the electrons? Why don't nuclei align?