# Notes of Spin

## Need to Understand

* Motivation: 
    - How does spin change the motion when aligned with the field? 
    - How does an unaligned field change the spin? 
    - How does spin change the motion when unaligned? 
    - What field is generated by spin? 
    - Spin has angular momentum, which is kind of crazy. How is this measured and tested. (Supposedly, resisting a turning force means it has angular momentum.) 
* Think of the two dimensions as a basis. Then, changes back and forth should be fine until a measurement is changed. But, it is weird that the measurement depends on the basis. 
* Where does the spin state live? 
    - How is spin only in a 2-dimensional space? 
    - I don't understand spin matrices and spinsors. 
        + A spinor takes two rotations to return to its original state. 
* How does spin work for multiple particles? 
    - There is a bigger matrix. 
* From a scientific view only its effect needs to be understood. 
    - Effects on measurement. (Just do for two arbitrary directions.)
    - Effects on the electromagnetic field on spin. (Pauli?)
    - Effects of the electromagnetic field on the trajectory. (Pauli?)
    - Effect of spin on the electromagnetic field. (Classical?)
    - Entanglement of multiple particles. 
* What really is precession? How is it measured? 
* How is spin both a "color" and with a geometric interpretation. 
* What does it really mean for two particles to have different spins? 
    -  When the spins of two particles in the same state are measured, then they are found to be opposite. But, the orientation could have been any direction. Does this mean that there is somehow more information than just a direction? 
        +  Maybe the extra degrees of freedom can come because there are four dimensions from there being two particles. 
* The $g$-factor is the ratio between spin and the magnetic momentum. Thus, spin means more than just the magnetic moment, although this doesn't need to be explained until QFT. 
* Do photons have a magnetic moment? Is this observed with normal light? 


## Material

* A spin direction can be fixed and other spins still make sense. 
* Should I mention the polarization experiment for photons? 
* Explain how to measure spin. 
    - I think that this is the point of carrying about the precession. 


## Vocabulary 

* Instead of *g-factor*, should I use *spin-momentum factor*. I could also just not name it. 
* The words *up* and *down* for spin is a little misleading because the spin of all particles is not $\frac{1}{2}$. 


## Notation

* Denote the components of the state by using the spin as the subscript. For example $\left(ψ_{+\frac{1}{2}}, ψ_{-\frac{1}{2}}\right)$ for an electron. 
* Are spin coordinates easier for multiple particles. 


## General

* Use this to reinforce quantum ideas. 
* Don't just consider particles whose spin is one*half. Start the chapter with mention the spins of several particles – electron, proton, neutron, helium nucleus, photon and a particle whose spin is 2. 
* Emphasize the matrix approach of the amplitudes. (For any tilt, all amplitudes are required.)