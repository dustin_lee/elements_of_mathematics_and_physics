## Outline for Methods for Characteristic Equations. 

## Introduction

* Explain when which method is best. 
    - Use Rayleigh Quotient to find eigenvalues for known (approximations of) eigenvectors. 
    - Use Inverse Iteration to find eigenvectors for known eigenvalues. 
    - Use the QR Algorithm to find the eigenvalues of dense matrices. 
    - Use the Divide-and-Conquer Algorithm if both the eigenvalues and eigenvectors are wanted of dense matrices. 
    - Use Jacobi for dense matrices on parallel machines? 
    - Use Lanczos for self-adjoint sparse matrices. 
    - Use Arnoldi for normal (non-self-adjoint) sparse matrices. 


## QR Algorithm

* 


## Divide-and-Conquer Algorithm. 

* 