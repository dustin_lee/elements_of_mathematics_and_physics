# Notes for Numbers

**Immediate**
* Need to explain that the domain of an expression as a function is all points where it exists. This is needed for power sums. Use *maximal domain*. 
* Have some graphs of polynomials 
* Have a graph of a logarithm and exponent functions. 


## Concerns 

* Do prime numbers actually matter? Are they used elsewhere? 


## Figures

* A figure that connects multiplication to addition? This would be in Natural Numbers. 
* Integer Exponentiation: 
    - Have even powers too. Probably have $x^0$, $x$, $x^$, $x^3$ and $x^{-2}$.  
* Real Exponentiation 
* Logarithms 


## General 

* Decision: For the basics of the various number systems, don't be too rigorous by having proofs for everything. 
* Decision: Keep the discussion of linear equations entirely algebraic, that is, do not make any geometric connections. In a sense, such geometric connections are a deep idea, and they are discussed in the chapter Linear Equations. The section Linear Equations in this chapter is just meant to provide the absolute minimum until this chapter. 


## Vocabulary and Notation

* Use the word *prime* instead of *irreducible*. (In general, I should lean towards layman words.)
* Should I use the term *mutually prime/irreducible* or *coprime*? (I like the German word *teilerfremd*.)
* Define *sums*, *products* and so on of equations.
* The word *between* includes the endpoints. The term *strictly between* is used to exclude the endpoints. 
* Actually, for intervals, use *left* and *right* instead of *start* and *end*. Use the term *boundary point* to refer to collectively mean both of these. 


## Material

* Should coordinates not be in this chapter? 
* Do explain that $v ≠ \mathrm{proj}_{b_1}(v) + \mathrm{proj}_{b_2}(v)$ if the basis is not orthogonal. 
    - Have a picture. 
    - Well, this uses angles. 
* There is a different level of infinity for how many more real numbers there are than rational numbers. Do we really need these? Well, what else is there? Transcendental numbers (such as $e$ and $π$) are used in practice. 
* Decimal system: All combinations are a unique real number, except those that have the digit $9$ infinitely many successive times. 
* Prove that the real numbers are categorical, that is, they are unique? 
* Since I am not dealing with systems of nonlinear equations, should I at least mention them? 


# Other

* It is trivial that the real numbers and the complex numbers are the only complete number fields. 