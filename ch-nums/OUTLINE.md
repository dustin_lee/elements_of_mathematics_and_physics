# Outline of Numbers

**Immediate Concerns**: 
* Does it make sense to have each number system in a different section? 
* Define absolute value


## Natural Numbers 

* Definition of positive integers. 
    - Can be thought of an extension of the positive integers.
* Arithmetic laws
* Order laws
---
* Euclidean division
* Prime numbers
* Fundamental Theorem of Arithmetic 
* There exists infinitely many prime numbers 


## Integers 

* Definition
* Arithmetic laws
* Order laws


## Rational Numbers

* Motivation briefly. 
* Definition. 
    - The terms *numerator* and *denominator*.
    - The term *reduced fraction*.
    - Can be thought of an extension of the integers.
* Arithmetic laws
* Order laws


## Real Numbers

* Motivation briefly. 
    - Prove that there does not exist a square root of 2. 
    - Can be thought of an extension of the rational numbers.
* Definition: 
    - Use Dedekind cuts
* Arithmetic laws
* Basic order laws
* Advanced order laws: 
    - Order Complete
    - Theorem of Eudoxus / Archimedean Property 
    - Smallest completion 
---
* Decimal system 
* Uncountability 
* Intervals 
--
* Order with functions
    - Comparison of functions
    - Monotone


## Polynomials and Complex Numbers

* Definition 
* Division
    - Analogue of The Fundamental Theorem of Arithmetic
* Zeros and roots
* Quadratic formula and so on
* Complex numbers
* Fundamental Theorem of Algebra
    - Use analytical results from later chapters
* Lagrange Interpolation Formula


<!-- **Polynomials**:
* The Binomial Theorem?  -->


## Exponentiation

**Exponentiation**: 
* Exponentiation by positive integers
* Exponentiation by integers
* Exponentiation by rational numbers
* Exponentiation by real numbers

**Logarithms**:
<!-- * Motivation? What really is the motivation? They just pop up sometimes and notation is needed. -->
* Definition. Define them as inverses of exponential functions
* Basic Rules:   
    -Exponentiation becomes multiplication, and multiplications becomes addition. 
* The Change-of-Base Formula.

**Table Method**: 
Explain how the table method can be used to compute them. Explain how they could be used computationally. And mention how faster methods are introduced later. 


## Linear Equations 