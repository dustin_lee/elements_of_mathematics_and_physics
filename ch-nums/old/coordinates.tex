\documentclass[12pt]{amsart} \usepackage{amssymb} \usepackage{latexsym}
\usepackage[T1]{fontenc}
\usepackage[letterpaper,margin=.75in]{geometry} \renewcommand{\baselinestretch}{1.5} \setlength{\parskip}{1em}
\usepackage{mathrsfs} 
\usepackage{enumerate} \usepackage[pdftex]{graphicx} \usepackage{tikz-cd}

\newcommand{\bb}{\mathbb} \newcommand{\cal}{\mathcal} \newcommand{\scr}{\mathscr}  \newcommand{\abs}[1]{\left| #1 \right|} \newcommand{\infinity}{\infty}

\newcommand{\textand}{\hspace{15pt} \text{and} \hspace{15pt}} \newcommand{\textor}{\hspace{15pt} \text{or} \hspace{15pt}}
\DeclareMathOperator{\m}{m} \renewcommand{\outer}{_{\text{out}}} \newcommand{\inner}{_\text{in}} \DeclareMathOperator{\cp}{cp} \DeclareMathOperator{\dom}{dom} \DeclareMathOperator{\rang}{rang} \DeclareMathOperator{\img}{img} \renewcommand{\d}{\text{d}} \DeclareMathOperator{\interior}{int} \DeclareMathOperator{\bd}{bd} \DeclareMathOperator{\cl}{cl}

\newcommand{\theorem}{\noindent \textbf{Theorem.\ }} \newcommand{\proposition}{\noindent \textbf{Proposition.\ }} \newcommand{\lemma}{\noindent \textbf{Lemma.\ }} \newcommand{\corollary}{\noindent \textbf{Corollary.\ }} \renewcommand{\proof}{\noindent \emph{Proof.\  }} \newcommand{\proofoflemma}{\noindent \emph{Proof of lemma.\ }} \newcommand{\remark}{\noindent \textbf{Remark.\ }} \newcommand{\definition}{\noindent \textbf{Definition.\ }} \newcommand{\note}{\noindent Note:\ } \newcommand{\stars}{\begin{center} * * * \end{center} } \newcommand{\axiom}{\noindent \textbf{Axiom.\ }}

\newcommand{\mat}{\left( \begin{smallmatrix}} \newcommand{\rix}{\end{smallmatrix} \right)} \newcommand{\Mat}{\left( \begin{matrix}} \newcommand{\Rix}{\end{matrix} \right)} \renewcommand{\empty}{\varnothing} \renewcommand{\phi}{\varphi} \renewcommand{\epsilon}{\varepsilon} \newcommand{\<}{\langle} \renewcommand{\>}{\rangle} \newcommand{\gen}[1]{\langle #1 \rangle} \newcommand{\iso}{\simeq} \newcommand{\ceil}[1]{\lceil #1 \rceil} \newcommand{\floor}[1]{\lfloor #1 \rfloor} \newcommand{\bs}{\backslash} \newcommand{\tensor}{\otimes} \renewcommand{\subset}{\subseteq} \newcommand{\ssneq}{\subsetneq} \renewcommand{\bar}[1]{\overline{#1}} \newcommand{\inv}{^{-1}} \newcommand{\Union}{\bigcup} \newcommand{\union}{\cup} \newcommand{\Intersection}{\bigcap} \newcommand{\intersection}{\cap} \newcommand{\set}[1]{\{ #1 \}} \newcommand{\Prod}{\prod} \newcommand{\derivative}{\partial} \newcommand{\degree}[1]{#1 ^{\large{\circ}}}

\begin{document}
\title{Coordinates}
\maketitle

\noindent [Outline of motivation: (1) Want planes and three-dimensional space and whatnot. (2) It is desired that this space satisfies certain relations. But, since more axioms are not wanted, a model -- more or less -- is given. There are actual axioms given later. (3) Either way, there should be coordinates. (4) These coordinates can be used to define this "model" $\mathbb R^n$. (5) Explicitly state that, when thinking about space as points, the origin is not special, although, when thinking about space as vectors, it is. 

The plane may thought of as the product $\mathbb R \times \mathbb R$ of the Real Numbers with itself. A point in the plane is considered as an ordered pair $(a,b)$ of real numbers $a$ and $b$, and these real numbers are called the \emph{coordinates} of this point.
\begin{center}
\begin{tikzpicture}
\draw (-2, 0) -> (4,0) node[right]{$\mathbb R$};
\draw (0, -2) -> (0, 4) node[above]{$\mathbb R$};
\draw[dashed] (3, 2) node[right]{$(a,b)$} -- (3,0) node[below]{$a$};
\draw[dashed] (3, 2) -- (0,2) node[left]{$b$};
\end{tikzpicture}
\end{center}
 Since two coordinates are  used to locate the points in the plane, the plane may be thought of as two dimensional. Similarly, one coordinate is used to describe points on the line, and three are used to describe points in three-dimensional space. Later, there will be situations where it is useful to consider higher dimensional spaces. 

\noindent [Location vectors. Adding. Scaling. Over other fields.]

\definition A \emph{vector space} over a field, which is called its \emph{base field}, is a set together with a binary function $+$ from the product of it with itself to itself and a binary function $\cdot$ from the product of its base field with it such that there exists an element $0$ in this set such that $x+0$ equals $x$ for any element in this set, and for any elements $x$, $y$ and $z$ in this set and any elements $a$ and $b$ in its base field,
\begin{enumerate}[(i)]
\item $x+y$ equals $y+x$;
\item $x + (y + z)$ equals $(x + y) + z$;
\item $1 \cdot x$ equals $x$;
\item $(ab) \cdot x$ equals $a \cdot (b \cdot x)$;
\item $a \cdot (x + y)$ equals $a \cdot x + a \cdot y$;
\item $(a + b) \cdot x$ equals $a \cdot x + b \cdot x$.
\end{enumerate}
A \emph{subspace} of a vector space is a subset of this vector space that is a vector space under the same binary functions. 

The elements of the base field of a vector space are called \emph{scalars}. The elements of a vector space are called \emph{vectors}, especially when they are though of as arrows. 


When the Real Numbers are considered to be a vector space, they are called the \emph{euclidean line}. The product of the Real Numbers with itself is called the \emph{euclidean plane}. An $n$-ary product of the Real Numbers, where $n$ is a positive integer, is called the $n$-dimensional \emph{euclidean space}. 

In the euclidean plane, any vector is equal to a sum of the form
\[
	a \cdot (1,0) + b \cdot (0,1),
\]
where $a$ and $b$ are real numbers. Furthermore, the vectors $(1,0)$ and $(0,1)$ are not redundant in the sense that the scalars $a$ and $b$ in this sum are unique for any given vector, that is, if a vector can be written as 
\[
	a_1 \cdot (1,0) + b_1 \cdot (0,1) \textand a_2 \cdot (1,0) + b_2 \cdot (0,1),
\]
where $a_1$, $a_2$, $b_1$ and $b_2$ are real numbers, then $a_1$ and $b_1$ equal $a_2$ and $b_2$, respectively. 
 
These ideas are generalized by the following definitions: A \emph{linear combination} of a set of vectors $\set{x_i}_{i \in \cal I}$ in a vector space is any sum of the form	
\[
	\sum_{i \in \cal I}a_ix_i,
\]
where the $a_i$ is a scalar for each index $i$. In such a linear combination, a scalar $a_i$ is called the \emph{coefficient} of $x_i$. A set of vectors in a vector space are said to \emph{generate} the space if every vector in this space is a linear combination of these vectors. A set of vectors $\set{x_i}_{i \in \cal I}$ in a vector space is said to be \emph{linearly independent} if any vector that can be written as a linear combination of these vectors is uniquely written as a linear combination of these vectors, that is, if a vector can be written as
\[
	\sum_{i \in \cal I}a_ix_i \textand \sum_{i \in \cal I}b_ix_i,
\]
where $a_i$ and $b_i$ are scalars for each index $i$, then $a_i$ equals $b_i$ for each index $i$. Equivalently, a set of vectors in linearly independent if every linear combination of these vectors that equals the $0$ is such that every coefficient of these vectors in this linear combination is $0$. A set of vectors that is not linearly independent is said to be \emph{linearly dependent}. A \emph{basis} of a vector space is a set of linearly independent vectors that generate the vector space. An \emph{ordered basis} is a basis that is totally ordered. If an \emph{ordered basis} $x_1$, $\dots$, $x_{n-1}$ and $x_n$ of finite cardinality of a vector space is fixed, then the vector $a_1x_1 + \dots + a_nx_n$, where $a_1$, $\dots$, $a_{n-1}$ and $a_n$ are scalars, which are called the \emph{coordinates} of this vector, is denoted by 
\[
	(a_1, \dots, a_n) \textor \Mat a_1 \\ \vdots \\ a_n \Rix
\]
The former notation is called a \emph{row vector} and the later notation is called a \emph{column vector}. The ordered basis $(1,0)$ and $(0,1)$ of the euclidean plane is called the \emph{standard bases} of it. Similarly, the ordered basis $(1,0, \dots, 0, 0)$, $\dots$, $(0, 0, \dots, 1, 0)$ and $(0, 0, \dots, 0, 1)$ of vectors in the $n$-dimensional euclidean plane is called the \emph{standard basis} of it. 

\proposition If a vector space has a basis of finite cardinality, then any other basis of this space has the same cardinality. 

\proof Let $\set{x_k}_{1 \leq k \leq n}$ and $\set{y_i}_{i \in \cal I}$ be two bases of the same vector space such that the former has finite cardinality. It is enough to show that the cardinality of the latter basis is at most the cardinality of the former because reversing the role of them will show that they have the same cardinality.

Suppose that the cardinaility of the basis $\set{y_i}_{i \in \cal I}$ is greater than the cardinaility of the basis $\set{x_k}_{1 \leq k \leq n}$, and let $y_1$, $\dots$, $y_n$ and $y_{n+1}$ be elements of this basis $\set{y_i}_{i \in cal I}$. It is now shown hat these vectors are linearly dependent. Since the vectors $x_1$, $\dots$, $x_{n-1}$ and $x_n$ generate the space, there exists scalars $a_1$, $\dots$, $a_{n-1}$ and $a_n$ such that
\[
	a_1x_1 + \cdots + a_nx_n = y_1.
\]
By possibly renaming the vectors $x_1$, $\dots$, $x_{n-1}$ and $x_n$, it may be assumes that $a_1$ is non-zero. Thus, 
\[
	x_1 = \frac{1}{a_1}y_1 - \frac{a_2}{a_1} - \cdots - \frac{a_n}{a_1}x_n.
\]
Since $x_1$ may be written as a linear combination of $y_1$, $x_2$, $\dots$, $x_{n-1}$ and $x_n$, any vector may be written as a linear combination of these vectors, that is, they are a basis for the space. Furthermore, there exists scalars $b_1$, $\dots$, $b_{n-1}$ and $b_n$ such that
\[
	b_1y_1 + b_2x_2 + \cdots + b_nx_n = y_2, 
\]
that is, 
\[
	b_1y_1 - y_2 + b_2x_2 + \cdots + b_nx_n = 0.
\]
Since $y_1$ and $y_2$ are linearly independent, one of the scalars $b_2$, $\dots$, $b_{n-1}$ or $b_n$ is zero. By possibly renaming the vectors $x_2$, $\dots$, $x_{n-1}$ and $x_n$, it may be assumed that $b_2$ is non-zero. By the previous argument, the vectors $y_1$, $y_2$, $x_3$, $\dots$, $x_{n-1}$ and $x_n$ are a basis for the space. By continuing to replace the vectors $x_1$, $\dots$, $x_{n-1}$ and $x_n$ with the vectors $y_1$, $\dots$, $y_n$ and $y_{n+1}$, it is shown that the vectors $y_1$, $\dots$, $y_{n-1}$ and $y_n$ are a basis of the space. Thus, the vectors $y_1$, $\dots$, $y_n$ and $y_{n+1}$ are linearly dependent, which contradicts that set of vectors $\set{y_i}_{i \in \cal I}$ is a basis for the space. 

\qed

This proposition also shows that if a vector space has a basis of infinite cardinality, then any other basis of this space has  an infinite cardinality. If a vector space has a basis of finite cardinality, then the cardinality of this basis is called the \emph{dimension} of the space. If a vector space has a basis of infinite cardinality, then the vector space is said to be of \emph{infinite dimension}. Although, vector spaces of infinite dimension will play  a role later, the reader may only consider the case of vector spaces of finite dimension for now. 



\proposition Every vector space has a basis. 

\proof This is proved by Zorn's lemma: Order the set of linearly independent vectors in a given vector space by inclusion. Since every chain of these sets of linearly independent vectors has an upper bound, namely their union, there exists a maximal set of linearly independent vectors. Furthermore, this maximal set generates the space, that is, is a basis. 

\qed

\noindent [Perhaps, this proof should be done by using transfinite induction.] 

This proof also shows that any set of linearly independent vectors may be extended to a basis. Thus, the dimension of any subspace of a vector space is at most the dimension of the vector space. In the case where the vector space is finite-dimensional, the dimension of any subspace that is not the whole space is less than the dimension of the vector space. 



\stars

\noindent [Motivation.] 

\definition A \emph{linear homomorphism} is a function $f$ between two vectors spaces over the same field such that $f(x+y)$ equal $f(x) + f(y)$ for any two vectors $x$ and $y$ in the domain of $f$, and $f(ax)$ equal $af(x)$ for any vector $x$ in the domain of $f$ and any scalar $a$ in the base field of the domain of $f$. 

A linear homomorphism maps $0$ to $0$ because of the equalities $f(0) = f(0 \cdot 0) = 0 \cdot f(0) = 0$. [Does it need to be shown that $0 \cdot x = 0$?]

A linear homomorphism is determined by how it maps a basis of its domain: Let $f$ be a linear homomorphism, and let $\set{x_i}_{i \in \cal I}$ and $\set{y_j}_{j \in \cal J}$ be bases of its domain and range, respectively. Then, the vector $\sum_{i \in \cal I}a_ix_i$, where the $a_i$'s are scalars, is mapped to 
\[
	\sum_{i \in \cal I}a_i f(x_i)
\]
under $f$. 
Since every vector $f(x_i)$ may be written as a sum $\sum_{j \in \cal J}b_{ij}y_j$, where  $b_{ij}$ is a scalar for any indicies $i$ and $j$, 
\[
	f(\sum_{i \in \cal I}a_ix_i) = \sum_{i \in \cal I}a_i f(x_i) = \sum_{i \in \cal I}( a_i \sum_{j \in \cal J}b_{ij}y_j) = \sum_{j \in \cal J}(\sum_{i \in \cal I}a_ib_{ij})y_j.
\]
This shows that the scalars $\set{b_{ij}}_{i \in \cal I;\ j \in \cal J}$ give a description of $f$. If $f$ is finite dimensional, its matrix representation is defined to be 
\[
	\Mat b_{11} && b_{12} && \cdots && b_{1n} \\ 
		b_{21} && b_{22} && \cdots && b_{2n} \\
		\vdots && \vdots && \ddots && \vdots \\
		b_{m1} && b_{m2} && \cdots && b_{mn} \Rix,
\]
where $n$ and $m$ are the dimensions of the domain and range of $f$, respectively. The column form of the image of the vector $(a_1, \dots, a_n)$ under $f$ is 
\[
	\Mat a_1 b_{11} + a_2 b_{12} + \cdots + a_n b_{1n} \\ 
		a_1 b_{21} + a_2 b_{22} + \cdots + a_n b_{2n} \\
		\vdots \\
		a_1 b_{m1} + a_2 b_{m2} + \cdots + a_n b_{mn} \Rix.
\]

\noindent [Matrix Multiplication.]

\noindent [Isomorphisms. Uniqueness up to dimension.] 


\noindent [Change of bases somewhere.]
\end{document}