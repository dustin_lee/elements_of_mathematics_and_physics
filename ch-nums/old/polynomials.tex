\documentclass[12pt]{amsart} \usepackage{amssymb} \usepackage{latexsym} 
\usepackage[T1]{fontenc}
\usepackage[letterpaper,margin=.75in]{geometry} \renewcommand{\baselinestretch}{1.5} \setlength{\parskip}{1em}
\usepackage{mathrsfs} 
\usepackage{enumerate} \usepackage[pdftex]{graphicx} \usepackage{tikz-cd}

\newcommand{\bb}{\mathbb} \newcommand{\cal}{\mathcal} \newcommand{\scr}{\mathscr}  \newcommand{\abs}[1]{\left| #1 \right|} 

\newcommand{\textand}{\hspace{15pt} \text{and} \hspace{15pt}} \newcommand{\textor}{\hspace{15pt} \text{or} \hspace{15pt}}
\DeclareMathOperator{\m}{m} \renewcommand{\outer}{_{\text{out}}} \newcommand{\inner}{_\text{in}} \DeclareMathOperator{\cp}{cp} \DeclareMathOperator{\dom}{dom} \DeclareMathOperator{\rang}{rang} \DeclareMathOperator{\img}{img} \renewcommand{\d}{\text{d}} \DeclareMathOperator{\interior}{int} \DeclareMathOperator{\bd}{bd} \DeclareMathOperator{\cl}{cl} \DeclareMathOperator{\diam}{diam} \DeclareMathOperator{\re}{re} \DeclareMathOperator{\im}{im} 

\newcommand{\theorem}{\noindent \textbf{Theorem.\ }} \newcommand{\proposition}{\noindent \textbf{Proposition.\ }} \newcommand{\lemma}{\noindent \textbf{Lemma.\ }} \newcommand{\corollary}{\noindent \textbf{Corollary.\ }} \renewcommand{\proof}{\noindent \emph{Proof.\  }} \newcommand{\proofoflemma}{\noindent \emph{Proof of lemma.\ }} \newcommand{\definition}{\noindent \textbf{Definition.\ }} \newcommand{\note}{\noindent Note:\ } \newcommand{\stars}{\begin{center} * * * \end{center} } \newcommand{\axiom}{\noindent \textbf{Axiom.\ }} \newcommand{\law}{\noindent \textbf{Law.\ }}

\newcommand{\dbar}{{\vphantom{d} \ooalign{\kern.12em\smash{\raisebox{.95ex}{\scalebox{.6}{\rotatebox[origin=c]{0}{$-$}}}}\hidewidth\cr$d$\cr} \kern.05em}}
\newcommand{\cbar}{{\vphantom{c} \ooalign{\kern-.05em\smash{\raisebox{.25ex}{\scalebox{.6}{\rotatebox[origin=c]{0}{$-$}}}}\hidewidth\cr$c$\cr} \kern.05em}}
\newcommand{\gbar}{{\vphantom{g} \ooalign{\kern.05em\smash{\raisebox{-.45ex}{\scalebox{.6}{\rotatebox[origin=c]{0}{$-$}}}}\hidewidth\cr$g$\cr} \kern.05em}}
\newcommand{\pibar}{{\vphantom{\pi} \ooalign{\kern.05em\smash{\raisebox{.2ex}{\scalebox{.6}{\rotatebox[origin=c]{0}{$-$}}}}\hidewidth\cr$\pi$\cr} \kern.05em}}
\newcommand{\gammabar}{{\vphantom{\gamma} \ooalign{\kern.1em\smash{\raisebox{-.2ex}{\scalebox{.6}{\rotatebox[origin=c]{0}{$-$}}}}\hidewidth\cr$\gamma$\cr} \kern.05em}}
\newcommand{\ebar}{{\vphantom{e} \ooalign{\kern-.1em\smash{\raisebox{.1ex}{\scalebox{.6}{\rotatebox[origin=c]{0}{$-$}}}}\hidewidth\cr$e$\cr} \kern.05em}}
\renewcommand{\hbar}{{\vphantom{h} \ooalign{\kern.05em\smash{\raisebox{.95ex}{\scalebox{.6}{\rotatebox[origin=c]{0}{$-$}}}}\hidewidth\cr$h$\cr} \kern.05em}}
\newcommand{\ibar}{{\vphantom{i} \ooalign{\kern.00em\smash{\raisebox{.15ex}{\scalebox{.6}{\rotatebox[origin=c]{0}{$-$}}}}\hidewidth\cr$i$\cr} \kern.05em}}

\newcommand{\mat}{\left( \begin{smallmatrix}} \newcommand{\rix}{\end{smallmatrix} \right)} \newcommand{\Mat}{\left( \begin{matrix}} \newcommand{\Rix}{\end{matrix} \right)} \renewcommand{\empty}{\varnothing} \renewcommand{\phi}{\varphi} \renewcommand{\epsilon}{\varepsilon} \newcommand{\<}{\langle} \renewcommand{\>}{\rangle} \newcommand{\gen}[1]{\langle #1 \rangle} \newcommand{\iso}{\simeq} \newcommand{\ceil}[1]{\lceil #1 \rceil} \newcommand{\floor}[1]{\lfloor #1 \rfloor} \newcommand{\bs}{\backslash} \newcommand{\tensor}{\otimes} \renewcommand{\subset}{\subseteq} \newcommand{\ssneq}{\subsetneq} \renewcommand{\bar}[1]{\overline{#1}} \newcommand{\inv}{^{-1}} \newcommand{\Union}{\bigcup} \newcommand{\union}{\cup} \newcommand{\Intersection}{\bigcap} \newcommand{\intersection}{\cap} \newcommand{\set}[1]{\{ #1 \}} \newcommand{\Prod}{\prod} \newcommand{\derivative}{\partial} \newcommand{\degree}[1]{#1 ^{\large{\circ}}} \newcommand{\degreee}{\ ^{\large{\circ}}} \newcommand{\edot}{^{^{\text{\hspace{-.18cm} \Huge{.}}}}} \newcommand{\der}{\partial} \newcommand{\te}{^{\text{th}}} \newcommand{\infinity}{\infty} \renewcommand{\sc}[1]{\small{\textsc{#1}}}

\begin{document}
\title{Polynomials}
\maketitle

% A \emph{polynomial} over a field is defined to be a function that maps any element $x$ to a given sum of the form $\sum_{0 \leq k \leq n}a_kx^k$, where $n$ is a non-negative integer and $a_k$ is an element of the given field for each index $k$. A polynomial is often denoted by the rule that defines it, that is, such a given sum $\sum_{0 \leq k \leq n}a_kx^k$. If the rule of a polynomial $f$ may be written as $\sum_{0 \leq k \leq n}a_kx^k$, where $a_n$ is non-zero, then $a_n$ is called its \emph{leading coefficient}, $a_0$ is called its \emph{constant term}, and $n$ is called the \emph{degree} of $f$ and is denoted by $\deg (f)$. [A polynomials of degrees $0$, $1$, $2$, $3$, $4$ and $5$ are called \emph{constant}, \emph{linear}, \emph{quadratic}, \emph{cubic}, \emph{quartic} and \emph{quintic}, respectively. At least use the terms \emph{constant} and \emph{linear}.] A \emph{solution} of a polynomial $f$ over a field is defined to be any element $a$ of this field such that $f(a)$ equals $0$. [Use the phrase "A solution to a polynomial equation defined by $f$. Speak of the degree of a polynomial equation.]


\noindent [Some sort of motivation (divide out solutions).] 

A polynomial $g$ is said to \emph{divide} another polynomial $f$ if there exists a polynomial $h$ such that $g \cdot h$ equals $f$. 

\proposition (\emph{Euclidean division}) Let $f$ and $g$ be non-zero polynomials over a given field. Then, there exists unique polynomials $h_1$ and $h_2$ over this field such that the equality 
\[
	f = h_1 \cdot g + h_2
\]
holds and that the degree of $h_2$ is $0$ or less than the degree of $g$. 

\proof First, the existence is proven: If the degree of $g$ is greater than $f$, then $h_1$ and $h_2$ may be $0$ and $f$, respectively. Now, assume that the degree of $f$ is at least the degree of $g$. This case is proven by induction: If the degree of $f$ is $0$, then $h_1$ and $h_2$ may be $\frac{f}{g}$ and $0$, respectively, which gives a base case. Now, assume that the existence holds for if $f$ is replaced by any polynomial of lesser degree. Then, there exists polynomials $h_4$ and $h_5$ such that the equality
\[
	f - \frac{a}{b}g\cdot x^{(\deg (f) - \deg (g))} = h_3 \cdot g + h_4
\]
holds, where $a$ and $b$ are the leading coefficients of $f$ and $g$, respectively, and that the degree of $h_4$ is $0$ or less than the degree of $g$. Thus, the equality 
\[
	f = (h_3 - \frac{a}{b}x^{(\deg (f) - \deg (g))})g + h_4
\]
holds and the degree of $h_4$ is $0$ or less than the degree of $g$. 

Now, the uniqueness is proven: Suppose that there exists polynomials $h_1$, $h_2$, $h_3$ and $h_4$ such that the equalities
\[
	f = h_1 \cdot g + h_2 \textand f = h_3 \cdot g + h_2
\]
hold and that the degrees of $h_2$ and $h_4$ are each $0$ or less than the degree of $g$. Then, $(h_1 - h_3)g$ equals $h_4 - h_2$. Thus, the degrees of $(h_1 - h_3)g$ and $h_4 - h_2$ are equal. Since the degree of $h_4 - h_2$ is $0$ or less than $g$ and the degree of $(h_1 - h_3)g$ is $0$ or at least the degree of $g$, the degrees of $(h_1 - h_3)g$ and $h_4 - h_2$ are each $0$, which implies that they $(h_1 - h_3)g$ and $h_4 - h_2$ are each $0$ because $g$ is non-zero. 

\qed

If $a$ is a solution to a polynomial $f$ over a field, then $(x-a)$ divides $f$: There exists polynomials $h_1$ and $h_2$ such that the equality
\[
	f = h_1(x-a) + h_2
\]
holds and that the degree of $h_2$ is $0$, that is, $h_2$ is constant. By evaluating both sides of this equality at $c$, the equalities
\[
	0 = f(a) = h_1(a)(a-a) + h_2 (a) = h_2(a)
\]
hold, which implies that $h_2$ is $0$, that is $(x-a)$ divides $f$. 

A polynomial of degree $n$ over a field has at most $n$ solutions: Let $a$ and $b$ be a solutions of a polynomial $f$ over a field. Then, there exists a polynomial $g$ such that
\[
	f = g(x-a)
\]
Furthermore, then the equalities
\[
	0 = f(b) = g(b)(b-a)
\]
holds. Since $b$ does not equal $a$, $b$ is a solution of $g$. Thus, there exists a polynomial $h$ such that the equality
\[
	f = h(x-b)(x-a)
\]
holds. Proceeding in this way proves that $f$ does not have more solutions than its degree. 

\noindent [Roots? Or have these in Chapter 2?] 

[A solution to polynomials of degree two is now found: Let $ax^2 + bx + c$ be a polynomial over a field. The equation 
\[
	ax^2 + bx^2 + c = 0
\]
may be rewritten as 
\[
	x^2 + \frac{b}{a} \cdot x = -\frac{c}{a},
\]
which may be rewritten as 
\[
	x^2 + \frac{b}{2a} \cdot x + (\frac{b}{2a})^2 = - \frac{c}{a} + (\frac{b}{2a})^2 \textand (x + \frac{b}{2a})^2 = - \frac{c}{a} + (\frac{b}{2a})^2,
\]
that is, 
\[
	x = \sqrt{(- \frac{c}{a} + (\frac{b}{2a})^2)} - \frac{b}{2a} = \frac{-b + \sqrt{b^2 - 4ac}}{2a}. 
\]
Since the square root gives two numbers, this gives both solutions, if this square roots exists.] 

\noindent [The solution to polynomial equations of degree 3.] 


% \stars

% Not every polynomial has a solution, for example, the polynomial $x^2 + 1$ over the Real Numbers. It is useful to be able to extend the a field to another field such that a given set of polynomials have solutions. [Explain.] In the case of the polynomial $x^2 + 1$, this can be done by adjoining an element and defining its square to be $-1$: Define the \emph{Complex Numbers} to be the set of expressions of the form
% \[
% a + b \cdot \ibar,
% \]
% where $a$ and $b$ are real numbers, and call the elements of this set \emph{complex numbers}. This set is a field when the sum and product of two complex numbers $a + b \cdot \ibar$ and $c + d \cdot \ibar$ are defined to be $(a + c) + (b + d) \cdot \ibar$ and $(ac - bd) + (ad + bc) \cdot \ibar$, respectively, because
% \begin{enumerate}
% \item $0 + 0 \cdot \ibar$ is an additive identity;
% \item $1 + 0 \cdot  \ibar$ is a multiplicative identity; 
% \item the addictive inverse of a complex number $a + b  \cdot \ibar$ is $(-a) + (-b)  \cdot  \ibar$;
% \item the multiplicative inverse of a complex number $a + b \ibar$ is $\frac{a}{a^2 + b^2} - \frac{b}{a^2 + b^2} \cdot \ibar$.
% \end{enumerate}
% The reason that the multiplicative inverse of a complex number $a + b \ibar$ is $\frac{a}{a^2 + b^2} - \frac{b}{a^2 + b^2} \ibar$ is that 
% \[
% 	\frac{1}{a + b\ibar} = \frac{1}{a + b\ibar}\frac{a - b\ibar}{a - b\ibar} = \frac{a - b\ibar}{a^2 + b^2} = \frac{a}{(a^2 + b^2)^{1/2}} - \frac{b}{a^2 + b^2} \ibar.
% \]
% The Real Numbers are considered a subfield of the Complex Numbers by associating them to all complex numbers of the form $a + \ibar$. Complex Numbers of the form $a + 0 \ibar$ and $0 + a \ibar$, where $a$ is a real numbers are denoted by $a$ and $a\ibar$, and are called \emph{real} and \emph{purely imaginary} respectively. The complex number $\ibar$ is called the \emph{imaginary unit}. If $a + b\ibar$ is a complex number, where $a$ and $b$ are real numbers, then the \emph{real part} and \emph{imaginary part} of this complex number are $a$ and $b$, and are denoted by $\re (a+ b\ibar)$ and $\im (a + b\ibar)$, respectively. 

% \emph{The Funamental Theorem of Algebra}, which is prove in the appendix [] of this chapter, says that the Complex Numbers are \emph{algebraically closed}, that is, every non-constant polynomial over them has a solution. 

% \noindent [How the formulas for polynomials of degree 2 and 3 still hold.] 

% \stars

% \noindent [Lagrange and Galois.] 




% \stars
% \stars
% \stars


% \noindent \textbf{Possible Material:}
% \begin{enumerate}[(i)]
% \item Include the Fundamental Theorem of Algebra in this chapter. In which form should it be stated? Also, mention the other form, at least in the Notes. 
% \end{enumerate}

% \noindent \textbf{Notes:}
% \begin{enumerate}[(i)]
% \item
% \end{enumerate}

% \noindent \textbf{Stuff for the Introduction:}
% \begin{enumerate}[(i)]
% \item Only have one proof in the introduction. A presentation of other proofs could be a project. 
% \end{enumerate}


\end{document}