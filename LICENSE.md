# License of the *Elements of Mathematics and Physics*

This work is protected by copyright laws, as any such work is [automatically](https://en.wikipedia.org/wiki/Copyright_law_of_the_United_States#Registration_procedure). 
The author (Dustin Enyeart) gives you permission to use it in anyway for personal use. 
You are not allowed to redistribute it in any form, either unmodified or modified, either in part or in whole, expect for reasonable quoting with proper citation. 
Instead of distributing the source files or compiled document of this work, please direct others to its [repository](https://gitlab.com/dustin_lee/elements) or other sources from the author. 

If you notify the author of any errors or suggestions through email or as a git merge request, then you implicitly agree to give the author all legal rights to such changes. 