# Outline of Generalizations of Integration and Differentiation

## Summary 

* Imaginary:
    - It gives integrals to functions that do not intuitively have integrals. (This is done by giving content to sets that were already imaginary.) 
    - Then, it add imaginary functions, which are called distributions. 
    - These thing are not themselves important. But, they help to prove existence existence. First, existence and uniqueness can be proved in a bigger space. Then, it is proved that the object is in the smaller space. 


## Introduction

* These are mostly technical, that is, they don't have physical meaning. 
* They are used for existence and uniqueness proofs of differentiation equations. 
    - Uniqueness. If uniqueness is proved in a bigger space, then the solution is also unique in the smaller space. 
    - Existence. Want to know that the method converges to something. The smoothness can then be proved after or can be assumed by the physical theory. 
* Expansion. 
    - The space of integrable functions is expanded by completion. But, their derivatives must also form a complete space, so the derivatives need to exists. 
    - Also, the space of square-integrable functions is more important because there is a sense of projections. (Note that any physical solution will be in this space just like it would be in the space of integrable functions.)
    - What is wanted is a complete space of function-like objects that are square integrable and infinitely differentiable. 
* Justify defining a generalization of content first instead of just completely the space. 
    - Is there really one? I guess that it makes it more intuitive/simple. 


## Countability Additive Content

* Motivate countability by the desire to complete the space of integrable functions. 
    - [Wrong. get another example.] As an example, consider the following sequence. Let $(p_i)$ be a sequence of points that is dense in the domain. For each index $i$, let $f_i$ be the function that is $0$ except on $p_j$ when $j<i$, where it is $1$. Then, this sequence does not converge to an integrable function. Actually, this example doesn't work because when the equivalence of functions is taken, these are all the same function.  
* Definition and construction. 
    - Do this for any content space. 
* This does not physically make sense, that is, this generalization of content is not physical. 
    - Use the example that the content of the set of rational numbers is $0$.
    - Explain that this definition of content is only used for the integral. 
        + Specifically, these additional null sets are needed. [Reference the next theorem?]
* In the completion of a finitely additive content space, every measurable set is the union of a set that is measurable in the finitely additive content space and a null set. 
    - Furthermore, in the completion of a topological additive content, every measurable set is the union of an open set and a null set. 
        + Define a topological countably additive content space to be the completion of a topological additive content. 
    - Should I give an example of an uncountable null set (such as the Cantor set)? 


## Integration for Countability Additive Content

* Notes. 
    - Should I explain the intuition of splitting the integral into a bounded part and a part that converges uniformly? I don't think that it actually helps with any of the proofs. But, this is the intuition for them. (This is what Egorov's theorem is all about.) I don't know how to explain it or where to fit it in🤷‍♂️.
* Definition. 
    - Definition. 
        + Definition of measurable functions. 
            * Continuous functions are measurable. 
            * (Riemann) Integrable functions are measurable. 
                - I am not sure how trivial this is. 
                - This means that this is a more general definition of integration. 
            * Equivalence of being pointwise approximated by measurable piecewise-constant functions and the preimages of measurable sets being measurable. 
    - Lemma. Egorov's theorem. 
        + Intuitive statement. Pointwise convergence of measurable functions is almost uniform convergence. 
    - Proof that the definition is consistent. 
        + Actually, can these proofs be simplified by the fact that uniform limits can already be exchanged with integrals?
    - The Bounded Convergence Theorem. 
        + This means that limits and integrals can be exchanged. 
        + This means that the choice of approximating functions does not matter. 
    - The integration laws hold. 
        - Just state these. 
        - Is the Triangle Inequality trivial? 
* Integration on unbounded domains. 
    - The Dominated Convergence Theorem. 
    - The Monotone Convergence Theorem. 
        + For the statement, have that $\lim{i \to \infty} ∫ f_i$ is finite. 
* The space of integrable functions is complete. 
    - State that this proof proves that a convergent subsequence in $L^1$ has a subsequence that converges pointwise to the same limit almost everywhere. Then, explain how the original sequence might converge pointwise almost everywhere. 
* The set of continuous functions is dense. 
    - Lemma. Urysohn. 
    - This means that both the set of Riemann integrable functions and the set of uniformly integrable functions are dense, that is, this is the completion for either spaces. Thus, this is the minimal completion of these spaces. 
        + Actually, are continuous functions uniformly integrable? 
    - (This means that the space is separable, that is, there exists a countable basis.) 


## Generalized Functions

* Intuition. 
    - This is a further completion of functions. 
        + These are so that there exists a complete space where derivatives exist. 
    - Explain the types of functions that are desired in this space. 
    - The unit-impulse distribution. 
    - Formally, these are sequences of function just as a real number is a sequence of rational numbers. 
    - Think of them as functionals. 
    - Somehow they form the smallest such space? 
    - How does the (Riesz) Representation Theorem fit in? 
* Definition. 
* Explain how to combine them with functions and with other distributions. (They form an algebra or whatever.)
* Definition of the integral of a generalized function.  
* Integrations laws hold. 


## Differentiation of Generalized Functions

* Intuition. 
    - Explain the physical situations where a generalization in differentiation makes sense. 
        + Impulse of a force. 
    - Explain that not all the derivatives introduce make physical sense. (This is already said in the Introduction, but it should be repeated.)
* Definition of the derivative of a generalized function. 
* Differentiation laws hold. 
* Generalized functions are infinitely differentiable. 


## Density 

*The (Riesz) Representation Theorem is is from the next chapter.*

* Generalized Density Theorem for Lebesgue content (Radon-Nikodym Theorem): 
    - I think that this can be proved with the (Riesz) Representation Theorem: I would just need to show that the new integral is bounded, which could be done by enforcing the assumption in the theorem. Then, the new measure is the integral of indicator function for each measurable set.
* Generalized Density Theorem for distributions?: 
    - Can this be proved in the same way? 


## (Results)

* The Trace Theorem. 
    - Should this be in *Linear Equations*? 
    - Functions can be whatever on the boundary, and this allows boundary conditions to make sense for differential equations. 
