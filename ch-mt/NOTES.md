# Notes for Generalizations of Integration

## Concerns

* I would like to just skip countability additive content. 
    - I still need to restrict to *measurable* functions. 
    - So, how do I do this? 
    - Really, I just need to get to Egorov. But, this requires a more general notion of convergence. 
    - Honestly, it is fine if I introduce countability additive content, but how do I motivate this? 


## Need to Understand

* Distributions: These are first introduced so that some differential equations have solutions. In the theme of the overview, it would make more sense to introduce the desired distributions and then complete the space instead of defining a complete space. Is this possible? What are the originally required distributions? Just the step functions and impulse functions? 


## Figures

* Urysohn's Lemma: 


## Material

* Relation of norms. 
    - I need to prove that the uniform norm, the integration norm and the square-integration norm are related and that they have equivalent convergence for functions in practice. Maybe prove that the same functions converge if they are (locally) Lipchitz continuous. 
    <!-- - There exists a sequence $(f_i)_{i≥1}$ and a function $f$ such that this sequence converges to $f$ in the space of integrable functions but does not converge to this function pointwise anywhere. (This is Exercise 2.12 from Stein and Shakarchi.) -->
    - I think that Morrey's inequality connects the uniform norm and the square-integration norm. 
* What about just skipping Lebesgue integration and going straight to distributions? 
* Explain how the these function spaces are thought of geometrically too. And, how this is an extension of the chapter Linear Equations right before. 
* Explain that this approach to Lebesgue integration shows that the completion still consists of functions. 
* Should I also have integration in infinite dimensions? I think that this is need for the Spectral Theorem. 
    - I guess that this is the idea. For a discrete spectrum, the spectrum theorem says that an operator can be written as a sum $A_1 + A_2 + \dots$, where each operator maps scales the characteristic space accordingly, but maps everything orthogonal to this to $0$. (These operators can be ordered by spectral/characteristic values.) Then, for a continuous spectrum, the operator can be written as an integral of such operators, where integration is over the scalar field according to the appropriate/spectral measure. 
* The (Riesz) Representation Theorem is really just an abstract version of the Density Theorem that is a bit more general.  
* Should I prove that the Lebesgue integral is the minimal generalization in the sense that Riemann integrable functions are dense? 
* How much does the idea of a countably additive content space help? The idea of a null set is all that is used to construct and prove results about the integral. 
* Not all functions are distributions. 


## Vocabulary

* Use a hyphen in the *Dominated-Convergence Theorem* or else it doesn't really make sense.


## General

* Is Haar measure to have measure for function spaces? 
    - Does this have any implications? 
    - Actually, I don't think that these spaces are locally compact
* One idea of a generalized function: A function is characterized by its values at points in its domain. But, it can also be characterized well – but not exactly – by its scalar product with the functions in a suitable class of test functions. (When the function is continuous, these are the same.) This latter characterization can be considered a *weak* characterization of functions. This latter definition can allow for more general objects.
* Maybe there are really two different types of weak solutions. Those for static problems and those for dynamic problems.
* The proof of the (Riesz) Representation Theorem is interesting in the sense that the orthogonal complement of the projection is only one dimension when $f$ is thought of as an integral. In such a case, the kernel could be the Fourier basis, and the one-dimensional orthogonal space is the space of constant functions. 
* The derivative of the unit impulse distribution picks out (the negative of) the derivative (instead of the value). 