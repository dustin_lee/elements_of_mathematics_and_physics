\documentclass[12pt]{amsart} \usepackage{amssymb} \usepackage{latexsym}
\usepackage[T1]{fontenc}
\usepackage[letterpaper,margin=.75in]{geometry} \renewcommand{\baselinestretch}{1.5} \setlength{\parskip}{1em}
\usepackage{mathrsfs} 
\usepackage{enumerate}  
\usepackage[pdftex]{graphicx} \usepackage{tikz-cd}

\newcommand{\bb}{\mathbb} \newcommand{\cal}{\mathcal} \newcommand{\scr}{\mathscr}  \newcommand{\abs}[1]{\left| #1 \right|} \newcommand{\infinity}{\infty}

\newcommand{\textand}{\hspace{15pt} \text{and} \hspace{15pt}} \newcommand{\textor}{\hspace{15pt} \text{or} \hspace{15pt}}

\DeclareMathOperator{\m}{m} \DeclareMathOperator{\diam}{diam} \renewcommand{\outer}{\text{out}} \newcommand{\inner}{\text{in}} \newcommand{\hahn}{_{\text{Hahn}}} \DeclareMathOperator{\cp}{cp}

\newcommand{\theorem}{\noindent \textbf{Theorem.\ }} \newcommand{\proposition}{\noindent \textbf{Proposition.\ }} \newcommand{\lemma}{\noindent \textbf{Lemma.\ }} \newcommand{\corollary}{\noindent \textbf{Corollary.\ }} \renewcommand{\proof}{\noindent \emph{Proof.\  }} \newcommand{\proofoflemma}{\noindent \emph{Proof of lemma.\ }} \newcommand{\remark}{\noindent \textbf{Remark.\ }} \newcommand{\definition}{\noindent \textbf{Definition.\ }} \newcommand{\note}{\noindent Note:\ } \newcommand{\stars}{\begin{center} * * * \end{center} }

\newcommand{\mat}{\left( \begin{smallmatrix}} \newcommand{\rix}{\end{smallmatrix} \right)} \renewcommand{\empty}{\varnothing} \renewcommand{\phi}{\varphi} \renewcommand{\epsilon}{\varepsilon} \newcommand{\<}{\langle} \renewcommand{\>}{\rangle} \newcommand{\gen}[1]{\langle #1 \rangle} \newcommand{\iso}{\simeq} \newcommand{\ceil}[1]{\lceil #1 \rceil} \newcommand{\floor}[1]{\lfloor #1 \rfloor} \newcommand{\set}[1]{\{ #1 \}} \newcommand{\bs}{\backslash} \newcommand{\tensor}{\otimes} \renewcommand{\subset}{\subseteq} \newcommand{\ssneq}{\subsetneq} \renewcommand{\bar}[1]{\overline{#1}} \newcommand{\inv}{^{-1}} \newcommand{\Union}{\bigcup} \newcommand{\union}{\cup} \newcommand{\Intersection}{\bigcap} \newcommand{\intersection}{\cap}

\begin{document}
\title{Measure}
\maketitle

Length, area and volume are ways the measure the size of subsets of euclidean space. The $n$-\emph{dimensional measure} of a rectangle $[a_1, b_1] \times \cdots \times [a_n, b_n]$ in the $n$-dimensional euclidean space is the product $\abs{b_1 - a_1} \cdots \abs{b_n - a_n}$. For example, the $1$-dimensional measure of a closed interval is the distance between its endpoints. It is reasonable that the measure of a rectangle with part of its boundary removed to be the measure of its closure. Furthermore, it is reasonable to allow rectangles to have sides of infinite length, such as the subsets $[0, 1] \times [0,\infinity )$ and $[0,1] \times (-\infinity , \infinity )$ in the euclidean plane, and defining their measure to be $\infinity$. The measure of the empty set is defined to be $0$. These more generalized rectangles, that is, the empty set and products of intervals that consist of more than one point, are called \emph{measurable rectangles}.  The measure of a finite disjoint union of measurable rectangles is defined to be the sum of the measures of each of these measurable rectangles. Since any finite union of measurable rectangles may be written as a finite disjoint union of measurable rectangles, any finite union of measurable rectangles is assigned a measure.  The measure of a finite union $E$ of measurable rectangles is denoted by $\m (E)$.



This notion of measure is now extended so that other subsets of euclidean spaces are assigned a measure, which is done by approximating sets by measurable  rectangles: Let $E$ be a subset of an euclidean space. An \emph{outer approximation} of $E$ is an at most countable set $\{ E_i \}_{i \in \cal I}$ of rectangles whose union contains $E$. An \emph{inner approximation} of $E$ is an at most countable set $\set{F_j}_{j \in \cal J}$ of disjoint rectangles that are contained in $E$. The \emph{outer measure} $\m_{\outer}$ of $E$ is 
\[
	\m_{\outer} (E) = \inf \set{\sum_{i \in \cal I} \m (E_i)},
\]
where the infimum is taken over all outer approximations $\set{E_i}_{i \in \cal I}$ of $E$. The \emph{inner measure} $\m_{\inner}$ of $E$ is 
\[
	\m_{\inner} (E) = \sup \set{\sum_{j \in \cal J} \m (F_j)},
\]
where the supremum is taken over all inner approximations $\set{F_j}_{j \in \cal J}$ of $E$.  The set $E$ is said to be \emph{Lebesgue measurable} if, for any positive real number $\epsilon$, there exists an outer approximation $\set{E_i}_{i \in \cal I}$ and an inner approximation $\set{F_j}_{j \in \cal J}$ of $E$ such that the outer measure of their difference $\Union_{i \in \cal I} E_i - \Union_{j \in \cal J}F_j$ is less than $\epsilon$.  One may then want to defined the measure of a Lebesgue measurable set to be the common value of the inner approximation and the outer approximation on it, but the proof that these values are the same is postponed until after the next theorem. Instead, the \emph{Lebesgue measure} of a Lebesgue measurable set is defined to be the value of the outer approximation on this set.  


A measurable rectangle is Lebesgue measurable because it is both an inner approximation and an outer approximation of itself. Furthermore, the outer measure agrees with the original measure on measurable rectangles: Let $E$ be a measurable rectangle. Since it covers itself, the outer measure of it is less than the original measure of it. For the reverse inequality, let $\epsilon$ be a positive real number and let $\{ E_i \}_{i \in \cal I}$ be an outer approximation of $E$ such that the sum $\sum_{i \in \cal I}m(E_i)$ is less than $\m_{\outer}(E) + \epsilon$. This outer approximation may be arranged into a sequence $(E_k)_{k \geq 1}$, and, by possibly replacing this sequence with the sequence $(E_k - \Union_{1 \leq \ell \leq k}E_{\ell})_{k \geq 1}$, it may be assumed that these sets are disjoint. Then,
\[
	\m(E) = \m(\Union_{j \in J}(E_i \intersection E)) = \sum_{i \in \cal I} \m(E_i \intersection E) \leq \sum_{i \in \cal I}\m(E_i) \leq \m_{\outer}(E) + \epsilon,
\]
which, by taking the limit as $\epsilon$ goes to $0$, proves that the original measure of $E$ is less than or equal to the outer measure of $E$. Because of this, the Lebesgue measure is denoted by $\m$ on Lebesgue measurable sets. Furthermore, the Lebesgue measure of a set is often called the measure, and a Lebesgue measurable set is often said to be measurable. 




The reason that countable sets are used in the definition of inner measure and outer measures is because it allows for more sets to be measurable, such the union of a sequence of squares of measures $1$, $\frac{1}{2}$, $\frac{1}{4}$, $\dots$, $\frac{1}{2^n}$, $\dots$. The reason that the outer measure is used to approximate the difference of the outer approximation and inner approximation is because it provides some sort of upper bound on what measure of this difference could be. One reason why the measurable sets are not just defined to be sets whose inner approximation is equal to its outer approximation is because such sets would not be closed under compliments. [May?]

The outer measure is that if $\set{E_i}_{i \in \cal I}$ is a countable set of subsets of euclidean space, then the inequality
\[
	\m_{\outer} (\Union_{i \in \cal I} E_i) \leq \sum_{i \in \cal I}\m_{\outer}(E_i)
\]
holds. Since the Lebesgue measure is the outer measure restricted to Lebesgue measurable sets, it also has this property.


\theorem The set of the Lebesgue measurable sets of an euclidean space is closed under countable unions, countable intersections and compliments, and if $\{ F_j \}_{j \in \cal J}$ is a countable set of disjoint Lebesgue measurable sets, then the equality
\[
	\sum_{j \in \cal J}\m(F_j) = \m(\Union_{j \in \cal J}F_j)
\]
holds. 

\proof  The Lebesgue measurable sets of an euclidean space are closed under complements because the compliments of an outer approximation and an inner approximation of a set is an inner approximation and an outer approximation of the compliments of this set, respectively. Because of this, to prove that they are closed countable intersections, it suffices to prove that they are closed under countable unions, which is now proven: Fix a positive real number $\epsilon$. Let $\{ E_i \}_{i \in \cal I}$ be a countable set of Lebesgue measurable sets. For each index $i$, let $\{ E_i^{(j)} \}_{j \in \cal J_{i}}$ be an outer approximation and let $\{ F_i^{(k)} \}_{k \in \cal K_i}$ be an inner approximation of $E_i$, respectively, such that the outer measure of their difference $\Union_{i \in \cal I_i}E_i^{(j)} - \Union_{k \in \cal K_{i}} F_{i}^{(k)}$ is less than $\epsilon_i$, where $\{ \epsilon_i \}_{i \in \cal I}$ is a set of positive real numbers such that their sum $\sum_{i \in \cal I}\epsilon_i$ is less than $\epsilon$. Then, the union of these inner approximations and the union of these outer approximations are an inner approximation and an outer approximation, respectively, such that  
\[
\begin{split}
	\m_{\outer}(\Union_{i \in \cal I}( \Union_{i \in \cal I_i}E_i^{(j)}) - \Union_{i \in \cal I}( \Union_{k \in \cal K_{i}} F_{i}^{(k)})) & \leq \m_{\outer}(\Union_{i \in \cal I}( \Union_{i \in \cal I_i}E_i^{(j)} - \Union_{k \in \cal K_{i}} F_{i}^{(k)})) \\
	&\leq \sum_{i \in \cal I}\m_{\outer} (\Union_{i \in \cal I_i}E_i^{(j)} - \Union_{k \in \cal K_{i}} F_{i}^{(k)}) \\
	 & \leq \sum_{i \in \cal I} \epsilon_i = \epsilon,
\end{split} 
\]
that is, the outer measure of their difference is less than $\epsilon$, which proves that the Lebesgue measurable sets are closed under countable unions. 

Now, the desired equality is proven: Suppose that these sets $\set{E_i}_{i \in \cal I}$ are disjoint. Then,
\[
	\m(\Union_{i \in \cal I} E_i) \leq \sum_{i \in \cal I;\ j \in \cal J}\m(E_i^{(j)}) \leq \sum_{i \in \cal I} (\m(E_i) + \epsilon_i) \leq (\sum_{i \in \cal I} \m(E_i)) + \epsilon, 
\]
Letting $\epsilon$ go to $0$ proves that the measure of the union $\Union_{i \in \cal I} E_i$ is less than the sum $\sum_{i \in \cal I} \m(E_i)$. Since this the reverse inequality also holds, this proves that the desired equality
\[
\sum_{i \in \cal I}\m(E_i) = \m(\Union_{i \in \cal I}E_i)
\]
holds. 

\qed



Now, it is proven that the inner measure agrees with the outer measure on measurable sets: Fix a positive real number $\epsilon$. It is enough to prove that they are $\epsilon$-near.  Let  $\{ E_i \}_{i \in \cal I}$ and $\set{F_j}_{j \in \cal J}$ be an outer approximation and an inner approximation of a measurable set, respectively, such that the outer measure of their difference $\Union_{i \in \cal I}E_i - \Union_{j \in \cal J}F_j$ is less than $\epsilon$. Since measurable sets are closed under countable unions, countable intersection and compliments, this difference $\Union_{i \in \cal I}E_i - \Union_{j \in \cal J}F_j$ is measurable and has measure less than $\epsilon$. Furthermore, the measure of the $\Union_{i \in \cal I}E_i$ is equal to the measure of $(\Union_{j \in \cal J}F_j) \union (\Union_{i \in \cal I}E_i - \Union_{j \in \cal J}F_j)$. Thus, the measure of $\Union_{i \in \cal I}E_i$ minus the measure of $\Union_{j \in \cal J}F_j$ is less than $\epsilon$.


The following theorem shows that several subsets of euclidean space, such as triangles and balls are measurable. 

\theorem The open sets and the closed sets of an euclidean space are measurable. 

\proof Since a closed set is the compliment of an open set, it suffices to only prove that open sets are measurable. Let $U$ be an open set of an euclidean space of dimension $m$. It suffices to prove that it is a countable union of rectangles. 


\noindent [Give a picture.] 

The following is an algorithm for picking a countable number of measurable rectangles whose union is $U$: Pick all of the rectangles of the form $[n_1, n_1 + 1] \times \cdots \times [n_m, n_m] + 1$, where $n_1$, $\dots$, $n_{m-1}$ and $n_m$ are integers, that are contained in $U$. Divides the rectangles of this form that intersect both $U$ and its compliment into $4$ closed cubes that intersect one another only on their boundaries. From these cubes pick the ones that are contained in $U$, and divide the ones that intersect with both the $U$ and its compliment. Continue in this way. 

\qed

The area of triangles is now calculated: Let $x$, $y$ and $z$ be points that divide a triangle. It may be assumed that the angle $\angle yzx$ is the largest angle of the triangle. Let $v$ be the projection of $z$ on to the line through $x$ and $y$. Then the area of this triangle is equal to the sum the area of the triangle $x$, $v$ and $z$ and the area of the triangle defined by $v$, $y$ and $z$. This latter two triangles are right. 
\begin{center}
\begin{tikzpicture}
\draw (0,0) node[anchor=north]{$x$}
  -- (4,0) node[anchor=north]{$y$}
  -- (1.5,3) node[anchor=south]{$z$}
  -- cycle;
\draw (1.5,0) -- (1.5, 3);
\draw[dashed] (0,0) -- (0,3);
\draw[dashed] (0,3) -- (4,3);
\draw[dashed] (4,3) -- (4,0);
\end{tikzpicture}
\end{center}
By reflecting a right triangle over its hypothenus, it is seen that its measure is half of the product of the length of its two sides that are not its hypothenus. Thus, the area of the triangle defined by $x$, $y$ and $z$ is 
\[
	\frac{1}{2} \m_1(\bar{xv})\m_1(\bar{zv}) + \frac{1}{2} \m_1(\bar{vy})\m_1(\bar{zv}),
\]
which is equal to $\frac{1}{2}\m_1(\bar{xy})\m_1(\bar{zv})$. 


The Lebesgue measure is invariant under translations and reflections because translations and reflections of a measurable rectangle is a measurable rectangle with the same measure. A scaling of an euclidean space by a factor of $a$ changes the measure of every Lebesgue measurable set by the factor $a^n$, where $n$ is the dimension of the space. Lebesgue measure is also invariant under rotations: The infimum used to defined the outer measure may be changed to be over all closed measurable rectangles. Similarly, the supremum used to define the inner measure may be change to be over all open measurable rectangles. Since the measure of balls is invariant under roations, to prove that the measure of any measurable set is invariant under rotations, it suffices to prove that every closed rectangle is a countable union of closed balls and every open rectangle is a countable union of open balls, which you may show. 


\stars

It is desirable to assign measure to subsets of an euclidean space that have a lower "dimension'' then the space. For example, curves and surfaces may have a Lebesgue measure of $0$ in the $3$-dimensional euclidean space, but one may want to assign length or area, respectively, to them. [Explain: Try to approximate with piecewise linear curves from both sides. Only can really do this for curves that curve. Can not do it in the $3$-dimension euclidean. Rectifiable curves.]

\noindent [Forward to outer measure.]

The $1$-\emph{dimensional outer measure} of a subset $E$ of an euclidean space to be 
\[
	\m_{1, \outer} (E) = \lim_{\delta \to 0} \inf \set{\sum_{i \in \cal I} \diam (E_i)},
\]
where $\delta$ denotes a positive real number and the infimum is taken over all countable sets $\set{E_i}_{i \in \cal I}$ of balls whose diameter is less than $\delta$ and whose union contains $E$. 

The reason why the limit as $\delta$ goes to $0$ is taken is to prevent balls from covering too much. For example, the union of the line between the points $(-1,0)$ and $(1,0)$ and the line between the points $(0,-1)$ and $(0,1)$ in the euclidean plane is expected to have a 1-dimensional measure of $4$, but they may be covered by the closed ball centered at the origin of radius $1$, which would mean that, without this limit, the outer measure of these lines is at most $2$. 

In general, if $n$ is a positive integer, then the $n$-\emph{dimensional outer measure} of a subset $E$ of an euclidean space is
\[
	\m_{n, \outer} (E) = \frac{\m (B(1))}{2^n} \lim_{\delta \to 0} \inf \set{\sum_{i \in \cal I} (\diam (E_i))^n}
\]
where $\delta$ denotes a positive real number, $\m(B(1))$ denotes the unit ball and the infimum is taken over all countable sets $\set{E_i}_{i \in \cal I}$ of balls whose diameter is less than $\delta$ and whose union contains $E$. 


The reason for raising the diameters by the power $n$ is because the Lebesgue measure of a ball is equal is proportional to its radius raised to the power of $n$. The reason for multiplying by $\frac{\m (B(1))}{2^n}$ is so that this measure will agree with the Lebesgue measure when $n$ is equal to the dimension of the space. 


The notation $\m_{n, \outer}^{\delta}(E)$ is used to denote
\[
	\inf \frac{\m (B(1))}{2^n} \set{\sum_{i \in \cal I} (\diam (E_i))^n},
\]
where the infimum is taken over all countable sets $\set{E_i}_{i \in \cal I}$ of balls whose diameter is less than $\delta$ and whose union contains $E$. Thus the $n$-dimension measure of a set $E$ is $\lim_{\delta \to 0} \m_{n, \outer}^{\delta}(E)$.




The following only depends on the properties of this outer measure. In general, an \emph{outer measure} $\m_{\outer}$ on a set is a function from the subsets of this set to the non-negative extended real numbers such that 
\begin{enumerate}[(i)]
\item $\m_{\outer} (\empty ) = 0$;
\item if a set $E$ in the domain of $\m_{\outer}$ is contained in another such set $F$, then $\m_{\outer} (E)$ is at most $\m_{\outer} (F)$;
\item if $\set{E_i}_{i \in \cal I}$ is a countable set of sets in the domain of $\m_{\outer}$, then the inequality
\[
	\m_{\outer} (\Union_{i \in \cal I} E_i) \leq \sum_{i \in \cal I}\m_{\outer}(E_i)
\]
holds.
\end{enumerate}
The outer measure defined for the  Lebesgue measure is an outer measure. Also, the n-dimensional outer measure just defined is an outer measure: The proof that it satisfies the first two conditions is straight-forward. To prove that it satisfies the third condition, let $\delta$ and $\epsilon$ be positive real numbers, let $\set{E_i}_{i \in \cal I}$ be a countable set of subsets of an euclidean space, and, for each index $i$ in $\cal I$, let $\set{F_j^{(i)}}_{j \in \cal J}$ be a set of subsets whose diameter is less than $\delta$ and whose union contains $E_i$ such that $\frac{B(1)}{2^n}\sum_{j \in \cal J} (\diam F_i^{(j)})^n$ is less than $\m_{n,\ \outer}^{\delta} + \epsilon_i$, where $\set{\epsilon}$ is a set of positive real numbers such that their sum $\sum_{i \in \cal I}$ is less than $\epsilon$. Then,
\[
\begin{split}
	\m_{n,\ \outer}^{\delta}(\Union_{i \in \cal I}(E_i) & \leq \frac{B(1)}{2^n} \sum_{i \in \cal I;\ j \in \cal J}(\diam F_i^{(j)})^n \\
	& \leq (\sum_{i \in \cal I} \m_{n,\ \outer}^{\delta}(E_i)) + \epsilon \\
	& \leq (\sum_{i \in \cal I}\m_{n,\ \outer}(E_i)) + \epsilon.
\end{split}
\]
Letting $\epsilon$ and then $\delta$ go to $0$ proves the desired inequality. 

A set $E$ is the domain of an outer measure $\m_{\outer}$ is said to be \emph{Carath\' eodory measurable} with respect to this outer measure if, for any other set $F$ in the domain of this outer measure, the equality
\[
	\m_{\outer}(F) = \m_{\outer}(F \intersection E) + \m_{\outer}(F-E)
\]
holds. Since the inequality $\m_{\outer}(F) \leq \m_{\outer}(F \intersection E) + \m_{\outer}(F-E)$ always holds, to prove that a set is Carath\' eodory measurable, it suffices to prove the inequality $\m_{\outer}(F) \geq \m_{\outer}(F \intersection E) + \m_{\outer}(F-E)$. The $n$-dimensional outer measure restricted to Carath\' eodory measurable sets is called the $n$-\emph{dimensional measure} and is denoted by $\m_{n}$.

The compliment of an outer approximation of the compliment of a set is an inner approximation. Thus, a subset of the real numbers is Lebesgue measurable exactly when it is Cara\' eodory measurable. [Expand on the motivation.] 

\theorem The set of the Carath\' eodory measurable sets with respect to an outer measure $\m_{\outer}$ is closed under countable unions, countable intersections and compliments, and if $\set{E_i}_{i \in \cal I}$ is an at most countable set of disjoint Carath\' eodory measurable sets, the the equality 
\[
	\sum_{i \in \cal I}\m_{\outer} (E_i) = \m_{\outer}(\Union_{i \in \cal I}E_i)
\]
holds. 

\proof The Carath\' eodory measurable sets are closed under compliments. Thus, to prove that they are closed under countable intersections, it suffices to prove that they are closed under countable unions. First, it is proven that they are closed under finite unions and that the desired equality holds when the sum is finite: Let $E_1$ and $E_2$ be two Carath\' eodory measurable sets, and let $F$ be any other set in the domain of this outer measure. Then,
\[
	\m_{\outer}(F) = \m_{\outer} (F \intersection E) + \m_{\outer} (F - E)
\]
\[
	\m_{\outer} (F \intersection E_1 \intersection E_2 + \m_{\outer} ((F \intersection E_1) - E_2) + \m_{\outer} ((F - E_1) \intersection E_2) + \m_{\outer} ((F - E_1) - E_2)
\]
\[
	\m_{\outer} (F \intersection (E_1 \union E_2)) + \m_{\outer} (F - (E_1 \union E_2)).
\]
Since the inequality $\m_{\outer} (F \intersection (E_1 \union E_2)) + \m_{\outer} (F - (E_1 \union E_2)) \leq \m_{\outer}(F)$ also holds, the union of $E_1$ and $E_2$ is Carath\'e odory measurable. If $E_1$ and $E_2$ are disjoint, then
\[
	\m_{\outer}(E_1 \union E_2) = \m_{\outer}((E_1 \union E_2) \intersection E_1) + \m_{\outer}((E_1 \union E_2) - E_1) = \m_{\outer}(E_1) + \m_{\outer}(E_2),
\]
that is, the desired equality holds for finite sums. 


Now, it is proven that they are closed under countable unions and that the desired equality holds: Let $(E_k)_{k \geq 1}$ be a sequence of Carath\' eodory measurable sets. It may be assumed that these sets are disjoint: This sequence may be replaced by the sequence $(E_k - \Union_{1 \leq \ell < k} E{_\ell})_{k \geq 1}$. For any set $F$ in the domain of the outer measure and for any positive integer $k$, 
\[
\begin{split} 
	\m_{\outer}(F)  &= \m_{\outer}(F \intersection \Union_{1 \leq \ell \leq k}E_{\ell}) + \m_{\outer}(F - \Union_{1 \leq \ell \leq k}E_{\ell}) \\
	&\geq (\sum_{1 \leq \ell \leq k}\m_{\outer}(F \intersection E_k)) + \m_{\outer}(F - \Union_{1 \leq \ell \leq k}E_{\ell}) \\
	& \geq (\sum_{1 \leq \ell \leq k} \m_{\outer}(F \intersection E_{\ell})) + \m_{\outer}(F - \Union_{\ell \geq 1}E_\ell.
\end{split}
\]
Letting $k$ go to infinity proves that the Carath\' eodory measurable sets are closed under countable unions. Choosing $F$ to be the union $\Union_{k \geq 1}E_k$ proves the desired equality. 

\qed

The $1$-dimensional, $2$-dimensional and $3$-dimensional measures of a Carath\' edory measurable set are called the length, area and volume, respectively. The following proposition expresses the idea that the area of a line segment is $0$ and the length of a disc is infinite. 

\proposition Let $n$ and $m$ be two positive integers such that $n$ is greater than $m$. If the $m$-dimensional outer measure of a subset of euclidean space is finite, then the $n$-dimensional outer measure of this set is $0$. If instead the $n$-dimensional outer measure of this subset is greater than $0$, then the $m$-dimensional outer measure of this set is infinite. 

\proof Since the second statement of this proposition is the contrapositive of the first statement, it suffices to prove the statement. Let $E$ be a subset of an euclidean space and let $\delta$ be a positive real number. Since every subset $F$ whose diameter is less than $\delta$ of an euclidean space is such that 
\[
	(\diam (F))^n = (\diam (F))^{n-m} (\diam (F))^m \leq \delta^{n-m} (\diam (F))^m,
\]
\[
	\m_{n, \outer}^{\delta} (E) \leq \delta^{n-m} \m_{n, \outer}^{\delta}(E) \leq \delta^{n-m}m_{m, \outer}(E).
\]
If the $m$-dimensional outer measure of $E$ is finite, then taking the limit as $\delta$ goes to $0$ proves that the $n$-dimensional outer measure of $E$ is $0$. 

\qed

\stars

\noindent [The measure of balls and their boundaries.] 

\stars

The following definition gives a general notion of measure, which will be needed later. 


\definition A \emph{measure space} is a set $X$ together with a set $\scr M$ of subsets of this set that contains the whole set $X$ and the empty set $\empty$, and a function $\m$ from this set of subsets to the non-negative extended real numbers such that 
\begin{enumerate}[(i)] 
\item $\m(\empty ) = 0$;
\item the set $\scr M$ is closed under countable unions, countable intersections and complements;
\item if $\{ E_i \}_{i \in \cal I}$ is a countable family of disjoint sets in $\scr M$, then 
\[
	\sum_{i \in \cal I}\m(E_i) = \m(\Union_{i \in \cal I}E_i). 
\]
\end{enumerate}
In such a case, the subsets in $\scr M$ are called \emph{measurable} sets, the function $\m$ is called a \emph{measure} or \emph{measure function}, and the value of $\m$ at a subset in $\scr M$ is called the \emph{measure} of this subset.

If the condition that the set of measurable sets $\scr M$ is closed under countable unions and countable intersections is replaced with being closed under finite unions and finite intersections in this definition and if the equality in the third condition holds only when the union is measurable, then such a space is called a \emph{premeasure space}, the function $\m$ is called a \emph{premeasure} and the sets in $\scr M$ are still said to be \emph{measurable}. An \emph{extension} of a premeasure to a measure is a measure on the underlying set of the premeasure space whose measurable sets include the measurable sets of the premeasure and agrees with the premeasure on these sets. The proof that allowed the measure of finite unions of measurable rectangles to be extended to the Lebesgue measure also proves that a premeasure may be extended to a measure, and this extension is called the \emph{Lebesgue extension}. The outer measure used to prove this extension may also be used to extend this premeasure to a measure by how the outer measure was used to define lower dimensional measures, and this extension is called the \emph{Carath\' eodory extension}. 




The Lebesgue extension and the Carath\' eodory extension do not always agree. For example, the real numbers may be made a premeasure space by defining its only measurable sets to be the entire set and the empty set, and defining the measure on these sets to be $\infinity$ and $0$, respectively. The Lebesgue measurable sets of this space are only the whole space and the empty set, while every non-empty subset of this set is Carath\' eodory measurable with a measure of $\infinity$. The next proposition gives uniqueness of the an extension of a premeasure when the space is \emph{sigma-finite}, that is, when it is a countable union of sets of finite measure. The sigma-finite condition is used to extend properties of sets of finite measure to the whole space. Euclidean spaces are sigma-finite.

\proposition The Lebesgue extension of a sigma-finite premeasure agrees with any extension of this premeasure on the Lebesgue measurable sets. 

\proof Let $X$ be a sigma-finite premeasure space, whose premeasure is $\m$. Denote by $\m$ the Lebesgue extension and by $\m '$ any extension of this premeasure. 

Since the space is sigma-finite, it is enough to prove that these two extensions agree on each Carath\'eodory measurable set $E$ of finite measure under the Lebesgue extension and that is measurable by $\m '$. Fix a positive real number $\epsilon$. Pick an outer approximation $\set{E_i}_{i \in \cal I}$ of $E$ by the premeasure so that $\m(\Union_{i \in \cal I}E_i)$ is less than $\m(E) + \epsilon$. Then,
\[
\m '(E) \leq \m '(\Union_{i \in \cal I}E_i) = \m(\Union_{i \in \cal I}E_i) \leq \m(E) + \epsilon .
\]
Thus, $\m '(E)$ is at most $\m(E)$. Furthermore, 
\[
\begin{split}
	\m(E) & \leq \m(\Union_{i \in \cal I}E_i) = \m '(\Union_{i \in \cal I}E_i) \\
	& \leq \m '(\Union_{i \in \cal I}E_i - E) + \m '(E) \\
	& \leq \m(\Union_{i \in \cal I}E_i - E) + \m '(E) \\
	& \leq \epsilon + \m '(E),
\end{split}
\]
which gives the reverse inequality.

\qed


This proves that extension of the premeasure space of the real numbers whose measurable sets are disjoint unions of closed measurable rectangles gives the normal measure on the real numbers, which makes it clearer that the measure of a measurable rectangle does not depend on its boundary. 



\stars 
\stars 
\stars


\noindent \textbf{Possible Material:}
\begin{enumerate}[(i)]
\item A technical point, which you may prove, will be needed: If $\m$ denotes the Lebesgue premeasure and $\set{E_i}_{i \in \cal I}$ is a countable set of disjoint sets such that this Lebesgue premeasure has values on them and on their union $\Union_{i \in \cal I}E_i$, then 
\[
	\m (\Union_{i \in \cal I}E_i) = \sum_{i \in \cal I}\m(E_i).
\]
This is equivalent to the condition that if $(F_k)_{k \geq 1}$ is a descending chain of sets such that this Lebesgue premeasure $\m$ has values on them and has the value 0 on their intersection $\Intersection_{k \geq 1}F_k$, then the equality
\[
	\lim_{k \to \infinity}\m(F_k) = 0
\]
holds. 
\item The length of a simple curve $\gamma$ from an interval $[a,b]$ to an euclidean space should not be larger than any sum of the form
\[
	\sum_{1 \leq k \leq n-1}\abs{\gamma (t_{k + 1}) - \gamma (t_k)},
\]
where $(t_k)_{1 \leq k \leq n}$ $t_1$ such that equals $a$, $t_n$ equals $b$ and $t_k$ is less than $t_{k + 1}$ for every index $k$.

A curve $\gamma$ is said to be \emph{rectifiable} if the supremum 
\[
	\sup \sum_{1 \leq k \leq n-1}\abs{\gamma (t_{k + 1}) - \gamma (t_k)}
\]
is finite, where this supremum is taken over all sequences $(t_k)_{1 \leq k \leq n}$ such that equals $a$, $t_n$ equals $b$ and $t_k$ is less than $t_{k + 1}$ for every index $k$. [There are simple curves that are not rectifiable, such as space-filling curves.] 
\item Intuitively, the length of a curve is how long it will if it is straighten out similarly to a rope being straightened out. 
\end{enumerate}

\noindent \textbf{Notes:}
\begin{enumerate}[(i)]
\item Talk about Peano-Jordan measure, where a set is \emph{Peano-Jordan measurable} if the inner measures and outer measures agree, where the outer approximations and inner approximations are defined to only be for finite (rather than countable) unions. The problems with this are sets of measure zero such as the Rational Numbers and not all open sets are measurable. 
\end{enumerate}

\noindent \textbf{Stuff for the Introduction:}
\begin{enumerate}[(i)]
\item As an exercise, show that there exists an open dense subset of the interval $[0,1]$ of measure less a given positive real number. 
\end{enumerate}










\end{document}