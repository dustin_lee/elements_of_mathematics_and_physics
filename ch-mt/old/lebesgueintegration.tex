\documentclass[12pt]{amsart} \usepackage{amssymb} \usepackage{latexsym}
\usepackage[T1]{fontenc}
\usepackage[letterpaper,margin=.75in]{geometry} \renewcommand{\baselinestretch}{1.5} \setlength{\parskip}{1em}
\usepackage{mathrsfs} 
\usepackage{enumerate} \usepackage[pdftex]{graphicx} \usepackage{tikz-cd}

\newcommand{\bb}{\mathbb} \newcommand{\cal}{\mathcal} \newcommand{\scr}{\mathscr}  \newcommand{\abs}[1]{\left| #1 \right|} \newcommand{\infinity}{\infty}

\newcommand{\textand}{\hspace{15pt} \text{and} \hspace{15pt}} \newcommand{\textor}{\hspace{15pt} \text{or} \hspace{15pt}}
\DeclareMathOperator{\m}{m} \renewcommand{\outer}{_{\text{out}}} \newcommand{\inner}{_\text{in}} \DeclareMathOperator{\cp}{cp}

\newcommand{\theorem}{\noindent \textbf{Theorem.\ }} \newcommand{\proposition}{\noindent \textbf{Proposition.\ }} \newcommand{\lemma}{\noindent \textbf{Lemma.\ }} \newcommand{\corollary}{\noindent \textbf{Corollary.\ }} \renewcommand{\proof}{\noindent \emph{Proof.\  }} \newcommand{\proofoflemma}{\noindent \emph{Proof of lemma.\ }} \newcommand{\remark}{\noindent \textbf{Remark.\ }} \newcommand{\definition}{\noindent \textbf{Definition.\ }} \newcommand{\note}{\noindent Note:\ } \newcommand{\stars}{\begin{center} * * * \end{center} }

\newcommand{\mat}{\left( \begin{smallmatrix}} \newcommand{\rix}{\end{smallmatrix} \right)} \renewcommand{\empty}{\varnothing} \renewcommand{\phi}{\varphi} \renewcommand{\epsilon}{\varepsilon} \newcommand{\<}{\langle} \renewcommand{\>}{\rangle} \newcommand{\gen}[1]{\langle #1 \rangle} \newcommand{\iso}{\simeq} \newcommand{\ceil}[1]{\lceil #1 \rceil} \newcommand{\floor}[1]{\lfloor #1 \rfloor} \newcommand{\bs}{\backslash} \newcommand{\tensor}{\otimes} \renewcommand{\subset}{\subseteq} \newcommand{\ssneq}{\subsetneq} \renewcommand{\bar}[1]{\overline{#1}} \newcommand{\inv}{^{-1}} \newcommand{\Union}{\bigcup} \newcommand{\union}{\cup} \newcommand{\Intersection}{\bigcap} \newcommand{\intersection}{\cap}

\begin{document}
\title{Integration}
\maketitle

\note Give an introduction. 

\proposition If $(\phi_k)_{k \geq 1}$ is a sequence of measurable peicewise constant functions from a measure space to an euclidean space that are bounded by a common bound, are supported in a common set of finite measure and converge pointwise to a function almost everywhere, then the sequence $(\int \phi_k)_{k \geq 1}$ of the integrals of these functions converges and the limit of this sequence is the same if the sequence $(\phi_k)_{k \geq 1}$ is replaced by another such sequence that converges pointwise to the same function. 

\proof Let $f$ be a function that this sequence $(\phi_{k})_{k \geq 1}$ converges pointwise to almost everywhere and let $E$ be a set of finite measure such that the functions $(\phi_{k})_{k \geq 1}$ are supported in it, and let $\m$ denote the measure function of the domain of these functions. It may be assumed that this sequence converges pointwise to $f$. 

First, the existence of of the limit of the sequence $(\int \phi_k)_{k \geq 1}$ is shown: Let $\epsilon$ be a postitive real number. Suppose that this sequence $(\phi_{k})_{k \geq 1}$ converges uniformly to $f$, that is, there exists positive integers $n$ and $m$ such that the values of $\phi_{n}$ and $\phi_{n}$ are $\epsilon$-near at every point in the domain. Then 
\[
	\abs{\int \phi_n - \int \phi_{n}} = \abs{\int (\phi_n - \phi_m)} \leq \int \abs{\phi_{n}-\phi_{m}} < \epsilon \m (E),
\]
that is, the sequence $(\int \phi_k)_{k \geq 1}$ is Cauchy and converges. The triangle inequality here follows from these integrals being finite sums.
The next lemma says that the sequence $(\phi_{k})_{k \geq 1}$ converges uniformly to $f$ except on a set $F$ of measure less than $\epsilon$. This means that if $n$ and $m$ are sufficiently large, then 
\[
	\abs{\int \phi_n - \int \phi_{m}} \leq \int \abs{\phi_{n}-\phi_{m}} = \int_{E-F} \abs{\phi_{n}-\phi_{m}} + \int_{F} \abs{\phi_{k}-\phi_{\ell}} < \epsilon m(E) + 2 \epsilon  B,
\]
 where $B$ is a common bound of the functions $(\phi_k)_{k \geq 1}$, that is, the sequence $(\int \phi_{k})_{k \geq 1}$ is Cauchy and converges. 

\lemma (\emph{Egorov's Theorem}) If a sequence of measurable functions from a measure space to an euclidean space are supported in a common set of finite measure and converge pointwise, then, for any positive real number, there exists a subset of their domain of measure less than this real number such that this sequence of functions converges uniformly on the complement of this set. 

\proofoflemma Let $(f_k)_{k \geq 1}$ be a sequence of measurable functions from a measure space to an euclidean space that are supported in a common set $F$ of finite measure and converge pointwise to a function $f$. Let $\epsilon$ be a positive real number. If it is shown that, for any positive real numbers $\delta$ and $\eta$, there exists a set of measure less than $\delta$ such that, if $k$ is sufficiently large, the values of $f_k$ and $f$ are $\eta$-near at every point outside of this set, then, by doing this for a sequence $(\eta_{\ell})_{\ell \geq 1}$ of positive real numbers that converges to $0$ and a sequence $(\delta_{\ell})_{\ell \geq 1}$ of positive real numbers such that their sum $\sum_{\ell \geq 1}\delta_{\ell}$ is less than $\epsilon$, then the union of these sets is a set that satisfies the proposition.

Let $\delta$ and $\eta$ be positive real numbers. For each positive integer $n$, define the set $F_n$ to consists of each point in $E$ such that the values of $f$ and $f_k$ are $\eta$-near at this points for every $k$ that is greater than $n$. Each set $F_n$ is measurable because the function $\abs{f_k - f}$ is measurable for each $k$, the pre-image of the interval $[0, \eta]$ of real numbers of this function is measurable, and the set $F_n$ is the intersection of such pre-images. [Clarify.] Since each $F_n$ is contained in $E_{n+1}$, the union of these sets is the set $E$, which implies that there exists an integer $n$ such that the complement of $F_n$ in $E$ is less than $\delta$. This concludes the proof of the lemma.

The uniqueness now shown: Let $(\psi_{k})_{k \geq 1}$ be another sequence of measurable peicewise constant functions that are bounded by a common bound, are supported in a common set of finite measure, which may be assumed to be $E$, and converge pointwise to the function $f$. Then, the sequence $(\phi_k - \psi_k)_{k \geq 1}$ of measurable peicewise constant functions converges pointwise to the function that is identically $0$. Let $\epsilon$ be a positive real number. By Egorov's theorem, there exists a set $F$ of measure less than $\epsilon$ such that the sequence $(\phi_k - \psi_k)_{k \geq 1}$ converges uniformly outside of $F$. Thus, if $n$ is sufficiently large, then
\[
	\abs{\int \phi_n - \int \psi_{n}} \leq \int \abs{\phi_{n}-\psi_{n}} < \epsilon m(E) + \epsilon B',
\]
where $B'$ is a common bound of the functions $(\phi_{k}-\psi_k)_{k \geq 1}$. This shows that the limits of the sequences $(\int \phi_{k})_{k \geq 1}$ and $(\int \psi_{k})_{k \geq 1}$ are equal.

\qed

The \emph{integral} of a bounded measurable function from a measure space to an euclidean space that is supported in a set of finite measure is defined to be the limit of the integrals of any sequence of measurable peicewise constant functions that converge pointwise almost everywhere to this function, are bounded by a common bound and are supported in a common set of finite measure. The integral of such a function $f$ is denoted by 
\[
	\int f \hspace{15pt} \text{or} \hspace{15pt} \int_{X}f,
\]
where $X$ is the domain of $f$. 

Now that the integrals of more functions have been defined, there should not be a reason for measurable peicewise constant functions to be any better at approximating integrals than these more general integrable functions. This is expressed in the following proposition, which basically says that limits and integrals may be exchanged.

\proposition (\emph{The Bounded Convergence Theorem})If $(f_k)_{k \geq 1}$ is a sequence of measurable functions from a measure space to an euclidean space that are bounded by a common bound, are supported in a common set of finite measure and converge pointwise to a function $f$ almost everywhere, then this function $f$ is measurable and the equality 
\[
	\lim_{k \to \infinity} \int f_{k} = \int f
\]
holds. 

\proof If it is shown that such a function $f$ is measurable, then the last part of the last proposition may be reused to prove the desired equality. [Clarify.] For each $k$, let $(\phi_{k}^{(\ell )})_{\ell \geq 1}$ be a sequence of measurable piecewise constant functions that converge pointwise to $f_k$ almost everywhere. Let $(\epsilon_{k})_{k \geq 1}$ be a sequence of positive real numbers that converge to 0. By Egorov's theorem, for each $k$, there exists a positive integer $\ell_{k}$ such that the values of $\phi_{k}^{(\ell_{k})}$ and $f_k$ are $\epsilon_{k}$-near outside a set of measure less than $\epsilon_k$. Then, the sequence $(\phi_{k}^{(\ell_k)})_{k \geq 1}$ converges pointwise to $f$ almost everywhere.

\qed

Now, integration of more general functions is defined, that is, for some functions that are possibly unbounded or not supported in a set of finite measure. This is done by piecing together the integrals in the setting that was already looked at. A function $f$ from a measure space to an euclidean space is said to be \emph{integrable} if, for any partition $\{ E_i \}_{i \in \cal I}$ of its domain into sets of finite non-zero measure such that $f$ is bounded on each of theses sets individually, the sum 
\[
	\sum_{i \in \cal I} \int_{E_i} f
\]
converges absolutely and is less than $\infinity$. If such a function $f$ is integrable, then the value of this sum is invariant under the chosen partition because, if $\{ E_i \}_{i \in \cal I}$ and $\{ F_j \}_{j \in \cal J}$ are two such paritions, then $\{ E_i \intersection F_j \}_{i \in \cal I ; j \in \cal J}$ is such a partition and
\[
	\sum_{i \in \cal I}\int_{E_i}f = \sum_{i \in \cal I}(\sum_{j \in \cal J}\int_{E_i \intersection E_j}f) = \sum_{i \in \cal I; j \in \cal J}\int_{E_i \intersection E_j} f = \sum_{j \in \cal J}(\sum_{i \in \cal I} \int_{E_i \intersection E_j} f) =\sum_{j \in \cal J}\int_{E_j}f
\]
by Fubini's theorem. [Clarify these first two equalities.] The value of such a sum $\sum_{i \in \cal I}\int_{E_i}f$ is called the \emph{integral} of $f$ and is denoted by $\int f$. The restriction of an integrable function $f$ to a measurable set $E$ is integrable and its integral is denoted by $\int_{E}f$. 

Since at most a countable number of terms of an absolutely convergent sum are non-zero, the support of an integrable function is sigma-finite. This show that an integrable function is measurable, and that questions about integrals over general measure spaces are likely to be reduced to the case of measure spaces of finite measure. 

An absolutely convergent sum is invariant under every permutation of the order of its terms. Since a sum that is invariant under every permutation of the order of its terms is an integral on the non-negative integers with the \emph{counting measure}, that is, the measure that assigns the measure $1$ to every integer, the next theorem generalizes this result to integrals. 

\theorem If a function from a measure space to an euclidean space is measurable and its absolute value is integrable, then it is integrable. Conversely, if a function from a measure space to an euclidean space is integrable, then its absolute value is integrable.
 
\proof Let $f$ be a function from a measure space to an euclidean space. First, suppose that $f$ is measurable and its absolute value $\abs{f}$ is integrable. Then, for any partition $\{ E_i \}_{i \in \cal I}$ of the domain of $f$ into measurable sets of finite non-zero measure such that $f$ is bounded on each one individually, the integral $\int_{E_i}f$ is defined for each index $i$, and the sum $\sum_{i \in \cal I}\int_{E_i}f$ is absolutely convergent because  the sum $\sum_{i \in \cal I} \abs{\int_{E_i}f}$ is less than or equal to the sum $\sum_{i \in \cal I} \int_{E_i}\abs{f}$, which is finite. This shows that $f$ is integrable. 

Now, suppose that $f$ is integrable. Let $\epsilon$ be a positive real number and a countable set $\{ \epsilon_i \}_{i \in \cal I}$ of positive real numbers such that their sum $\sum_{i \in \cal I}\epsilon_{i}$ is less than $\epsilon$. To show that the absolute value of $f$ is integrable, it suffice to show that there exists a countable partition $\{ E_i \}_{i \in \cal I}$ of the support of $f$ such that $\int_{E_i}\abs{f}$ is less than or equal to $\abs{\int_{E_i}f} + \epsilon_{i}$ for every index $i$, because this implies that the sum $\sum_{i \in \cal I}\int_{E_i}\abs{f}$ converges. Since $f$ is integrable, there exists a partition $\{ E_i \}_{i \in \cal I}$ of the support of $f$ into sets of finite non-zero measure such that $f$ is bounded on each one individually. For each index $i$, let $\delta_i$ be a positive real number, let $\phi_i$ be a measurable peicewise constant function that is $\delta_i$-near $f$ at every point in $E_i$ except a subset $E'_i$ of measure less than $\delta_i$, and let $\{ E_i^{(j)} \}_{j \in \cal J}$ be a finite partition of $F_i$ such that $\phi_i$ is constant on each set in this partition. Then, for each index $i$ [clarify the first step] 
\[
\begin{split}
	\sum_{j \in \cal J}\abs{\int_{E_i^{(j)}}f} & \leq \sum_{j \in \cal J'}\abs{\int_{E_i^{(j)}}\phi} + \delta_i \m (E) + B \m (E') \\	
	& = \sum_{j \in \cal J}\int_{E_i^{(j)}}\abs{\phi} + \delta_i \m (E) + B \m (E') \\
	& \leq \sum_{j \in \cal J}\int_{E_i^{(j)}}\abs{f} + 2 \delta_i \m (E) + 2 B \delta_i,
\end{split}
\]
where $B$ is a common bound of $\phi_i$ and $f$ on $F_i$, and $\m$ is the measure function of the domain. Choosing $\delta_i$ so small that the sum $\sum_{j \in \cal J} 2 \delta_i \m (E) + 2 B \delta_i$ is less than $\epsilon_i$ for each index $i$ concludes the proof. 

\qed


Although integration of a function was defined by piecing together approximations of restrictions of this function, the next theorem says that such an approximation may happen globally. 

\theorem (\emph{The Dominated Convergence Theorem}) If $(f_k)_{k \geq 1}$ is a sequence of non-negative integrable functions from a measure space to the real numbers that are dominated by a common integrable function and converge pointwise to a function $f$, then this function $f$ is integrable and the equality
\[
	\lim_{k \to \infinity} \int f_k = \int f
\]
holds. 

\proof Let $\epsilon$ be a positive real number. Let $g$ be an integrable function that dominates each function in the sequence $(f_k)_{k \geq 1}$. Let $\{ E_i \}_{i \in \cal I}$ be a partition of the domain of $g$ into non-zero measurable sets such that $g$ is bounded on each one individually.


Since the sum $\sum_{i \in \cal I}\int_{E_i} f$ is less than the sum $\sum_{i \in \cal I}\int_{E_i} g$, it converges and there exists a finite subset $\cal I'$ of $\cal I$ such that the finite sum $\sum_{i \in \cal I'} \int_{E_i}f$ is $\epsilon$-near the sum $\sum_{i \in \cal I}\int_{E_i} f$. Denote by $E$ the union $\Union_{i \in \cal I'}E_i$. By the Bounded Convergence Theorem, the integrals $\int_E f_k$ and $\int_E f$ are $\epsilon$-near for sufficiently large $k$. Thus, 
\[
\begin{split}
	\abs{\int f - \int f_k} & = \abs{ \int_E f + \int_{\cp (E)} f - \int_E f_k - \int_{\cp (E)} f_k} \\
	& \leq \abs{\int_E f - \int_E f_k} + \abs{\int_{\cp (E)} f - \int_{\cp (E)} f_k} \\
	& \leq \epsilon + \int_{\cp (E)}\abs{f - f_k} \\
	& \leq \epsilon + \m(\cp (E)) \int_{\cp (E)}2g \\
	& \leq \epsilon + 2 \epsilon \int_{\cp (E)}g
\end{split}
\]
for sufficiently large $k$.

\qed


\stars

Everything in this chapter holds if euclidean spaces are replaced with Banach spaces. 

\end{document}