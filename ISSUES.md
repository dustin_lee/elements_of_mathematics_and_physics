# Issues 

> "Errors once discovered are more than half mended."  
–– George Washington


* Newton's Method:
    - Double zero: I do not know a proof (or even a good statement) for the convergence of the method when there is a double zero. 
    - Multivariate version: I skipped this. It requires the multivariate version of Taylor's Theorem. I plan to come back when I do the chapter on optimization. 
* Equivalent definitions of integration:
    - Partition independence: Finish proof. 
    - Partitions up to singletons: How does this fit in? 
* More details on how to FTC gives the integral equation
* Norm-Equivalence Theorem: I do not like the statement, but I do not know how to prove it more generally. 
* I am not being exactly honest with orders of convergence. I am just showing that they are at least something. Should I give examples of worst case scenario? (For most of these methods, the worst case is the common case. Actually, this may be a strength. Should this be talked about?)