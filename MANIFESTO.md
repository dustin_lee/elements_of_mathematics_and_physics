# Manifesto of the Elements of Mathematics and Physics

> "One can measure the importance of a scientific work  
> by the number of earlier publications rendered superfluous by it."  
> –– David Hilbert


## Introduction

> "Individual scientific facts are the leaves and twigs of a great tree.  
> They must be connected downwards, into smaller and larger branches,  
> into the limbs, and then into the trunk itself.  
> To visualize the tree, we must see the connexions.  
> At each major fork, we need to comprehend in sufficient detail all that is borne above it.  
> But a unified picture can only be made by one person comprehending the whole scene."  
> –– John Ziman

Human understanding of mathematics and physics has accumulated over thousands of years from the deep thought of thousands and the work of millions. 
There is a lot to learn. 
And, it is hard. 
The lack of good books does not make it any easier.

The purpose of the *Elements of Mathematics and Physics* is to make it easier to learn mainstream mathematics and physics. 
It is coherent exposition that provides a consistent and simple viewpoint of core of mathematics and physics from set theory to general relativity and quantum electrodynamics. 

It is pedagogical in the sense that there is motivation and intuition for the all material. 
By *motivation*, it is meant that the material provides foundational understanding or has an end in engineering. 

It strives to be linguistically ideal. 
It aims towards clarity, simplicity and conciseness. 
This is focused on as much as the material itself. 

The intended audience is upper-level undergraduates to professors and professional engineers. 
The reader is only expected to be familiar with introductory proof-based real analysis and introductory calculus-based physics. 


## The Current Situation

> "It is difficult for us who have been socialized into the peculiar culture of university faculty  
> to recognize how esoteric we have allowed university mathematics 
in particular to become.  
> We imagine (incorrectly) that dominance of the abstract over the concrete,  
> absence of ties to applications,  
> and an emphasis on rigor over fluency of use  
> are inherent in the discipline."  
> –– David S. Moore 

The current scientific literature is flawed from unnecessary abstraction, downplay of numerical methods, lack of motivation and intuition and poor writing.

**Unnecessary Abstraction**:
Modern mathematics has become [bourbakized](https://en.wikipedia.org/wiki/Nicolas_Bourbaki) into abstract nonsense. 
Students are encouraged to learn the esoteric instead of the mainstream, the unpractical instead of the practical, the abstract instead of the concrete; such as differential geometry instead of Euclid, or Galois Theory instead of the Finite-Difference Method.
The excessive abstraction of modern mathematics pushes physicists and engineer away from rigour and toward hand-waving arguments, which, in turn, pushes mathematicians away from physics and engineering. 

**Downplay of Numerical Methods**:
Much of academia is computationally behind by several decades. 
The curricula is intrenched in symbolic methods from academics teaching academics in almost entirely isolated from industry engineering. 
For example, in courses on differential equations, less useful symbolic method are almost exclusively taught instead of the more useful numerical methods. 

**Spaghetti Education**:
Current degree programs do not provide a coherent pedagogical understanding of mathematics and physics. 
Modern results are pushed over useful results. 
Then, to gloss over this jump, motivation is given in form of phrases such as *This has applications in physics* or *This is important* rather than building on more elementary theory or pointing to actual applications. 
Furthermore, topics are often expanded beyond common usefulness to fill up a course. 
An example is characteristic equations (eigenvalue problems) in linear algebra. 
A linear algebra course would be too short without them, and students learn about these early in an undergraduate program. 
The students are told that this theory has many applications. 
But, a physics student might not see any legitimate application of characteristic equations until they learn about the Standing Schrödinger Equation, which, might not be until their senior year.
Mathematics or engineering student might never see such an application. 
And, even once the physics student knows about the application to the Standing Schrödinger Equation, they do not have the tools to deal with it. 
First, they wouldn't know how to make a finite-dimensional approximation of this infinite-dimensional equation. 
Second, they wouldn't know enough numerical methods to actually solve such a large finite-dimensional equation.

**Flawed Culture of Writing**: 
First, in courses such as calculus, students do not use any words on their work to the extent that many develop the impression that words to no belong in mathematics. 
Then, these these students are exposed to proof-based courses, where they pick up the writing style of lectures, that is, a scattering of chalkboard slang. 
And, when these undergraduate students become graduate students, they often notice that poor writing can be an advantage because it gives the impression of depth and discourages the reader from checking the details. 
Finally, when these students become a professor and write a book, their book is based on their lecture notes instead of having the original intention to write a book, which often leads to simply rehashing the material of a previous book with less pedagogy, and they use the excuse that the pedagogy will be explained in the lecture.
In short, most scientist do not significantly train intentionally to write well. 
But, to write a good book, the author must both know the subject and how to write. 
Modern scientists care so little about quality writing that, even with modern tools such as LaTeX, they normally produce worse texts than those from fifty years ago.


## Core Principles

> "When you cannot measure it,  
> when you cannot express it in numbers,  
> your knowledge is of a meager and unsatisfactory kind."  
> –– Lord Kelvin

The two core principles of the *Elements* are pedagogical completeness and clarity. 

**Pedagogical Completeness**:
As concepts and results are introduced, they are motivated by explicitly explaining their role in foundational understanding or how they have a specific end in engineering.
Such and end in engineering is either computational methods or the great inventions of man, such as the lever, the light bulb, the internal-combustion engine, the airplane, the bipropellant rocket, the nuclear reactor and the transistor. 
This motivation provides a map of the connection between the subjects so that the reader can see the forest from the trees. 
The reader is kept grounded by such motivation together with not having abstraction for is own sake. 
But, at the same time, intuition is provided to help the reader deeply understand the concepts and results themselves. 

**Clarity**:
The *Elements* aims to linguistically clear. 
This is done through simplicity, conciseness, consistency, explicitness and compartmentalization. 
Mathematical notation is clear and simple, and it only used when it is better than words. 
Vocabulary and phrasing are both simple and consistent. 
Proofs are organized clearly, and their steps are motivated throughout them. 