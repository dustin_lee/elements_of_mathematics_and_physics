# The Goal of the Elements

> "No masterpiece was ever created by a lazy artist."  
–– Salvador Dali

I would like to say that the Elements will be the greatest book yet written by man. 
But, the word *great* is too ambiguous. 
For example, one can easily argue that the books of the bible are the greatest books written by man. 
Instead, the goal will be more specific. 

First, it will be the best written book. 
The attention that I will give to the linguistics will be unprecedented in history. 
I will pay close attention to vocabulary and phrasing. 
I will use the linguistic minimum for the communication of such complex ideas. 
Just the work put into the Style Guide will be about what is put into a normal book. 

Second, the pedagogical completeness will be unmatched. 
The ideas will be communicated effectively through intuition, simplicity, compartmentalization and interconnections. 
The broadness of material is quite wide. 
And, the required understanding of each topic will be that of a master. 

The Elements will represent the height of the human mind. 
The contents are the most advanced thoughts of man that have accumulated over thousands of years. 
And, it exposition will be that of the masterpiece of master. 