# Outline of Electricity and Magnetism

## Summary 

* Mention the different formulations. 
	* Maxwell's equations. 
	* The Electromagnetic Wave Equation. 
	* Jefimenko's equations. 
	* Potentials. 
	* The Principle of Least Action. 


## Statics

* Electric Charge. 
	- Introduction.
		* Charge exists. Its basic properties.
		* An electroscope can be used to detect charge. (They do not need to touch the object.) (Use gold leaves.)
		* Movement and amount.
			- Introduce conductors and insulators.
			- The theory the charge of one sign is stationary and the other sign flows. (Should I explain why the one-fluid and the two-fluid theories do not work?)
			- That a charged object might discharge by a spark indicates this theory. (How hard is it to see the direction of a spark?)
			- Can create more charged bodies by driving the charge out of a conductor with a charged body.
			- The movement of charge appears to be instantaneous.
			- The Conservation of Charge.
			- Coulomb's law.
		* Introduce fields graphically.
	- Polarization.
		* I guess introduce general polarization before dielectrics.
		* Dielectrics – the local shift of charge.
			- Polarization density.
			- Give a rough introduction to permittivity. What about capacitance?
* Magnetism. 
	- Introduction. 
		* There exists certain material that when suspended in water always point north. These devices are called compasses.
		* The basic properties of magnets.
		* When a magnet is broken into two, the parts are weaker magnets. There are not monopoles.
		* It is not electric charge, that is, they do not interact with charge when stationary.
		* Explain magnets by polarization density.
	- Permeability.
		* Magnets can also attract certain material that are not magnets – the local shift, but this shift is only ever local because magnetism does not appear fluid-like in the way that electrical charge is.


## Electric Current

* Introduction.
	- Introduce batteries (chemical source), thermoelectric sources (thermal source) and maybe dams (mechanical source).
		* Don't explain how these work, but say when they will be explained.  
	- It appears that electricity is flowing because the wire heats up and an electroscope senses charge.
		* Explain this effect by charge distributing itself through the wire. The charge distributes itself in the wire (basically) immediately after being connected to the battery. (Even in AC current, this is a negligible effect.)  (Talk about bends?)
	- Because of the heating, electricity is energy. Mention that generators and motors will be discussed later.
	- Introduce potential (voltage).
		* The amount of charge following is not actually known.
		* Ohm's law.
			- This is valid regardless of what is causing the resistance. (For example, energy might be dissipated in a light bulb.)
		* The power is equal to the current multiplied by the voltage. The power used by a load (resistor, light bulb, etc.) is $I^2 ⋅ R$ or $\frac{(ΔV)^2}{R}$.
		$$
		* Give a numerical difference of the difference of resistivity of common insulators and conductors. (The number of orders of magnitude that conductivity ranges over is comparable to the radius of the electron orbits in atoms to the radius of planetary orbits.)
* Lorentz Law.
	- Introduction.
		* Wires are pushed in a magnetic field.
		* This can be used to see if electric current really is moving charge.
			- The spinning wheel experiment.
	- The law.
	- Define the Tesla. 
	- The Hall Effect.
		* This effect is minor. Gold foil is used to see it.
		* Some material – such as Zinc – show that the charge carrier is positive. Later, it will be shown that this is actually a hole.
* The Biot-Savart Law. (Actually, probably just drop this and go straight to Ampère's law.)
	- Can't I just skip to Maxwell's equations and use the same motivation? 
	- Motivation. 
		- Electricity affects magnets. Thus, by the Law of Reciprocal Forces, magnets should affect current.
	- The law.
	- This only holds for steady currents.
	- This gives a way to measure current. 
	- This does not give the amount of charge or its speed in a wire.
	- Solenoids act like magnets.
	- This indicates that magnetism is just from moving charge. Significant evidence is given later.


## The Fields

*I'm not sure how this material should be place.*

* Fields in matter. 
	- The electric displacement field $D$ is $ε_0⋅E + P$, where $P$ is the polarization, which is in the form $f(E)⋅E$. Thus, factor $ε_0 + f(E)$ is the permittivity. 
	- Magnetization $M$. 


## Maxwell's Laws

* Notes. 
	- Each of Maxwell's laws should be its own subsection. Then, have a subsection for fields in matter, the summary and the Electromagnetic Wave Equation. 
* Maxwell's laws. 
	- The Law of Electromagnetic Induction (Faraday's law). 
		* By relativity, a moving magnetic source should also induce induction. This does happen. Furthermore, any changing magnet field induces induction.
			- The Lorenz Force Law does not explain this last case.
			- The cause of a current induced by a looped wire and a magnetic is different depending on which is moving. While this is not quite a break of the Principle of Relativity, it does feel weird. Should I mention this or just wait until relativity to say that the Lorenz Force Law is derivable?
			- Explain the lack of relativity here and that this will be justified later. For now, just assume that these are different causes of the same effect and they the causes are different for each frame.
		* There is a coupling of the field. A moving magnet induces a current, and a change in electric field induces a magnetic field.
		* The integral form of the Law of Electromagnetic Induction
			- Lenz's law (Don't put this in a law environment.)
				* The ring experiment?
				* This how a force is required when moving a magnetic through a coil to create electricity.
		* The differential form of the Law of Electromagnetic Induction
	- Electric Flux Law (Gauß's law). 
		+ The Law of Electromagnetic Induction gives the rotation. To fully describe the electric field, the diverge is also needed. 
		+ This gives a way to measure the inverse-square law accurately by putting a charge in a container.
		+ Note. Write this correctly with permittivity and permeability – for example, ($\div (ε ⋅ E ) = ρ$).
	- Ampère's law.
		+ Steady state version. (It is from the Biot-Savart Law).
		+ Maxwell's addition to Ampère's law.
			* This is to get the current density to satisfy the Continuity Equation. 
	- Magnetic Flux Law.
		+ The Law of Electromagnetic Induction gives the rotation. To fully describe the field, the diverge is also needed. 
	- Summary. 
		* Restate the laws and call them Maxwell's equations. 
		* These completely describe the fields. And the Lorentz Force Law completely describes the force on an electrically charged particle. Computationally, each is computed from the other at each time step. 
		* These lead to some interesting behavior. (The fields are real.)
			- In the next section, it is proved that information propagates at the speed of light. 
			- Charges get pushed by their own fields. This is described by the Abraham-Lorentz Formula. (This describes the force on an electrically charged particle by its own field.) This is proved later. 


## Electromagnetic Waves (and Light)

* The Electromagnetic Wave Equation (with sources, that is, heterogeneous).
	- This is a reformulation of the law of electricity and magnetism. 
	- Conjecture how these the magnetic field and the electric field could sustain together.
	- Speed.
		* Show that solutions to the Electromagnetic Wave Equation travel at the speed of light.
		* I think that this already predicts the speed of electromagnetic field.
		* This predicts that the speed of light is constant in all frames.
		* It travels different in different mediums. 
	- Experimental verification: Light can be created by electric current around a small circuit.
	- Explain reflection, refraction and polarization.
		* Explain the boundary conditions of the Electromagnetic Wave Equation.
		* Explain refraction by the ladder argument, that is, if two men walk with a ladder and once slows down for a short period, then they will be traveling in a different direction.
		* I think that the reflection of light at an interface can be explained by the continuity of the electric field/flux(?): the only way for this to hold is that some light must stay outside.


## Energy and Momentum of the Electromagnetic Fields 

* Energy.
	- What do we really mean by the energy of light? How do we measure the conversion? 
	- Poynting's theorem.
		* Should I used Poynting's argument?
			- Relativity says the energy has mass and must satisfy the Continuity Equation.
			- Does energy density even make sense without relativity?
		* Intensity of light.
			- When waves are combined, the waves are added and then squared to get the intensity. (This is sort of the origin of why the wave function needs to be squared.)
	- I need to relate this energy to static energy of assembly.
	- Examples of the energy of a capacitor flow in through the sides?
* Momentum.
	- Motivational examples.
		* Light consists of an electromagnetic field, which can move things, so the momentum transfer is delayed be the speed of light, so light must carry the momentum.
		* Angular momentum by moving a magnetic through a wheel of charge.
	- The momentum density.
		* Perhaps, just do this by relativity by using the general relation between momentum density and energy flux density?
			- I think that Einstein's example of a photon being shot in a room explains this relation. (Look at this part in Feynman again.)
	- Verification: Radiation pressure
		* This explains why the tails of comets always point away from the sun.
		* Lasers can suspend particles.
		* Lasers can be used to grab and move individual biological cells. 


## The Electromagnetic Potentials

* The existence of the potentials. 
* Maxwell's equations. 
* The Coulomb and Lorentz gauges. 
* The wave equations for the potentials.
* Potential for moving charges. (Liénard-Wiechert potentials.)
	- These are guessed at by the delay in field. But, they still need to be proved to work. A similar argument does not work for the fields themselves. 
* The Abraham-Lorentz Formula. (Reaction Force.)
	- Larmor's formula. (Power radiated.)
* Potentials for a rotating charge? 
* Jefimenko's equations. 
	- Notes. 
		* Maybe I should introduce these after the Electromagnetic Wave Equation (so that the three descriptions of the electric and magnetic fields are together), but say that they will be derived after introducing the Electromagnetic Potentials. 
	- The Coulomb term does propagate as expected. It appears that the other terms are from other effects.
	- Philosophy.
		* Maxwell's equations don't express causality, but just coupling. Jefimenko's equations do express causality.
		* Wikipedia: "neither Maxwell's equations nor their solutions indicate an existence of causal links between electric and magnetic fields. Therefore, we must conclude that an electromagnetic field is a dual entity always having an electric and a magnetic component simultaneously created by their common sources: time-variable electric charges and currents"
	- Write the Heaviside-Feynman Equations explicitly.
	

## Electrical Devices

(These should just be spread throughout.)

* Notes. 
	- Should I talk about circuits in general? Should I rename this chapter?
* Generators. (Work can induce electricity.)
* Motors. (Energy can induced work.)
* Transformers.
	- Transformers only work with alternating current. This allows alternating current to be used by easily being able to step the voltage up and down. Lower voltages are better for use and higher voltages are better for transmission: The energy loss of transmission is $I⋅R^2$, which can be reduced with the same power by increasing the voltage and decreasing the current because the power is $P=V⋅I$.
* Incandescent light bulb.
	- Mention that an inert gas is used to avoid the filament burning up.
* Radio/antennas. (Sending and receiving signals.)
	- A capacitor and an inductor can be used to change the resonance of the circuit, that is, so it is tuned to a certain frequency. 
* Transmission lines?
	- I think that this is just from reducing the Electromagnetic Wave Equation to one dimension.
* Van-der-Graaf Generators?


<!-- ## Numerics

* Methods.
	- Yee's method. 
		+ This is for Maxwell's equations. 
		+ Just mention it.
	- Is FEM for Maxwell's Equation any good? 
	- FEM for the Electromagnetic Wave Equation. 
		+ Solves for the fields separately, compared to Maxwell's Equation. 
		+ What is the scheme? 
		+ What about doing it for the potentials? 
			- Would this be better because it preserves more information? It guarantees that the fields are from the divergence and rotation of potentials. 
			- ChatGPT says that it can be difficult to enforce the (Lorenz) gauge condition.
	- Method. Jefimenko's equations.
		+ This is just integration. 
		+ When dealing with retardation, just keep previous states? 
		+ How does this work for boundary conditions and various medium? Just reflect and whatnot naively? 
	- Explain the benefits and draw backs of each. 
* Explain the response loop again. 
* Boundary conditions: 
	- Open boundary conditions can be simulated using an absorbing boundary conditions – such a perfectly matched layer (PML). (Will this already be covered in Dynamic Partial Differential Equations?)
	- Conducting boundary conditions?: On the surface of a perfect conductor, the normal of the electric field is $0$. Why is this?
	- Reflecting boundary conditions.  -->
