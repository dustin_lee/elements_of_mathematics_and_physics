# Notes for Power Sums

## Need to Understand

* How much complex analysis should I have? Does it have any use? 
    - Are there any results from analytic continuation? Like, it is interesting. 
    - I would like to prove FTA with Liouville's theorem
    - Anyway: I think that analytic continuation can be proved directly for analytic functions. So, I would just need to prove that complex-differentiable functions are analytic. 


## Figures

* Divergence does not mean that it explodes. Have an example in a figure by having successive approximations that bounce around without exploding. Do it for the natural logarithm. 


## Material 

* Higher-Order Taylor's Theorem: 
    - I only need Taylor's Theorem. Power sums are not needed. 
    - Should Taylor's Theorem even be somewhere else, like in integration? 
* If a power sum approximation exists, then this means that the function can be determined only by its local behavior at a point. (This can be motivation for analytic continuation.)
* Is there a reason to have Abel's Theorem?
* Euler's Formula: Is the product formula useful? I could have a graphic like [this](https://en.wikipedia.org/wiki/Euler%27s_identity#/media/File:ExpIPi.gif).
* It would have the statement that $\ln(t)$ and its power sum around $1$ are equal at $2$. I could mention this when discussing the power sum of the natural logarithm and state that this will be proved with analytic continuation. 
* I might need the matrix exponential for quantum mechanics. If I do need it, then it should be the last section of this chapter. 
* If I want to use an example of a power sum approximation that does not converge to the original function, then I can use the function that is $0$ when $t$ is at most $0$ and 
    $$
        \exp( -\frac{1}{t} )
    $$  
    otherwise.
Each derivative of this function is $0$ because the relations
    $$
        \lim_{t \rightarrow 0} \frac{\exp\left( -\frac{1}{t} \right)}{t}
        = \lim_{t \rightarrow 0} \frac{1}{t⋅\exp\left( \frac{1}{t} \right)}
        = \lim_{t \rightarrow 0} \frac{1}{t⋅\left( \sum_{i≥0} \frac{1}{i!}⋅\frac{1}{t^i} \right)}
        = \lim_{t \rightarrow 0} \frac{1}{t + \sum_{i≥1} \frac{1}{i!}⋅\frac{1}{t^{i-1}}}
        = \lim_{t \rightarrow 0} \frac{1}{\sum_{i≥1} \frac{1}{i!}⋅\frac{1}{t^{i-1}} }
        = \frac{1}{\infty}
        = 0
    $$ 
    hold. 
* The Warren-Dadarlat Example (or radius of convergence is general) is motivation of the metric of the complex numbers. 


## Vocabulary

* Instead of *radius of convergence* and *interval of convergence*, I should probably use *convergence radius* and *convergence interval*. 


## General

*