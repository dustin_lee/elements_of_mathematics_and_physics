%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Summary 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% This introduces power sum expansions. 
% Furthermore, Taylor's Theorem is proved. 
% Thus, in practice, most functions are equal to their power sum expansion on a nonsingular interval. 
% \scenebreak


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Concept 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Let $f$ be an infinitely differentiable from a nonsingular interval to a euclidean space, and let $c$ be a point in its domain. 
Then, by the Linear-Approximation Theorem, this function $f(t)$ is approximately 
\[
    f(c) + ∂f(c)⋅(t-c)    
\]
for points that are near $c$. 
Furthermore, by the Fundamental Theorem of Calculus, the relations 
\begin{align}
    f(t)
    &= f(c) + ∫_{s:c}^t ∂f(s) \\
    &= f(c) + ∫_{s:c}^t \left( ∂f(a) + ∫_{r:c}^s ∂^2f(r) \right) \\
    &= f(c) + ∂f(c)⋅(t-c) + ∫_{s:c}^t \left( ∫_{r:c}^s ∂^2f(r) \right)
\end{align}
hold. 
Thus, the error of this approximation is 
\[
    ∫_{s:c}^t \left( ∫_{r:c}^s ∂^2f(r) \right)
    .
\]

Now, this reasoning is extended to get an approximation with more terms. 
Let $n$ be a positive integer. 
Then, the relations 
\begin{align}
    f(t)
    &= f(c) 
        + ∫_{s_1:c}^t \left( ∂f(c)
        + ∫_{s_{2}:c}^{s_{1}} \left( ∂^{2}f(c)
        + \ldots
        + ∫_{s_{n+1}:c}^{s_{n}} ∂^{n+1}f(s_{n+1}) \right) \right) \\
    &= f(c) 
        + ∂f(c)⋅(t-c) 
        + \ldots 
        + \frac{1}{n!} ⋅ ∂^{n}f(c)⋅(t-c)^n 
        + ∫_{s_1:c}^t ∫_{s_2:c}^{s_1} 
            \ldots ∫_{s_{n+1}:c}^{s_n} ∂^{n+1}f(s_{n+1})
\end{align}
hold. 
Thus, the error of the approximation
\[
    f(c) + ∂f(c)⋅(t-c) + \cdots + \frac{1}{n!} ⋅ ∂^{n}f(c)⋅(t-c)^n 
\]
of the function $f$ is 
\[
    ∫_{s_1:c}^t ∫_{s_2:c}^{s_1} \ldots ∫_{s_{n+1}:c}^{s_{n}} ∂^{n+1}f(s_{n+1})
    .
\]
Furthermore, the relations
\begin{align}
    &\abs{∫_{s_1:c}^t ∫_{s_2:c}^{s_1}
          \ldots
          ∫_{s_{n+1}:c}^{s_{n}} 
            ∂^{n+1}f(s_{n+1})} \\
    &≤ ∫_{s_1:\min(c, t)}^{\max(c,t)} ∫_{s_2:\min(c, s_1)}^{\max(c, s_1)}
       \ldots
       ∫_{s_{n+1}:\min(c, s_n)}^{\max(c, s_n)}
            \abs{∂^{n+1}f(s_{n+1})} \\
    &≤ ∫_{s_1:\min(c, t)}^{\max(c,t)} ∫_{s_2:\min(c, s_1)}^{\max(c, s_1)}
       \ldots
       ∫_{s_{n+1}:\min(c, s_n)}^{\max(c, s_n)}
            \sup_{\min(c, t) ≤ r ≤ \max(c,t)} \abs{∂^{n+1}f(r)} \\
    &= \frac{1}{(n+1)!}
        ⋅ \sup_{\min(c,t) ≤ s ≤ \min(c, t)}  \abs{∂^{n+1}f(s)}
        ⋅ \abs{t-c}^{n+1}
\end{align}
hold. 
Thus, the size of this error is at most
\[
    \frac{1}{(n+1)!}
    ⋅ \sup_{\min(c,t) ≤ s ≤ \min(c, t)}  \abs{∂^{n+1}f(s)}
    ⋅ \abs{t-c}^{n+1}
    . 
\]
This is called \emph{Taylor's Theorem}\index{Taylor's Theorem}. 
The function
\[
    f(c) + ∂f(c)⋅(t-c) + \cdots + \frac{1}{n!} ⋅ ∂^{n}f(c)⋅(t-c)^n 
\]
is called the $n\te$ \emph{power sum expansion}\index{partial power sum expansion} of the function $f$ around $c$. 

In practice, Taylor's Theorem is a fast method to compute most functions to an arbitrary accuracy because the number
\[
    \frac{1}{(n+1)!}
\]
normally grows much faster than 
\[
    \abs{∂^{n+1}f}
\]
does. 
In Section~\ref{psums:sec:some-expansions}, Taylor's Theorem is used to compute the Exponential Function, logarithms, exponents and some trigonometric functions.   


\begin{figure}[H]
\centering
    \includegraphics[width=0.5\textwidth]{psums-fig-taylor}
    \caption{Taylor's Theorem. The function is approximated better for more terms of the power sum expansion.} 
\end{figure}


Let $I$ be a nonsignular interval, and let $c$ be in its interior. 
If the number 
\[
    \lim_{i→∞} 
    \left( 
        \frac{1}{(i+1)!}
        ⋅ \sup_{s \in I}  \abs{∂^{i+1}f(s)}
        ⋅ \abs{d-c}^{i+1}
    \right) 
\]
is $0$ for any point $d$ in the interval $I$, then the function $f$ is equal to 
\[
    ∑_{i≥0} \frac{1}{i!} ⋅ ∂^{i}f(c) ⋅ (t-c)^i
\]
on the interval $I$.
The function
\[
    ∑_{i≥0} \frac{1}{i!} ⋅ ∂^{i}f(c) ⋅ (t-c)^i
\]
is called the \emph{power sum expansion}\index{power sum expansion} or the of the function $f$ around $c$.

There exists functions that are unequal to their power sum expansions at a point on any nonsingular interval that has this point. 
For example, the power sum expansion of the function 
\[
    \begin{cases}
        e^{-1/t^2} & t ≠ 0 \\
        0 & t = 0
    \end{cases}
\]
around $0$ is 
\[
    ∑_{i≥0} \frac{1}{i!} ⋅ 0 ⋅ t^i
    ,
\]
that is, it is $0$. 
