# Outline for Power Sums

## Concept


* Taylor's theorem: Finitely many terms
* Power sum expansion: Infinitely many terms
* Can converge to a different function 


## Convergence

* Definition of power sums 
* Radius of convergence 
    - Theorem 
    - Definition
    - Hadamard's Formula
* Uniform convergence


## Methods

* Multiplication
* Division
    - (This is used for the power sum for $\tan(t)$)
* Integration
* Differentiability
    - Unique coefficients
* Composition? 


## Some Power Sum Expansion

**The Exponential Function**:  
* Expansion 
* Use as a numerical method

**Natural logarithm**: 
* Expansion
* Use as a numerical method
* Use for other logarithms

**Binomial Theorem**: 
* Expansion 
* Use as a numerical method

**Trigonometric functions**: 
* Expansions of the Sine Function and Cosine Function
* Use as numerical methods
* Archimedes's Number: Expansion of the Arctangent Function


## Complex Functions

* Euler's formula
<!-- * Complex differentiability 
    - Cauchy-Riemann Equations
    - Analytic continuation 
    - Infinitely differentiable 
    - Liouville's theorem and a proof of the FTA  -->
