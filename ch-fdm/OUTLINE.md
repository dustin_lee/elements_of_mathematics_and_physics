# Outline of the Finite-Difference Method

## Introduction

* Finite-Difference is about approximating the operator, and FEM is about approximating the solution directly.
    - The function $\proj_{W}(A|_V)$ is between two finite-dimensional spaces whose dimensions are equal. (The projection of the restriction.)
  

## Static Differential Equations

*  Iterative Methods. 
  

## Dynamic Partial Differential Equations

* 


## Stiff Equations

* Implicit schemes are used for these. 


## Characteristic Equations

* 