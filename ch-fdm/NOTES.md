# Notes for the Finite-Difference Method 

## Need to Understand

* I don't really understand the reduction of space for the finite-difference method, and how to get (stability/convergence) results from it. 
    - Should it be thought of as a restriction? A quotient? What would the operator be? 
* Reason for ghost nodes. Why is it bad to use a low-order finite-difference scheme to estimate around the boundary. Why should this affect the order of the entire scheme?


## Notation

* Use $φ(x_i, t_j)$ for the actual solution and $φ_n(x_i, t_j)$ for an approximate solution.


## Material

* One cannot empirically check if the determinantes of the matrix (for a static equation) explodes because they are too hard to compute. I guess that this is done by actually looking at the numbers. Are there any good theorem for this? Does this make FDM for static equations difficult? 
* Explain the stability condition for the wave equation. 
    - Explain this in three steps. 
        + First, mention that the wave needs to propagate at its speed. And, give the condition for the one-dimensional case. 
        + Then, give it for the two-dimensional case. Explain that the square root is from going diagonally across the grid. Maybe have a picture. 
        + Then, give the $n$-dimensional case. 
* Neumann stability just says that none of the basis functions (harmonic functions $sin$ and $cos$) get amplified. 


## General

* Stability for the Wave Equation. The speed of the wave equation is the speed of information. If a membrane is cut, it decides how long it takes to affect the motion at a point.
* Stability for the Heat Equation. I think that this is just to prevent neighboring nodes from getting hotter. 
* The (Lax) Equivalence Theorem is only for linear equations.
* Crank-Nicolson. This is implicit, but it only creates a linear system, which can be solved for somewhat quickly.
* Multivariate Taylor sums can be used to look for the order of schemes.
* Symmetry can be used to simplify the calculation.
* Finite difference methods can be applied to complex shapes, but it is more difficult.
    - Rectangular (or parallelogram) grids.
        * Have a picture to explain the squared boundaries and irregularly spacing.
        * Any line must go through the whole domain. Thus, increased fineness in one area also increases the computation in other areas. 
    - Irregular grids. 
        * Difficulties with stability proofs. 
        * Order of convergence can be affected by single nodes. 
* A scheme $P_h$ is consistent with an equation $f(u) = w$, if for any smooth function $φ$, the value $P_h(φ)$ converges to $f(φ)$. This is used for the Lax-Richtmyer Equivalence Theorem. 
* The Lax-Richtmyer Equivalence Theorem is useful because the determination of the consistency and stability of schemes is mostly just algebraic manipulation. 
* Motivate the Lax-Richtmyer Equivalance Theorem by first talking about consistency is required. Then, give an example of how consistency is not enough. Then, introduce stability. 
    + An example of where consistency is not enough is given as Example 1.4.3 in *Finit Difference Schemese and Partial Differential Equations* by Strikwerda. 