# Notes for UQ

## Need to Understand 

* Metropolis Algorithm. 
    - What is this used for? 
        + Can the velocities also be sampled? 
    - I guess that it is more likely to be close to the mean if only preferring short jumps, and it will hover around it. (There are more options to go towards it.)



## Key Concepts

**Sensitivity**: 
* Measures significance: 
    - Screens out insignificant errors. 
    - Indicates where it is most important to get better measurements or consistent materials.
* Pros: 
    - Is not intrusive if AD is not used to compute the derivative. 
* Cons: 
    - Cannot get an accurate derivative numerically. But, AD can be used sometimes. And, although, derivatives are not accurately computed numerically, their order can be accurately computed, which is what matters for this. 
    - The number of variables can become big when a random field is used. This matter for second-order approximations. 
        + Supposedly, there are various methods to help with second-order approximations. 
* Notes: 
    - Supposedly, regression is useful to rate sensitivities. 
        + I guess that it uses fewer evaluations. 
        + There are many methods for this.

**Error Propagation**: 
* Cons: 
    - Intrusive. 

**Monte Carlo**: 
* Idea:
    - The relation 
        $$ 
        \mean(Q) = ∫_{x_1} ... ∫_{x_n} \sol_p(x_1, ..., x_n)⋅f_1(x_1)⋅...⋅f_n(x_n)
        $$ 
        holds. 
    - Get convergence of the CLT.
* Pros: 
    - Not intrusive.
* Notes: 
    - Run until there is convergence for every QoI. If the data is being collected anyway, then might as well get better estimates for the QoIs that already have empirical convergence. 

**Reliability Methods**: 
* Idea: One number that measure the probability of success by having thresholds. 
* Only care about bounds (confidence). 
    - Can assume independence of different failures. 
* Supposedly, the closest failure point can be computed with optimization. 
    - Measure distance in terms of standard deviations. 
    - But, how is the (hyper)surface searched? 
* Notes: 
    - I prefer the term *failure/success probability/chance*. 

**Polynomial Expansion**: 
* Idea: Estimate the integral with quadrature with respect to the distribution: 
    - I don't understand how a quadrature scheme means anything for the product. 
        + FEM already has a polynomial form, so this may somehow connect. (But, the spaces are different.)
* Pros: 
    - Not intrusive. 
* Cons: 
    - Suffers from the curse of dimensionality. 
        + But, supposedly, this can be mitigated with sparse quadrature methods. 
    - Suffers from Gibbs's Phenomenon at discontinuities. 
* Requires a quadrature scheme for each distribution. 

**Bounded Continuity**?:
* I thought of this. 
* Idea:
    - The solution to a differential condition depends continuously on the initial condition. 
    - Bound the output by involving $\exp(L⋅T)$ and the standard deviation. 
* Cons: 
    - Is a bound, so it cannot be used for subsequence computations. 


## Material 

* The Metropolis Algorithm. 
* Ising Model? 


## Unsorted

* Inverse: Supposedly, these methods are related to methods for updating parameters from empirical data. This can be used to reduce uncertainties more. 
* Some methods may not increase or even reduce uncertainty. For example, optimization may find the same solution regardless of uncertainty in the initial step.
* Care about correspondence of input probability densities? What is an example?
* Root finding: Does a double root imply unstability? 
* How much can coupled systems be decoupled? 
* Mote Carlo: Convergence of CLT: Run there is convergence for all QoIs. The extra data might as well be fully used. 
* Monte Carlo: Error:
    - If the error for a classical method is of the order $n^-m$ for one dimension, then it is of the order $n^{-m/d}$ for $d$ dimensions. 
    - But, the error for the Monte Carlo Method is alway $n^{-1/2}$, regardless of dimension. 
    - Also give the error of the Monte Carlo Method as a function of the standard deviation. 
* Importance sampling. 
    - To reduce the error, either the number of samples needs to increase or the standard deviation needs to decrease. Doing the later makes more sense computationally. 
    - 
* The Metropolis algorithm. 
    - This is an example of importance sampling. 