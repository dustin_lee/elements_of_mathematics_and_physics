BUILDDIR=build
FILES=$(wildcard \
		elements.tex \
		edition.tex \
		colophon.tex \
		tex/*.sty \
		tex/replace.sh \
		preface/preface.tex \
		overview/*.tex \
		overview/*.png \
		overview/*jpg \
		ch-*/*.tex \
		ch-*/*.png \
		ch-*/*.jpg \
		notation/*.tex)

all: copy latex

copy: 
	@mkdir -p $(BUILDDIR)
	@cp -f $(FILES) $(BUILDDIR)

latex: copy 
	@cd $(BUILDDIR) \
	&& bash replace.sh \
	&& latexmk -lualatex elements.tex
	@cp $(BUILDDIR)/elements.pdf .

clean: 
	@rm -rf $(BUILDDIR)/*


.PHONY: all clean 
