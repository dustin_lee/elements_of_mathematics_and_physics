Let a room travel at a constant velocity $v$ relative to an inertial reference frame. 
Furthermore, let the room be oriented so that the velocity $v$ points from its back wall to its front. 
Let an observer be outside the room in the original inertial reference frame. 
And, let another observer be inside the room. 
Now, some observations of these two observes are compared. 
That is, it is studied how physics does transform between different inertial reference frames. 


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% First
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

First, let a light pulse be emitted from the middle of the room. 
To the observer inside the room, the light simultaneously reaches the front and the back wall.  
But, to the inside observer, the light reaches the back wall before it reaches the front wall because the back wall is moving towards the light and the front wall is moving away from the light. 
Thus, simultaneous events in an inertial reference frame may not be simultaneous in another inertial reference frame.
Simultaneous events at the same spatial point are still simultaneous in all inertial reference frames. % !! Odd placement? 


\begin{figure}[H]
\centering
	\includegraphics[width=0.6\textwidth]{sr-fig-thought-exp-1}
	\caption{Light is emitted from the middle of a moving room. Because the speed of light is constant, the light reaches the back of the room before the front.}
\end{figure}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Second
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Second, let a light pulse be emitted from the middle of the room such that it reflects off the floor and returns to its source. 
Let $d$ be the distance between the light source and the floor. 
Let $a$ and $a'$ be the durations between when the light pulse is emitted and when it returns to its source to the inside and outside observers, respectively. 
To the inside observer, the light pulse travels directly down and then directly up. Thus, the number $a'$ is 
\[
    \frac{2⋅d}{\c}
    .
\]
To the outside observer, the light pulse instead travels at an angle because the room is moving. 
To the outside observer, by the Pythagorean Theorem, the path length of the light pulse is 
\[
	2 ⋅ \left(d^2 + \left(\abs{v}⋅\frac{a}{2}\right)^2
		\right)^{\frac{1}{2}}
	.
\]
Thus, the relation
\[
	a =
	\frac{2⋅\left(d^2 + \left(\abs{v}⋅\frac{a}{2}\right)^2
			\right)^{\frac{1}{2}}}
	{\c}
\]
holds. 
Thus, the relations
\[
	a^2 = \frac{4⋅\left(d^2 + \left(\abs{v}⋅\frac{a}{2}\right)^2\right)}{\c^2}
\]
and 
\[
	a^2 ⋅ \left(1 - \frac{\abs{v}^2}{\c^2}\right) = \frac{4⋅d^2}{\c^2}
\]
both hold. 
Thus, the relation
\[
	a' = \left( 1 - \frac{\abs{v}^2}{\c^2} \right)^{\frac{1}{2}} ⋅ a
\]
holds.
Thus, time is slower for moving observers. 
This has been experimentally verified by observing that the decay rate of subatomic particles is slower at high speeds in agreement with this relation. 

Let there be clocks in two different inertial reference frames that start at the same time.
Then, both clocks will see the other clock as being slower. 
And, if these clocks are then brought together in the same inertial reference frame, it may seem that both will see the other clock as slower. 
At first, this may seem like a contradiction. 
But, the clocks cannot not be bought together into the same inertial reference frame without at least one them accelerating.
That is, two clocks in two different initial reference frames cannot be directly compared twice. 
Acceleration is dealt with in the next chapter. 


\begin{figure}[H]
\centering
	\includegraphics[width=0.6\textwidth]{sr-fig-thought-exp-2}
	\caption{Light is emitted from the middle of a moving room and, it is reflected back to its source. Because the speed of light is constant, the duration for this process when observed inside the room is different from when observed outside the room.}
\end{figure}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Third
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Third, let a light pulse be emitted from the back wall so that it reflects from the front wall and returns to its source.
Let $d$ and $d'$ be the length of the room to outside and inside observers, respectively. 
Let $a$ and $a'$ be the durations between when the light pulse is emitted and when the light pulse returns to it source to outside and inside observers, respectively. 
Because the path length of the light pulse is $2⋅d'$ to the inside observer, the number $a'$ is
\[
    \frac{2⋅d'}{\c}
    .
\]
To the outside observer, the front wall moves away from the light pulse and the back wall moves towards it.
Thus, the number $a$ is 
\[
    \frac{d}{\c - \abs{v}} + \frac{d}{\c + \abs{v}}
	,
\]
that is, it is 
\[
	\frac{2 ⋅ d}{\c ⋅ \left( 1 - \frac{\abs{v}^2}{\c^2} \right)}
	.
\]
Because the relation
\[
	a' = a ⋅ \left( 1 - \frac{\abs{v}^2}{\c^2} \right)^{\frac{1}{2}}
\]
holds, the relation
\[
	\frac{2⋅d'}{\c}
	= \frac{2 ⋅ d}{\c ⋅ \left( 1 - \frac{\abs{v}^2}{\c^2} \right)}
		⋅  \left( 1 - \frac{\abs{v}^2}{\c^2} \right)^{\frac{1}{2}}
\]
holds. 
Thus, the relation
\[
	d' = \frac{1}{\left( 1 - \frac{\abs{v}^2}{\c^2} \right)^{\frac{1}{2}}} ⋅ d.
\]
holds.
Thus, distances are shorter along the direction of motion for moving observers. 

Let the two observers use rulers to measure the length of the room. 
At first, it doesn't seem that they are able to get different measurements. 
That is, again, at first, this may seem to be a contradiction. 
But, when using a ruler to measure distance, the markings are looked at simultaneously. 
And, simultaneous events in one reference frame are not necessarily simultaneous. 
That is, to each observer, the other observer is not measuring the room because they are comparing each marking on the ruler at different times. 

Both observers agree on width and height of the room, that is, distances perpendicular to the velocity do not change.
This is proved by contradiction. 
Assume that the observers do not agree on such these. 
Let there be a ball that barely fits through a hole. 
Let the hole be in an inertial frame, and let the ball travel through it at a constant velocity. 
Then, either the hole will shrink in the reference frame of the ball or the ball will grow in the reference frame of the hole. 
And, either way, the ball will not fit through the hole in one of these two reference frames. 
This is a contradiction because the outcome must be the same in any inertial reference frame. 


\begin{figure}[H]
\centering
	\includegraphics[width=0.6\textwidth]{sr-fig-thought-exp-3}
	\caption{The length of a moving is measured by emitting lighting from the back and reflecting the light from the front so that it returns to its source. Because the speed of light is constant, the length of the room when observed inside the room is different from when observed outside the room.}
\end{figure}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Fourth
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Fourth, again, let two pulses of light be simultaneously emitted from the middle of the room such that one travels forwards and the other travels backwards.  
To the inside observer, these light pulses simultaneously reach the front wall and the back wall. 
To the outside observer, the one light pulse reaches the back wall before the other reaches the back wall because the back wall moves towards the light pulses and the front wall moves away. 
Let $d$ and $d'$ be the length of the room to observers in the original reference frame and the room, respectively. 
Let $a$ be the time difference from when the one light pulse reaches the back wall to when the other reaches the front wall to an observer in the original reference frame. 
This number $a$ is 
\[
    \frac{\frac{d}{2}}{\c - \abs{v}} - \frac{\frac{d}{2}}{\c + \abs{v}}
\]
that is, it is 
\[
	\frac{d⋅\abs{v}}{c^2 - \abs{v}^2}
	.
\]
Because the relation
\[
    d = \left( 1 - \frac{\abs{v}^2}{\c^2} \right)^{\frac{1}{2}} ⋅ d'
\]
holds, the relation
\[
	a = \frac{d'⋅v}{c^2 ⋅ \left( 1 - \frac{\abs{v}^2}{\c^2} \right)^{\frac{1}{2}}}
\]
holds. 
This quantifies the time dilation of simultaneous events at different positions. 


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Lorentz
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

The next law is the accumulation of these results. 
That is, it describes the changes in the observation of space and time between inertial reference frames. 
The relation in it are called the \emph{Lorentz Transformation}\index{Lorentz Transformation}. 

\begin{law}
Let $v$ be the velocity of a second inertial reference frame to an original inertial reference frame. 
Let $a$ and $a'$ be the time differences from a first event to a second event in the original and the second reference frame, respectively. 
Let $w$ and $w'$ be the spatial differences from this first event to the second event in the original and the second reference frame, respectively. 
Then, the relations
\[
	a' = \frac{a - \frac{v•w}{\c^2}}{\blorentz}
\]
and 
\[
    w' = \proj_{v^{\perp}}(w) + \frac{\proj_{v}(w) -  a⋅v}{\blorentz}
\]
hold.
\end{law} 

Let the coordinates of space be such that only the first component of $v$ is not nonzero. 
Then, the Lorentz Transformation is the relations
\begin{gather}
	a' = \frac{a - \frac{v_1⋅w_1}{\c^2}}{\blorentz}, \\
	w_2' = w_2,
	\textspace
	w_3' = w_3
\end{gather}
and
\[
    w'_1 = \frac{w_1 -  a⋅v_1}{\blorentz}
	,
\]
where subscripts denote components.

The number 
\[
	\lorentz
\]
is from time dilation and length contradiction.  
The number
\[
	\frac{v•w}{\c^2}
\]
is from the time dilation of simultaneous events at different positions. 
And, the vector $-a⋅v$ is similar to the Galilean Transformation.

If the velocity between two inertial reference frames is small compared to the speed of light, the Lorentz Transformation is approximately the Galilean Transformation.
Even if the speed between two inertial frames is $\SI{60 000 000}{\meter\per\second}$, which is approximately one fifth the speed of light, then the number 
\[
	\lorentz
\]
is still approximately $0.980$.