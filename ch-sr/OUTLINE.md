# Outline of Special Relativity

## The Introduction

* Explain the problems.
    - The laws of electricity and magnetism are not relativistically invariant classically.
    - The speed of light is constant.
        + This is predicted by Maxwell.
        + It has been experimentally tested that the speed of light does not depend on the velocity of its emitter. This has been done by measuring the velocity of light on Earth at different parts of its orbit.
            * The Michelson-Morley experiment. (Although, there are now direct experiments.)
            * Various astronomical observations do not depend on whether the objects are moving towards or away form Earth.
* Explain that new laws of mechanics are described, and that these are compatible with Maxwell's equations.
* State that the speed of light is invariant in a law environment.
* Explain that this chapters uses the Principle of Relativity and that the speed of light is invariant to prove that different observers measure time and distance differently.


## Inertial Frames

* Lorentz Transformation.
    - ~~First thought experiment. (Simultaneity.)~~
        + ~~Simultaneous at the same location is always simultaneous.~~ 
    - ~~Second thought experiment. (Rate of time.)~~
        + ~~Intuitive statement.~~
        + ~~Experiments of confirmation.~~
            * ~~Mention the decay of elementary particles.~~
            * Mention the experiment where an atomic clock was flown around the world. (But, this involves acceleration.)
        + ~~Twin Paradox.~~
    - ~~Third thought experiment. (Changes in space.)~~
        + ~~Intuitive statement.~~
        + ~~Explain why they don't agree on the distance measured.~~
    - ~~Fourth thought experiment. (Changes in time.)~~
    - ~~Statement.~~ 
    - ~~After. Mention how their limit as $\frac{\abs{v}}{\c}$ goes to $0$ they become the galilean transformations.~~
        + ~~Give a numerical example on how fast an object needs to move for relativistic effects to be significant.~~
* Causality. 
    - Notes. 
        + Should I talk about invariance of the sign of $ct^2 - |x|^2$? 
    - ~~The temporal order of events is different in different frames.~~
    - ~~Rectify this with causality.~~ 
        + ~~Give a hard intuitive example of casual effect.~~
        + ~~The ideas of past, present and future must be modified.~~ 
        + ~~The past and future must be at least a big as the light cone.~~ 
        + ~~Prove that they cannot be bigger because everything else is in the present.~~
        + Have figure that has the casual past, casual future, casual present and light cone labeled. (Don't bother defining the light cone, but just label it.)
    - Signals cannot travel faster than light.
        + Proof.
            * I am not sure how to pick the points. But, it is probably just solving the equation from the Lorentz Transformation similarly to how it was proved how big the casual present is. Maybe I should even bother with this algebra. (I could also do two transformations where each one make two events simultaneous.)
            * ~~Have a figure.~~ 
        + ~~Evidence. Particles in accelerators are often over $99.9\%$ of the speed of light, but they never go faster than light.~~
        + ~~Mention that this does not mean that one cannot travel arbitrary distances.~~
    `* ~~Because signals cannot travel faster than light, there must be an (nonclassical) effect that prevents objects from reaching the speed of light.~~
* ~~The Velocity Addition Rule.~~


## Mechanics

* Momentum.
    - Note. Do this in the style of Huygens.
    - The momentum is $γ⋅m⋅v$ (if the momentum is conserved).
* Force.
    - The Law of Superposition of Forces still holds.
    - Faster objects have more inertia. The inertial mass is $γ⋅m$.
    - Thus, it is harder to push objects faster and faster. (This explains why objects cannot be pushed faster than the speed of light.)
* Kinetic Energy.
    - Do this by the amount of work that can be extracted from a moving particle.
    - The kinetic energy is $m⋅\c^2⋅(γ-1)$. 
        + The $-1$ is from plugging in $0$. 
    - Thus, reaching the speed of light takes infinite energy. Probably have a graph to show the infinite energy required to reach the speed of light. 
* The Mass-Energy Equivalence: 
    - Energy and mass are the same thing: Use the photon argument. 
    - Conversions:
        + Weight of nuclei. 
        + Annihilation of particles. 
        + Photons have decayed into electron-positron pairs. 
        + Feynman's collision. (What is this?)
    - Another way of saying this: Energy also has inertia.
        + It does not depend on the form of energy because, if it did, then changing the form would change the inertia, which would violate the Energy-Conservation Law because an object can be accelerated up with low inertia and then, after a change in the form of energy, the more energy than put in can be extracted through deceleration. (Does this argument depend on gravity?)
        + Alternative thought experiments: Balls or light bouncing around in a box. 
    - Give numbers on the equivalence. Explain how it is difficult to see this in chemistry, but, nuclear reactions allow for this to be measured.
    - Energy-Momentum Relation
* The formula for the lagrangian. 


## Electricity and Magnetism

* Maxwell's laws are invariant under Lorenz transformations. They still hold. 
* Charge is invariant of frame. 
    - Is this because of Gauß's law? 
* Briefly mention this argument: If something (such as charge) is conserved, then it must be conserved locally. It disappears somewhere and instantaneously reappears somewhere else because this disappearing and reappearing might not be simultaneous in another frame.
* Different frames see different electromagnetic fields, but the outcomes are the same.  Consider a loop moving through a magnetic field. 
* The formula for transformation of the electromagnetic fields between frames. 
    - In a way, this is the main goal of this chapter. 
* Light: The Doppler Effect:
    - Does this matter? Does it give experimental evidence that Maxwell's Equations and SR are still valid? Maybe it is just worth mentioning that this accounts for this. 
    - Include something along the lines of the picture doppler.jpg in the folder figures. (Also, include something similar for the classical Doppler Effect.)
    - Are Jefimenko's equations the easiest way to describe this? 




