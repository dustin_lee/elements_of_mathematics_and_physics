The chronological order of events can differ in different reference frames, but the chronological order of causal effects cannot differ. 
For example, if it is observed in a reference frame that a fire boils water, then it cannot be observed in another reference frame that the fire started because the water was boiled. 

The \emph{causal past}\index{causal past} and \emph{casual future}\index{casual future} of an event are the sets of all events that are in the past and future in every inertial reference frame, respectively.  
The \emph{casual present}\index{casual present} of an event is the set of all events that are not in its casual past or casual future. 

Let $a$ and $w$ be the time difference and the spatial difference, respectively, from a first event to a second event in an inertial reference frame.
If a signal can originate at the first event and reach the position of the second event by when the second event happens, then the second event is in the casual future of the first event, and the first event is in the casual past of the second event. 
Because, light can be used as a signal, the first event is in the causal past of the second event if the relations
\[
    \abs{w} ≤ \c⋅\abs{a} 
    \textand 
    a > 0
\]
hold, and the first event is in the casual future of the second event if the relations
\[
    \abs{w} ≤ \c⋅\abs{a} 
    \textand 
    a < 0
\]
hold.

Now, it is proved that, if the relation 
\[
    \abs{w} > \c⋅\abs{a}
\]
holds, then the two events are in the casual present of each other, that is, the casual past and the casual future of an event contain exactly those events such that light can be used as a signal between them. 
Assume that the relation
\[
    \abs{w} > \c⋅\abs{a}
\]
holds.
It suffices to prove that there exists an inertial reference frame in which the two events are simultaneous.
By the Lorentz Transformation, it suffices to prove that a velocity $v$ exists whose size is smaller than the speed of light such that the number
\[
    \frac{a - \frac{v•w}{\c^2}}{\blorentz}
\]
is $0$, that is, such that the relation
\[
    a = \frac{v•w}{\c^2}
\]
holds.  
Thus, the two events are simultaneous in the inertial reference frame whose velocity is
\[
    \frac{a⋅\c^2⋅w}{\abs{w}^2}
\]
relative to the original reference frame. 
Furthermore, the size of this velocity is smaller than the speed of light because the relations
\[
    \abs{\frac{a⋅\c^2⋅w}{\abs{w}^2}}
    = \frac{\abs{a}⋅\c^2⋅\abs{w}}{\abs{w}^2}
    = \frac{\abs{a}⋅\c^2}{\abs{w}}
    < \c
\]
hold, where the last relation holds because the relation
\[
    \abs{w} > \c⋅\abs{a}
\]
holds.  
Similarly, it can be proved that if two events are in the causal present of each other, then, in some inertial reference frame, one event is before the other event, and, in some other inertial reference frame, the one event is after the other. 


\begin{figure}[H]
\centering
    \includegraphics[width=0.5\linewidth]{sr-fig-light-cone}
    \caption{Casual past, casual future and casual present.}
\end{figure}


This means that the causal present cannot be completely known to one observer.
Thus, the casual future cannot be fully predicted by one observer. 
That is, the cause of an event in the future may depend on something in the causal present that the observer does not yet know. 

This also means that a signal cannot travel faster than the speed of light.
If a signal could travel faster than light, then a signal could be sent between two events that are in the causal present of each other.
Thus, in some inertial frame, the signal would arrive before it was sent. 


\begin{figure}[H]
\centering
	\includegraphics[width=0.5\linewidth]{sr-fig-faster-than-light}
    \caption{}
\end{figure}

Speeds bigger than $0.999⋅\c$ have been observed for particles in particle accelerators. 
But, speeds bigger than the speed of light have not been observed. 

Here, the \emph{speed of light} means the speed of light in vacuum. 
The speed of light is slower in other medium.
For example, it has been neutrinos have been observed to travel faster than light in air. 

Intuitively, this speed limit is really the speed of causality. 
And, light travels at this speed because it is massless. 

Because signals cannot travel faster than light, there must be an effect that prevents objects from reaching the speed of light.
In the next section, it is proved that the inertia of an object increases as its speed increases, that is, it takes infinite energy to accelerate an object to the speed of light. 

That there exists a speed limit does not imply that arbitrarily long distances cannot be traveled in arbitrarily short time durations. 
This is because distances become shorter as the observer moves faster. 

