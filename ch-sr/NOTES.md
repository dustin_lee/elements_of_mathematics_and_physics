# Notes for Special Relativity

## Concerns

* I am using photons to get the Energy-Mass Equivalence. But, then, I use it to get the energy and momentum of photons. 
    - Can I connect energy and momentum of light classically? I think that the relation $E=p⋅c$ holds for light waves. And, I think that just this can be used. 


## Need to Understand

* What is the point of the Energy-Momentum Relation? 
    - This quantifies that inertia/momentum of energy? Well, specifically, I want it for massless particles. 
    - For photons, can I just relate the classical energy and momentum? 
    - Honestly, it kind of just seems like crap. There are already formulas for the energy and momentum of massive particles and light. Why would I care about the "relativistic energy"?


## Figures

* Thought Experiments: 
    - I would like for both time slices to be superimposed on each other. I could probably use some shading or something to indicate that they are different time slices.


## Material

* That we can see double star systems is evidence that the speed of light is constant. If it depended on the velocity of the stars, the image would become muddled over light years. 
**Electricity and Magnetism**:
    * Just see directly how the equations transform under changes of frame
        - Then, show that the forces are consistent
        - But, what equations should be looked at? Maybe Jefimenko's are easier because they are about sources. But, would I already have these proved given that (I think) they require potentials? 
    * Understand how magnetism is a relativistic effect of electricity?
    * Why is charge invariant? 
    * Energy flow.
* Michelson-Morely: Have a figure for it, and explain it. But, do not talk too much about it in the main body of text. 
* Should I also write the Lorentz Transformation by coordinates? 
* Should mention the size of mass defects? (They are about 1%.)


## Vocabulary

* Mass is only to refer to rest mass, that is, the terms *relativistic mass* and *inertial mass* are not used. 
    - The term *inertia* is used to refer to $γ⋅m$. 
    - (Furthermore, I need a word for *gravitational mass*. I don't want to use *mass energy* because I want this to be the energy from mass. I could just use *energy*.)





---
---
---
**Old**
---


**Decision**:
The term *relativistic energy* or *total energy* is not used. 
This is thought of as the kinetic energy and the mass-energy separately. 





## Vocabulary 

* Perhaps, instead of using *total energy*, I should use *intrinsic/isolated/personal/individual energy* (and the term *total energy* should only apply to systems).




## Material

* The present cannot be known. Thus, the future cannot quite be predicted. 
* Use the speed of a satellite to understand the size of the Lorenz factor. 
* The magnetic field required to bend particles in an accelerator agrees exactly with the change of mass of a particle. Say this as experimental evidence. 
* The low-speed approximation of the kinetic energy makes sense. The power series is just a way to show that the classical kinetic energy is the dominating factor at low energies. 


## General Notes

* Fundamental particles cannot change mass. Does this prevent some light emission from happening? For example, an electron cannot emit two photons in the way that is explained. 
* Einstein said that the force on a moving charge from a magnetic field is really just a force from an electric field. On a level, this feels right, but I don't think that there is much to it. Both fields are still needed. I don't feel like they are truly unified. On the other hand, in QED, they are both mediated by photons (the only difference (I think) is the spin).
* I shouldn't talk about the fields of a moving charge (such as a charge in uniform motion) because Jefimenko's Equations already deal with this.
* When should I talk about fields in general? I guess at least before I talk about how magnetism is a relativistic effect of electricity.
* Give a numeric example of how fast an object needs to go to notice relativistic effects: Calculate $γ$ for $\frac{1}{7}⋅\c$, which should be about $1.1$. Mention that, at the speed $\frac{1}{7}⋅c$, it takes one second to circle the Earth.
* Draw the electric fields of moving charges. Relate this to light emission of decelerated charges.
* A possible name for this chapter could be *The Consistency of the Speed of Light*.
* Check out the Wikipedia [page](https://en.wikipedia.org/wiki/Tests_of_special_relativity) on tests of special relativity.
* I think that the answer to the Suicidal-Physicist Paradox is that locally the simultaneity of events does not depend on the frame.
* Substitutions in the Lorenz transformation. (Probably don't mention these.)
    - Changing to a frame at the speed $v$ and then changing back by plugging in $-v$ is the identity.
* Numbers: About one thousandth of the mass of uranium in a nuclear bomb is converted to energy. Thus, to get an object to three fifths the speed of light takes a nuclear bomb such that the rest mass of its uranium is $250$ times the rest mass of the object.
* Just to keep in mind, mechanical energy is always observed in collisions in special relativity because the thermal energy and so on is seen as mass.
* Although I will probably not use it, I understand Feynman's argument about a charge moving along a wire at the speed of the electrons. In the rest frame, it is pushed by the magnetic field. In its frame, the electrons are more spaced out and the protons are more bunched together; thus, it is pushed by the electric field. 